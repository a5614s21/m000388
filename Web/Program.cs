using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.EntityFrameworkCore;
using System.Globalization;
using Web.Repositories.Admin;
using WebApp.Data;
using WebApp.Models;
using Microsoft.AspNetCore.Authentication.Negotiate;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Authorization;
using WebApp.Services;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
var connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

//AD登入
builder.Services.AddAuthentication(NegotiateDefaults.AuthenticationScheme)
   .AddNegotiate();
/*
builder.Services.AddAuthorization(options =>
{
    options.FallbackPolicy = options.DefaultPolicy;
});*/


/*
builder.Services.AddMvc(o =>
{
    var policy = new AuthorizationPolicyBuilder()
        .RequireAuthenticatedUser()
        .Build();
    o.Filters.Add(new AuthorizeFilter(policy));
});
*/



builder.Services.AddDbContext<ApplicationDbContext>(options =>
    options.UseSqlServer(connectionString));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
    .AddEntityFrameworkStores<ApplicationDbContext>();





//注入Repository
builder.Services.AddScoped<IRepository<ArticleCategory , LanguageResource, string>, ArticleCategoryRepository>();
builder.Services.AddScoped<IRepository<ArticleNews , LanguageResource, string>, ArticleNewsRepository>();
builder.Services.AddScoped<IRepository<ArticlePage , LanguageResource, string>, ArticlePageRepository>();
builder.Services.AddScoped<IRepository<ArticleColumn , LanguageResource, string>, ArticleColumnRepository>();
builder.Services.AddScoped<IRepository<ArticleLocation, LanguageResource, string>, ArticleLocationRepository>();
builder.Services.AddScoped<IRepository<ProductCategory, LanguageResource, string>, ProductCategoryRepository>();
builder.Services.AddScoped<IRepository<ProductSet, LanguageResource, string>, ProductSetRepository>();
builder.Services.AddScoped<IRepository<ProductSpecCategory, LanguageResource, string>, ProductSpecCategoryRepository>();
builder.Services.AddScoped<IRepository<ProductSpec, LanguageResource, string>, ProductSpecRepository>();


builder.Services.AddScoped<IRepository<ArticleDownload, LanguageResource, string>, ArticleDownloadRepository>();

builder.Services.AddScoped<IRepository<AdvertisingCategory, LanguageResource, string>, AdvertisingCategoryRepository>();
builder.Services.AddScoped<IRepository<Advertising, LanguageResource, string>, AdvertisingRepository>();
builder.Services.AddScoped<IRepository<Member, LanguageResource, string>, MemberRepository>();
builder.Services.AddScoped<IRepository<WebData, LanguageResource, string>, WebDataRepository>();
builder.Services.AddScoped<IRepository<InboxNotify, LanguageResource, string>, InboxNotifyRepository>();
builder.Services.AddScoped<IRepository<SystemItem, LanguageResource, string>, SystemItemRepository>();

builder.Services.AddScoped<IRepository<Contact, LanguageResource, string>, ContactRepository>();
builder.Services.AddScoped<IRepository<IdentityUser, LanguageResource, string>, IdentityUserRepository>();
builder.Services.AddScoped<IRepository<IdentityRole, LanguageResource, string>, IdentityRoleRepository>();
builder.Services.AddScoped<IRepository<Firewall, LanguageResource, string>, FirewallRepository>();
builder.Services.AddScoped<IRepository<SystemLog, LanguageResource, string>, SystemLogRepository>();


builder.Services.AddScoped<IRepository<SiteParameterGroup, LanguageResource, string>, SiteParameterGroupRepository>();
builder.Services.AddScoped<IRepository<SiteParameterItem, LanguageResource, string>, SiteParameterItemRepository>();

builder.Services.AddScoped<IRepository<QuestionnaireGroup, LanguageResource, string>, QuestionnaireGroupRepository>();
builder.Services.AddScoped<IRepository<QuestionnaireItem, LanguageResource, string>, QuestionnaireItemRepository>();


builder.Services.Configure<ForwardedHeadersOptions>(options =>
{
    options.ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto;
});





builder.Services.AddScoped<ICaptcha, Captcha>();


builder.Services.AddDistributedMemoryCache();

builder.Services.AddSession(options =>
{
    options.IdleTimeout = TimeSpan.FromMinutes(3);
    options.Cookie.HttpOnly = true;
    options.Cookie.IsEssential = true;
});

builder.Services.AddCookiePolicy(options =>
{
    options.CheckConsentNeeded = context => true;
    options.MinimumSameSitePolicy = SameSiteMode.None;
});


builder.Services.ConfigureApplicationCookie(options =>
{
    // Cookie settings
    options.Cookie.Name = "Admin";
    options.Cookie.HttpOnly = true;
    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);

    options.LoginPath = "/Siteadmin/Login";
    //options.AccessDeniedPath = "/Identity/Account/AccessDenied";
    options.SlidingExpiration = true;
});


//多國語系
builder.Services.AddSingleton<LocalService>();
builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");
//builder.Services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);//要使用View多國語系的話就加這行程式碼

builder.Services.AddMvc()
        .AddViewLocalization()
        .AddDataAnnotationsLocalization(options =>
        {
            options.DataAnnotationLocalizerProvider = (type, factory) =>
            {
                var assemblyName = new AssemblyName(typeof(ShareResources).GetTypeInfo().Assembly.FullName);
                return factory.Create("ShareResources", assemblyName.Name);
            };
        });


var app = builder.Build();

app.Use(async (context, next) =>
{
    await next();
    if (context.Response.StatusCode == 404)
    {
        context.Request.Path = "/Home/Error";
        await next();
    }

    //context.Response.Headers.Add(
    //                "Content-Security-Policy",
    //                "style-src 'unsafe-hashes' https:; object-src 'self'; script-src  https://www.googletagmanager.com https://www.google-analytics.com/analytics.js 'sha256-e1CQIcsc2giIeRZhsWrp52sOUJh7ymWIxUs9YHnr6Kk=' 'sha256-uMhZDs72+MUxACyw2StJa7Ol3KuFZ5prPGytzQPPKxE=' 'sha256-5uYDSplXaJSFjIlbC0dO6Sarx+dqSQTMYouNJfm2v38=' 'sha256-VBEaEIhHvHkcU4tAZctglyGkwZWqNVDCN1bcC5atjsA=' 'sha256-EiQGSEDnGyKINUa/FUxYP3v+r8ayz44gSi43oNA5+HY=' 'sha256-G/RBtYGxgpgHDQ/uHdbdnJ29y4/rGIq9gOV2L5x3FHk=' 'sha256-kSTZFVoUd2xBrpAdfgb35KjAKdkyB70zw4ZXUqy41gk=' 'sha256-Jpi2IpGJ2yLiIykHXHesRzAeWmbKk5JQ+IZtsfuWz0w=' 'sha256-ggjnJ6FKx/3kh+JPKlZc+D5J7j3IvZHNwiOXLfung5k=' 'sha256-6xl9bP7aiRRnnOGCn9vIFRB10opszBQtXHY5gGlEdpU=' 'sha256-UeIUwt3daQG6F/mOYRdn7Z3VqPmraR3XjsDwK1uuzo8=' 'sha256-IwWBKq2FN3g0va+072yp7wqB041zdAmIQmIRYy8RT50=' 'sha256-o3aMPMVYma0WKGfdYGX/oir7ooE3uZnxCqVhujsqdS8=' 'sha256-qoF3u+/Z7eGg6ZT6f243AroavxWH7/lcYq7jxwCuLII=' 'sha256-yf0sCI5mRdZRxoscdzyY7aRmnjvnDDIjbMXMFAMslDQ=' 'sha256-tgsbVEJNPBAZlNkdGuFJ9pUR7rJd3m7sZACs9rmIoLQ=' 'sha256-iuKKe7JB1wYkaK0+ircWQxrwO1S3f11B0VPKyp0Z7eA=' 'sha256-cS6PlyYvEOXWW4lnPIgpB30o+qZE4pTVXDoTs02Qnio=' 'sha256-ydht7zQBtMcd2+cTDgD1dGPgrqyuwTroBwMkF5TalRU=' 'sha256-DIFMkqLQkpfS6AbhfY88LAW8V8ogXQmD3S/B6yaX94c=' 'sha256-FeTSsQ4NcJvY6j5JuFRBuyMfw472B3gir/RGqjJSmO4=' 'sha256-oUfqvPJm+PKFgGSDBlNyDA6uAcgQe+sHeas2FAEUB64=' 'sha256-kxxD2sYX+mzDo4Cm/U6mQY6RHdDji9enZLj7WvqltzU=' 'sha256-YcW0pTbODVP06oUAglZiionJp2CH+1UgYxhB5wAccWI=' 'sha256-IB4SCoo99C8XZDBjFqP8AMCm+bfPau3TrabAUCuO1EA=' 'sha256-gM7OF4nxl2Z3qynTT8VN9HarInT9ZqVS+B3GQH6Mlgc=' 'sha256-LgtGspHnJ+lk72kAP4JiR2Fo3xNoNH7JxqjQ7Z8BTAA=' 'sha256-3DtBkxgfmvRTJjbcsWXOl7yhblAerPbfE0ChFikY/h4=' 'sha256-eE82VpTyWv5hhs8RMNZRI0M2tdUgt1Ha9FCnmu5SN+4=' 'sha256-HMYp7bl+ENVn1dZd3pzuJUyQu4cq6q2cff9Uk23J+vI=' 'sha256-i3x5y5jShl0/+jQoti9oHOef9gwLUNuPRk/wLlQLhQI=' 'sha256-EJBEyvRTbtjwD9QgA2I1+qY1aasWnH+aufrS92ZAGiI=' 'sha256-2+vNCW/M4XK2utP4rEabnm1T/qzL3vs3d1HYQiEg8zY=' 'sha256-A2UxnLoXra00weZ5/+9g8kjThYpSsdg8NxCBo7hXE70=' 'sha256-pg7XYbXCiH05ZH2Ykep7tKlAoNvtuOajMsOvhUQoCsc=' 'sha256-GK2ClQy3ZNrqPEJu15sgpYKSlF5wd6nssnjNHUmvXlg=' 'sha256-GK2ClQy3ZNrqPEJu15sgpYKSlF5wd6nssnjNHUmvXlg=' 'sha256-Rr8NU6HeHAFPhN2skdrrm6FNzPSIuRUqvt4rAuIf3t4=' 'sha256-FDWwJRr5wNhBlyMPT8M0w3fyq43oUov4P6KyA5e3AG0=' 'sha256-NmN/SfPKtqXwhdfWP56UFbY39neAxlh8/AKy5I4L36Q=' 'sha256-p1TJzkHlaGx9Ljb0O5hMgALOby20mDkbKHUKWzVoOX0=' 'sha256-bkmMHYuKJnLT/X7ilwHiBzmT3ZBAueJWG7RCYy9bHSs=' 'sha256-9+mzWu23oF8zuec+iMGOTf77AKKUWYA8o7p3d4Bs0Ww=' 'sha256-+rmEQYjQB+J2be+V5IuBzaiAKquglktTPIRNH6DJc0g=' 'sha256-xU7rl1+kerWWllWkLh6hcUi46RARjHAcbR8OJprDMXs=' 'sha256-GK2ClQy3ZNrqPEJu15sgpYKSlF5wd6nssnjNHUmvXlg=' 'sha256-JuifJgiuiYlFP73BJBUVzcG/63r54i9earqye6vtCa4=' 'sha256-EI0Va0RBJY2pGs8luCAuRHMSSPtngiV1iCeawT0a0kk=' 'sha256-KJWp/4mX3TbJ84ye4xxQUN9Qcq/RNPuNxS/xnPf43EA=' 'sha256-5iZkeAGG7gLxgegvzRmfLRb9ZR11oxO06AgIVybQeL8=' 'sha256-dSZvM5bohhTXdaa0mFHQRDTUuH4dETbHmaUS0xSnJHs=' 'sha256-hNvhHz+EvjUVSHf4gpASC5y2PnvHb/RrVp/8ah6HHok=' 'sha256-J06JHkZVK1aclJEz4rKOfMrgDrnlZdTvabDYbFIeXBo=' 'sha256-iqcMgJmImIAQD4rKs2ZmkeBVjRVsGfcjyccCwvZeOho=' 'sha256-pjlL0AJCZSVILkiiDrdCw0bOYsPYLXI9v8m7ErSdPJM=' 'sha256-4YiQqLgCCREkC4KJUbPzkF2yN8VlzbzQQa8At168ovQ=' 'sha256-19W2VRWi945NYX4a5Q41GXud53bWaJHp5Lqhzpnf3F4=' https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js 'self' ; "
    //            );
    //await next();

});
//<add name="Content-Security-Policy" value="object-src 'self'; script-src  https://www.googletagmanager.com https://www.google-analytics.com/analytics.js 'sha256-e1CQIcsc2giIeRZhsWrp52sOUJh7ymWIxUs9YHnr6Kk=' 'sha256-uMhZDs72+MUxACyw2StJa7Ol3KuFZ5prPGytzQPPKxE=' 'sha256-5uYDSplXaJSFjIlbC0dO6Sarx+dqSQTMYouNJfm2v38=' 'sha256-VBEaEIhHvHkcU4tAZctglyGkwZWqNVDCN1bcC5atjsA=' 'sha256-EiQGSEDnGyKINUa/FUxYP3v+r8ayz44gSi43oNA5+HY=' 'sha256-G/RBtYGxgpgHDQ/uHdbdnJ29y4/rGIq9gOV2L5x3FHk=' 'sha256-kSTZFVoUd2xBrpAdfgb35KjAKdkyB70zw4ZXUqy41gk=' 'sha256-Jpi2IpGJ2yLiIykHXHesRzAeWmbKk5JQ+IZtsfuWz0w=' 'sha256-ggjnJ6FKx/3kh+JPKlZc+D5J7j3IvZHNwiOXLfung5k=' 'sha256-6xl9bP7aiRRnnOGCn9vIFRB10opszBQtXHY5gGlEdpU=' 'sha256-UeIUwt3daQG6F/mOYRdn7Z3VqPmraR3XjsDwK1uuzo8=' 'sha256-IwWBKq2FN3g0va+072yp7wqB041zdAmIQmIRYy8RT50=' 'sha256-o3aMPMVYma0WKGfdYGX/oir7ooE3uZnxCqVhujsqdS8=' 'sha256-qoF3u+/Z7eGg6ZT6f243AroavxWH7/lcYq7jxwCuLII=' 'sha256-yf0sCI5mRdZRxoscdzyY7aRmnjvnDDIjbMXMFAMslDQ=' 'sha256-tgsbVEJNPBAZlNkdGuFJ9pUR7rJd3m7sZACs9rmIoLQ=' 'sha256-iuKKe7JB1wYkaK0+ircWQxrwO1S3f11B0VPKyp0Z7eA=' 'sha256-cS6PlyYvEOXWW4lnPIgpB30o+qZE4pTVXDoTs02Qnio=' 'sha256-ydht7zQBtMcd2+cTDgD1dGPgrqyuwTroBwMkF5TalRU=' 'sha256-DIFMkqLQkpfS6AbhfY88LAW8V8ogXQmD3S/B6yaX94c=' 'sha256-FeTSsQ4NcJvY6j5JuFRBuyMfw472B3gir/RGqjJSmO4=' 'sha256-oUfqvPJm+PKFgGSDBlNyDA6uAcgQe+sHeas2FAEUB64=' 'sha256-kxxD2sYX+mzDo4Cm/U6mQY6RHdDji9enZLj7WvqltzU=' 'sha256-YcW0pTbODVP06oUAglZiionJp2CH+1UgYxhB5wAccWI=' 'sha256-IB4SCoo99C8XZDBjFqP8AMCm+bfPau3TrabAUCuO1EA=' 'sha256-gM7OF4nxl2Z3qynTT8VN9HarInT9ZqVS+B3GQH6Mlgc=' 'sha256-LgtGspHnJ+lk72kAP4JiR2Fo3xNoNH7JxqjQ7Z8BTAA=' 'sha256-3DtBkxgfmvRTJjbcsWXOl7yhblAerPbfE0ChFikY/h4=' 'sha256-eE82VpTyWv5hhs8RMNZRI0M2tdUgt1Ha9FCnmu5SN+4=' 'sha256-HMYp7bl+ENVn1dZd3pzuJUyQu4cq6q2cff9Uk23J+vI=' 'sha256-i3x5y5jShl0/+jQoti9oHOef9gwLUNuPRk/wLlQLhQI=' 'sha256-EJBEyvRTbtjwD9QgA2I1+qY1aasWnH+aufrS92ZAGiI=' 'sha256-2+vNCW/M4XK2utP4rEabnm1T/qzL3vs3d1HYQiEg8zY=' 'sha256-A2UxnLoXra00weZ5/+9g8kjThYpSsdg8NxCBo7hXE70=' 'sha256-pg7XYbXCiH05ZH2Ykep7tKlAoNvtuOajMsOvhUQoCsc=' 'sha256-GK2ClQy3ZNrqPEJu15sgpYKSlF5wd6nssnjNHUmvXlg=' 'sha256-GK2ClQy3ZNrqPEJu15sgpYKSlF5wd6nssnjNHUmvXlg=' 'sha256-Rr8NU6HeHAFPhN2skdrrm6FNzPSIuRUqvt4rAuIf3t4=' 'sha256-FDWwJRr5wNhBlyMPT8M0w3fyq43oUov4P6KyA5e3AG0=' 'sha256-NmN/SfPKtqXwhdfWP56UFbY39neAxlh8/AKy5I4L36Q=' 'sha256-p1TJzkHlaGx9Ljb0O5hMgALOby20mDkbKHUKWzVoOX0=' 'sha256-bkmMHYuKJnLT/X7ilwHiBzmT3ZBAueJWG7RCYy9bHSs=' 'sha256-9+mzWu23oF8zuec+iMGOTf77AKKUWYA8o7p3d4Bs0Ww=' 'sha256-+rmEQYjQB+J2be+V5IuBzaiAKquglktTPIRNH6DJc0g=' 'sha256-xU7rl1+kerWWllWkLh6hcUi46RARjHAcbR8OJprDMXs=' 'sha256-GK2ClQy3ZNrqPEJu15sgpYKSlF5wd6nssnjNHUmvXlg=' 'sha256-JuifJgiuiYlFP73BJBUVzcG/63r54i9earqye6vtCa4=' 'sha256-EI0Va0RBJY2pGs8luCAuRHMSSPtngiV1iCeawT0a0kk=' 'sha256-KJWp/4mX3TbJ84ye4xxQUN9Qcq/RNPuNxS/xnPf43EA=' 'sha256-5iZkeAGG7gLxgegvzRmfLRb9ZR11oxO06AgIVybQeL8=' 'sha256-dSZvM5bohhTXdaa0mFHQRDTUuH4dETbHmaUS0xSnJHs=' 'sha256-hNvhHz+EvjUVSHf4gpASC5y2PnvHb/RrVp/8ah6HHok=' 'sha256-J06JHkZVK1aclJEz4rKOfMrgDrnlZdTvabDYbFIeXBo=' 'sha256-iqcMgJmImIAQD4rKs2ZmkeBVjRVsGfcjyccCwvZeOho=' 'sha256-pjlL0AJCZSVILkiiDrdCw0bOYsPYLXI9v8m7ErSdPJM=' 'sha256-4YiQqLgCCREkC4KJUbPzkF2yN8VlzbzQQa8At168ovQ=' 'sha256-19W2VRWi945NYX4a5Q41GXud53bWaJHp5Lqhzpnf3F4=' https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js 'self' ; " />

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseMigrationsEndPoint();
}
else
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

//多國語系全球化判斷
var supportedCultures = new CultureInfo[] {
                new CultureInfo("en"),
                new CultureInfo("zh-TW"),
                new CultureInfo("zh-CN"),
            };



app.UseRequestLocalization(new RequestLocalizationOptions()
            { 
                SupportedCultures = supportedCultures,
                SupportedUICultures = supportedCultures,
                //當預設Provider偵測不到用戶支持上述Culture的話，就會是↓
                DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en"),//Default UICulture、Culture 
            }); 



//app.UseHttpsRedirection();//https
app.UseStaticFiles();
//app.UseSession();
app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.UseSession();

app.UseCookiePolicy();

app.UseEndpoints(endpoints =>
{
   

    endpoints.MapControllerRoute(
          name: "MyArea",          
          pattern: "{area:exists}/{controller=Home}/{action=Index}/{id?}/{uri?}" );

    //含語系
     endpoints.MapControllerRoute(
         name: "default",
         pattern: "{culture}/{controller=Home}/{action=Index}/{id?}");



    endpoints.MapRazorPages();
});




app.MapRazorPages();

app.Run();
