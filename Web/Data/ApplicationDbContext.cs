﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using WebApp.Models;
using Microsoft.EntityFrameworkCore.Query;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext() : base() { }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<WebData> WebData { get; set; }
        public virtual DbSet<SystemMenu> SystemMenu { get; set; }
        public virtual DbSet<ArticleCategory> ArticleCategory { get; set; }
        public virtual DbSet<ArticleNews> ArticleNews { get; set; }
        public virtual DbSet<ArticlePage> ArticlePage { get; set; }
        public virtual DbSet<Columns> Columns { get; set; }

        public virtual DbSet<ArticleColumn> ArticleColumn { get; set; }
        public virtual DbSet<ProductCategory> ProductCategory { get; set; }

        public virtual DbSet<ProductSet> ProductSet { get; set; }

        public virtual DbSet<ArticleDownload> ArticleDownload { get; set; }

        public virtual DbSet<ArticleLocation> ArticleLocation { get; set; }

        public virtual DbSet<AdvertisingCategory> AdvertisingCategory { get; set; }

        public virtual DbSet<Advertising> Advertising { get; set; }

        public virtual DbSet<Member> Member { get; set; }
        public virtual DbSet<InboxNotify> InboxNotify { get; set; }

        public virtual DbSet<Contact> Contact { get; set; }


        public virtual DbSet<SiteParameterGroup> SiteParameterGroup { get; set; }

        public virtual DbSet<SiteParameterItem> SiteParameterItem { get; set; }

        public virtual DbSet<SystemItem> SystemItem { get; set; }

        public virtual DbSet<IdentityUser> IdentityUser { get; set; }
        public virtual DbSet<IdentityRole> IdentityRole { get; set; }

        public virtual DbSet<IdentityUserRole<string>> IdentityUserRole { get; set; }

        public virtual DbSet<RolePermission> RolePermission { get; set; }

        public virtual DbSet<Firewall> Firewall { get; set; }

        public virtual DbSet<SystemLog> SystemLog { get; set; }


        public virtual DbSet<DataText> DataText { get; set; }

        public virtual DbSet<ProductSpecCategory> ProductSpecCategory { get; set; }
        public virtual DbSet<ProductSpec> ProductSpec { get; set; }

        public virtual DbSet<SpecData> SpecData { get; set; }
        public virtual DbSet<Languages> Languages { get; set; }

        public virtual DbSet<QuestionnaireGroup> QuestionnaireGroup { get; set; }

        public virtual DbSet<QuestionnaireItem> QuestionnaireItem { get; set; }

        public virtual DbSet<LanguageResource> LanguageResource { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(new ConfigurationBuilder()
                     .AddJsonFile(Path.Combine(Directory.GetCurrentDirectory(), $"appsettings.json"))
                     .AddEnvironmentVariables()
                     .Build()
                     .GetConnectionString("DefaultConnection"));
        }



        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

           // try
          //  {
              


                List<LanguageResource> resources = new List<LanguageResource>();


                (new Seeder.LanguageSeeder()).run(builder);

          


                var pages = (new Seeder.ArticlePageSeeder()).run(builder);
                resources.AddRange(pages);

                var menu = (new Seeder.SystemMenuSeeder()).run(builder);
                resources.AddRange(menu);


                var webData = (new Seeder.WebDataSeeder()).run(builder);
                resources.AddRange(webData);

                var adv = (new Seeder.AdvertisingSeeder()).run(builder);
                resources.AddRange(adv);

                var news = (new Seeder.ArticleNewsSeeder()).run(builder);
                resources.AddRange(news);


                 var product = (new Seeder.ProductSeeder()).run(builder);
                 resources.AddRange(product);


                /* var productSpec = (new Seeder.ProductSpecSeeder()).run(builder);
                 resources.AddRange(productSpec);
                */
                var siteItem = (new Seeder.SiteItemSeeder()).run(builder);
                resources.AddRange(siteItem);

            var questionnaireItem = (new Seeder.QuestionnaireSeeder()).run(builder);
            resources.AddRange(questionnaireItem);


            var InboxNotify = (new Seeder.InboxNotifySeeder()).run(builder);
                resources.AddRange(InboxNotify);

           
                (new Seeder.SystemItemSeeder()).run(builder);
           
         
                (new Seeder.ColumnSeeder()).run(builder);


               


                (new Seeder.LanguageResourceSeeder()).run(builder, resources);

                (new Seeder.TablesColumnDescription()).run(builder);


          /*  }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
            }*/
            

            /*
            builder.Entity<LanguageResource>()
             .HasOne(p => p.ArticleNews)
             .WithMany(b => b.LangData)
             .HasForeignKey(p => p.model_id);

            
            builder.Entity<LanguageResource>()
             .HasOne(p => p.ArticlePage)
             .WithMany(b => b.LangData)
             .HasForeignKey(p => p.model_id);

            builder.Entity<LanguageResource>()
            .HasOne(p => p.ArticleCategory)
            .WithMany(b => b.LangData)
            .HasForeignKey(p => p.model_id);

            builder.Entity<LanguageResource>()
            .HasOne(p => p.ArticleColumn)
            .WithMany(b => b.LangData)
            .HasForeignKey(p => p.model_id);


            builder.Entity<LanguageResource>()
             .HasOne(p => p.SystemMenu)
             .WithMany(b => b.LangData)
             .HasForeignKey(p => p.model_id);

            */
            


            //

        }

    }






}