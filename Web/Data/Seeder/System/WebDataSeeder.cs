﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class WebDataSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {


            builder.Entity<WebData>().HasData(
              new WebData()
              {
                  id = "web",                  
              }
              );


            Dictionary<string, object> contact = new Dictionary<string, object>();
            
            contact.Add("email", "test@minmax.biz");
          


            List<LanguageResource> resources = new List<LanguageResource>()
            {

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 WebDataId = "web",
                 model_type = "WebData",
                 language_id = "zh-TW",
                 title = "矽品",
                 active = true,
                 contact = JsonConvert.SerializeObject(contact),
                 pic = "{\"icon\":[{\"path\":\"/static/admin/images/common/com_logo.png\",\"title\":\"\"}]}",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 //start_at = DateTime.Now,
                 social = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "facebook", "#" }, { "ig", "#" }, { "line", "#" } }),
                 seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "矽品" }, { "description", "矽品" } })
                 
             },

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 WebDataId = "web",
                 model_type = "WebData",
                 language_id = "en",
                 title = "SPIL",
                 active = true,
                 pic = "{\"icon\":[{\"path\":\"/static/admin/images/common/com_logo.png\",\"title\":\"\"}]}",
                 contact = JsonConvert.SerializeObject(contact),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 //start_at = DateTime.Now,
                 social = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "facebook", "#" }, { "ig", "#" }, { "line", "#" } }),
                 seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "SPIL" }, { "description", "SPIL" } })
             },
              new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 WebDataId = "web",
                 model_type = "WebData",
                 language_id = "zh-CN",
                 title = "SPIL",
                 pic = "{\"icon\":[{\"path\":\"/static/admin/images/common/com_logo.png\",\"title\":\"\"}]}",
                 active = true,
                 contact = JsonConvert.SerializeObject(contact),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 //start_at = DateTime.Now,
                  social = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "facebook", "" }, { "ig", "" }, { "line", "" } }),
                 seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "SPIL" }, { "description", "SPIL" } })
             },

            };


            return resources;


          
        }
      


    }
}
