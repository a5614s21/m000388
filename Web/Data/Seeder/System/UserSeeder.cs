﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class TablesColumnDescription
    {
        public void run(ModelBuilder builder)
        {
            this.SeedUsers(builder);
            this.SeedRoles(builder);
            this.SeedUserRoles(builder);
        }

        public void SeedUsers(ModelBuilder builder)
        {
            CipherService cipherService = new CipherService();

            IdentityUser user = new IdentityUser()
            {
                Id = "b74ddd14-6340-4840-95c2-db12554843e5",
                UserName = cipherService.Encrypt("系統管理員"),
                NormalizedUserName = "admin",
                Email = cipherService.Encrypt("test@minmax.biz"),
                LockoutEnabled = false,
                PhoneNumber = cipherService.Encrypt("1234567890"),
                EmailConfirmed = true
            };

            PasswordHasher<IdentityUser> passwordHasher = new PasswordHasher<IdentityUser>();     

            user.PasswordHash = passwordHasher.HashPassword(user, "24241872");

            builder.Entity<IdentityUser>().HasData(user);



            user = new IdentityUser()
            {
                Id = "sysadmin",
                UserName = cipherService.Encrypt("超級管理員"),
                NormalizedUserName = "sysadmin",
                Email = cipherService.Encrypt("test@minmax.biz"),
                LockoutEnabled = false,
                PhoneNumber = cipherService.Encrypt("1234567890"),
                EmailConfirmed = true
            };   

            user.PasswordHash = passwordHasher.HashPassword(user, "m24241872@M");

            builder.Entity<IdentityUser>().HasData(user);



            user = new IdentityUser()
            {
                Id = "design7",
                UserName = cipherService.Encrypt("超級管理員"),
                NormalizedUserName = "Daniel-PC\\design7",
                Email = cipherService.Encrypt("test@minmax.biz"),
                LockoutEnabled = false,
                PhoneNumber = cipherService.Encrypt("1234567890"),
                EmailConfirmed = true
            };   

            user.PasswordHash = passwordHasher.HashPassword(user, "24241872");

            builder.Entity<IdentityUser>().HasData(user);


        }

        public void SeedRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityRole>().HasData(
                new IdentityRole() { Id = "fab4fac1-c546-41de-aebc-a14da6895711", Name = "總管理", ConcurrencyStamp = "1", NormalizedName = "Admin" },
                new IdentityRole() { Id = "c7b013f0-5201-4317-abd8-c211f91b7330", Name = "建檔人員", ConcurrencyStamp = "2", NormalizedName = "HR" }
                );

            List<string> guid = new List<string>();

            for (int i = 1; i <= 51; i++)
            {
                guid.Add(i.ToString());
            }



            List<RolePermission> rolePermission = new List<RolePermission>();

            foreach(string id in guid)
            {
                rolePermission.Add(new RolePermission() { id = Guid.NewGuid().ToString(), RoleId = "fab4fac1-c546-41de-aebc-a14da6895711", MenuId = id, Permission = "F" });
            }


            builder.Entity<RolePermission>().HasData(
                rolePermission


                );



            /*
            ApplicationDbContext DB = new ApplicationDbContext();

            List<SystemMenu> systemMenus = DB.SystemMenu.Where(m => string.IsNullOrEmpty(m.parent_id)).Where(m => string.IsNullOrEmpty(m.model)).ToList();


            foreach(SystemMenu item in systemMenus)
            {

             builder.Entity<RolePermission>().HasData(             
               new RolePermission() { id = Guid.NewGuid().ToString(), RoleId = "fab4fac1-c546-41de-aebc-a14da6895711", MenuId = item.id, Permission = "F" }
               );
            }
            */
        }

        public void SeedUserRoles(ModelBuilder builder)
        {
            builder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string>() { RoleId = "fab4fac1-c546-41de-aebc-a14da6895711", UserId = "b74ddd14-6340-4840-95c2-db12554843e5" },
                new IdentityUserRole<string>() { RoleId = "fab4fac1-c546-41de-aebc-a14da6895711", UserId = "sysadmin" },
                new IdentityUserRole<string>() { RoleId = "fab4fac1-c546-41de-aebc-a14da6895711", UserId = "design7" }
                );
        }
    }
}
