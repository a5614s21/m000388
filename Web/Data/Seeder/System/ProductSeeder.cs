﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class ProductSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {

            

            builder.Entity<ProductCategory>().HasData(

            #region ICPackaging
                 new ProductCategory()
                 {
                     id = "ICPackaging",
                     uri = "ICPackaging",
                     parent_id = "Product",
                     sort = 1,
                     active = true,
                 },


               new ProductCategory()
               {
                   id = "LaminateSubstrate",
                   uri = "ICPackaging",
                   parent_id = "ICPackaging",
                   sort = 1,
                   active = true,
               
               },
                 //子
                 new ProductCategory()
                 {
                     id = "Wirebond",
                     uri = "ICPackaging",
                     parent_id = "LaminateSubstrate",
                     sort = 1,
                     active = true,

                 },

                 new ProductCategory()
                    {
                        id = "FlipChip",
                        uri = "ICPackaging",
                        parent_id = "LaminateSubstrate",
                        sort = 2,
                        active = true,

                    },


                  new ProductCategory()
                  {
                      id = "Leadframe",
                      uri = "ICPackaging",
                      parent_id = "ICPackaging",
                      sort = 2,
                      active = true,

                  },

                 //子
                 new ProductCategory()
                 {
                     id = "Quad",
                     uri = "ICPackaging",
                     parent_id = "Leadframe",
                     sort = 1,
                     active = true,

                 },
                    new ProductCategory()
                    {
                        id = "Dual",
                        uri = "ICPackaging",
                        parent_id = "Leadframe",
                        sort = 2,
                        active = true,

                    },

                     new ProductCategory()
                     {
                         id = "WaferLevelWLCSP",
                         uri = "ICPackaging",
                         parent_id = "ICPackaging",
                         sort = 3,
                         active = true,

                     },

                     new ProductCategory()
                     {
                         id = "StackedDie",
                         uri = "ICPackaging",
                         parent_id = "ICPackaging",
                         sort = 4,
                         active = true,

                     },

                      new ProductCategory()
                      {
                          id = "MultiPackage",
                          uri = "ICPackaging",
                          parent_id = "ICPackaging",
                          sort = 5,
                          active = true,

                      },
                      #endregion


               #region Service
                 new ProductCategory()
                 {
                     id = "Service",
                     uri = "Service",
                     parent_id = "TopService",
                     sort = 1,
                     active = true,
                 },


               new ProductCategory()
               {
                   id = "Assembly",
                   uri = "Service",
                   parent_id = "Service",
                   sort = 1,
                   active = true,

               },
                 //子
                 new ProductCategory()
                 {
                     id = "Design",
                     uri = "Service",
                     parent_id = "Assembly",
                     sort = 1,
                     active = true,

                 },

                 new ProductCategory()
                 {
                     id = "PackageCharacterization",
                     uri = "Service",
                     parent_id = "Assembly",
                     sort = 2,
                     active = true,

                 },
                  new ProductCategory()
                  {
                      id = "ReliabilityTestFailureAnalysis",
                      uri = "Service",
                      parent_id = "Assembly",
                      sort = 2,
                      active = true,

                  },




                  new ProductCategory()
                  {
                      id = "Test",
                      uri = "Service",
                      parent_id = "Service",
                      sort = 2,
                      active = true,

                  },

                 //子
                 new ProductCategory()
                 {
                     id = "Testercapability",
                     uri = "Service",
                     parent_id = "Test",
                     sort = 1,
                     active = true,

                 },

                  new ProductCategory()
                  {
                      id = "CPservice",
                      uri = "Service",
                      parent_id = "Test",
                      sort = 2,
                      active = true,

                  },

                  new ProductCategory()
                  {
                      id = "FTSLTservice",
                      uri = "Service",
                      parent_id = "Test",
                      sort = 3,
                      active = true,

                  },
                  new ProductCategory()
                  {
                      id = "Burninservice",
                      uri = "Service",
                      parent_id = "Test",
                      sort = 4,
                      active = true,

                  }, 
                  new ProductCategory()
                  {
                      id = "Backendservice",
                      uri = "Service",
                      parent_id = "Test",
                      sort = 5,
                      active = true,

                  },
                  new ProductCategory()
                  {
                      id = "Engineeringservice",
                      uri = "Service",
                      parent_id = "Test",
                      sort = 6,
                      active = true,

                  },







                     new ProductCategory()
                     {
                         id = "Bumping",
                         uri = "Service",
                         parent_id = "Service",
                         sort = 3,
                         active = true,

                     },

                     new ProductCategory()
                     {
                         id = "Quality",
                         uri = "Service",
                         parent_id = "Service",
                         sort = 4,
                         active = true,

                     },


            #endregion


            #region Service
                 new ProductCategory()
                 {
                     id = "Technology",
                     uri = "Technology",
                     parent_id = "TopTechnology",
                     sort = 1,
                     active = true,
                 },
                    new ProductCategory()
                    {
                        id = "25D3DwithTSV",
                        uri = "Technology",
                        parent_id = "Technology",
                        sort = 1,
                        active = true,

                    },
                     new ProductCategory()
                     {
                         id = "SysteminPackageSiP",
                         uri = "Technology",
                         parent_id = "Technology",
                         sort = 2,
                         active = true,

                     },
                     new ProductCategory()
                     {
                         id = "PackageonPackagePoP",
                         uri = "Technology",
                         parent_id = "Technology",
                         sort = 3,
                         active = true,

                     },
                     new ProductCategory()
                     {
                         id = "FanOutWLP",
                         uri = "Technology",
                         parent_id = "Technology",
                         sort = 4,
                         active = true,

                     },
                     new ProductCategory()
                     {
                         id = "FanOutWLP2",
                         uri = "Technology",
                         parent_id = "Technology",
                         sort = 5,
                         active = true,

                     },
                 //子
                 new ProductCategory()
                 {
                     id = "CuPillarandBondonTraceBOT",
                     uri = "Technology",
                     parent_id = "FanOutWLP2",
                     sort = 1,
                     active = true,

                 },
                  new ProductCategory()
                  {
                      id = "FinePitchDieBonding",
                      uri = "Technology",
                      parent_id = "FanOutWLP2",
                      sort = 2,
                      active = true,

                  },
                      new ProductCategory()
                      {
                          id = "FinePitchWB",
                          uri = "Technology",
                          parent_id = "Technology",
                          sort = 6,
                          active = true,

                      },
                       new ProductCategory()
                       {
                           id = "Molding",
                           uri = "Technology",
                           parent_id = "Technology",
                           sort = 7,
                           active = true,

                       },
                 //子
                 new ProductCategory()
                 {
                     id = "ThinMold",
                     uri = "Technology",
                     parent_id = "Molding",
                     sort = 1,
                     active = true,

                 },
                  new ProductCategory()
                  {
                      id = "ExposedDie",
                      uri = "Technology",
                      parent_id = "Molding",
                      sort = 2,
                      active = true,

                  },
                   new ProductCategory()
                   {
                       id = "AntennainPackageAiP",
                       uri = "Technology",
                       parent_id = "Technology",
                       sort = 8,
                       active = true,

                   }
                   #endregion
               );



            List<LanguageResource> resources = new List<LanguageResource>()
            {
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "ICPackaging",
                     model_type = "ProductCategory",
                     language_id = "zh-TW",
                     title = "IC Packaging",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/ProductSet/ICPackaging.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Product" }, { "description", "Product" } })
                 },
                new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "ICPackaging",
                     model_type = "ProductCategory",
                     language_id = "en",
                     title = "IC Packaging",
                     active = true,
                     parent_id = "",
                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/ProductSet/ICPackaging.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Product" }, { "description", "Product" } })
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "ICPackaging",
                     model_type = "ProductCategory",
                     language_id = "zh-CN",
                     title = "IC Packaging",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/ProductSet/ICPackaging.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Product" }, { "description", "Product" } })
                 },

               new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "Service",
                     model_type = "ProductCategory",
                     language_id = "zh-TW",
                     title = "Services",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Services/info.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Services" }, { "description", "Services" } })
                 },
                new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "Service",
                     model_type = "ProductCategory",
                     language_id = "en",
                     title = "Services",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Services/info.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Services" }, { "description", "Services" } })
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "Service",
                     model_type = "ProductCategory",
                     language_id = "zh-CN",
                     title = "Services",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Services/info.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Services" }, { "description", "Services" } })
                 },

                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "Technology",
                     model_type = "ProductCategory",
                     language_id = "zh-TW",
                     title = "Technology",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Services/info.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Technology" }, { "description", "Technology" } })
                 },
                   new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "Technology",
                     model_type = "ProductCategory",
                     language_id = "en",
                     title = "Technology",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Services/info.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Technology" }, { "description", "Technology" } })
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductCategoryId = "Technology",
                     model_type = "ProductCategory",
                     language_id = "zh-CN",
                     title = "Technology",
                     active = true,
                     parent_id = "",
                      details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Services/info.html") } }),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     //start_at = DateTime.Now,
                     
                     seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Technology" }, { "description", "Technology" } })
                 },
            };


            List<LanguageResource> ICPackagingResources = this.ICPackaging();
            resources.AddRange(ICPackagingResources);




            List<LanguageResource> ICPackagingProductResources = this.ICPackagingProduct(builder);
            resources.AddRange(ICPackagingProductResources);



            List<LanguageResource> ServiceResources = this.Service();
            resources.AddRange(ServiceResources);



            List<LanguageResource> ServiceProductResources = this.ServiceProduct(builder);
            resources.AddRange(ServiceProductResources);



            List<LanguageResource> TechnologyResources = this.Technology();
            resources.AddRange(TechnologyResources);



            //List<LanguageResource> productResources = this.product(builder);
            //resources.AddRange(productResources);



            return resources;


        }


        public List<LanguageResource> ICPackaging()
        {
            List<string> ids = new List<string>()
            {
                "LaminateSubstrate",
                "Wirebond",
                "FlipChip" ,
                "Leadframe" ,
                "Quad",
                "Dual",
                "WaferLevelWLCSP",
                "StackedDie",
                "MultiPackage",
            };

            List<string> parent_id = new List<string>()
            {
                "ICPackaging",
                "LaminateSubstrate",
                "LaminateSubstrate" ,
                "ICPackaging" ,
                "Leadframe",
                "Leadframe",
                "ICPackaging",
                "ICPackaging",
                "ICPackaging",
            };



            List<string> title = new List<string>()
            {
                "Laminate Substrate",
                "Wirebond",
                "FlipChip" ,
                "Leadframe" ,
                "Quad",
                "Dual",
                "Wafer Level (WLCSP)",
                "Stacked Die",
                "Multi-Package",
            };


            List<string> notes = new List<string>()
            {
                "",
                "",
                "" ,
                "" ,
                "" ,
                "" ,
                "Bumping Service provides the most completed wafer bumping products to meet all customers' requirement. Which includes direct bumping, repassivation, redistribution, wafer level CSP, and ...",
                "The electronics market trend is to offer more functionality, better performance, higher density, lower cost, along with smaller footprints and lower profiles. Siliconware Stacked die packages are...",
                "Siliconware's Multi-Package BGA provides an alternative multi-chip module solution when know-good-die, KGD, is not feasible. Multiple chips are integrated individually after undergoing functional...",
            };

            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging2-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging2-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging2-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging2-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging-list-3.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging-list-4.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging-list-5.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",

            };



            List<string> editor = new List<string>()
            {
                "",
                "",
                "" ,
                "" ,
                "" ,
                "" ,
                MapPath("Content/Templates/ProductSet/ICPackagingPage.html"),
                MapPath("Content/Templates/ProductSet/ICPackagingPage.html") ,
                MapPath("Content/Templates/ProductSet/ICPackagingPage.html") ,
            };



            List<LanguageResource> resources = new List<LanguageResource>();

            int i = 0;
            foreach (string id in ids)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "en",
                    parent_id = parent_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "zh-TW",
                    parent_id = parent_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "zh-CN",
                    parent_id = parent_id[i],
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }



            return resources;



        }


        public List<LanguageResource> ICPackagingProduct(ModelBuilder builder)
        {

            List<string> category_id = new List<string>()
            {
                "Wirebond",
                "Wirebond",
                "Wirebond" ,
                "Wirebond" ,
                "FlipChip",
                "FlipChip",
                "FlipChip",
                "Quad",
                "Quad",
                "Quad",
                "Dual",
                "Dual",
                "Dual",
            };


            List<string> id = new List<string>()
            {
                "ProductPBGA",
                "ProductEDHSBGA",
                "ProductTFBGAVFBGA" ,
                "ProductLGA" ,
                "Productdemo1",
                "Productdemo2",
                "Productdemo3",
                "Productdemo4",
                "Productdemo5",
                "Productdemo6",
                "Productdemo7",
                "Productdemo8",
                "Productdemo9",
            };


            List<string> title = new List<string>()
            {
                "PBGA",
                "EDHS-BGA",
                "TFBGA / VFBGA" ,
                "LGA" ,
                "demo1",
                "demo2",
                "demo3",
                "demo1",
                "demo2",
                "demo3",
                "demo1",
                "demo2",
                "demo3",
            };


            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-2.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-3.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-4.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-2.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-3.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-4.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-2.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-3.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-4.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/packaging/packaging3-list-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/packaging/page-img1.webp\",\"title\":\"\"}]}",
            };




            List<string> notes = new List<string>()
            {
                "PBGA is a die-up design, plastic overmolded BGA using 2, 4 or 6 layer BT substrate and 0.8 or 1.0 or 1.27mm ball pitch and above. It offers high a I/O replacement for the QFP package when the I/O",
                "Siliconware's EDHS-BGA is a unique low cost solution for medium power applications (3-5W). With traditional die-up package design, it offers improved thermal dissipation by virtue of theExposed Drop-in...",
                "Siliconware's Thin / Very Thin and Fine-pitch BGAs are cavity up, wire bonded, and overmolded on rigid substrate chip scale packages. They offer small scale and light weight as well as a costsaving solution. It..." ,
                "Instead of solder balls, Siliconware's LGA packages are assembled with a grid array thin and flat lands underneath the package for second level interconnection. It reduces the total mountedheight ..." ,
                "PBGA is a die-up design, plastic overmolded BGA using 2, 4 or 6 layer BT substrate and 0.8 or 1.0 or 1.27mm ball pitch and above. It offers high a I/O replacement for the QFP package when the I/O",
                "Siliconware's EDHS-BGA is a unique low cost solution for medium power applications (3-5W). With traditional die-up package design, it offers improved thermal dissipation by virtue of theExposed Drop-in...",
                "Siliconware's Thin / Very Thin and Fine-pitch BGAs are cavity up, wire bonded, and overmolded on rigid substrate chip scale packages. They offer small scale and light weight as well as a costsaving solution. It..." ,
                "Instead of solder balls, Siliconware's LGA packages are assembled with a grid array thin and flat lands underneath the package for second level interconnection. It reduces the total mountedheight ..." ,
                "PBGA is a die-up design, plastic overmolded BGA using 2, 4 or 6 layer BT substrate and 0.8 or 1.0 or 1.27mm ball pitch and above. It offers high a I/O replacement for the QFP package when the I/O",
                "Siliconware's EDHS-BGA is a unique low cost solution for medium power applications (3-5W). With traditional die-up package design, it offers improved thermal dissipation by virtue of theExposed Drop-in...",
                "Siliconware's Thin / Very Thin and Fine-pitch BGAs are cavity up, wire bonded, and overmolded on rigid substrate chip scale packages. They offer small scale and light weight as well as a costsaving solution. It..." ,
                "Instead of solder balls, Siliconware's LGA packages are assembled with a grid array thin and flat lands underneath the package for second level interconnection. It reduces the total mountedheight ..." ,
               "PBGA is a die-up design, plastic overmolded BGA using 2, 4 or 6 layer BT substrate and 0.8 or 1.0 or 1.27mm ball pitch and above. It offers high a I/O replacement for the QFP package when the I/O",
            };

            List<ProductSet> products = new List<ProductSet>();
            int i = 0;
            foreach (string item in title)
            {


                products.Add(new ProductSet()
                {
                    id = id[i],
                    uri = "ICPackaging",
                    category_id = category_id[i],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ProductSet>().HasData(
                products
              );






            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSet item in products)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSet",
                    ProductSetId = item.id,
                    language_id = "en",
                    category_id = category_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes" , notes[i] } ,{ "editor", MapPath("Content/Templates/ProductSet/ICPackagingPage.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSet",
                    ProductSetId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i] }, { "editor", MapPath("Content/Templates/ProductSet/ICPackagingPage.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSet",
                    ProductSetId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i] }, { "editor", MapPath("Content/Templates/ProductSet/ICPackagingPage.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }



            return resources;



        }


        public List<LanguageResource> Service()
        {
            List<string> ids = new List<string>()
            {
                "Assembly",
                "Design",
                "PackageCharacterization" ,
                "ReliabilityTestFailureAnalysis" ,
                "Test",
                "Testercapability",
                "CPservice",
                "FTSLTservice",
                "Burninservice",
                "Backendservice",
                "Engineeringservice",
                "Bumping",
                "Quality",
            };


            List<string> parent_id = new List<string>()
            {
                "Service",
                "Assembly",
                "Assembly" ,
                "Assembly" ,
                "Service",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Test",
                "Service",
                "Service",
            };



            List<string> title = new List<string>()
            {
                "Assembly",
                "Design",
                "Package Characterization" ,
                "Reliability Test & Failure Analysis" ,
                "Test",
                "Tester capability",
                "CP service",
                "FT/SLT service",
                "Burn-in service",
                "Back end service",
                "Engineeringservice",
                "Bumping",
                "Quality",
            };


            List<string> notes = new List<string>()
            {
                "Assembly Service includes all required IC packaging processes for chipsets. SPIL offers a full range of in-house design and consultation services.",
                "",
                "" ,
                "" ,
                "Test Service validates the assembly process by utilizing the latest testing solutions for a wide range of digital, and mixed signal devices.",
                "",
                "",
                "",
                "",
                "",
                "",
                "Bumping Service provides the most completed wafer bumping products to meet all customers' requirement. Which includes direct bumping, repassivation, redistribution, wafer level CSP, and ...",
                "Quality Management focuses on company-wide customer satisfaction.",
            };

            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/service/servie-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-3.webp\",\"title\":\"\"}]}",

                "{\"pic\":[{\"path\":\"/styles/images/service/servie-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie2-list-3.webp\",\"title\":\"\"}]}",

                "{\"pic\":[{\"path\":\"/styles/images/service/servie-list-3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie-list-3.webp\",\"title\":\"\"}]}",
              
            };



            List<string> editor = new List<string>()
            {
                MapPath("Content/Templates/Services/second.html"),
                MapPath("Content/Templates/Services/third.html"),
                MapPath("Content/Templates/Services/third.html"),
                MapPath("Content/Templates/Services/third.html"),
                MapPath("Content/Templates/Services/second.html"),

                MapPath("Content/Templates/Services/page.html"),
                MapPath("Content/Templates/Services/page.html"),
                MapPath("Content/Templates/Services/page.html"),
                MapPath("Content/Templates/Services/page.html"),
                MapPath("Content/Templates/Services/page.html"),
                MapPath("Content/Templates/Services/page.html"),

                MapPath("Content/Templates/Services/page.html"),
                MapPath("Content/Templates/Services/page.html"),


            };





            List<LanguageResource> resources = new List<LanguageResource>();

            int i = 0;
            foreach (string id in ids)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "en",
                    parent_id = parent_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "zh-TW",
                    parent_id = parent_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "zh-CN",
                    parent_id = parent_id[i],
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }



            return resources;



        }

        public List<LanguageResource> ServiceProduct(ModelBuilder builder)
        {

            List<string> category_id = new List<string>()
            {
                "Design",
                "Design",
                "Design" ,
                "PackageCharacterization" ,
                "PackageCharacterization" ,
                "PackageCharacterization" ,
                "ReliabilityTestFailureAnalysis",
                "ReliabilityTestFailureAnalysis",
                "ReliabilityTestFailureAnalysis",
            };


            List<string> id = new List<string>()
            {
                "ServiceSubstrate",
                "ServiceLeadFrame",
                "ServiceBumpingMask" ,
                "ServiceDemo1",
                "ServiceDemo2",
                "ServiceDemo3",
                "ServiceDemo4",
                "ServiceDemo5",
                "ServiceDemo6",
               
            };


            List<string> title = new List<string>()
            {
                "Substrate",
                "Lead Frame",
                "Bumping Mask" ,
                "demo1",
                "demo2",
                "demo3",
                "demo1",
                "demo2",
                "demo3",
            };


            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/service/servie3-list-3.webp\",\"title\":\"\"}]}",

            };




            List<string> notes = new List<string>()
            {
                "This page defines the complete substrate design information needed in order to complete an accurate layout and meet your production requirements. All products require the design of substrate as the...",
                "This page defines the complete lead frame design information needed in order to complete accurate layout and meet your production requirements.",
                "This page defines the complete bumping mask design information needed in order to complete accurate layout and meet your production requirements.",
                "This page defines the complete substrate design information needed in order to complete an accurate layout and meet your production requirements. All products require the design of substrate as the...",
                "This page defines the complete lead frame design information needed in order to complete accurate layout and meet your production requirements.",
                "This page defines the complete bumping mask design information needed in order to complete accurate layout and meet your production requirements.",
                "This page defines the complete substrate design information needed in order to complete an accurate layout and meet your production requirements. All products require the design of substrate as the...",
                "This page defines the complete lead frame design information needed in order to complete accurate layout and meet your production requirements.",
                "This page defines the complete bumping mask design information needed in order to complete accurate layout and meet your production requirements.",

            };

            List<ProductCategory> products = new List<ProductCategory>();
            int i = 0;
            foreach (string item in title)
            {


                products.Add(new ProductCategory()
                {
                    id = id[i],
                    uri = "Service",
                    parent_id = category_id[i],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ProductCategory>().HasData(
                products
              );






            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductCategory item in products)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = item.id,
                    language_id = "en",
                    parent_id = category_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i] }, { "editor", MapPath("Content/Templates/Services/page.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = item.id,
                    language_id = "zh-TW",
                    parent_id = category_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i] }, { "editor", MapPath("Content/Templates/Services/page.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = item.id,
                    language_id = "zh-CN",
                    parent_id = category_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i] }, { "editor", MapPath("Content/Templates/Services/page.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }



            return resources;



        }


        public List<LanguageResource> Technology()
        {
            List<string> ids = new List<string>()
            {
                "25D3DwithTSV",
                "SysteminPackageSiP",
                "PackageonPackagePoP" ,
                "FanOutWLP" ,
                "FanOutWLP2",
                "CuPillarandBondonTraceBOT",
                "FinePitchDieBonding",
                "FinePitchWB",
                "Molding",
                "ThinMold",
                "ExposedDie",
                "AntennainPackageAiP",
            };


            List<string> parent_id = new List<string>()
            {
                "Technology",
                "Technology",
                "Technology" ,
                "Technology" ,
                "Technology" ,
                "FanOutWLP2",
                "FanOutWLP2",
                "Technology",
                "Technology",
                 "Molding",
                 "Molding",
                "Technology",
            };



            List<string> title = new List<string>()
            {
                "2.5D & 3D with TSV",
                "System in Package (SiP)",
                "Package on Package (PoP)" ,
                "FanOut-WLP" ,
                "FanOut-WLP" ,
                "Cu Pillar and Bond on Trace (BOT)",
                "Fine Pitch Die Bonding",
                "Fine Pitch WB",
                "Molding",
                "Thin Mold",
                "Exposed Die",
                "Antenna in Package (AiP)",
            };


            List<string> notes = new List<string>()
            {
                "The electronics market trend is to offer more functionality, better performance, higher density, lower cost, along with smaller footprints and lower profiles. Siliconware Stacked die packages are...",
                "A System in Package (SiP) is a combination of one or more semiconductor devices plus optionally passive components that define a certain functional block within a quasi-package that is then, in turn, ...",
                "Package on package (PoP) is an integrated circuit packaging method to combine vertically discrete logic and memory ball grid array (BGA) packages." ,
                "The electronics market trend is to offer more functionality, better performance, higher density, lower cost, along with smaller footprints and lower profiles. Siliconware Stacked die packages are..." ,
                "Professional package design is key to achieving maximum performance, cost savings and faster time to market. Siliconware has an experienced and professional group providing lead frame, substrate...",
                "Definitive characterization is an essential and integral part of the manufacturing of quality packages that are designed to provide long-term, consistent performance.",
                "The electronics market trend is to offer more functionality, better performance, higher density, lower cost, along with smaller footprints and lower profiles. Siliconware Stacked die packages are..." ,
                "Package on package (PoP) is an integrated circuit packaging method to combine vertically discrete logic and memory ball grid array (BGA) packages.",

                "The electronics market trend is to offer more functionality, better performance, higher density, lower cost, along with smaller footprints and lower profiles. Siliconware Stacked die packages are...",
                "Professional package design is key to achieving maximum performance, cost savings and faster time to market. Siliconware has an experienced and professional group providing lead frame, substrate...",
                "Definitive characterization is an essential and integral part of the manufacturing of quality packages that are designed to provide long-term, consistent performance.",
                "Package on package (PoP) is an integrated circuit packaging method to combine vertically discrete logic and memory ball grid array (BGA) packages.",
            };

            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img2.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img3.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img4.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img5.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img5-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img5-2.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img6.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img7.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img5-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img7-1.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/techonogy/technology-img7-2.webp\",\"title\":\"\"}],\"path_pic\":[{\"path\":\"/styles/images/techonogy/technology-img1.webp\",\"title\":\"\"}]}",

            };



            List<string> editor = new List<string>()
            {
                MapPath("Content/Templates/Techonogy/info1.html"),
                MapPath("Content/Templates/Techonogy/info2.html"),
                MapPath("Content/Templates/Techonogy/info3.html"),
                MapPath("Content/Templates/Techonogy/info4.html"),
                MapPath("Content/Templates/Techonogy/list1.html"),
                MapPath("Content/Templates/Techonogy/info5.html"),
                MapPath("Content/Templates/Techonogy/info5-2.html"),
                MapPath("Content/Templates/Techonogy/info6.html"),
                MapPath("Content/Templates/Techonogy/list2.html"),
                MapPath("Content/Templates/Techonogy/info7.html"),
                MapPath("Content/Templates/Techonogy/info7-2.html"),
                MapPath("Content/Templates/Techonogy/info8.html"),
            };





            List<LanguageResource> resources = new List<LanguageResource>();

            int i = 0;
            foreach (string id in ids)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "en",
                    parent_id = parent_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "zh-TW",
                    parent_id = parent_id[i],
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductCategory",
                    ProductCategoryId = id,
                    language_id = "zh-CN",
                    parent_id = parent_id[i],
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", notes[i].ToString() }, { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }



            return resources;



        }

        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
