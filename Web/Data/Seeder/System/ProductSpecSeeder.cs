﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class ProductSpecSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {

            List<string> category_ids = new List<string>()
            {
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
            };

            builder.Entity<ProductSpecCategory>().HasData(


                 new ProductSpecCategory()
                 {
                     id = "OutputPowerW",
                     uri = "search",
                     parent_id = "",
                     sort = 1,
                     input_type = "input",
                     active = true,
                 },
                 //application
                 new ProductSpecCategory()
                 {
                     id = "OutputCurrentA",
                     uri = "search",
                     parent_id = "",
                     sort = 2,
                     active = true,
                     input_type = "input",
                 },
                 new ProductSpecCategory()
                 {
                     id = "OutputVoltageV",
                     uri = "search",
                     parent_id = "",
                     sort = 3,
                     active = true,
                     input_type = "input",
                 },
                 new ProductSpecCategory()
                 {
                     id = "Dimensions",
                     uri = "search",
                     parent_id = "",
                     sort = 4,
                     active = true,
                     input_type = "select",
                 },

                  new ProductSpecCategory()
                  {
                      id = "SingleMulti",
                      uri = "search",
                      parent_id = "",
                      sort = 5,
                      active = true,
                      input_type = "input",
                  },
                  new ProductSpecCategory()
                  {
                      id = "EfficiencyLevel",
                      uri = "search",
                      parent_id = "",
                      sort = 6,
                      active = true,
                      input_type = "select",
                  },

                  new ProductSpecCategory()
                  {
                      id = "OutputCableType",
                      uri = "search",
                      parent_id = "",
                      sort = 7,
                      active = true,
                      input_type = "select",
                  },

                  new ProductSpecCategory()
                  {
                      id = "InputVoltageRabgeV",
                      uri = "search",
                      parent_id = "",
                      sort = 8,
                      active = true,
                      input_type = "select",
                  },
                  new ProductSpecCategory()
                  {
                      id = "Safety",
                      uri = "search",
                      parent_id = "",
                      sort = 9,
                      active = true,
                      input_type = "select",
                  },
                  new ProductSpecCategory()
                  {
                      id = "Dimming",
                      uri = "search",
                      parent_id = "",
                      sort = 10,
                      active = true,
                      input_type = "select",
                  },
                  new ProductSpecCategory()
                  {
                      id = "Feature",
                      uri = "feature",
                      parent_id = "",
                      sort = 11,
                      active = true,
                      input_type = "multiple",
                  },
                      new ProductSpecCategory()
                      {
                          id = "Tag",
                          uri = "tags",
                          parent_id = "",
                          sort = 12,
                          active = true,
                          input_type = "multiple",
                      },
                        new ProductSpecCategory()
                        {
                            id = "SafetyLogo",
                            uri = "safety",
                            parent_id = "",
                            sort = 13,
                            active = true,
                            input_type = "multiple",
                        },
                         new ProductSpecCategory()
                         {
                             id = "Series",
                             uri = "series",
                             parent_id = "",
                             sort = 14,
                             active = true,
                             input_type = "select",
                         },
                            new ProductSpecCategory()
                            {
                                id = "Files",
                                uri = "files",
                                parent_id = "",
                                sort = 15,
                                active = true,
                                input_type = "select",
                            }
               );



            List<LanguageResource> resources = new List<LanguageResource>()
            {
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "OutputPowerW",
                     language_id = "zh-TW",
                     title = "Output Power (W)",
                     active = true,
                     details = "",
                     pic = "",
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "OutputPowerW",
                     language_id = "en",
                    title = "Output Power (W)",
                     active = true,
                     details = "",
                     pic = "",
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                   },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     ProductSpecCategoryId = "OutputPowerW",
                     model_type = "ProductSpecCategory",
                     language_id = "zh-CN",
                    title = "Output Power (W)",
                     active = true,
                     details = "",
                     pic = "",
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     },

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                      ProductSpecCategoryId = "OutputCurrentA",
                     language_id = "zh-TW",
                     title = "Output Current (A)",
                     active = true,
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                    id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                      ProductSpecCategoryId = "OutputCurrentA",
                     language_id = "en",
                     title = "Output Current (A)",
                     active = true,
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                      ProductSpecCategoryId = "OutputCurrentA",
                     language_id = "zh-CN",
                     title = "Output Current (A)",
                     active = true,
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                      ProductSpecCategoryId = "OutputVoltageV",
                     language_id = "zh-TW",
                     title = "Output Voltage (V)",
                     active = true,
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                   id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                       ProductSpecCategoryId = "OutputVoltageV",
                     language_id = "en",
                     title = "Output Voltage (V)",
                     active = true,
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                    id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                       ProductSpecCategoryId = "OutputVoltageV",
                     language_id = "zh-CN",
                     title = "Output Voltage (V)",
                     active = true,
                     input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },



                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Dimensions",
                     language_id = "zh-TW",
                     title = "Dimensions (mm)",
                     active = true,
                      input_type = "select",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                    id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Dimensions",
                     language_id = "en",
                     title = "Dimensions (mm)",
                     active = true,
                      input_type = "select",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                   id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Dimensions",
                     language_id = "zh-CN",
                     title = "Dimensions (mm)",
                     active = true,
                      input_type = "select",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 //SingleMulti
                 new LanguageResource()
                 {
                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "SingleMulti",
                     language_id = "zh-TW",
                     title = "Single/Multi",
                     active = true,
                         input_type = "input",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "SingleMulti",
                     language_id = "en",
                     title = "Single/Multi",
                         input_type = "input",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "SingleMulti",
                     language_id = "zh-CN",
                     title = "Single/Multi",
                         input_type = "input",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 new LanguageResource()
                 {
                   id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "EfficiencyLevel",
                     language_id = "zh-TW",
                     title = "Efficiency Level (80+)",
                     active = true,
                     input_type = "select",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "EfficiencyLevel",
                     language_id = "en",
                     title = "Efficiency Level (80+)",
                     active = true,
                     input_type = "select",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "EfficiencyLevel",
                     language_id = "zh-CN",
                     title = "Efficiency Level (80+)",
                     active = true,
                     input_type = "select",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 //Application

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "OutputCableType",
                     language_id = "zh-TW",
                     title = "Output Cable Type",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,

                 },
                 new LanguageResource()
                 {
                   id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "OutputCableType",
                     language_id = "en",
                     title = "Output Cable Type",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "OutputCableType",
                     language_id = "zh-CN",
                     title = "Output Cable Type",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "InputVoltageRabgeV",
                     language_id = "zh-TW",
                     title = "Input Voltage Rabge (V)",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,

                 },
                 new LanguageResource()
                 {
                    id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "InputVoltageRabgeV",
                     language_id = "en",
                     title = "Input Voltage Rabge (V)",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "InputVoltageRabgeV",
                     language_id = "zh-CN",
                     title = "Input Voltage Rabge (V)",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 new LanguageResource()
                 {


                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Safety",
                     language_id = "zh-TW",
                     title = "Safety",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,

                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Safety",
                     language_id = "en",
                     title = "Safety",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,

                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Safety",
                     language_id = "zh-CN",
                     title = "Safety",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },



                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Dimming",
                     language_id = "zh-TW",
                     title = "Dimming",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {

                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Dimming",
                     language_id = "en",
                     title = "Dimming",
                     input_type = "select",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Dimming",
                     language_id = "zh-CN",
                     input_type = "select",
                     title = "Dimming",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Feature",
                     language_id = "zh-TW",
                     title = "Feature",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {

                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Feature",
                     language_id = "en",
                     title = "Feature",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Feature",
                     language_id = "zh-CN",
                     title = "Feature",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Tag",
                     language_id = "zh-TW",
                     title = "Tag",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {

                  id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Tag",
                     language_id = "en",
                     title = "Tag",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Tag",
                     language_id = "zh-CN",
                     title = "Tag",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "SafetyLogo",
                     language_id = "zh-TW",
                     title = "Safety",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                    new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "SafetyLogo",
                     language_id = "en",
                     title = "Safety",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "SafetyLogo",
                     language_id = "zh-CN",
                     title = "Safety",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Series",
                     language_id = "zh-TW",
                     title = "Series",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Series",
                     language_id = "en",
                     title = "Series",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Series",
                     language_id = "zh-CN",
                     title = "Series",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Files",
                     language_id = "zh-TW",
                     title = "Technical Support",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Files",
                     language_id = "en",
                     title = "Technical Support",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "ProductSpecCategory",
                     ProductSpecCategoryId = "Files",
                     language_id = "zh-CN",
                     title = "Technical Support",
                     input_type = "multiple",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 }
            };


            List<LanguageResource> specResources = this.Dimensions(builder);
            resources.AddRange(specResources);


            specResources = this.SingleMulti(builder);
            resources.AddRange(specResources);

            specResources = this.EfficiencyLevel(builder);
            resources.AddRange(specResources);

            specResources = this.OutputCableType(builder);
            resources.AddRange(specResources);

            specResources = this.InputVoltageRabgeV(builder);
            resources.AddRange(specResources);

            specResources = this.Feature(builder);
            resources.AddRange(specResources);

            specResources = this.Tag(builder);
            resources.AddRange(specResources);


            specResources = this.Safety(builder);
            resources.AddRange(specResources);


            specResources = this.Series(builder);
            resources.AddRange(specResources);


            specResources = this.Files(builder);
            resources.AddRange(specResources);

            return resources;


        }


        public List<LanguageResource> Dimensions(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "140x150x86",
                "160x150x86",
                "82x150x65",
                "150x81.5x40.5",
                "85x200x53",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "Dimensions_" + i.ToString(),
                    uri = "search",
                    category_id = "Dimensions",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    category_id = "Dimensions",
                    title = vs[i].ToString(),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }

        public List<LanguageResource> SingleMulti(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Multi ATX",
                "Single TFX",
                "Multi Flex ATX",
                "Single SFF",
                "Single ATX",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "SingleMulti_" + i.ToString(),
                    uri = "search",
                    category_id = "SingleMulti",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    category_id = "SingleMulti",
                    language_id = "en",
                    title = vs[i].ToString(),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> EfficiencyLevel(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Gold",
                "Platinum",
                "Bronze",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "EfficiencyLevel_" + i.ToString(),
                    uri = "search",
                    category_id = "EfficiencyLevel",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    category_id = "EfficiencyLevel",
                    title = vs[i].ToString(),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }

        public List<LanguageResource> OutputCableType(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Direct",
                "Modular",
                "Direct",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "OutputCableType_" + i.ToString(),
                    uri = "search",
                    category_id = "OutputCableType",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    category_id = "OutputCableType",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }

        public List<LanguageResource> InputVoltageRabgeV(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "100-240",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "InputVoltageRabgeV_" + i.ToString(),
                    uri = "search",
                    category_id = "InputVoltageRabgeV",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    category_id = "InputVoltageRabgeV",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> Feature(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "DALI-2",
                "Programable",
                "Constant current",
                "AC Loop",
                "Universal AC input: 90-264Vac, 300Vac surge",
                "Working temperature 0~+40℃",
                "Energy Star efficiency Level VI compliant",
                "Low standby power consumption",
                "Protections: SCP, OLP, OVP and OTP",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    // id = (new Helper()).GenerateIntID(),
                    id = "Feature_" + i.ToString(),
                    uri = "feature",
                    category_id = "Feature",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    category_id = "Feature",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }



        public List<LanguageResource> Tag(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "ATX",
                "550W",
                "Gold",
                "Platinum",
                "60950",
                "62368",
                "Full Range",
                "Modular Cable",
                "Direct Cable",
                "Bronze",
                "180W",
                "FLEX ATX",
                "TFX",
                "250W",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "tags_" + i.ToString(),
                    uri = "tags",
                    category_id = "Tag",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    category_id = "Tag",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> Safety(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Safety1",
                "Safety2",
                "Safety3",
                "Safety4",
                "Safety5",
                "Safety6",
            };

            List<string> pic = new List<string>
            {
                "/styles/images/product/detail-logo01.svg",
                "/styles/images/product/detail-logo02.svg",
                "/styles/images/product/detail-logo03.svg",
                "/styles/images/product/detail-logo04.svg",
                "/styles/images/product/detail-logo05.svg",
                "/styles/images/product/detail-logo06.svg",
            };

            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "safety_" + i.ToString(),
                    uri = "safety",
                    category_id = "SafetyLogo",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    pic = "{\"pic\":[{\"path\":\"" + pic[i] + "\",\"title\":\"\"}]}",
                    category_id = "SafetyLogo",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> Series(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Series01",
                "Series02",
                "Series03",
                "Series04",
                "Series05",
            };


            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "series_" + i.ToString(),
                    uri = "series",
                    category_id = "Series",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    category_id = "Series",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> Files(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "File Type 1",
                "File Type 2",
                "File Type 3",
                "File Type 4",
            };


            List<ProductSpec> productSpec = new List<ProductSpec>();
            int i = 1;
            foreach (string item in vs)
            {
                productSpec.Add(new ProductSpec()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "files" + i.ToString(),
                    uri = "files",
                    category_id = "Files",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<ProductSpec>().HasData(
               productSpec
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ProductSpec item in productSpec)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ProductSpec",
                    ProductSpecId = item.id,
                    language_id = "en",
                    title = vs[i].ToString(),
                    category_id = "Files",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }

        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
