﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;

namespace WebApp.Seeder
{
    public class LanguageSeeder
    {
        public void run(ModelBuilder builder)
        {
            this.data(builder);
        }



        public void data(ModelBuilder builder)
        {



            builder.Entity<Languages>().HasData(

                 new Languages()
                 {
                     id = Guid.NewGuid().ToString(),
                     language_id = "zh-TW",
                     title = "繁體中文",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     sort = 2,
                     options = "",

                 },
                  new Languages()
                  {
                      id = Guid.NewGuid().ToString(),
                      language_id = "en",
                      title = "English",
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now,
                      sort = 1,
                      options = "",
                  },
                    new Languages()
                    {
                        id = Guid.NewGuid().ToString(),
                        language_id = "zh-CN",
                        title = "簡体中文",
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        sort = 3,
                        options = "",
                    }


                );




        }



        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
