﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class SiteItemSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {


            builder.Entity<SiteParameterGroup>().HasData(


                 new SiteParameterGroup()
                 {
                     id = "CountryArea",
                     uri = "CountryArea",
                     parent_id = "",
                     sort = 1,
                     active = true,
                 },
                 new SiteParameterGroup()
                 {
                     id = "ProductCategory",
                     uri = "ProductCategory",
                     parent_id = "",
                     sort = 2,
                     active = true,
                 }
                 
               );



            List<LanguageResource> resources = new List<LanguageResource>()
            {
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "SiteParameterGroup",
                     SiteParameterGroupId = "CountryArea",
                     language_id = "zh-TW",
                     title = "國家/地區",
                     active = true,
                     details = "",
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "SiteParameterGroup",
                     SiteParameterGroupId = "CountryArea",
                     language_id = "en",
                     title = "Country / Area",
                     active = true,
                     details = "",
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "SiteParameterGroup",
                     SiteParameterGroupId = "CountryArea",
                     language_id = "zh-CN",
                     title = "國家/地區",
                     active = true,
                     details = "",
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "SiteParameterGroup",
                      SiteParameterGroupId = "ProductCategory",
                     language_id = "zh-TW",
                     title = "Product Category",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "SiteParameterGroup",
                      SiteParameterGroupId = "ProductCategory",
                     language_id = "en",
                     title = "Product Category",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "SiteParameterGroup",
                      SiteParameterGroupId = "ProductCategory",
                     language_id = "zh-CN",
                     title = "Product Category",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

              



              
            };


            List<LanguageResource> itemResources = this.CountryArea(builder);
            resources.AddRange(itemResources);


            itemResources = this.ProductCategory(builder);
            resources.AddRange(itemResources);


          

            return resources;


        }


        public List<LanguageResource> CountryArea(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Asia",
                "America",
                "Europe",
               
            };
            List<string> vsTW = new List<string>
            {
                "亞洲",
                "美國",
                "歐洲",

            };

            List<string> vsCN = new List<string>
            {
                "亚洲",
                "美国",
                "欧洲",

            };



            List<SiteParameterItem> siteParameterItem = new List<SiteParameterItem>();
            int i = 1;
            foreach (string item in vs)
            {
                siteParameterItem.Add(new SiteParameterItem()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "Country_" + i.ToString(),
                    uri = "CountryArea",
                    category_id = "CountryArea",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<SiteParameterItem>().HasData(
               siteParameterItem
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (SiteParameterItem item in siteParameterItem)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "SiteParameterItem",
                    SiteParameterItemId = item.id,
                    language_id = "en",
                    category_id = "CountryArea",
                    title = vs[i].ToString(),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "SiteParameterItem",
                    SiteParameterItemId = item.id,
                    language_id = "zh-TW",
                    category_id = "CountryArea",
                    title = vsTW[i].ToString(),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "SiteParameterItem",
                    SiteParameterItemId = item.id,
                    language_id = "zh-CN",
                    category_id = "CountryArea",
                    title = vsCN[i].ToString(),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }
        public List<LanguageResource> ProductCategory(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "關於矽品",
                "IC Packaging",
                "Service",
                "Technology",
                "財務資訊",
                "人力資源",
                "企業永續",

            };

            List<string> email = new List<string>
            {
                "test@minmax.biz",
                "test@minmax.biz",
                "test@minmax.biz",
                "test@minmax.biz",
                "test@minmax.biz",
                "test@minmax.biz",
                "test@minmax.biz",

            };


            List<SiteParameterItem> siteParameterItem = new List<SiteParameterItem>();
            int i = 1;
            foreach (string item in vs)
            {
                siteParameterItem.Add(new SiteParameterItem()
                {
                    //id = (new Helper()).GenerateIntID(),
                    id = "ProductCategory_" + i.ToString(),
                    uri = "ProductCategory",
                    category_id = "ProductCategory",
                    sort = i,
                    active = true,
                });
                i++;
            }



            builder.Entity<SiteParameterItem>().HasData(
               siteParameterItem
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (SiteParameterItem item in siteParameterItem)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "SiteParameterItem",
                    SiteParameterItemId = item.id,
                    language_id = "en",
                    category_id = "ProductCategory",
                    title = vs[i].ToString(),
                    active = true,
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "email", email[i].ToString() } }),
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "SiteParameterItem",
                    SiteParameterItemId = item.id,
                    language_id = "zh-TW",
                    category_id = "ProductCategory",
                    title = vs[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "email", email[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "SiteParameterItem",
                    SiteParameterItemId = item.id,
                    language_id = "zh-CN",
                    category_id = "ProductCategory",
                    title = vs[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "email", email[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }
   

        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
