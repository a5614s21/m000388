﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class InboxNotifySeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {



            builder.Entity<InboxNotify>().HasData(

                 new InboxNotify()
                 {
                     id = "contact",
                     uri = "contact",
                     sort = 1,
                     active = true,
                 },
                   new InboxNotify()
                   {
                       id = "Questionnaire",
                       uri = "Questionnaire",
                       sort = 2,
                       active = true,
                   },
                      new InboxNotify()
                      {
                          id = "re_contact",
                          uri = "re_contact",
                          sort = 3,
                          active = true,
                      }



                );



            List<LanguageResource> resources = new List<LanguageResource>()
            {
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "contact",
                     model_type = "InboxNotify",
                     language_id = "zh-TW",
                     title = "客服中心 - 聯絡表單通知",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/Contact.html") } ,
                             { "subject", "客服中心來信" },
                             { "notes", "客服中心來信" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "contact",
                     model_type = "InboxNotify",
                     language_id = "en",
                     title = "客服中心 - 聯絡表單通知",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/Contact.html") } ,
                             { "subject", "客服中心來信" },
                             { "notes", "客服中心來信" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                       id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "contact",
                     model_type = "InboxNotify",
                     language_id = "zh-CN",
                     title = "客服中心 - 聯絡表單通知",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/Contact.html") } ,
                             { "subject", "客服中心來信" },
                             { "notes", "客服中心來信" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "Questionnaire",
                     model_type = "InboxNotify",
                     language_id = "zh-TW",
                     title = "線上問卷通知",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/Questionnaire.html") } ,
                             { "subject", "線上問卷" },
                             { "notes", "線上問卷來信" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "Questionnaire",
                     model_type = "InboxNotify",
                     language_id = "en",
                     title = "線上問卷通知",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/Questionnaire.html") } ,
                             { "subject", "線上問卷" },
                             { "notes", "線上問卷來信" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "Questionnaire",
                     model_type = "InboxNotify",
                     language_id = "zh-CN",
                     title = "線上問卷通知",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/Questionnaire.html") } ,
                             { "subject", "線上問卷" },
                             { "notes", "線上問卷來信" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                     new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "re_contact",
                     model_type = "InboxNotify",
                     language_id = "zh-TW",
                     title = "客服中心 - 聯絡表單回覆",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/ReContact.html") } ,
                             { "subject", "客服中心回覆" },
                             { "notes", "客服中心回覆" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                      new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "re_contact",
                     model_type = "InboxNotify",
                     language_id = "en",
                     title = "客服中心 - 聯絡表單回覆",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/ReContact.html") } ,
                             { "subject", "客服中心回覆" },
                             { "notes", "客服中心回覆" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                       new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     InboxNotifyId = "re_contact",
                     model_type = "InboxNotify",
                     language_id = "zh-CN",
                     title = "客服中心 - 聯絡表單回覆",
                     active = true,

                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {
                             { "editor", MapPath("Content/Templates/Email/ReContact.html") } ,
                             { "subject", "客服中心回覆" },
                             { "notes", "客服中心回覆" } ,
                             { "email", "m0205@minmax.biz" }
                      }
                     ),
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

            };

            return resources;
        }



        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
