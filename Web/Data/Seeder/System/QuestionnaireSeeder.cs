﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class QuestionnaireSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {


            builder.Entity<QuestionnaireGroup>().HasData(


                 new QuestionnaireGroup()
                 {
                     id = "information",
                     uri = "questionnaire",
                     parent_id = "",
                     sort = 1,                  
                     active = true,
                 },
                 new QuestionnaireGroup()
                 {
                     id = "relation",
                     uri = "questionnaire",
                     parent_id = "",
                     sort = 2,
                     active = true,
                 },
                    new QuestionnaireGroup()
                    {
                        id = "responsibility",
                        uri = "questionnaire",
                        parent_id = "",
                        sort = 3,
                        active = true,
                    },

                    //子類
                     new QuestionnaireGroup()
                     {
                         id = "Governance",
                         uri = "questionnaire",
                         parent_id = "responsibility",
                         sort = 1,
                         active = true,
                     },
                     new QuestionnaireGroup()
                     {
                         id = "Economic",
                         uri = "questionnaire",
                         parent_id = "responsibility",
                         sort = 2,
                         active = true,
                     },
                     new QuestionnaireGroup()
                     {
                         id = "Environmental",
                         uri = "questionnaire",
                         parent_id = "responsibility",
                         sort = 3,
                         active = true,
                     },
                     new QuestionnaireGroup()
                     {
                         id = "Social",
                         uri = "questionnaire",
                         parent_id = "responsibility",
                         sort = 4,
                         active = true,
                     }
               );



            List<LanguageResource> resources = new List<LanguageResource>()
            {
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                     QuestionnaireGroupId = "information",
                     language_id = "zh-TW",
                     title = "Your contact information",
                     active = true,
                     details = "",
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                     QuestionnaireGroupId = "information",
                     language_id = "en",
                     title = "Your contact information",
                     active = true,
                     details = "",
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                     QuestionnaireGroupId = "information",
                     language_id = "zh-CN",
                     title = "Your contact information",
                     active = true,
                     details = "",
                     pic = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },



                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "relation",
                     language_id = "zh-TW",
                     title = "Your relation with SPIL?",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "relation",
                     language_id = "en",
                      title = "Your relation with SPIL?",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "relation",
                     language_id = "zh-CN",
                       title = "Your relation with SPIL?",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },




                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "responsibility",
                     language_id = "zh-TW",
                     title = "Please put on a check in each box that corresponds to the corporate responsibility issues.",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                  new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "responsibility",
                     language_id = "en",
                     title = "Please put on a check in each box that corresponds to the corporate responsibility issues.",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "responsibility",
                     language_id = "zh-CN",
                     title = "Please put on a check in each box that corresponds to the corporate responsibility issues.",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 //子項

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Governance",
                     language_id = "zh-TW",
                     title = "Governance",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },

                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Governance",
                     language_id = "en",
                     title = "Governance",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Governance",
                     language_id = "zh-CN",
                     title = "Governance",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Economic",
                     language_id = "zh-TW",
                     title = "Economic",
                     active = true,
                     parent_id = "responsibility",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Economic",
                     language_id = "en",
                     title = "Economic",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Economic",
                     language_id = "zh-CN",
                     title = "Economic",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Environmental",
                     language_id = "zh-TW",
                     title = "Environmental",
                     active = true,
                     parent_id = "responsibility",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Environmental",
                     language_id = "en",
                     title = "Environmental",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Environmental",
                     language_id = "zh-CN",
                     title = "Environmental",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },


                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Social",
                     language_id = "zh-TW",
                     title = "Social",
                     active = true,
                     parent_id = "responsibility",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Social",
                     language_id = "en",
                     title = "Social",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     model_type = "QuestionnaireGroup",
                      QuestionnaireGroupId = "Social",
                     language_id = "zh-CN",
                     title = "Social",
                     parent_id = "responsibility",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
            };


            List<LanguageResource> itemResources = this.questionnaire1(builder);
            resources.AddRange(itemResources);

            List<LanguageResource> item2Resources = this.questionnaire2(builder);
            resources.AddRange(item2Resources);

            List<LanguageResource> item3Resources = this.questionnaire3(builder);
            resources.AddRange(item3Resources);

            return resources;


        }


        public List<LanguageResource> questionnaire1(ModelBuilder builder)
        {
            List<string> vs = new List<string>
            {
                "Name",
                "Company",
                "PhoneNumber",
                "E-mail"
               
            };
            List<string> vsTW = new List<string>
            {
                "姓名",
                "公司",
                "電話",
                "E-mail"

            };

            List<string> vsJP = new List<string>
            {
                "Name",
                "Company",
                "PhoneNumber",
                "E-mail"

            };
            List<string> spec = new List<string>
            {
                "name",
                "company",
                "tel",
                "email"

            };

            List<string> type = new List<string>
            {
                "text",
                "text",
                "text",
                "text"

            };

            List<FormModel> formModels = new List<FormModel>();

            formModels.Add(new FormModel
            {
                type = "text",
                row_id = "name",
                required = "",
                value = "",

            });

            formModels.Add(new FormModel
            {
                type = "text",
                row_id = "company",
                required = "",
                value = "",

            });
            formModels.Add(new FormModel
            {
                type = "text",
                row_id = "tel",
                required = "",
                value = "",

            });
            formModels.Add(new FormModel
            {
                type = "text",
                row_id = "email",
                required = "",
                value = "",

            });


            List<QuestionnaireItem> questionnaireItem = new List<QuestionnaireItem>();
            int i = 1;
            foreach (string item in vs)
            {
                questionnaireItem.Add(new QuestionnaireItem()
                {
                    id = "questionnaire1_" + i.ToString(),
                    uri = "questionnaire",
                    category_id = "information",
                    sort = i,
                    spec = spec[(i-1)],
                    active = true,
                    type = type[(i - 1)],
                });
                i++;
            }



            builder.Entity<QuestionnaireItem>().HasData(
               questionnaireItem
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (QuestionnaireItem item in questionnaireItem)
            {
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "en",
                    category_id = "information",
                    options = JsonConvert.SerializeObject(formModels[i]),
                    title = vs[i].ToString(),
                    spec = spec[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "zh-TW",
                    category_id = "information",
                    options = JsonConvert.SerializeObject(formModels[i]),
                    title = vsTW[i].ToString(),
                    active = true,
                    spec = spec[i],
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "zh-CN",
                    category_id = "information",
                    options = JsonConvert.SerializeObject(formModels[i]),
                    title = vsJP[i].ToString(),
                    active = true,
                    spec = spec[i],
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> questionnaire2(ModelBuilder builder)
        {

            List<string> vs = new List<string>
            {
                "Your relation with SPIL",
                "Do you know the corporate social responsibility report issued by ASE Technology Holding Co., Ltd. every year?",
                "Have you read the corporate social responsibility report of ASE Technology Holding Co., Ltd.?",

            };
            List<string> vsTW = new List<string>
            {
                "Your relation with SPIL",
                "是否知道「日月光控股」每年發行企業社會責任報告書?",
                "是否閱讀過「日月光控股」的企業社會責任報告書?",

            };

            List<string> vsJP = new List<string>
            {
                "Your relation with SPIL",
                "Do you know the corporate social responsibility report issued by ASE Technology Holding Co., Ltd. every year?",
                "Have you read the corporate social responsibility report of ASE Technology Holding Co., Ltd.?",

            };

            List<string> spec = new List<string>
            {
                "relation1",
                "relation2",
                "relation3",

            };

            List<string> type = new List<string>
            {
                "checkbox",
                "radio",
                "radio",

            };

            List<FormModel> formModels = new List<FormModel>();

            formModels.Add(new FormModel
            {
                type = "checkbox",
                row_id = "relation1",
                required = "",               

            });

            formModels.Add(new FormModel
            {
                type = "radio",
                row_id = "relation2",
                required = "",

            });
            formModels.Add(new FormModel
            {
                type = "radio",
                row_id = "relation3",
                required = "",

            });
            


            List<QuestionnaireItem> questionnaireItem = new List<QuestionnaireItem>();
            int i = 1;
            foreach (string item in vs)
            {
                questionnaireItem.Add(new QuestionnaireItem()
                {
                    id = "questionnaire2_" + i.ToString(),
                    uri = "questionnaire",
                    category_id = "relation",
                    sort = i,
                    spec = spec[(i-1)],
                    active = true,
                    type = type[(i - 1)],
                });
                i++;
            }



            builder.Entity<QuestionnaireItem>().HasData(
               questionnaireItem
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (QuestionnaireItem item in questionnaireItem)
            {

                List<Dictionary<string, string>> checkbox = new List<Dictionary<string, string>>();
                checkbox.Add(new Dictionary<string, string> { { "Shareholders / Investors", "Shareholders / Investors" } });
                checkbox.Add(new Dictionary<string, string> { { "Employees", "Employees" } });
                checkbox.Add(new Dictionary<string, string> { { "Suppliers / Contractors / Partner", "Suppliers / Contractors / Partner" } });
                checkbox.Add(new Dictionary<string, string> { { "Customers", "Customers" } });
                checkbox.Add(new Dictionary<string, string> { { "Communities", "Communities" } });
                checkbox.Add(new Dictionary<string, string> { { "Media", "Media" } });
                checkbox.Add(new Dictionary<string, string> { { "Government", "Government" } });
                checkbox.Add(new Dictionary<string, string> { { "NPO/NGO", "NPO/NGO" } });
                checkbox.Add(new Dictionary<string, string> { { "Industry associations", "Industry associations" } });
                checkbox.Add(new Dictionary<string, string> { { "Education", "Education" } });
                checkbox.Add(new Dictionary<string, string> { { "Other", "Other" } });

                dynamic tempOption = new Dictionary<string, string>() { { "Yes", "Yes" }, { "No", "No" } };

                if (formModels[i].type == "checkbox")
                {
                    tempOption = checkbox;
                }

                formModels[i].value = JsonConvert.SerializeObject(tempOption);

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "en",
                    category_id = "relation",
                    title = vs[i].ToString(),
                    spec = spec[i],
                    options = JsonConvert.SerializeObject(formModels[i]),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                //

                tempOption = new Dictionary<string, string>() { { "是", "是" }, { "否", "否" } };

                if (formModels[i].type == "checkbox")
                {
                    tempOption = checkbox;
                }
                formModels[i].value = JsonConvert.SerializeObject(tempOption);

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "zh-TW",
                    category_id = "relation",
                    title = vsTW[i].ToString(),
                    spec = spec[i],
                    options = JsonConvert.SerializeObject(formModels[i]),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                tempOption = new Dictionary<string, string>() { { "Yes", "Yes" }, { "No", "No" } };

                if (formModels[i].type == "checkbox")
                {
                    tempOption = checkbox;
                }

                formModels[i].value = JsonConvert.SerializeObject(tempOption);

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "zh-CN",
                    category_id = "relation",
                    title = vsJP[i].ToString(),
                    spec = spec[i],
                    options = JsonConvert.SerializeObject(formModels[i]),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public List<LanguageResource> questionnaire3(ModelBuilder builder)
        {

            List<string> vs = new List<string>
            {
                "Corporate Governance",
                "Risk Management",
                "Regulation Compliance",
                "Ethics Compliance and Code of Conduct",
                "Stakeholder Communication",

                "Financial / Economics Performance",
                "Tax Policy",
                "Product Quality and Research Development",
                "Customer Satisfaction Survey",
                "Customer Service Management",
                "Supply Chain Management",
                "Industry Localization",
                "Contractors Management",
                "Conflict Minerals Management",

                "GHG Emission",
                "Product Carbon Footprint",
                "Energy Management",
                "Water Source Management",
                "Wastewater Withdraw and Control",
                "Air Pollutant Control and Management",
                "Waste Management and Recycle",
                "Use of Raw Material and Recycle Material",
                "Hazardous Substance Management",
                "Design and Development of Green Product/ Service",
                "Green Supply Chain Management",
                "Green Accounting",
                "Green Logistics",

                "Occupational Safety and Health",
                "Employee Relations",
                "Employee Rights",
                "Career Development and Education Training",
                "Employee Benefit and Welfare",
                "Talent Attraction and Retention",
                "Social Performance of Supplier Management",
                "Community Involvement and Philanthropy",


            };
            List<string> vsNotes = new List<string>
            {
                "Structure and function of Board of Directors, audit, and shareholding",
                "Organizations and analysis of different kinds of risk management and the content of each management",
                "Environmental laws and regulations,Labor laws and regulations, Financial and Tax Rules laws and regulations",
                "Code of Conduct making, such as anti-corruption or confidentiality protection, policies and violations.",
                "Approaches of stakeholder engagement, communication content, and feedback.",

                "Financial information, such as Revenue, expenditure, and net profit",
                "Total tax payments, Tax Preferences",
                "Investment and cost of research and development, creation of products and production procedure, and environmental innovation.",
                "Content of customer satisfaction survey, frequency, and object, and approaches to improve customer satisfaction",
                "Measures of customer service management.",
                "Approaches of supply chain management on the aspects of quality, delivery, ability, and flexibility.",
                "To promote the localization production and support local enterprises.",
                "Construction safety management, self-management",
                "Conflict Minerals Management policies, norms",

                "GHG inventory, information disclosure, and GHG emission reduction.",
                "Management of product carbon footprint, which includes the inventory and measurement of emission and approaches of reducing carbon footprint",
                "Measures and performance of all kinds of energy management approached",
                "Condition of water resource use, quality management, and water saving measures",
                "Management approaches of wastewater discharge, Management of run-off wastewater",
                "Management approaches of air pollutants",
                "Approaches of waste management, resource recycling and its performance",
                "Application and use of all the raw materials and recycle materials",
                "Management of hazardous materials and management approaches of hazardous materials during production",
                "Requirement of green product design, environmental impact assessment, and environmental declaration of the products.",
                "Management policy of green supplier, supplier evaluation and audit.",
                "Promoting green accounting system, moderate to expose environmental improvement and resource protection activities and outcomes",
                "Optimize the transport routes and delivery methods, reverse recovery of packing materials…etc.",

                "Management system of occupational safety, disabling injury frequency, the incidence of occupational diseases, and number of death",
                "Approaches of employee negotiations, labor disputes, employee communication channels",
                "Protection of employee rights, Complaints mechanism, and sexual harassment",
                "System of employee career development, education and training content, and performance indicators.",
                "Employee Benefit and Welfare",
                "Employee’s performance review, compensation and reward systems",
                "Management approaches of supplier performance on social aspect, including labor force, human right, anti-discrimination, conflict-free materials, and so on.",
                "Strategies and approaches of community involvement and philanthropy, philanthropic activities and input.",

            };

            List<string> category_id = new List<string>
            {
                "Governance",
                "Governance",
                "Governance",
                "Governance",
                "Governance",

                "Economic",
                "Economic",
                "Economic",
                "Economic",
                "Economic",
                "Economic",
                "Economic",
                "Economic",
                "Economic",

                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",
                "Environmental",

                "Social",
                "Social",
                "Social",
                "Social",
                "Social",
                "Social",
                "Social",
                "Social",


            };

            FormModel formModels = new FormModel
            {
                type = "radio",
                row_id = "",
                required = "",
                //value = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "level_1", "1" }, { "level_2", "2" }, { "level_3", "3" }, { "level_4", "4" } }),
                value = "",
            };

            List<QuestionnaireItem> questionnaireItem = new List<QuestionnaireItem>();
            int i = 1;
            foreach (string item in vs)
            {
                questionnaireItem.Add(new QuestionnaireItem()
                {
                    id = "questionnaire3_" + i.ToString(),
                    uri = "questionnaire",
                    category_id = category_id[(i-1)],
                    spec = "respons" + i,
                    sort = i,
                    active = true,
                    type = "radio",
                });
                i++;
            }



            builder.Entity<QuestionnaireItem>().HasData(
               questionnaireItem
             );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (QuestionnaireItem item in questionnaireItem)
            {
                formModels.row_id = "respons" + (i + 1);

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "en",
                    category_id = category_id[i],
                    title = vs[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "notes", vsNotes[i] } }),
                    options = JsonConvert.SerializeObject(formModels),
                    active = true,
                    spec = "respons" + (i + 1),
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

              
                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id[i],
                    title = vs[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "notes", vsNotes[i] } }),
                    options = JsonConvert.SerializeObject(formModels),
                    active = true,
                    spec = "respons" + (i + 1),
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

               

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "QuestionnaireItem",
                    QuestionnaireItemId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id[i],
                    title = vs[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, string>() { { "notes", vsNotes[i] } }),
                    options = JsonConvert.SerializeObject(formModels),
                    active = true,
                    spec = "respons" + (i + 1),
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });

                i++;
            }

            return resources;
        }


        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
