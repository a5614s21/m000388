﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class SystemMenuSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {

            


            List<string> topGuid = new List<string>();

            for (int i = 1; i <= 18; i++)
            {
                topGuid.Add((new Helper()).GenerateIntID());
            }




            List<string> guid = new List<string>();

            for(int i=1;i<=51;i++)
            {
                guid.Add(i.ToString());
            }






            string parent_id = Guid.NewGuid().ToString();

            builder.Entity<SystemMenu>().HasData(

                //第一層
                new SystemMenu()
                {
                    id = "default",
                    lang = "zh-TW",
                    title = "DEFAULT",
                    uri = "default",
                    parent_id = "",
                    sort = 1,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                },

                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[0]),
                        lang = "zh-TW",
                        title = "後臺首頁",
                        uri = "siteadmin_index",
                        link = "/Siteadmin/Home/Index",
                        parent_id = "default",
                        icon = "icon-home3",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[1]),
                        lang = "zh-TW",
                        title = "網站首頁",
                        uri = "home_index",
                        link = "/",
                        parent_id = "default",
                        icon = "icon-home1",
                        sort = 2,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },




                //第一層
                new SystemMenu()
                {
                    id = "module",
                    lang = "zh-TW",
                    title = "MODULE",
                    uri = "module",
                    parent_id = "",
                    sort = 2,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                },


                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[2]),
                        lang = "zh-TW",
                        title = "內容管理",
                        uri = "article",
                        link = "",
                        parent_id = "module",
                        icon = "icon-file-text",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


            #region 內容管理-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[0],
                        lang = "zh-TW",
                        title = "新聞稿",
                        model = "ArticleNews",
                        uri = "news",
                        link = "/Siteadmin/ArticleNews/List/news",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


                     new SystemMenu()
                     {
                         id = guid[1],
                         lang = "zh-TW",
                         title = "關於矽品",
                         model = "ArticlePage",
                         uri = "About",
                         link = "/Siteadmin/ArticlePage/List/About",
                         parent_id = parent_id,
                         icon = "",
                         sort = 2,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },

                       new SystemMenu()
                       {
                           id = guid[2],
                           lang = "zh-TW",
                           title = "里程碑",
                           model = "ArticleColumn",
                           uri = "history",
                           link = "/Siteadmin/ArticleColumn/List/history",
                           parent_id = parent_id,
                           icon = "",
                           sort = 3,
                           active = true,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },


                       new SystemMenu()
                       {
                           id = guid[3],
                           lang = "zh-TW",
                           title = "檔案下載",
                           model = "ArticleDownload",
                           uri = "article_download",
                           link = "",
                           parent_id = parent_id,
                           icon = "",
                           sort = 3,
                           active = false,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },


                        new SystemMenu()
                        {
                            id = guid[4],
                            lang = "zh-TW",
                            title = "營業據點",
                            model = "ArticleLocation",
                            uri = "article_Location",
                            link = "",
                            parent_id = parent_id,
                            icon = "",
                            sort = 5,
                            active = true,
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now
                        },


                     new SystemMenu()
                     {
                         id = guid[5],
                         lang = "zh-TW",
                         title = "影音管理",
                         model = "ArticleNews",
                         uri = "video",
                         link = "/Siteadmin/ArticleNews/List/video",
                         parent_id = parent_id,
                         icon = "",
                         sort = 6,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },

                      new SystemMenu()
                      {
                          id = guid[39],
                          lang = "zh-TW",
                          title = "證書",
                          model = "ArticleDownload",
                          uri = "certification",
                          link = "/Siteadmin/ArticleDownload/List/certification",
                          parent_id = parent_id,
                          icon = "",
                          sort = 7,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },

                     new SystemMenu()
                     {
                         id = guid[6],
                         lang = "zh-TW",
                         title = "內容類別管理",
                         model = "ArticleCategory",
                         uri = "article_category",
                         link = "",
                         parent_id = parent_id,
                         icon = "",
                         sort = 50,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },
            #endregion ---------------------------------------------------------------------


                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[3]),
                        lang = "zh-TW",
                        title = "財務資訊",
                        link = "",
                        parent_id = "module",
                        icon = "icon-book",
                        uri = "finance",
                        sort = 11,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },

            #region 財務資訊-細項---------------------------------------------------------------------


                     new SystemMenu()
                     {
                         id = guid[7],
                         lang = "zh-TW",
                         title = "月銷售額",
                         model = "ArticleDownload",
                         uri = "finance",
                         link = "/Siteadmin/ArticleDownload/List/finance",
                         parent_id = parent_id,
                         icon = "",
                         sort = 1,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },

                        new SystemMenu()
                        {
                            id = guid[8],
                            lang = "zh-TW",
                            title = "季報表",
                            model = "ArticleDownload",
                            uri = "quarterly",
                            link = "/Siteadmin/ArticleDownload/List/quarterly",
                            parent_id = parent_id,
                            icon = "",
                            sort = 18,
                            active = true,
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now
                        },

                             new SystemMenu()
                             {
                                 id = guid[9],
                                 lang = "zh-TW",
                                 title = "美國證券交易委員會文件",
                                 model = "ArticleDownload",
                                 uri = "filings",
                                 link = "/Siteadmin/ArticleDownload/List/filings",
                                 parent_id = parent_id,
                                 icon = "",
                                 sort = 19,
                                 active = true,
                                 options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                                 created_at = DateTime.Now,
                                 updated_at = DateTime.Now
                             },

                              new SystemMenu()
                              {
                                  id = guid[10],
                                  lang = "zh-TW",
                                  title = "合併財務報表",
                                  model = "ArticleDownload",
                                  uri = "consolidated",
                                  link = "/Siteadmin/ArticleDownload/List/consolidated",
                                  parent_id = parent_id,
                                  icon = "",
                                  sort = 29,
                                  active = true,
                                  options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                                  created_at = DateTime.Now,
                                  updated_at = DateTime.Now
                              },

                                new SystemMenu()
                                {
                                    id = guid[11],
                                    lang = "zh-TW",
                                    title = "財務報告",
                                    model = "ArticleDownload",
                                    uri = "reports",
                                    link = "/Siteadmin/ArticleDownload/List/reports",
                                    parent_id = parent_id,
                                    icon = "",
                                    sort = 30,
                                    active = true,
                                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                                    created_at = DateTime.Now,
                                    updated_at = DateTime.Now
                                },


                                new SystemMenu()
                                {
                                    id = guid[45],
                                    lang = "zh-TW",
                                    title = "年度報告",
                                    model = "ArticleDownload",
                                    uri = "annual",
                                    link = "/Siteadmin/ArticleDownload/List/annual",
                                    parent_id = parent_id,
                                    icon = "",
                                    sort = 30,
                                    active = true,
                                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                                    created_at = DateTime.Now,
                                    updated_at = DateTime.Now
                                },
            #endregion ---------------------------------------------------------------------


                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[4]),
                        lang = "zh-TW",
                        title = "產品管理",
                        link = "",
                        parent_id = "module",
                        icon = "icon-codepen",
                        uri = "product",
                        sort = 10,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },

            #region 產品管理-細項---------------------------------------------------------------------

                 

                      new SystemMenu()
                      {
                          id = guid[12],
                          lang = "zh-TW",
                          title = "主選單管理",
                          model = "ProductCategory",
                          uri = "",
                          link = "",
                          parent_id = parent_id,
                          icon = "",
                          sort = 1,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },

                    new SystemMenu()
                    {
                        id = guid[13],
                        lang = "zh-TW",
                        title = "IC Packaging",
                        model = "ProductCategory",
                        uri = "ICPackaging",
                        link = "/Siteadmin/ProductCategory/List/ICPackaging",
                        parent_id = parent_id,
                        icon = "",
                        sort = 2,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


                      new SystemMenu()
                      {
                          id = guid[14],
                          lang = "zh-TW",
                          title = "IC Packaging Product",
                          model = "ProductSet",
                          uri = "ICPackaging",
                          link = "",
                          parent_id = parent_id,
                          icon = "",
                          sort = 3,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },


                    new SystemMenu()
                    {
                        id = guid[15],
                        lang = "zh-TW",
                        title = "Services",
                        model = "ProductCategory",
                        uri = "TopService",
                        link = "/Siteadmin/ProductCategory/List/TopService",
                        parent_id = parent_id,
                        icon = "",
                        sort = 4,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


                    

                     new SystemMenu()
                     {
                         id = guid[16],
                         lang = "zh-TW",
                         title = "Technology",
                         model = "ProductCategory",
                         uri = "TopTechnology",
                         link = "/Siteadmin/ProductCategory/List/TopTechnology",
                         parent_id = parent_id,
                         icon = "",
                         sort = 5,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },




            #endregion ---------------------------------------------------------------------



                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[5]),
                        lang = "zh-TW",
                        title = "人力資源",
                        uri = "Careers",
                        link = "",
                        parent_id = "module",
                        icon = "icon-user",
                        sort = 12,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },



            #region 人力資源-細項---------------------------------------------------------------------

                     new SystemMenu()
                     {
                         id = guid[17],
                         lang = "zh-TW",
                         title = "靜態頁面",
                         model = "ArticlePage",
                         uri = "Careers",
                         link = "/Siteadmin/ArticlePage/List/Careers",
                         parent_id = parent_id,
                         icon = "",
                         sort = 2,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },
                      new SystemMenu()
                      {
                          id = guid[18],
                          lang = "zh-TW",
                          title = "獎項",
                          model = "ArticleColumn",
                          uri = "awards",
                          link = "/Siteadmin/ArticleColumn/List/awards",
                          parent_id = parent_id,
                          icon = "",
                          sort = 3,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },
                         new SystemMenu()
                         {
                             id = guid[19],
                             lang = "zh-TW",
                             title = "訓儲人員心得",
                             model = "ArticleColumn",
                             uri = "experience",
                             link = "/Siteadmin/ArticleColumn/List/experience",
                             parent_id = parent_id,
                             icon = "",
                             sort = 4,
                             active = true,
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now
                         },

            #endregion ---------------------------------------------------------------------


                  new SystemMenu()
                  {
                      id = (parent_id = topGuid[6]),
                      lang = "zh-TW",
                      title = "永續發展",
                      uri = "Sustainability",
                      link = "",
                      parent_id = "module",
                      icon = "icon-user",
                      sort = 12,
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now
                  },


            #region 人力資源-細項---------------------------------------------------------------------

                     new SystemMenu()
                     {
                         id = guid[20],
                         lang = "zh-TW",
                         title = "靜態頁面",
                         model = "ArticlePage",
                         uri = "Sustainability",
                         link = "/Siteadmin/ArticlePage/List/Sustainability",
                         parent_id = parent_id,
                         icon = "",
                         sort = 1,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },

                      new SystemMenu()
                      {
                          id = guid[49],
                          lang = "zh-TW",
                          title = "企業社會責任",
                          model = "ArticlePage",
                          uri = "CS",
                          link = "/Siteadmin/ArticlePage/List/CS",
                          parent_id = parent_id,
                          icon = "",
                          sort = 2,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },
                      new SystemMenu()
                      {
                          id = guid[21],
                          lang = "zh-TW",
                          title = "下載資訊",
                          model = "ArticleDownload",
                          uri = "Sustainability",
                          link = "/Siteadmin/ArticleDownload/List/Sustainability",
                          parent_id = parent_id,
                          icon = "",
                          sort = 3,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },
                     

            #endregion ---------------------------------------------------------------------



                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[7]),
                        lang = "zh-TW",
                        title = "廣告模組",
                        uri = "advertising",
                        link = "",
                        parent_id = "module",
                        icon = "icon-map",
                        sort = 20,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


            #region 廣告模組-細項---------------------------------------------------------------------

                 /*    new SystemMenu()
                     {
                         id = guid[22],
                         lang = "zh-TW",
                         title = "廣告分類",
                         model = "AdvertisingCategory",
                         uri = "advertising_category",
                         link = "/Siteadmin/AdvertisingCategory/List",
                         parent_id = parent_id,
                         icon = "",
                         sort = 1,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "N" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },*/



                    new SystemMenu()
                    {
                        id = guid[22],
                        lang = "zh-TW",
                        title = "首頁Banner",
                        model = "Advertising",
                        uri = "Banner",
                        link = "/Siteadmin/Advertising/List/Banner",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
                     new SystemMenu()
                     {
                         id = guid[23],
                         lang = "zh-TW",
                         title = "Financial Report",
                         model = "Advertising",
                         uri = "report",
                         link = "/Siteadmin/Advertising/List/report",
                         parent_id = parent_id,
                         icon = "",
                         sort = 2,
                         active = true,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },
                      new SystemMenu()
                      {
                          id = guid[40],
                          lang = "zh-TW",
                          title = "服務展示區塊",
                          model = "Advertising",
                          uri = "ServiceAd",
                          link = "/Siteadmin/Advertising/List/ServiceAd",
                          parent_id = parent_id,
                          icon = "",
                          sort = 3,
                          active = true,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },

                        new SystemMenu()
                        {
                            id = guid[44],
                            lang = "zh-TW",
                            title = "相關數據",
                            model = "Advertising",
                            uri = "NumEditor",
                            link = "/Siteadmin/Advertising/Edit/NumEditor/NumEditor",
                            parent_id = parent_id,
                            icon = "",
                            sort = 4,
                            active = true,
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now
                        },

                       new SystemMenu()
                       {
                           id = guid[41],
                           lang = "zh-TW",
                           title = "首頁中心編輯器廣告",
                           model = "Advertising",
                           uri = "MainEditor",
                           link = "/Siteadmin/Advertising/Edit/MainEditor/MainEditor",
                           parent_id = parent_id,
                           icon = "",
                           sort = 5,
                           active = true,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },

                       new SystemMenu()
                       {
                           id = guid[42],
                           lang = "zh-TW",
                           title = "首頁中心區塊",
                           model = "Advertising",
                           uri = "MainAd",
                           link = "/Siteadmin/Advertising/List/MainAd",
                           parent_id = parent_id,
                           icon = "",
                           sort = 6,
                           active = true,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },

                          new SystemMenu()
                          {
                              id = guid[43],
                              lang = "zh-TW",
                              title = "下方區塊",
                              model = "Advertising",
                              uri = "DownAd",
                              link = "/Siteadmin/Advertising/List/DownAd",
                              parent_id = parent_id,
                              icon = "",
                              sort = 7,
                              active = true,
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now
                          },

            #endregion ---------------------------------------------------------------------



                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[8]),
                        lang = "zh-TW",
                        title = "會員中心",
                        link = "",
                        parent_id = "module",
                        uri = "member",
                        icon = "icon-users",
                        sort = 30,
                        active = false,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


            #region 會員中心-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[24],
                        lang = "zh-TW",
                        title = "會員資料管理",
                        model = "Member",
                        uri = "member_data",
                        link = "",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


            #endregion ---------------------------------------------------------------------



                    //第二層




                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[9]),
                        lang = "zh-TW",
                        title = "客服中心",
                        uri = "service",
                        link = "",
                        parent_id = "module",
                        icon = "icon-man",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },


            #region 客服中心-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[25],
                        lang = "zh-TW",
                        title = "收件匣",
                        model = "Contact",
                        uri = "Contact",
                        link = "/Siteadmin/Contact/List/Contact",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "Y" }, { "list", "N" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
                      new SystemMenu()
                      {
                          id = guid[38],
                          lang = "zh-TW",
                          title = "線上問卷",
                          model = "Contact",
                          uri = "Questionnaire",
                          link = "/Siteadmin/Contact/List/Questionnaire",
                          parent_id = parent_id,
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                          icon = "",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now
                      },

                       new SystemMenu()
                       {
                           id = guid[46],
                           lang = "zh-TW",
                           title = "電子報訂閱",
                           model = "Contact",
                           uri = "Subscribe",
                           link = "/Siteadmin/Contact/List/Subscribe",
                           parent_id = parent_id,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           icon = "",
                           sort = 3,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },

                       new SystemMenu()
                       {
                           id = guid[47],
                           lang = "zh-TW",
                           title = "問卷相關類別",
                           model = "QuestionnaireGroup",
                           uri = "",
                           link = "",
                           parent_id = parent_id,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           icon = "",
                           sort = 4,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },


                       new SystemMenu()
                       {
                           id = guid[48],
                           lang = "zh-TW",
                           title = "問卷項目",
                           model = "QuestionnaireItem",
                           uri = "",
                           link = "",
                           parent_id = parent_id,
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                           icon = "",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now
                       },


                     new SystemMenu()
                     {
                         id = guid[26],
                         lang = "zh-TW",
                         title = "事件通知信件",
                         model = "InboxNotify",
                         uri = "inbox_notify",
                         link = "",
                         parent_id = parent_id,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         icon = "",
                         sort = 99,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },
                    
            #endregion ---------------------------------------------------------------------



                //第一層
                new SystemMenu()
                {
                    id = "system",
                    lang = "zh-TW",
                    title = "SYSTEM",
                    uri = "system",
                    parent_id = "",
                    active = true,
                    sort = 10,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now
                },
                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[10]),
                        lang = "zh-TW",
                        title = "控制台",
                        uri = "system",
                        link = "",
                        parent_id = "system",
                        icon = "icon-cog",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },

            #region 控制台-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[28],
                        lang = "zh-TW",
                        title = "網站基本資訊",
                        model = "WebData",
                        uri = "webdata",
                        link = "/Siteadmin/WebData/Edit/web",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "N" }, { "edit_list", "N" } }),
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
                     new SystemMenu()
                     {
                         id = guid[29],
                         lang = "zh-TW",
                         title = "電子郵件伺服器",
                         model = "SystemItem",
                         uri = "smtp",
                         link = "/Siteadmin/SystemItem/Edit/smtp/smtp",
                         parent_id = parent_id,
                         icon = "",
                         sort = 2,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "N" }, { "edit_list", "N" } }),
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },


                        new SystemMenu()
                        {
                            id = guid[30],
                            lang = "zh-TW",
                            title = "網站參數群組",
                            model = "SiteParameterGroup",
                            uri = "site",
                            link = "",
                            parent_id = parent_id,
                            icon = "",
                            sort = 3,
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "N" }, { "edit_list", "N" } }),
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now
                        },

                         new SystemMenu()
                         {
                             id = guid[31],
                             lang = "zh-TW",
                             title = "網站參數資訊",
                             model = "SiteParameterItem",
                             uri = "site",
                             link = "",
                             parent_id = parent_id,
                             icon = "",
                             sort = 4,
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now
                         },


                          new SystemMenu()
                          {
                              id = guid[50],
                              lang = "zh-TW",
                              title = "檔案總管",
                              model = "WebData",
                              uri = "webdata",
                              link = "/Siteadmin/File/Management",
                              parent_id = parent_id,
                              icon = "",
                              sort = 99,
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "N" }, { "list", "N" }, { "edit_list", "N" } }),
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now
                          },

            #endregion ---------------------------------------------------------------------

                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[11]),
                        lang = "zh-TW",
                        title = "帳戶資訊",
                        uri = "admin",
                        link = "",
                        parent_id = "system",
                        icon = "icon-group",
                        sort = 2,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },

            #region 帳戶資訊-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[32],
                        lang = "zh-TW",
                        title = "後臺帳戶管理",
                        model = "IdentityUser",
                        uri = "asp_net_users",
                        link = "",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        active = true,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
                     new SystemMenu()
                     {
                         id = guid[33],
                         lang = "zh-TW",
                         title = "群組角色管理",
                         model = "IdentityRole",
                         uri = "asp_net_roles",
                         link = "",
                         parent_id = parent_id,
                         icon = "",
                         sort = 2,
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now
                     },


            #endregion ---------------------------------------------------------------------

                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[12]),
                        lang = "zh-TW",
                        title = "資訊安全",
                        uri = "shield",
                        link = "",
                        parent_id = "system",
                        icon = "icon-shield",
                        sort = 3,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
            #region 資訊安全-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[34],
                        lang = "zh-TW",
                        title = "防火牆",
                        model = "Firewall",
                        uri = "firewall",
                        link = "",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
                        new SystemMenu()
                        {
                            id = guid[35],
                            lang = "zh-TW",
                            title = "登入紀錄",
                            model = "SystemLog",
                            uri = "login",
                            link = "/Siteadmin/SystemLog/List/login",
                            parent_id = parent_id,
                            icon = "",
                            sort = 2,
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "Y" }, { "edit", "N" }, { "list", "Y" }, { "edit_list", "N" } }),
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now
                        },
                        new SystemMenu()
                        {
                            id = guid[36],
                            lang = "zh-TW",
                            title = "操作紀錄",
                            model = "SystemLog",
                            uri = "modify",
                            link = "/Siteadmin/SystemLog/List/modify",
                            parent_id = parent_id,
                            icon = "",
                            sort = 3,
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "N" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "N" } }),
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now
                        },


            #endregion ---------------------------------------------------------------------



                    //第二層

                    new SystemMenu()
                    {
                        id = (parent_id = topGuid[13]),
                        lang = "zh-TW",
                        title = "系統設定",
                        uri = "sysadmin",
                        link = "",
                        parent_id = "system",
                        icon = "icon-shield",
                        sort = 50,
                        active = false,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    },
            #region 系統設定-細項---------------------------------------------------------------------
                    new SystemMenu()
                    {
                        id = guid[37],
                        lang = "zh-TW",
                        title = "欄位設定",
                        model = "Columns",
                        uri = "columns",
                        link = "",
                        parent_id = parent_id,
                        icon = "",
                        sort = 1,
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "add", "Y" }, { "del", "Y" }, { "list", "Y" }, { "edit_list", "Y" } }),
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    }


                    #endregion ---------------------------------------------------------------------



                );




            List<Dictionary<string, string>> zhTwTitle = new List<Dictionary<string, string>>
            {
               new Dictionary<string, string>{ {"model_id" , topGuid[0] } , { "title" , "後臺首頁" } },
               new Dictionary<string, string>{ {"model_id" , topGuid[1] } , { "title" , "網站首頁" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[2] } , { "title" , "內容管理" } },
               new Dictionary<string, string>{ {"model_id" , guid[0] } , { "title" , "新聞稿" } },
               new Dictionary<string, string>{ {"model_id" , guid[1] } , { "title" , "靜態頁面" } },
               new Dictionary<string, string>{ {"model_id" , guid[2] } , { "title" , "項目管理" } },
               new Dictionary<string, string>{ {"model_id" , guid[3] } , { "title" , "檔案下載" } },
               new Dictionary<string, string>{ {"model_id" , guid[26] } , { "title" , "營業據點" } },
                      new Dictionary<string, string>{ {"model_id" , guid[34] } , { "title" , "影音管理" } },
               new Dictionary<string, string>{ {"model_id" , guid[4] } , { "title" , "內容類別管理" } },


              new Dictionary<string, string>{ {"model_id" , topGuid[14] } , { "title" , "財務資訊" } },
              new Dictionary<string, string>{ {"model_id" , guid[29] } , { "title" , "月銷售額" } },
              new Dictionary<string, string>{ {"model_id" , guid[30] } , { "title" , "季報表" } },
              new Dictionary<string, string>{ {"model_id" , guid[31] } , { "title" , "美國證券交易委員會文件" } },
                 new Dictionary<string, string>{ {"model_id" , guid[33] } , { "title" , "合併財務報表" } },
                   new Dictionary<string, string>{ {"model_id" , guid[32] } , { "title" , "財務報告" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[3] } , { "title" , "產品管理" } },
               new Dictionary<string, string>{ {"model_id" , guid[5] } , { "title" , "IC Packaging" } },
               new Dictionary<string, string>{ {"model_id" , guid[6] } , { "title" , "IC Packaging Product" } },

                   new Dictionary<string, string>{ {"model_id" , guid[35] } , { "title" , "Services" } },
                   new Dictionary<string, string>{ {"model_id" , guid[36] } , { "title" , "Technology" } },

                   new Dictionary<string, string>{ {"model_id" , guid[37] } , { "title" , "Services Item" } },

             // new Dictionary<string, string>{ {"model_id" , guid[24] } , { "title" , "篩選類型" } },
           //   new Dictionary<string, string>{ {"model_id" , guid[25] } , { "title" , "篩選條件" } },


               new Dictionary<string, string>{ {"model_id" , topGuid[4] } , { "title" , "廣告模組" } },
               new Dictionary<string, string>{ {"model_id" , guid[8] } , { "title" , "廣告分類" } },
               new Dictionary<string, string>{ {"model_id" , guid[7] } , { "title" , "廣告內容" } },
             
            //   new Dictionary<string, string>{ {"model_id" , guid[9] } , { "title" , "頁底區塊" } },
             //  new Dictionary<string, string>{ {"model_id" , guid[21] } , { "title" , "各頁Banner" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[5] } , { "title" , "會員中心" } },
               new Dictionary<string, string>{ {"model_id" , guid[10] } , { "title" , "會員資料管理" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[6] } , { "title" , "客服中心" } },
               new Dictionary<string, string>{ {"model_id" , guid[11] } , { "title" , "收件匣" } },
               new Dictionary<string, string>{ {"model_id" , guid[12] } , { "title" , "事件通知信件" } },
               new Dictionary<string, string>{ {"model_id" , guid[13] } , { "title" , "信件主旨" } },


               new Dictionary<string, string>{ {"model_id" , topGuid[7] } , { "title" , "控制台" } },
               new Dictionary<string, string>{ {"model_id" , guid[14] } , { "title" , "網站基本資訊" } },
               new Dictionary<string, string>{ {"model_id" , guid[15] } , { "title" , "電子郵件伺服器" } },

               new Dictionary<string, string>{ {"model_id" , guid[27] } , { "title" , "網站參數群組" } },
               new Dictionary<string, string>{ {"model_id" , guid[28] } , { "title" , "網站參數資訊" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[8] } , { "title" , "帳戶資訊" } },
               new Dictionary<string, string>{ {"model_id" , guid[16] } , { "title" , "後臺帳戶管理" } },
               new Dictionary<string, string>{ {"model_id" , guid[17] } , { "title" , "群組角色管理" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[9] } , { "title" , "資訊安全" } },
               new Dictionary<string, string>{ {"model_id" , guid[18] } , { "title" , "防火牆" } },
               new Dictionary<string, string>{ {"model_id" , guid[19] } , { "title" , "登入紀錄" } },
               new Dictionary<string, string>{ {"model_id" , guid[20] } , { "title" , "操作紀錄" } },


               new Dictionary<string, string>{ {"model_id" , topGuid[10] } , { "title" , "系統設定" } },
               new Dictionary<string, string>{ {"model_id" , guid[22] } , { "title" , "欄位設定" } },

            };

            List<Dictionary<string, string>> enTitle = new List<Dictionary<string, string>>
            {
               new Dictionary<string, string>{ {"model_id" , topGuid[0] } , { "title" , "Admin" } },
               new Dictionary<string, string>{ {"model_id" , topGuid[1] } , { "title" , "Home" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[2] } , { "title" , "Content" } },
               new Dictionary<string, string>{ {"model_id" , guid[0] } , { "title" , "News" } },
               new Dictionary<string, string>{ {"model_id" , guid[1] } , { "title" , "Article Page" } },
               new Dictionary<string, string>{ {"model_id" , guid[2] } , { "title" , "Columns" } },
               new Dictionary<string, string>{ {"model_id" , guid[3] } , { "title" , "Download" } },
               new Dictionary<string, string>{ {"model_id" , guid[26] } , { "title" , "Location" } },
               new Dictionary<string, string>{ {"model_id" , guid[34] } , { "title" , "Video" } },
               new Dictionary<string, string>{ {"model_id" , guid[4] } , { "title" , "Article Categories" } },


                   new Dictionary<string, string>{ {"model_id" , topGuid[14] } , { "title" , "財務資訊" } },
                  new Dictionary<string, string>{ {"model_id" , guid[29] } , { "title" , "月銷售額" } },
                      new Dictionary<string, string>{ {"model_id" , guid[30] } , { "title" , "季報表" } },
              new Dictionary<string, string>{ {"model_id" , guid[31] } , { "title" , "美國證券交易委員會文件" } },
               new Dictionary<string, string>{ {"model_id" , guid[33] } , { "title" , "合併財務報表" } },
              new Dictionary<string, string>{ {"model_id" , guid[32] } , { "title" , "財務報告" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[3] } , { "title" , "Product" } },
               new Dictionary<string, string>{ {"model_id" , guid[5] } , { "title" , "IC Packaging" } },
               new Dictionary<string, string>{ {"model_id" , guid[6] } , { "title" , "IC Packaging Product" } },

                new Dictionary<string, string>{ {"model_id" , guid[35] } , { "title" , "Services" } },
                   new Dictionary<string, string>{ {"model_id" , guid[36] } , { "title" , "Technology" } },

                   new Dictionary<string, string>{ {"model_id" , guid[37] } , { "title" , "Services Item" } },
              //new Dictionary<string, string>{ {"model_id" , guid[24] } , { "title" , "篩選類型" } },
              //new Dictionary<string, string>{ {"model_id" , guid[25] } , { "title" , "篩選條件" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[4] } , { "title" , "Advertise" } },
               new Dictionary<string, string>{ {"model_id" , guid[8] } , { "title" , "Advertise Categories" } },
               new Dictionary<string, string>{ {"model_id" , guid[7] } , { "title" , "Advertise" } },

             
               //new Dictionary<string, string>{ {"model_id" , guid[9] } , { "title" , "頁底區塊" } },
              // new Dictionary<string, string>{ {"model_id" , guid[21] } , { "title" , "Page Banner" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[5] } , { "title" , "Member" } },
               new Dictionary<string, string>{ {"model_id" , guid[10] } , { "title" , "Members" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[6] } , { "title" , "Service" } },
               new Dictionary<string, string>{ {"model_id" , guid[11] } , { "title" , "Inbox" } },
               new Dictionary<string, string>{ {"model_id" , guid[12] } , { "title" , "Event Notification" } },
               new Dictionary<string, string>{ {"model_id" , guid[13] } , { "title" , "Letter Subject" } },


               new Dictionary<string, string>{ {"model_id" , topGuid[7] } , { "title" , "Console" } },
               new Dictionary<string, string>{ {"model_id" , guid[14] } , { "title" , "Website Information" } },
               new Dictionary<string, string>{ {"model_id" , guid[15] } , { "title" , "SMTP" } },
               new Dictionary<string, string>{ {"model_id" , guid[27] } , { "title" , "網站參數群組" } },
               new Dictionary<string, string>{ {"model_id" , guid[28] } , { "title" , "網站參數資訊" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[8] } , { "title" , "Administrator" } },
               new Dictionary<string, string>{ {"model_id" , guid[16] } , { "title" , "Accounts" } },
               new Dictionary<string, string>{ {"model_id" , guid[17] } , { "title" , "Roles" } },

               new Dictionary<string, string>{ {"model_id" , topGuid[9] } , { "title" , "Security" } },
               new Dictionary<string, string>{ {"model_id" , guid[18] } , { "title" , "Firewall" } },
               new Dictionary<string, string>{ {"model_id" , guid[19] } , { "title" , "Login History" } },
               new Dictionary<string, string>{ {"model_id" , guid[20] } , { "title" , "Operation Record" } },


               new Dictionary<string, string>{ {"model_id" , topGuid[10] } , { "title" , "Program" } },
               new Dictionary<string, string>{ {"model_id" , guid[22] } , { "title" , "Field Settings" } },
            };


            List<LanguageResource> resources = new List<LanguageResource>();
            /*
            for (int i = 0; i < zhTwTitle.Count; i++)
            {
                Dictionary<string, string> zhTw = zhTwTitle[i];
                Dictionary<string, string> en = enTitle[i];

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    SystemMenuId = zhTw["model_id"],
                    model_type = "SystemMenu",
                    language_id = "zh-TW",
                    title = zhTw["title"],
                    active = true,
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { }),
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    //start_at = DateTime.Now,
                    tags = "",
                    top = true,
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { }),
                    pic = JsonConvert.SerializeObject(new Dictionary<string, object>() { }),
                    seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { })
                });

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    SystemMenuId = en["model_id"],
                    model_type = "SystemMenu",
                    language_id = "en",
                    title = en["title"],
                    active = true,
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { }),
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    //start_at = DateTime.Now,
                    tags = "",
                    top = true,
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { }),
                    pic = JsonConvert.SerializeObject(new Dictionary<string, object>() { }),
                    seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { })
                });
            }
            */
            return resources;

            //builder.Entity<LanguageResource>().HasData(resources);
        }



    }
}
