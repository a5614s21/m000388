﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class ArticleNewsSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
           return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {

            // string category_id = Guid.NewGuid().ToString();

            List<string> category_ids = new List<string>()
            {
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
            };

            List<string> column_ids = new List<string>()
            {
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
            };

            List<string> download_ids = new List<string>()
            {
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
                  (new Helper()).GenerateIntID(),
            };




            builder.Entity<ArticleCategory>().HasData(

              /*  new ArticleCategory()
                  {
                      id = "about",
                      uri = "about",
                      parent_id = "",
                      sort = 1,
                      active = true,

                 },*/
                new ArticleCategory()
                 {
                     id = "news",

                     uri = "news",
                     parent_id = "",
                     sort = 2,
                     active = true,

                 },
                

                   //NEWs
                new ArticleCategory()
                   {
                       id = "Announcement",

                       uri = "news",
                       parent_id = "news",
                       sort = 1,
                       active = true,

                   },
                new ArticleCategory()
                   {
                       id = "AnnualReport",

                       uri = "news",
                       parent_id = "news",
                       sort = 2,
                       active = true,

                   },
                new ArticleCategory()
                    {
                        id = "FinancialReport",

                        uri = "news",
                        parent_id = "news",
                        sort = 3,
                        active = true,

                    },
                new ArticleCategory()
                     {
                         id = "NewProducts",

                         uri = "news",
                         parent_id = "news",
                         sort = 4,
                         active = true,

                     },

                new ArticleCategory()
                 {
                     id = "history",
                    
                     uri = "column",
                     parent_id = "",
                     sort = 3,
                     active = true,
                    
                },
                  

                //Locations and Business Contact
                new ArticleCategory()
                {
                    id = "aboutLocations",

                    uri = "aboutLocations",
                    parent_id = "",
                    sort = 4,
                    active = true,

                },

                   new ArticleCategory()
                   {
                       id = "location",

                       uri = "location",
                       parent_id = "aboutLocations",
                       sort = 4,
                       active = true,

                   },


                new ArticleCategory()
                     {
                         id = "location1",

                         uri = "location",
                         parent_id = "location",
                         sort = 1,
                         active = true,

                     },
                new ArticleCategory()
                     {
                         id = "location2",

                         uri = "location",
                         parent_id = "location",
                         sort = 2,
                         active = true,

                     },
                new ArticleCategory()
                     {
                         id = "location3",

                         uri = "location",
                         parent_id = "location",
                         sort = 3,
                         active = true,

                     },
                new ArticleCategory()
                      {
                          id = "location4",

                          uri = "location",
                          parent_id = "location",
                          sort = 4,
                          active = true,

                      },


                   new ArticleCategory()
                   {
                       id = "video",

                       uri = "video",
                       parent_id = "",
                       sort = 5,
                       active = true,

                   },

                   new ArticleCategory()
                   {
                       id = "certification",

                       uri = "certification",
                       parent_id = "",
                       sort = 5,
                       active = true,

                   },
                   new ArticleCategory()
                   {
                       id = "certification1",

                       uri = "certification",
                       parent_id = "certification",
                       sort = 1,
                       active = true,

                   },
                    new ArticleCategory()
                    {
                        id = "certification2",

                        uri = "certification",
                        parent_id = "certification",
                        sort = 2,
                        active = true,

                    },
                     new ArticleCategory()
                     {
                         id = "awards",

                         uri = "awards",
                         parent_id = "",
                         sort = 4,
                         active = true,

                     },
                      new ArticleCategory()
                      {
                          id = "experience",

                          uri = "experience",
                          parent_id = "",
                          sort = 5,
                          active = true,

                      }





               );



            List<LanguageResource> resources = new List<LanguageResource>()
            {

              /*new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 ArticleCategoryId = "about",
                 model_type = "ArticleCategory",
                 language_id = "zh-TW",
                 title = "關於我們",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 //start_at = DateTime.Now,
                 seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "關於我們" }, { "description", "關於我們" } })
             },*/
            new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "news",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "最新消息",
                  active = true,
                  details = "",
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "最新消息" }, { "description", "最新消息" } })
              },
            new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "news",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "News",
                  active = true,
                  details = "",
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "News" }, { "description", "News" } })
              },
             new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "news",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "最新消息",
                  active = true,
                  details = "",
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "ニュース" }, { "description", "ニュース" } })
              },



              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "Announcement",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "公告",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#0094AB" }}),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "公告" }, { "description", "公告" } })
              },
               new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "Announcement",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "Announcement",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#0094AB" }}),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Announcement" }, { "description", "Announcement" } })
              },
                 new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "Announcement",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "Announcement",
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#0094AB" }}),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Announcement" }, { "description", "Announcement" } })
              },


               new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "AnnualReport",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "年度報告",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#005EAB" } }),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "年度報告" }, { "description", "年度報告" } })
              },

                new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "AnnualReport",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "Annual Report",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#005EAB" } }),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Annual Report" }, { "description", "Annual Report" } })
              },
                 new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "AnnualReport",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "Annual Report",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#005EAB" } }),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Annual Report" }, { "description", "Annual Report" } })
              },


              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "FinancialReport",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "財務報告",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#005EAB" } }),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "財務報告" }, { "description", "財務報告" } })
              },

              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "FinancialReport",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "Financial Report",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#005EAB" } }),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Financial Report" }, { "description", "Financial Report" } })
              },
              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "FinancialReport",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "Financial Report",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#005EAB" } }),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Financial Report" }, { "description", "Financial Report" } })
              },

              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "NewProducts",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "最新產品",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#0094AB" }}),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "最新產品" }, { "description", "最新產品" } })
              },
              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "NewProducts",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "New Products",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#0094AB" }}),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "New Products" }, { "description", "New Products" } })
              },
              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "NewProducts",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "New Products",
                  active = true,
                  details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "color", "#0094AB" }}),
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "New Products" }, { "description", "New Products" } })
              },


             //history
             new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "history",
                       model_type = "ArticleCategory",
                       language_id = "zh-TW",
                       title = "歷史沿革",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "歷史沿革" }, { "description", "歷史沿革" } })
                   },
    
     
             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 ArticleCategoryId = "history",
                 model_type = "ArticleCategory",
                 language_id = "en",
                 title = "History",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 //start_at = DateTime.Now,
                 seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "歷史沿革" }, { "description", "歷史沿革" } })
             },
              new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 ArticleCategoryId = "history",
                 model_type = "ArticleCategory",
                 language_id = "zh-CN",
                 title = "History",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 //start_at = DateTime.Now,
                 seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "歷史沿革" }, { "description", "歷史沿革" } })
             },



                  new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "aboutLocations",
                               model_type = "ArticleCategory",
                               language_id = "zh-TW",
                               title = "Location",
                               active = true,
                            details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Locations/Locations.html") } }),
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Location" }, { "description", "Location" } })
                           },
                       new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "aboutLocations",
                               model_type = "ArticleCategory",
                               language_id = "en",
                               title = "Location",
                               active = true,
                              details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Locations/Locations.html") } }),
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                                seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Location" }, { "description", "Location" } })
                           },
       new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "aboutLocations",
                               model_type = "ArticleCategory",
                               language_id = "zh-CN",
                               title = "Location",
                               active = true,
                               details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Locations/Locations.html") } }),
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                                seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Location" }, { "description", "Location" } })
                           },




                new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location",
                               model_type = "ArticleCategory",
                               language_id = "zh-TW",
                               title = "地點和業務聯繫方式",
                               active = true,
                              details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Locations/Locations.html") } }),
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "地點和業務聯繫方式" }, { "description", "地點和業務聯繫方式" } })
                           },
                       new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location",
                               model_type = "ArticleCategory",
                               language_id = "en",
                               title = "Locations and Business Contact",
                               active = true,
                             details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Locations/Locations.html") } }),
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Locations and Business Contact" }, { "description", "Locations and Business Contact" } })
                           },
       new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location",
                               model_type = "ArticleCategory",
                               language_id = "zh-CN",
                               title = "Locations and Business Contact",
                               active = true,
                              details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Locations/Locations.html") } }),
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Locations and Business Contact" }, { "description", "Locations and Business Contact" } })
                           },

       //location

       


        new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location1",
                               model_type = "ArticleCategory",
                               language_id = "zh-TW",
                               title = "台灣",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "台灣" }, { "description", "台灣" } })
                           },
          new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location1",
                               model_type = "ArticleCategory",
                               language_id = "en",
                               title = "Taiwan",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Taiwan" }, { "description", "Taiwan" } })
                           },
              new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location1",
                               model_type = "ArticleCategory",
                               language_id = "zh-CN",
                               title = "Taiwan",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Taiwan" }, { "description", "Taiwan" } })
                           },



                new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location2",
                               model_type = "ArticleCategory",
                               language_id = "zh-TW",
                               title = "舉報投訴",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "舉報投訴" }, { "description", "舉報投訴" } })
                           },

                  new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location2",
                               model_type = "ArticleCategory",
                               language_id = "en",
                               title = "To Report Complaints",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "To Report Complaints" }, { "description", "To Report Complaints" } })
                           },
                   new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location2",
                               model_type = "ArticleCategory",
                               language_id = "zh-CN",
                               title = "To Report Complaints",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "To Report Complaints" }, { "description", "To Report Complaints" } })
                           },



                         new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location3",
                               model_type = "ArticleCategory",
                               language_id = "zh-TW",
                               title = "北美",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "中國" }, { "description", "中國" } })
                           },
                           new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location3",
                               model_type = "ArticleCategory",
                               language_id = "en",
                               title = "North America",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "North America" }, { "description", "North America" } })
                           },
                            new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location3",
                               model_type = "ArticleCategory",
                               language_id = "zh-CN",
                               title = "North America",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "North America" }, { "description", "North America" } })
                           },



                               new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location4",
                               model_type = "ArticleCategory",
                               language_id = "zh-TW",
                               title = "中國",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "中國" }, { "description", "中國" } })
                           },

                          new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location4",
                               model_type = "ArticleCategory",
                               language_id = "en",
                               title = "China",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "China" }, { "description", "China" } })
                           },



                           new LanguageResource()
                           {
                               id = (new Helper()).GenerateIntID(),
                               ArticleCategoryId = "location4",
                               model_type = "ArticleCategory",
                               language_id = "zh-CN",
                               title = "China",
                               active = true,
                               details = "",
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               //start_at = DateTime.Now,
                               seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "China" }, { "description", "China" } })
                           },

                            new LanguageResource()
                              {
                                  id = (new Helper()).GenerateIntID(),
                                  ArticleCategoryId = "video",
                                  model_type = "ArticleCategory",
                                  language_id = "zh-TW",
                                  title = "影音訊息",
                                  active = true,
                                  details = "",
                                  created_at = DateTime.Now,
                                  updated_at = DateTime.Now,
                                  //start_at = DateTime.Now,
                                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "影音訊息" }, { "description", "影音訊息" } })
                              },

                             new LanguageResource()
                              {
                                  id = (new Helper()).GenerateIntID(),
                                  ArticleCategoryId = "video",
                                  model_type = "ArticleCategory",
                                  language_id = "en",
                                  title = "Video",
                                  active = true,
                                  details = "",
                                  created_at = DateTime.Now,
                                  updated_at = DateTime.Now,
                                  //start_at = DateTime.Now,
                                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "影音訊息" }, { "description", "影音訊息" } })
                              },
                              new LanguageResource()
                              {
                                  id = (new Helper()).GenerateIntID(),
                                  ArticleCategoryId = "video",
                                  model_type = "ArticleCategory",
                                  language_id = "zh-CN",
                                  title = "影音訊息",
                                  active = true,
                                  details = "",
                                  created_at = DateTime.Now,
                                  updated_at = DateTime.Now,
                                  //start_at = DateTime.Now,
                                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "影音訊息" }, { "description", "影音訊息" } })
                              },


              //Certification
              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "Certification",
                  active = true,                
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification" }, { "description", "Certification" } })
              },
              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "Certification",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification" }, { "description", "Certification" } })
              },
              new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "Certification",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification" }, { "description", "Certification" } })
              },

               new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification1",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "Certification1",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification1" }, { "description", "Certification1" } })
              },
                new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification1",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "Certification1",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification1" }, { "description", "Certification1" } })
              },
                 new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification1",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "Certification1",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification1" }, { "description", "Certification1" } })
              },

               new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification2",
                  model_type = "ArticleCategory",
                  language_id = "zh-TW",
                  title = "Certification2",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification2" }, { "description", "Certification2" } })
              },
               new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification2",
                  model_type = "ArticleCategory",
                  language_id = "en",
                  title = "Certification2",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification2" }, { "description", "Certification2" } })
              },
               new LanguageResource()
              {
                  id = (new Helper()).GenerateIntID(),
                  ArticleCategoryId = "certification2",
                  model_type = "ArticleCategory",
                  language_id = "zh-CN",
                  title = "Certification2",
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  //start_at = DateTime.Now,
                  seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Certification2" }, { "description", "Certification2" } })
              },
                  //awards
             new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "awards",
                       model_type = "ArticleCategory",
                       language_id = "zh-TW",
                       title = "獎項",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "獎項" }, { "description", "獎項" } })
                   },
                new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "awards",
                       model_type = "ArticleCategory",
                       language_id = "en",
                       title = "Awards",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Awards" }, { "description", "Awards" } })
                   },
                 new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "awards",
                       model_type = "ArticleCategory",
                       language_id = "zh-CN",
                       title = "Awards",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "Awards" }, { "description", "Awards" } })
                   },

                  //訓儲人員心得
             new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "experience",
                       model_type = "ArticleCategory",
                       language_id = "zh-TW",
                       title = "訓儲人員心得",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "訓儲人員心得" }, { "description", "訓儲人員心得" } })
                   },

               new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "experience",
                       model_type = "ArticleCategory",
                       language_id = "en",
                       title = "訓儲人員心得",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "訓儲人員心得" }, { "description", "訓儲人員心得" } })
                   },
               new LanguageResource()
                   {
                       id = (new Helper()).GenerateIntID(),
                       ArticleCategoryId = "experience",
                       model_type = "ArticleCategory",
                       language_id = "zh-TW",
                       title = "訓儲人員心得",
                       active = true,
                       details = "",
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       //start_at = DateTime.Now,
                       seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", "訓儲人員心得" }, { "description", "訓儲人員心得" } })
                   },
            };


           
                List<LanguageResource> newsResources = this.news(builder);
                resources.AddRange(newsResources);
        
            List<LanguageResource> locationResources = this.location(builder);
            resources.AddRange(locationResources);

   
            List<LanguageResource> historyResources = this.history(builder);
            resources.AddRange(historyResources);


            List<LanguageResource> awardsResources = this.awards(builder);
            resources.AddRange(awardsResources);

            List<LanguageResource> experienceResources = this.experience(builder);
            resources.AddRange(experienceResources);


            List<LanguageResource> videoResources = this.video(builder);
            resources.AddRange(videoResources);


            List<LanguageResource> certificationResources = this.certification(builder);
            resources.AddRange(certificationResources);


            List<LanguageResource> financeResources = this.finance(builder);
            resources.AddRange(financeResources);



           


            List<LanguageResource> quarterlyResources = this.quarterly(builder);
            resources.AddRange(quarterlyResources);

            List<LanguageResource> filingsResources = this.filings(builder);
            resources.AddRange(filingsResources);



            List<LanguageResource> reportsResources = this.reports(builder);
            resources.AddRange(reportsResources);

          List<LanguageResource> consolidatedResources = this.consolidated(builder);
            resources.AddRange(consolidatedResources);

            List<LanguageResource> annualResources = this.annual(builder);
            resources.AddRange(annualResources);


            List<LanguageResource> downloadResources = this.download(builder);
            resources.AddRange(downloadResources);


            return resources;


        }

        public List<LanguageResource> news(ModelBuilder builder)
        {

            List<string> category_id = new List<string>
            {
                "Announcement",
                "Announcement",
                "AnnualReport",
                "FinancialReport",
                "NewProducts",
            };



            List<string> title = new List<string>
            {
                "SPIL Overview & Fact Sheet",
                "Monthly Sales Report---Mar 2022 has been released",
                "SPIL Overview & Fact Sheet",
                "Monthly Sales Report---Mar 2022 has been released",
                "SPIL Overview & Fact Sheet",
            };

            List<string> editor = new List<string>
            {
                MapPath("Content/Templates/News/NewsDetail.html"),
                MapPath("Content/Templates/News/NewsDetail.html"),
                MapPath("Content/Templates/News/NewsDetail.html"),
                MapPath("Content/Templates/News/NewsDetail.html"),
                MapPath("Content/Templates/News/NewsDetail.html"),
            };



            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/news/news1.jpg\",\"title\":\"2222\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/news/news2.jpg\",\"title\":\"2222\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/news/news1.jpg\",\"title\":\"2222\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/news/news2.jpg\",\"title\":\"2222\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/news/news1.jpg\",\"title\":\"2222\"}]}",
            };


            List<bool> top = new List<bool>
            {
               true,
               true,
               true,
               false,
               true,
            };

            List<ArticleNews> articleNews = new List<ArticleNews>();
            int i = 1;
            foreach (string item in title)
            {
                articleNews.Add(new ArticleNews()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "news",
                    category_id = category_id[(i-1)],
                    sort = i,
                    active = true,
                    top = top[(i - 1)]
                });
                i++;
            }


            builder.Entity<ArticleNews>().HasData(
                articleNews
              );



            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleNews item in articleNews)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleNews",
                    ArticleNewsId = item.id,
                    language_id = "en",
                    category_id = category_id[i],
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() }, { "notes", "Because of rapid movement toward semiconductor package miniaturization, a number of solutions have evolved as well" } }),
                    active = true,
                    top = top[i],
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleNews",
                    ArticleNewsId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id[i],
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() }, { "notes", "Because of rapid movement toward semiconductor package miniaturization, a number of solutions have evolved as well" } }),
                    active = true,
                    top = top[i],
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleNews",
                    ArticleNewsId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id[i],
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } , { "notes" , "Because of rapid movement toward semiconductor package miniaturization, a number of solutions have evolved as well" } }),
                    active = true,
                    top = top[i],
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;

        }



        public List<LanguageResource> video(ModelBuilder builder)
        {


            List<string> title = new List<string>
            {
                "Zhong Ke Facility Profile",
                "SPIL Company Profile (Technology and Services)",
                "SPIL Company Profile (Full)",
                "Zhong Ke Facility Profile",
                "SPIL Company Profile (Technology and Services)",
                "SPIL Company Profile (Full)",
            };

            List<string> youtube = new List<string>
            {
                "https://www.youtube.com/watch?v=kDJwzUwdGz8",
                "https://www.youtube.com/watch?v=kDJwzUwdGz8",
                "https://www.youtube.com/watch?v=kDJwzUwdGz8",
                "https://www.youtube.com/watch?v=kDJwzUwdGz8",
                "https://www.youtube.com/watch?v=kDJwzUwdGz8",
                "https://www.youtube.com/watch?v=kDJwzUwdGz8",
            };



            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/about/video2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/video1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/video3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/video2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/video1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/video3.webp\",\"title\":\"\"}]}",
            };


        

            List<ArticleNews> articleNews = new List<ArticleNews>();
            int i = 1;
            foreach (string item in title)
            {
                articleNews.Add(new ArticleNews()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "video",
                    category_id = "video",
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleNews>().HasData(
                articleNews
              );



            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleNews item in articleNews)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleNews",
                    ArticleNewsId = item.id,
                    language_id = "en",
                    category_id = "video",
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "youtube", youtube[i].ToString() } }),
                    active = true,
                    start_at = DateTime.Now,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleNews",
                    ArticleNewsId = item.id,
                    language_id = "zh-TW",
                    category_id = "video",
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "youtube", youtube[i].ToString() } }),
                    active = true,
                    start_at = DateTime.Now,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleNews",
                    ArticleNewsId = item.id,
                    language_id = "zh-CN",
                    category_id = "video",
                    title = title[i].ToString(),
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "youtube", youtube[i].ToString() }  }),
                    active = true,
                    start_at = DateTime.Now,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;

        }


        public List<LanguageResource> history(ModelBuilder builder)
        {


            List<string> year = new List<string>
            {
                "2008",
                "2008",
                "2007",
                "2004",
                "2004",
                "2003",
                "2003",
                "2002",
                "2001",
                "2000",
                "2000",
                "2000",
                "1999",
                "1999",
                "1997",
                "1995",
                "1995",
                "1995",
                "1993",
                "1993",
                "1993",
                "1989",
                "1984",
                "1984",
            };

            List<string> month = new List<string>
            {
                "8",
                "11",
                "7",
                "2",
                "12",
                "3",
                "8",
                "1",
                "12",
                "6",
                "9",
                "12",
                "1",
                "5",
                "7",
                "10",
                "10",
                "12",
                "1",
                "4",
                "4",
                "8",
                "5",
                "8",
            };


            List<string> title = new List<string>
            {
                "QC 080000 (IECQ HSPM) certification",
                "Introduced CNS15506 Taiwan Occupational Safety and Health Management System and passed certication",
                "Completed and activated Chang Hua facility",
                "Issuance of Euro Convertible Bonds",
                "Introduced OHSAS 18001 Certication system and passed certification",
                "Passed Sony Corporation’s Green Partner qualification",
                "TS16949 certification",
                "Issuance of Euro Convertible Bonds",
                "Found Siliconware Technology (SuZhou) Limited",
                "Issuance of ADRs; NASDAQ: SPIL",
                "Completed and activated Chung Shan facility",
                "Merged with Siliconware Corporate and founded Hsin Chu II facility",
                "QS 9000 certification",
                "Introduced ISO 14001 EMS International Certification system and passed certification",
                "Issuance of Euro Convertible Bond",
                "Issuance GDRs",
                "Completed construction of Da Fong facility in Taichung",
                "SPIL Headquarters moved to Da Fong facility",
                "ISO 9001 certification",
                "Stock began trading on Taiwan Stock Exchange: TAIEX: 2325",
                "Completed and activated Hsin Chu I facility",
                "SPIL became a public company",
                "Siliconware Precision Industries Co., Ltd (SPIL) founded",
                "Began operations in its first factory",
            };

          


            List<ArticleColumn> articleColumn = new List<ArticleColumn>();
            int i = 1;
            foreach (string item in title)
            {
                articleColumn.Add(new ArticleColumn()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "history",
                    category_id = "history",
                    sort = i,
                    year = year[( i - 1)],
                    month = month[(i - 1)],
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleColumn>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleColumn item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "en",
                    category_id = "history",
                    title = title[i].ToString(),
                    year = item.year,
                    month = item.month,
                    //details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "zh-TW",
                    category_id = "history",
                    title = title[i].ToString(),
                    year = item.year,
                    month = item.month,
                    //details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "zh-CN",
                    category_id = "history",
                    year = item.year,
                    month = item.month,
                    title = title[i].ToString(),
                    //details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }

        public List<LanguageResource> awards(ModelBuilder builder)
        {


            List<string> title = new List<string>
            {
                "National Awards",
                "City Government Awards",
                "Executive Yuan Awards",
                "Executive Yuan Awards",
              
            };

            List<string> editor = new List<string>
            {
                MapPath("Content/Templates/Awards/1.html"),
                MapPath("Content/Templates/Awards/2.html"),
                MapPath("Content/Templates/Awards/3.html"),
                MapPath("Content/Templates/Awards/4.html"),

            };


            List<ArticleColumn> articleColumn = new List<ArticleColumn>();
            int i = 1;
            foreach (string item in title)
            {
                articleColumn.Add(new ArticleColumn()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "awards",
                    category_id = "awards",
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleColumn>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleColumn item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "en",
                    category_id = "awards",
                    title = title[i].ToString(),
                    year = item.year,
                    month = item.month,
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "zh-TW",
                    category_id = "awards",
                    title = title[i].ToString(),
                    year = item.year,
                    month = item.month,
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "zh-CN",
                    category_id = "awards",
                    year = item.year,
                    month = item.month,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }

        public List<LanguageResource> experience(ModelBuilder builder)
        {


            List<string> title = new List<string>
            {
                "Sean",
                "Frank",
                "James",
                "Hsiang",
                "Chia Te",
                "Kai",

            };

            List<string> editor = new List<string>
            {
                MapPath("Content/Templates/Experience/1.html"),
                MapPath("Content/Templates/Experience/2.html"),
                MapPath("Content/Templates/Experience/3.html"),
                MapPath("Content/Templates/Experience/4.html"),
                MapPath("Content/Templates/Experience/5.html"),
                MapPath("Content/Templates/Experience/6.html"),

            };

            List<string> sub_title = new List<string>
            {
                "交通大學物理研究所 / 研發中心 - 製程工程單位",
                "彰化師範大學統計資訊研究所 / 事業三處 - 製造系統單位",
                "成功大學電腦與通訊研究所 / 智慧製造處 - 智能化管理單位",
                "中正大學電機工程研究所 / 事業六處 - 技術開發單位",
                "台灣大學物理系應用物理研究所 / 事業三處 - 技術開發單位",
                "中正大學物理所 / 事業三處 - BUMPING製程單位",

            };

            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/career/substitute-3-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/career/substitute-3-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/career/substitute-3-3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/career/substitute-3-4.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/career/substitute-3-5.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/career/substitute-3-6.webp\",\"title\":\"\"}]}",
            };



            List<string> year = new List<string>
            {
                "2018",
                "2018",
                "2019",
                "2019",
                "2019",
                "2020",

            };

            List<ArticleColumn> articleColumn = new List<ArticleColumn>();
            int i = 1;
            foreach (string item in title)
            {
                articleColumn.Add(new ArticleColumn()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "experience",
                    category_id = "experience",
                    year = year[(i-1)],
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleColumn>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleColumn item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "en",
                    category_id = "experience",
                    title = title[i].ToString(),
                    year = item.year,
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() }, { "sub_title", sub_title[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "zh-TW",
                    category_id = "experience",
                    title = title[i].ToString(),
                    year = item.year,
                    pic = pic[i],
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() }, { "sub_title", sub_title[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleColumn",
                    ArticleColumnId = item.id,
                    language_id = "zh-CN",
                    category_id = "experience",
                    year = item.year,
                    pic = pic[i],
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() }, { "sub_title", sub_title[i].ToString() } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }



        public List<LanguageResource> location(ModelBuilder builder)
        {


            List<string> category_id = new List<string>
            {
                "location1",
                "location1",
                "location1",
                "location1",
                "location1",
                "location1",
                "location1",
                "location1",

                "location2",
                "location3",
                "location4",

            };


            List<string> title = new List<string>
            {
                "Corporate Headquarters & Da Fong Facility",
                "Chung Shan Facility",
                "Zhong Ke Facility",
                "Zhong Gong Facility",
                "Hsinchu Facility (Hsinchu I)",
                "Hsinchu Facility (Hsinchu III)",
                "Hsinchu Facility (Jie Show A)",


                "Changhua Facility",




                "Contact Information",
                "USA",
                "SuZhou",
            };

            List<string> details = new List<string>
            {
                JsonConvert.SerializeObject(new Dictionary<string, object>() {                 
                    { "map", "https://goo.gl/maps/bnDNPPYJPdVcbB366" },
                    { "tel" , "886-4-2534-1525" },
                    { "fax" , "886-4-2534-2025" },
                    { "email" , "info@spil.com.tw" },
                    { "editor",MapPath("Content/Templates/Locations/1.html") },
                }),
                 JsonConvert.SerializeObject(new Dictionary<string, object>() {
                   { "map", "https://goo.gl/maps/irHEoW5oD3kz5trA8" },
                     { "tel" , "886-4-2534-1525" },
                    { "fax" , "886-4-2534-2025" },
                    { "email" , "" },
                     { "editor",MapPath("Content/Templates/Locations/2.html") },
                }),
                  JsonConvert.SerializeObject(new Dictionary<string, object>() {
                   { "map", "https://goo.gl/maps/HxK3ZY8ENEPfusqj6" },
                    { "tel" , "886-4-2534-1525" },
                    { "fax" , "" },
                    { "email" , "" },
                     { "editor",MapPath("Content/Templates/Locations/3.html") },
                }),


                JsonConvert.SerializeObject(new Dictionary<string, object>() {
                { "map", "https://goo.gl/maps/QneKAQMpVEKco4SF8" },

                 { "tel" , "886-4-2354-2068" },
                    { "fax" , "886-4-2350-1846" },
                    { "email" , "" },
                  { "editor",MapPath("Content/Templates/Locations/4.html") },
                }),

                  JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "https://goo.gl/maps/qmYY626dgCeCQ5ns8" },
                     { "tel" , "886-3-577-3151" },
                    { "fax" , "Fax: 886-3-578-7039" },
                    { "email" , "" },
                      { "editor",MapPath("Content/Templates/Locations/5.html") },
                }),


                   JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "https://goo.gl/maps/kLV8hgd3mYrdEkRW6" },
                     { "tel" , "886-3-577-3151" },
                    { "fax" , "" },
                    { "email" , "" },
                      { "editor",MapPath("Content/Templates/Locations/6.html") },
                }),
                    JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "https://goo.gl/maps/XkUfA9CtGrjGzV967" },
                     { "tel" , "886-3-577-6621" },
                    { "fax" , "886-3-563-2352" },
                    { "email" , "" },
                      { "editor",MapPath("Content/Templates/Locations/7.html") },
                })  ,
                     JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "https://goo.gl/maps/KzDeAB3d6sQPjYkZ9" },
                      { "tel" , "886-4-721-8888" },
                    { "fax" , "886-4-736-8831" },
                    { "email" , "" },
                      { "editor",MapPath("Content/Templates/Locations/8.html") },
                }),
                      JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "" },
                       { "tel" , "886-4-2534-1525 x 1620" },
                    { "fax" , "886-4-736-8831" },
                    { "email" , "petition@spil.com.tw" },
                      { "editor",MapPath("Content/Templates/Locations/9.html") },
                }),
                        JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "" },
                      { "tel" , "1-408-573-5500" },
                    { "fax" , "1-408-573-5530" },
                    { "email" , "petition@spil.com.tw" },
                      { "editor",MapPath("Content/Templates/Locations/10.html") },
                }),
                          JsonConvert.SerializeObject(new Dictionary<string, object>() {
                    { "map", "" },
                      { "tel" , "86-0512-62535288" },
                    { "fax" , "1-408-573-5530" },
                    { "email" , " york@spil.com.cn" },
                      { "editor",MapPath("Content/Templates/Locations/11.html") },
                })


            };



            List<ArticleLocation> articleColumn = new List<ArticleLocation>();
            int i = 0;
            foreach (string item in title)
            {
                articleColumn.Add(new ArticleLocation()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "location",
                    category_id = category_id[i],
                    sort = (i+1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleLocation>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleLocation item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleLocation",
                    ArticleLocationId = item.id,
                    language_id = "en",
                    category_id = category_id[i],
                    title = title[i].ToString(),
                    details = details[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleLocation",
                    ArticleLocationId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id[i],
                    title = title[i].ToString(),
                    details = details[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleLocation",
                    ArticleLocationId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id[i],
                    title = title[i].ToString(),
                    details = details[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }

       public List<LanguageResource> certification(ModelBuilder builder)
        {


            List<string> category_id = new List<string>()
            {
                "certification1",
                "certification2",
            };



            List<string> title = new List<string>()
            {
                "ISO9001 Quality Management System",
                "ISO 14001 - Environmental Management System Certification",
                "Award for International Trade Outstanding Export/Import Business Certificate" ,
                "Award for International Trade Outstanding Export/Import Business Certificate" ,
                "ISO9001 Quality Management System",
            };


            List<string> subTitle = new List<string>()
            {
                "ISO 9001：2015",
                "ISO 9001：2015",
                "" ,
                "" ,
                "ISO 9001：2015",
            };

            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/about/certification1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/certification2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/certification3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/certification4.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/about/certification5.webp\",\"title\":\"\"}]}",
            };


            string editor = @"<p>Issuing Organization：SGS</p>
                                <p> Validity Period：2019 / 06 / 03~2022 / 06 / 03 </p>
                                <p> Certification Date：2010 / 06 / 03 </p>
                                <p> Certification Number：TW10 / 00270 </p> ";



            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {


                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "certification",
                    category_id = "certification1",                 
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );






            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {

               

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "certification1",
                    pic = pic[i],
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/about/Monthly revenue Jan 2022 CHT.pdf\"}]}",
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", subTitle[i].ToString() } , { "editor" , editor }  }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "certification1",
                    pic = pic[i],
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/about/Monthly revenue Jan 2022 CHT.pdf\"}]}",
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", subTitle[i].ToString() }, { "editor", editor } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "certification1",
                    title = title[i].ToString(),
                    pic = pic[i],
                    files = "{\"file\":[{\"path\":\"/styles/images/about/Monthly revenue Jan 2022 CHT.pdf\"}]}",
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", subTitle[i].ToString() }, { "editor", editor } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }




            List<ArticleDownload> articleColumn2 = new List<ArticleDownload>();
            i = 0;
            foreach (string item in title)
            {


                articleColumn2.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "certification",
                    category_id = "certification2",
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleDownload>().HasData(
                articleColumn2
              );
                       

            i = 0;
            foreach (ArticleDownload item in articleColumn2)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "certification2",
                    pic = pic[i],
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/about/Monthly revenue Jan 2022 CHT.pdf\"}]}",
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", subTitle[i].ToString() }, { "editor", editor } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "certification2",
                    pic = pic[i],
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/about/Monthly revenue Jan 2022 CHT.pdf\"}]}",
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", subTitle[i].ToString() }, { "editor", editor } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "certification2",
                    title = title[i].ToString(),
                    pic = pic[i],
                    files = "{\"file\":[{\"path\":\"/styles/images/about/Monthly revenue Jan 2022 CHT.pdf\"}]}",
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", subTitle[i].ToString() }, { "editor", editor } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }


            return resources;






        }


        public List<LanguageResource> finance(ModelBuilder builder)
        {


            List<string> title = new List<string>();

            for (int y = int.Parse(DateTime.Now.ToString("yyyy")); y >= 2012; y--)
            {
                if(y != 2017)
                {
                    int nowMonth = 12;
                    if (y == 2022)
                    {
                        nowMonth = 6;
                    }

                    for (int m = 1; m <= nowMonth; m++)
                    {
                        title.Add(y.ToString() + "-" + m.ToString() + "-Monthly Sales");
                    }

                }
              
            }





          List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {

                var y = item.Split('-').ToList();

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "finance",
                    category_id = "finance",
                    year = y[0],
                    month = y[1],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {

               

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id =  "",
                    year = item.year,
                    month = item.month,
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }

        public List<LanguageResource> quarterly(ModelBuilder builder)
        {


            List<string> title = new List<string>();

            for (int y = int.Parse(DateTime.Now.ToString("yyyy")); y >= 2009; y--)
            {
                if (y != 2017 && y != 2012)
                {
                    int quarterly = 4;
                    if (y == 2022)
                    {
                        quarterly = 1;
                    }

                    for (int m = 1; m <= quarterly; m++)
                    {
                        title.Add(y.ToString() + "-" + m.ToString() + "-Quarterly Results");
                    }

                }

            }





            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {

                var y = item.Split('-').ToList();

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "quarterly",
                    category_id = "quarterly",
                    year = y[0],
                    month = y[1],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            string files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\",\"title\":\"Presentation Material\"},{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\",\"title\":\"Earnings Release News\"},{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\",\"title\":\"Financial Statement\"},{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\",\"title\":\"Investor Conference Call Dial-in information\"}]}";


            List<VideoModel> lstStuModel = new List<VideoModel>()
            {
                new VideoModel(){path="https://www.youtube.com/embed/VYZLOmVXVQM",title="Investor Conference Call Live Webcast – in Mandarin",sub_title="2:30 PM on Apr. 29, 2015 Taiwan Time"},
                new VideoModel(){path="https://www.youtube.com/embed/0mh6HFmiGeQ",title="Investor Conference Call Live Webcast – in English",sub_title="8:00 PM on Apr. 29, 2015 Taiwan Time"},
            };


            string video = JsonConvert.SerializeObject(lstStuModel);



            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "",
                    year = item.year,
                    month = item.month,
                    title = title[i].ToString(),
                    video = video,
                    files = files,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    title = title[i].ToString(),
                    video = video,
                    files = files,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    title = title[i].ToString(),
                    video = video,
                    files = files,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }


        public List<LanguageResource> filings(ModelBuilder builder)
        {


            List<string> title = new List<string>();

            List<string> year = new List<string>();

            for (int y = int.Parse(DateTime.Now.ToString("yyyy")); y >= 2012; y--)
            {
                if (y != 2017)
                {
                    
                    title.Add(y.ToString());
                   // year.Add(y.ToString());

                }

            }





            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "filings",
                    category_id = "filings",
                   // year = year[i],                   
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "",
                   // year = item.year,
                    title = title[i].ToString(),
                   // details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", "20F" } }),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}],\"file2\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = "{\"pic\":[{\"path\":\"/styles/images/news/news1.jpg\",\"title\":\"\"}]}",                    
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    //year = item.year,
                    title = title[i].ToString(),
                   // details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", "20F" } }),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}],\"file2\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = "{\"pic\":[{\"path\":\"/styles/images/news/news1.jpg\",\"title\":\"\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    //year = item.year,
                    title = title[i].ToString(),
                    //details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "sub_title", "20F" } }),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}],\"file2\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = "{\"pic\":[{\"path\":\"/styles/images/news/news1.jpg\",\"title\":\"\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }


        public List<LanguageResource> reports(ModelBuilder builder)
        {


            List<string> title = new List<string>();
            List<string> year = new List<string>();
            for (int y = int.Parse(DateTime.Now.ToString("yyyy")); y >= 2013; y--)
            {
                if (y != 2017 && y != 2012)
                {
                    int quarterly = 4;
                    if (y == 2022)
                    {
                        quarterly = 1;
                    }

                    for (int m = 1; m <= quarterly; m++)
                    {
                        title.Add(m.ToString() + "Q");
                        year.Add(y.ToString() + "-" + m.ToString());
                    }

                }

            }





            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {

                var y = year[i].Split('-').ToList();

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "reports",
                    category_id = "reports",
                    year = y[0],
                    month = y[1],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }




            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "",
                    year = item.year,
                    month = item.month,
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }

        public List<LanguageResource> consolidated(ModelBuilder builder)
        {


            List<string> title = new List<string>();
            List<string> year = new List<string>();
            for (int y = int.Parse(DateTime.Now.ToString("yyyy")); y >= 2013; y--)
            {
                if (y != 2017 && y != 2012)
                {
                    int quarterly = 4;
                    if (y == 2022)
                    {
                        quarterly = 1;
                    }

                    for (int m = 1; m <= quarterly; m++)
                    {
                        title.Add(m.ToString() + "Q");
                        year.Add(y.ToString() + "-" + m.ToString());
                    }

                }

            }





            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {

                var y = year[i].Split('-').ToList();

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "consolidated",
                    category_id = "consolidated",
                    year = y[0],
                    month = y[1],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }




            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "",
                    year = item.year,
                    month = item.month,
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }


        public List<LanguageResource> annual(ModelBuilder builder)
        {


            List<string> title = new List<string>();

            List<string> year = new List<string>();

            for (int y = int.Parse(DateTime.Now.ToString("yyyy")); y >= 2012; y--)
            {
                if (y != 2017)
                {

                    title.Add("Annual Report");
                    year.Add(y.ToString());

                }

            }





            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in title)
            {

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "annual",
                    category_id = "annual",
                    year = year[i],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "",
                    year = item.year,
                    title = title[i].ToString(),               
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = "{\"pic\":[{\"path\":\"/styles/images/finance/annual-bg2.jpg\",\"title\":\"\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    year = item.year,
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = "{\"pic\":[{\"path\":\"/styles/images/finance/annual-bg2.jpg\",\"title\":\"\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    year = item.year,
                    title = title[i].ToString(),
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = "{\"pic\":[{\"path\":\"/styles/images/finance/annual-bg2.jpg\",\"title\":\"\"}]}",
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }

        public List<LanguageResource> download(ModelBuilder builder)
        {


            List<string> title = new List<string>()
            {

            };

            List<string> year = new List<string>()
            {
                "2020",
                "2019",
                "2018",
                "2017",
                "2016",
                "2015",
                "2014",
                "2013",
            };



            List<string> pic = new List<string>
            {
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-1.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-2.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-3.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-4.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-5.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-6.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-7.webp\",\"title\":\"\"}]}",
                "{\"pic\":[{\"path\":\"/styles/images/sustainability/download-8.webp\",\"title\":\"\"}]}",
            };


            List<ArticleDownload> articleColumn = new List<ArticleDownload>();
            int i = 0;
            foreach (string item in year)
            {

                articleColumn.Add(new ArticleDownload()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "Sustainability",
                    category_id = "Sustainability",
                    year = year[i],
                    sort = (i + 1),
                    active = true,
                });
                i++;
            }


            builder.Entity<ArticleDownload>().HasData(
                articleColumn
              );

            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticleDownload item in articleColumn)
            {



                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "en",
                    category_id = "",
                    year = item.year,
                    title = "Corporate Social Responsibility Report English",               
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = pic[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-TW",
                    category_id = "",
                    year = item.year,
                    title = "Corporate Social Responsibility Report English",
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = pic[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticleDownload",
                    ArticleDownloadId = item.id,
                    language_id = "zh-CN",
                    category_id = "",
                    year = item.year,
                    title = "Corporate Social Responsibility Report English",
                    files = "{\"file\":[{\"path\":\"/styles/images/downlad/110年中文年報PDF檔Final.pdf\"}]}",
                    pic = pic[i],
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;






        }



        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
