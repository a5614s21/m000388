﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class AdvertisingSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {




            builder.Entity<AdvertisingCategory>().HasData(
                new AdvertisingCategory()
                {
                    id = "IndexBanner",
                    uri = "banner",
                    parent_id = "",
                    sort = 1,
                    active = true,
                },
                  new AdvertisingCategory()
                  {
                      id = "FinancialReport",
                      uri = "FinancialReport",
                      parent_id = "",
                      sort = 3,
                      active = true,
                  },

                   new AdvertisingCategory()
                   {
                       id = "ServiceAd",
                       uri = "ServiceAd",
                       parent_id = "",
                       sort = 5,
                       active = true,
                   },

                      new AdvertisingCategory()
                      {
                          id = "NumEditor",
                          uri = "NumEditor",
                          parent_id = "",
                          sort = 6,
                          active = true,
                      },
                      new AdvertisingCategory()
                      {
                          id = "MainEditor",
                          uri = "MainEditor",
                          parent_id = "",
                          sort = 6,
                          active = true,
                      },
                    new AdvertisingCategory()
                     {
                          id = "MainAd",
                          uri = "MainAd",
                          parent_id = "",
                          sort = 7,
                          active = true,
                      },

                  new AdvertisingCategory()
                  {
                      id = "DownAd",
                      uri = "DownAd",
                      parent_id = "",
                      sort = 10,
                      active = true,
                  }
                
               );



            List<LanguageResource> resources = new List<LanguageResource>()
            {

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "IndexBanner",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-TW",
                 title = "首頁Banner",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
             },
              new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "IndexBanner",
                 model_type = "AdvertisingCategory",
                 language_id = "en",
                 title = "首頁Banner",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
             },
                new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "IndexBanner",
                 model_type = "AdvertisingCategory",
                 language_id = "jp",
                 title = "首頁Banner",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
             },
                //服務
                new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "ServiceAd",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-TW",
                 title = "服務展示區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
                 new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "ServiceAd",
                 model_type = "AdvertisingCategory",
                 language_id = "en",
                 title = "服務展示區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
               new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "ServiceAd",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-CN",
                 title = "服務展示區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },

                 //中間編輯器
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "NumEditor",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-TW",
                 title = "相關數據",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
               new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "NumEditor",
                 model_type = "AdvertisingCategory",
                 language_id = "en",
                 title = "相關數據",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
                new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "NumEditor",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-CN",
                 title = "相關數據",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },


               //中間編輯器
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "MainEditor",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-TW",
                 title = "首頁中心編輯器廣告",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "MainEditor",
                 model_type = "AdvertisingCategory",
                 language_id = "en",
                 title = "首頁中心編輯器廣告",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "MainEditor",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-CN",
                 title = "首頁中心編輯器廣告",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },

              //中間區塊管理
               new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "MainAd",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-TW",
                 title = "首頁中心區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
                new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "MainAd",
                 model_type = "AdvertisingCategory",
                 language_id = "en",
                 title = "首頁中心區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
                 new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "MainAd",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-CN",
                 title = "首頁中心區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },

                //下方區塊
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "DownAd",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-TW",
                 title = "下方區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "DownAd",
                 model_type = "AdvertisingCategory",
                 language_id = "en",
                 title = "下方區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },
              new LanguageResource()
              {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingCategoryId = "DownAd",
                 model_type = "AdvertisingCategory",
                 language_id = "zh-CN",
                 title = "下方區塊",
                 active = true,
                 details = "",
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
              },

                //輪播
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     AdvertisingCategoryId = "FinancialReport",
                     model_type = "AdvertisingCategory",
                     language_id = "zh-TW",
                     title = "FinancialReport",
                     active = true,
                     details = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     AdvertisingCategoryId = "FinancialReport",
                     model_type = "AdvertisingCategory",
                     language_id = "en",
                     title = "FinancialReport",
                     active = true,
                     details = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
                 new LanguageResource()
                 {
                     id = (new Helper()).GenerateIntID(),
                     AdvertisingCategoryId = "FinancialReport",
                     model_type = "AdvertisingCategory",
                     language_id = "zh-CN",
                     title = "FinancialReport",
                     active = true,
                     details = "",
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                 },
            };



            List<LanguageResource> bannerResources = this.banner(builder, "IndexBanner");
            resources.AddRange(bannerResources);

            List<LanguageResource> adResources = this.report(builder, "FinancialReport");
            resources.AddRange(adResources);

             List<LanguageResource> indexInfoResources = this.DownAd(builder, "DownAd");
            resources.AddRange(indexInfoResources);


            List<LanguageResource> ServiceAdResources = this.ServiceAd(builder, "ServiceAd");
            resources.AddRange(ServiceAdResources);

            List<LanguageResource> NumEditorResources = this.NumEditor(builder, "NumEditor");
            resources.AddRange(NumEditorResources);

            List<LanguageResource> MainEditorResources = this.MainEditor(builder, "MainEditor");
            resources.AddRange(MainEditorResources);



            List<LanguageResource> MainAdResources = this.MainAd(builder, "MainAd");
            resources.AddRange(MainAdResources);


            return resources;

        }


        public List<LanguageResource> banner(ModelBuilder builder, string category_id)
        {

            List<string> guids = new List<string>
            {
               (new Helper()).GenerateIntID(),
               (new Helper()).GenerateIntID(),
               (new Helper()).GenerateIntID()
            };


            builder.Entity<Advertising>().HasData(

            new Advertising()
            {
                id = guids[0],
                category_id = category_id,
                sort = 1,
                uri = "Banner",
                active = true,
            },
            new Advertising()
            {
                id = guids[1],
                category_id = category_id,
                sort = 2,
                uri = "Banner",
                active = true,
            },

               new Advertising()
               {
                   id = guids[2],
                   category_id = category_id,
                   sort = 3,
                   uri = "Banner",
                   active = true,
               }


           );


            List<LanguageResource> resources = new List<LanguageResource>() {

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[0],
                 model_type = "Advertising",
                 language_id = "zh-TW",
                 title = "贏得客戶信任，共創高科技未來",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/zh-TW/banner1.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 1,
             },

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[0],
                 model_type = "Advertising",
                 language_id = "en",
                 title = "Win Customers' Confidence and Create the High-Tech Future",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/en/banner1.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 1,
             },
             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[0],
                 model_type = "Advertising",
                 language_id = "zh-CN",
                 title = "Win Customers' Confidence and Create the High-Tech Future",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/en/banner1.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 1,
             },


             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[1],
                 model_type = "Advertising",
                 language_id = "zh-TW",
                 title = "贏得客戶信任，共創高科技未來",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/zh-TW/banner2.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 2,
             },

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[1],
                 model_type = "Advertising",
                 language_id = "en",
                 title = "Win Customers' Confidence and Create the High-Tech Future",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/en/banner2.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 2,
             },
             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[1],
                 model_type = "Advertising",
                 language_id = "zh-CN",
                 title = "Win Customers' Confidence and Create the High-Tech Future",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/en/banner2.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 2,
             },

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[2],
                 model_type = "Advertising",
                 language_id = "zh-TW",
                 title = "贏得客戶信任，共創高科技未來",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/zh-TW/banner3.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 3,
             },

             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[2],
                 model_type = "Advertising",
                 language_id = "en",
                 title = "Win Customers' Confidence and Create the High-Tech Future",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/en/banner3.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 3,
             },
             new LanguageResource()
             {
                 id = (new Helper()).GenerateIntID(),
                 AdvertisingId = guids[2],
                 model_type = "Advertising",
                 language_id = "zh-CN",
                 title = "Win Customers' Confidence and Create the High-Tech Future",
                 active = true,
                 details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/en/banner3.html") } }),
                 created_at = DateTime.Now,
                 updated_at = DateTime.Now,
                 top = true,
                 sort = 3,
             },

            };

            return resources;

        }

        public List<LanguageResource> DownAd(ModelBuilder builder, string category_id)
        {


            List<string> title = new List<string>
            {
                "Contact Us",
                "Financials",
                "Stakeholders",
            };
            List<string> pic = new List<string>
            {
                 "{\"pic\":[{\"path\":\"/styles/images/home/link1.webp\",\"title\":\"Contact Us\"}]}",
                 "{\"pic\":[{\"path\":\"/styles/images/home/link2.webp\",\"title\":\"Financials\"}]}",
                 "{\"pic\":[{\"path\":\"/styles/images/home/link3.webp\",\"title\":\"Stakeholders\"}]}",
           
            };
            List<string> link = new List<string>
            {
                "/Contact",
                "",
                "",
            };

            List<Advertising> advertising = new List<Advertising>();
            int i = 1;
            foreach (string item in title)
            {
                advertising.Add(new Advertising()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "DownAd",
                    category_id = category_id,
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<Advertising>().HasData(
                advertising
              );


            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (Advertising item in advertising)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "en",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", link[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", link[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", link[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;


        }

        public List<LanguageResource> report(ModelBuilder builder, string category_id)
        {


            List<string> title = new List<string>
            {
                "Monthly Sales Report--Sep 2022 has been released",
                "Monthly Sales Report--Sep 2021 has been released",
                "Monthly Sales Report--Sep 2020 has been released",
            };


            List<Advertising> advertising = new List<Advertising>();
            int i = 1;
            foreach (string item in title)
            {
                advertising.Add(new Advertising()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "report",
                    category_id = category_id,
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<Advertising>().HasData(
                advertising
              );


            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (Advertising item in advertising)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "en",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", "#1"} }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", "#1" } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", "#1" } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;


        }


        public List<LanguageResource> ServiceAd(ModelBuilder builder, string category_id)
        {


            List<string> title = new List<string>
            {
                "IC Packaging",
                "Services",
                "Technology",
            };
            List<string> pic = new List<string>
            {

                 "{\"pic\":[{\"path\":\"/styles/images/home/fin-img1.webp\",\"title\":\"IC Packaging\"}],\"icon\":[{\"path\":\"/styles/images/home/fin-icon1.png\",\"title\":\"IC Packaging\"}]}",
                 "{\"pic\":[{\"path\":\"/styles/images/home/fin-img2.webp\",\"title\":\"Services\"}],\"icon\":[{\"path\":\"/styles/images/home/fin-icon2.png\",\"title\":\"IC Packaging\"}]}",
                 "{\"pic\":[{\"path\":\"/styles/images/home/fin-img3.webp\",\"title\":\"Technology\"}],\"icon\":[{\"path\":\"/styles/images/home/fin-icon3.png\",\"title\":\"IC Packaging\"}]}",

            };

            List<string> notes = new List<string>
            {
                "SPIL provides comprehensive packaging, offering lead frame base and substrate base packages, with either wire bond or flip chip interconnections. These packages satisfy most applications for customers serving the PC, hand-held product, consumer product and wire communication markets.",
                "Siliconware is one of the leading service-oriented companies in the semiconductor manufacturing field. Services are held increasingly more accountable to the business. They support to deliver on both quality and cost-effectiveness to our company and our valuable customers.",
                "SPIL's superior achievements in Reseach and Development have resulted in new technologies that will be utilized to meet future packaging demands.",
            };

            List<string> links = new List<string>
            {
                "/IC_Packaging",
                "/Service",
                "/Technology",
            };

            List<Advertising> advertising = new List<Advertising>();
            int i = 1;
            foreach (string item in title)
            {
                advertising.Add(new Advertising()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "ServiceAd",
                    category_id = category_id,
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<Advertising>().HasData(
                advertising
              );


            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (Advertising item in advertising)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "en",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", links[i] } ,{ "notes", notes[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", links[i] }, { "notes", notes[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "link", links[i] }, { "notes", notes[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;


        }


        public List<LanguageResource> NumEditor(ModelBuilder builder, string category_id)
        {


            List<string> title = new List<string>
            {
                "相關數據",
            };
            //
            List<Advertising> advertising = new List<Advertising>();
            int i = 1;
            foreach (string item in title)
            {
                advertising.Add(new Advertising()
                {
                    id = "NumEditor",
                    uri = "NumEditor",
                    category_id = "NumEditor",
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<Advertising>().HasData(
                advertising
              );


            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (Advertising item in advertising)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "en",
                    category_id = category_id,
                    title = title[i].ToString(),

                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/NumEditor.html") } }),

                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/editor.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/editor.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;


        }

        public List<LanguageResource> MainEditor(ModelBuilder builder, string category_id)
        {


            List<string> title = new List<string>
            {
                "We Are SPIL",
            };
            //
            List<Advertising> advertising = new List<Advertising>();
            int i = 1;
            foreach (string item in title)
            {
                advertising.Add(new Advertising()
                {
                    id = "MainEditor",
                    uri = "MainEditor",
                    category_id = category_id,
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<Advertising>().HasData(
                advertising
              );


            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (Advertising item in advertising)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "en",
                    category_id = category_id,
                    title = title[i].ToString(),

                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/editor.html") } }),

                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/editor.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id,
                    title = title[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", MapPath("Content/Templates/Index/editor.html") } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;


        }


        public List<LanguageResource> MainAd(ModelBuilder builder, string category_id)
        {


            List<string> title = new List<string>
            {
                "Career",
                "Corporate Sustainability",
                "Career",
            };

            List<string> pic = new List<string>
            {
                 "{\"pic\":[{\"path\":\"/styles/images/home/invest-1.webp\",\"title\":\"Career\"}]}",
                 "{\"pic\":[{\"path\":\"/styles/images/home/invest-2.webp\",\"title\":\"Corporate Sustainability\"}]}",
                 "{\"pic\":[{\"path\":\"/styles/images/home/invest-1.webp\",\"title\":\"Career\"}]}",

            };
            List<string> editor = new List<string>
            {
                MapPath("Content/Templates/Index/mainAd1.html") ,
                MapPath("Content/Templates/Index/mainAd2.html") ,
                MapPath("Content/Templates/Index/mainAd1.html") ,

            };
            //
            List<Advertising> advertising = new List<Advertising>();
            int i = 1;
            foreach (string item in title)
            {
                advertising.Add(new Advertising()
                {
                    id = (new Helper()).GenerateIntID(),
                    uri = "MainAd",
                    category_id = category_id,
                    sort = i,
                    active = true,
                });
                i++;
            }


            builder.Entity<Advertising>().HasData(
                advertising
              );


            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (Advertising item in advertising)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "en",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i] } }),

                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "Advertising",
                    AdvertisingId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id,
                    title = title[i].ToString(),
                    pic = pic[i].ToString(),
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i] } }),
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }

            return resources;


        }

        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
