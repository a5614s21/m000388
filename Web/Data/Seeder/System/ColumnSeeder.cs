﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using System;

namespace WebApp.Seeder
{
    public class ColumnSeeder
    {
        public void run(ModelBuilder builder)
        {
            this.news(builder);
            this.column(builder);
            this.download(builder);

            this.newsData(builder);

            this.videoData(builder);


            this.about(builder);

            this.Sustainability(builder);
            this.CS(builder);
            this.Careers(builder);

             this.awardsData(builder);
            this.experienceData(builder);
            this.SustainabilityDownload(builder);

            this.historyData(builder);

            this.downloadData(builder);

            this.topLocaltion(builder);


            this.localtion(builder);


            this.localtionData(builder);

            this.productCategory(builder);

            this.productSet(builder);


           // this.productSpecCategory(builder);
          //  this.productSpec(builder);

            this.advertisingBanner(builder);
            //this.advertisingIndex(builder);
            //this.advertisingFooter(builder);
            // this.advertisingPathBanner(builder);


            this.member(builder);

            this.webData(builder);
            this.InboxNotify(builder);
            this.systemGroup(builder);
            this.systemItem(builder);



            this.smtp(builder);
            this.contact(builder);

            this.questionnaire(builder);

            this.AspNetUsers(builder);

            this.AspNetRole(builder);
            
            this.Firewall(builder);

            this.finance(builder);


            this.quarterly(builder);
            this.filings(builder);
            this.consolidated(builder);
            this.reports(builder);
            this.annual(builder);

            this.certification(builder);


            this.QuestionnaireGroup(builder);
            this.QuestionnaireItem(builder);

            this.certificationCategory(builder);
        }



        public void news(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = "news",
                        model = "ArticleCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "news",
                         model = "ArticleCategory",
                         column_name = "parent_id",
                         sub_column_name = "",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"news\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "色碼",
                           uri = "news",
                           model = "ArticleCategory",
                           column_name = "details",
                           sub_column_name = "color",
                           sort = 1,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                       },

                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "news",
                            model = "ArticleCategory",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "news",
                              model = "ArticleCategory",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "news",
                        model = "ArticleCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void certificationCategory(ModelBuilder builder)
        {
            string uri = "certification";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = uri,
                        model = "ArticleCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "預設分類",
                           uri = uri,
                           model = "ArticleCategory",
                           column_name = "parent_id",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "adv",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" }, { "value", "certification" } })
                       },


                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = uri,
                            model = "ArticleCategory",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = uri,
                              model = "ArticleCategory",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = ++sortIndex,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void column(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = "column",
                        model = "ArticleCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "英文標題",
                         uri = "column",
                         model = "ArticleCategory",
                         column_name = "details",
                         sub_column_name = "en_title",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                     },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "column",
                         model = "ArticleCategory",
                         column_name = "parent_id",
                         sub_column_name = "",
                         sort = 3,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"news\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "Icon",
                           uri = "column",
                           model = "ArticleCategory",
                           column_name = "icon",
                           sub_column_name = "",
                           sort = 4,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "Y" }, { "required", "required" }, { "notes", "建議大小 56 x 66，第一張為前景圖，第二張為滑鼠效果圖片" } })
                       },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "Banner",
                          uri = "column",
                          model = "ArticleCategory",
                          column_name = "pic",
                          sub_column_name = "",
                          sort = 5,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 1920 x 750" } })
                      },
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "頁首圖",
                            uri = "column",
                            model = "ArticleCategory",
                            column_name = "header_pic",
                            sub_column_name = "",
                            sort = 6,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 496 x 200" } })
                        },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "列表圖",
                           uri = "column",
                           model = "ArticleCategory",
                           column_name = "list_pic",
                           sub_column_name = "",
                           sort = 6,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 496 x 200" } })
                       },
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "首頁圖",
                            uri = "column",
                            model = "ArticleCategory",
                            column_name = "index_pic",
                            sub_column_name = "",
                            sort = 7,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 496 x 200" } })
                        },
                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "首頁右側形象圖",
                             uri = "column",
                             model = "ArticleCategory",
                             column_name = "index_right_pic",
                             sub_column_name = "",
                             sort = 8,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 496 x 200" } })
                         },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = "column",
                           model = "ArticleCategory",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = 9,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },
                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "column",
                            model = "ArticleCategory",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "column",
                              model = "ArticleCategory",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "column",
                        model = "ArticleCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void download(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = "download",
                        model = "ArticleCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "download",
                         model = "ArticleCategory",
                         column_name = "parent_id",
                         sub_column_name = "",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"download\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },
                    //meta
                    /*   new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "SEO敘述",
                           uri = "news",
                           model = "ArticleCategory",
                           column_name = "seo",
                           sub_column_name = "description",
                           sort = 1,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "meta",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                       },

                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "SEO關鍵字",
                             uri = "news",
                             model = "ArticleCategory",
                             column_name = "seo",
                             sub_column_name = "keywords",
                             sort = 2,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "meta",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                         },
                    */
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "download",
                        model = "ArticleCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void topLocaltion(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = "aboutLocations",
                        model = "ArticleCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "分類",
                           uri = "aboutLocations",
                           model = "ArticleCategory",
                           column_name = "parent_id",
                           sub_column_name = "",
                           sort = 2,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "required" }, { "notes", "" } })
                       },
                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "詳細內容",
                             uri = "aboutLocations",
                             model = "ArticleCategory",
                             column_name = "details",
                             sub_column_name = "editor",
                             sort = 9,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                         },
                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "aboutLocations",
                            model = "ArticleCategory",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                           new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "SEO關鍵字",
                               uri = "aboutLocations",
                               model = "ArticleCategory",
                               column_name = "seo",
                               sub_column_name = "keywords",
                               sort = 2,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "meta",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                           },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "aboutLocations",
                        model = "ArticleCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void localtion(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = "location",
                        model = "ArticleCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "預設分類",
                             uri = "location",
                             model = "ArticleCategory",
                             column_name = "parent_id",
                             sub_column_name = "",
                             sort =  2,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "adv",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" }, { "value", "location" } })
                         },


                    /*  new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "分類",
                          uri = "location",
                          model = "ArticleCategory",
                          column_name = "parent_id",
                          sub_column_name = "",
                          sort = 2,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"location\",\"parent_id\":\"aboutLocations\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                      },
                     //meta
                   /*    new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "news",
                            model = "ArticleCategory",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "news",
                              model = "ArticleCategory",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },*/

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "location",
                        model = "ArticleCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void newsData(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "新聞標題",
                        uri = "news",
                        model = "ArticleNews",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "news",
                         model = "ArticleNews",
                         column_name = "category_id",
                         sub_column_name = "",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"\",\"parent_id\":\"news\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "圖片",
                          uri = "news",
                          model = "ArticleNews",
                          column_name = "pic",
                          sub_column_name = "pic",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 351 x 451" } })
                      },

                           new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "列表簡述",
                               uri = "news",
                               model = "ArticleNews",
                               column_name = "details",
                               sub_column_name = "notes",
                               sort = 4,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "required" }, { "notes", "" } })
                           },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = "news",
                           model = "ArticleNews",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },

                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "檔案",
                            uri = "news",
                            model = "ArticleNews",
                            column_name = "files",
                            sub_column_name = "file",
                            sort = 6,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "N" }, { "required", "required" }, { "notes", "" } })
                        },


                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "news",
                            model = "ArticleNews",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "news",
                              model = "ArticleNews",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },



                          //adv
                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "發佈時間",
                              uri = "news",
                              model = "ArticleNews",
                              column_name = "start_at",
                              sub_column_name = "",
                              sort = 39,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "adv",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                          },

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "結束日期",
                                uri = "news",
                                model = "ArticleNews",
                                column_name = "end_at",
                                sub_column_name = "",
                                sort = 40,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "adv",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                            },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "置頂",
                          uri = "news",
                          model = "ArticleNews",
                          column_name = "top",
                          sub_column_name = "",
                          sort = 49,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "adv",
                          options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"False\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                      },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "news",
                        model = "ArticleNews",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void videoData(ModelBuilder builder)
        {
            string uri = "video";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleNews",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = uri,
                         model = "ArticleNews",
                         column_name = "category_id",
                         sub_column_name = "",
                         sort = ++sortIndex,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"video\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "圖片",
                          uri = uri,
                          model = "ArticleNews",
                          column_name = "pic",
                          sub_column_name = "pic",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 351 x 451" } })
                      },

                           new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "Youtube",
                               uri = uri,
                               model = "ArticleNews",
                               column_name = "details",
                               sub_column_name = "youtube",
                               sort = ++sortIndex,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                           },

                          //adv
                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "發佈時間",
                              uri = uri,
                              model = "ArticleNews",
                              column_name = "start_at",
                              sub_column_name = "",
                              sort = ++sortIndex,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "adv",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                          },

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "結束日期",
                                uri = uri,
                                model = "ArticleNews",
                                column_name = "end_at",
                                sub_column_name = "",
                                sort = ++sortIndex,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "adv",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                            },
                     
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleNews",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void about(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = "about",
                        model = "ArticlePage",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "about",
                         model = "ArticlePage",
                         column_name = "category_id",
                         sub_column_name = "",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" } })
                     },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "列表圖",
                          uri = "about",
                          model = "ArticlePage",
                          column_name = "pic",
                          sub_column_name = "pic",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "" }, { "notes", "建議寬高 261 x 151" } })
                      },


                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = "about",
                           model = "ArticlePage",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },



                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "about",
                            model = "ArticlePage",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "about",
                              model = "ArticlePage",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },

                    //adv

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "about",
                        model = "ArticlePage",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" } })
                    }




                );




        }

        public void Sustainability(ModelBuilder builder)
        {
            string uri = "Sustainability";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticlePage",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                 

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = uri,
                           model = "ArticlePage",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },

                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = uri,
                            model = "ArticlePage",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = uri,
                              model = "ArticlePage",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = ++sortIndex,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          }

                    //adv

                 /*   new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "about",
                        model = "ArticlePage",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" } })
                    }
                 */



                );




        }


        public void CS(ModelBuilder builder)
        {
            string uri = "CS";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticlePage",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },



                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = uri,
                           model = "ArticlePage",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },


                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "預設分類",
                             uri = uri,
                             model = "ArticlePage",
                             column_name = "category_id",
                             sub_column_name = "",
                             sort = ++sortIndex,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "adv",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" }, { "value", "CS" } })
                         },


                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = uri,
                            model = "ArticlePage",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = uri,
                              model = "ArticlePage",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = ++sortIndex,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },

                      //adv

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "啟用狀態",
                          uri = uri,
                          model = "ArticlePage",
                          column_name = "active",
                          sub_column_name = "",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "adv",
                          options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                      }




                );




        }

        public void Careers(ModelBuilder builder)
        {
            string uri = "Careers";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticlePage",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },



                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = uri,
                           model = "ArticlePage",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },



                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = uri,
                            model = "ArticlePage",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = uri,
                              model = "ArticlePage",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = ++sortIndex,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          }

                //adv

                /*   new Columns()
                   {
                       id = Guid.NewGuid().ToString(),
                       lang = "zh-Hant",
                       title = "啟用狀態",
                       uri = "about",
                       model = "ArticlePage",
                       column_name = "active",
                       sub_column_name = "",
                       sort = ++sortIndex,
                       active = true,
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       layout = "adv",
                       options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" } })
                   }
                */



                );




        }

     


        public void awardsData(ModelBuilder builder)
        {
            string uri = "awards";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleColumn",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                      

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "詳細內容",
                           uri = uri,
                           model = "ArticleColumn",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },




                    //adv
                    
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleColumn",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void experienceData(ModelBuilder builder)
        {
            string uri = "experience";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "姓名",
                        uri = uri,
                        model = "ArticleColumn",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "西元年",
                           uri = uri,
                           model = "ArticleColumn",
                           column_name = "year",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                       },
                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "學歷/單位",
                              uri = uri,
                              model = "ArticleColumn",
                              column_name = "details",
                              sub_column_name = "sub_title",
                              sort = ++sortIndex,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "main",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                          },

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "照片",
                                uri = uri,
                                model = "ArticleColumn",
                                column_name = "pic",
                                sub_column_name = "pic",
                                sort = ++sortIndex,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "main",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 1920 x 750" } })
                            },

                     new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "內容",
                           uri = uri,
                           model = "ArticleColumn",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },







                    //adv

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleColumn",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }



        public void SustainabilityDownload(ModelBuilder builder)
        {
            string uri = "Sustainability";
            int sortIndex = 0;
            string modelName = "ArticleDownload";

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = modelName,
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "西元年",
                           uri = uri,
                           model = modelName,
                           column_name = "year",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                       },
                       

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "列表圖",
                                uri = uri,
                                model = modelName,
                                column_name = "pic",
                                sub_column_name = "pic",
                                sort = ++sortIndex,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "main",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 1920 x 750" } })
                            },

                           new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "檔案",
                               uri = uri,
                               model = modelName,
                               column_name = "files",
                               sub_column_name = "file",
                               sort = ++sortIndex,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "N" }, { "required", "required" }, { "notes", "" } })
                           },


                    //adv

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = modelName,
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void historyData(ModelBuilder builder)
        {
            string uri = "history";
            int sortIndex = 0;
            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "記事",
                        uri = uri,
                        model = "ArticleColumn",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "西元年",
                         uri = uri,
                         model = "ArticleColumn",
                         column_name = "year",
                         sub_column_name = "",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                     },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "月份",
                          uri = uri,
                          model = "ArticleColumn",
                          column_name = "month",
                          sub_column_name = "",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "month" }, { "required", "required" }, { "notes", "" } })
                      },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleColumn",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }
        public void downloadData(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "名稱",
                        uri = "download",
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "產品編號",
                         uri = "download",
                         model = "ArticleDownload",
                         column_name = "details",
                         sub_column_name = "no",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                     },

                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "download",
                         model = "ArticleDownload",
                         column_name = "category_id",
                         sub_column_name = "",
                         sort = 3,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"download\",\"parent_id\":\"download\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "列表圖",
                          uri = "download",
                          model = "ArticleDownload",
                          column_name = "pic",
                          sub_column_name = "",
                          sort = 5,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 335 x 453" } })
                      },

                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "檔案",
                            uri = "download",
                            model = "ArticleDownload",
                            column_name = "files",
                            sub_column_name = "",
                            sort = 7,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "N" }, { "required", "required" }, { "notes", "" } })
                        },


                    /*  new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "詳細內容",
                          uri = "download",
                          model = "ArticleDownload",
                          column_name = "details",
                          sub_download_name = "editor",
                          sort = 8,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                      },
                       //meta
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "SEO敘述",
                           uri = "download",
                           model = "ArticleDownload",
                           column_name = "seo",
                           sub_download_name = "description",
                           sort = 1,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "meta",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                       },

                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "SEO關鍵字",
                             uri = "download",
                             model = "ArticleDownload",
                             column_name = "seo",
                             sub_download_name = "keywords",
                             sort = 2,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "meta",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                         },*/

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "download",
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void localtionData(ModelBuilder builder)
        {
            string uri = "location";
            int sortIndex = 0;


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "據點名稱",
                        uri = uri,
                        model = "ArticleLocation",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                      new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "分類",
                          uri = uri,
                          model = "ArticleLocation",
                           column_name = "category_id",
                           sub_column_name = "",
                          sort = ++sortIndex,
                          active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"location\",\"parent_id\":\"location\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                      },

                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "電話",
                            uri = uri,
                            model = "ArticleLocation",
                            column_name = "details",
                            sub_column_name = "tel",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                        },
                             new Columns()
                             {
                                 id = Guid.NewGuid().ToString(),
                                 lang = "zh-Hant",
                                 title = "傳真",
                                 uri = uri,
                                 model = "ArticleLocation",
                                 column_name = "details",
                                 sub_column_name = "fax",
                                 sort = ++sortIndex,
                                 active = true,
                                 created_at = DateTime.Now,
                                 updated_at = DateTime.Now,
                                 layout = "main",
                                 options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                             },
                              new Columns()
                              {
                                  id = Guid.NewGuid().ToString(),
                                  lang = "zh-Hant",
                                  title = "電子郵件",
                                  uri = uri,
                                  model = "ArticleLocation",
                                  column_name = "details",
                                  sub_column_name = "email",
                                  sort = ++sortIndex,
                                  active = true,
                                  created_at = DateTime.Now,
                                  updated_at = DateTime.Now,
                                  layout = "main",
                                  options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                              },


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "詳細內容",
                          uri = uri,
                          model = "ArticleLocation",
                          column_name = "details",
                          sub_column_name = "editor",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                      },
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "地圖連結",
                           uri = uri,
                           model = "ArticleLocation",
                           column_name = "details",
                           sub_column_name = "map",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                       },
                      
                    //meta
                   
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleLocation",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void productCategory(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類名稱",
                        uri = "",
                        model = "ProductCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = "",
                         model = "ProductCategory",
                         column_name = "parent_id",
                         sub_column_name = "",
                         sort = 2,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ProductCategory\",\"uri\":\"\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"3\",\"show_first\":\"0\"}"
                     },
                    /*  new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "自訂網頁名稱",
                          uri = "",
                          model = "ProductCategory",
                          column_name = "path",
                          sub_column_name = "",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "自定義網頁Path，http://網址/{自訂名稱}" } })
                      },*/



                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "圖片",
                            uri = "",
                            model = "ProductCategory",
                            column_name = "pic",
                            sub_column_name = "pic",
                            sort = 7,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 66 x 66" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "內頁圖片",
                              uri = "",
                              model = "ProductCategory",
                              column_name = "pic",
                              sub_column_name = "path_pic",
                              sort = 8,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "main",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "" }, { "notes", "建議大小 1000 x 750" } })
                          },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "簡述",
                              uri = "",
                              model = "ProductCategory",
                              column_name = "details",
                              sub_column_name = "notes",
                              sort = 9,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "main",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "" } })
                          },

                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "說明內容",
                            uri = "news",
                            model = "ProductCategory",
                            column_name = "details",
                            sub_column_name = "editor",
                            sort = 10,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                        },




                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "",
                            model = "ProductCategory",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "",
                              model = "ProductCategory",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "",
                        model = "ProductCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }



         public void productSpecCategory(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "類型名稱",
                        uri = "",
                        model = "ProductSpecCategory",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },


                    /*  new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "圖片",
                          uri = "",
                          model = "ProductSpecCategory",
                          column_name = "pic",
                          sub_column_name = "icon",
                          sort = 7,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 66 x 66" } })
                      },*/
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "輸入模式",
                        uri = "",
                        model = "ProductSpecCategory",
                        column_name = "input_type",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"input\",\"value\":{\"input\":\"個別輸入\",\"select\":\"單選\",\"multiple\":\"多選\"}}"
                    },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "",
                        model = "ProductSpecCategory",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );

        }


        public void productSpec(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "規格名",
                        uri = "",
                        model = "ProductSpec",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "分類",
                           uri = "product",
                           model = "ProductSpec",
                           column_name = "category_id",
                           sub_column_name = "",
                           sort = 4,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ProductSpecCategory\",\"uri\":\"\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"2\"}"
                       },

                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "圖片",
                         uri = "",
                         model = "ProductSpec",
                         column_name = "pic",
                         sub_column_name = "icon",
                         sort = 7,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 66 x 66" } })
                     },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "",
                        model = "ProductSpec",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );

        }

        public void productSet(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "產品名稱",
                        uri = "product",
                        model = "ProductSet",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                   /*   new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "自訂網頁名稱",
                          uri = "product",
                          model = "ProductSet",
                          column_name = "path",
                          sub_column_name = "",
                          sort = 2,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "自定義網頁Path，http://網址/{自訂名稱}" } })
                      },*/


                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "分類",
                           uri = "product",
                           model = "ProductSet",
                           column_name = "category_id",
                           sub_column_name = "",
                           sort = 3,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ProductCategory\",\"uri\":\"\",\"parent_id\":\"ICPackaging\",\"getCategory\":\"1\",\"limit\":\"2\"}"
                       },
                    

                       

                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "圖片",
                             uri = "product",
                             model = "ProductSet",
                             column_name = "pic",
                             sub_column_name = "pic",
                             sort = 14,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "Y" }, { "required", "required" }, { "notes", "建議大小 658 x 691" } })
                         },
                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "內頁圖片",
                              uri = "product",
                              model = "ProductSet",
                              column_name = "pic",
                              sub_column_name = "path_pic",
                              sort = 15,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "main",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "" }, { "notes", "建議大小 1000 x 750" } })
                          },
                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "簡述",
                              uri = "product",
                              model = "ProductSet",
                              column_name = "details",
                              sub_column_name = "notes",
                              sort = 19,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "main",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "" } })
                          },
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "說明內容",
                            uri = "product",
                            model = "ProductSet",
                            column_name = "details",
                            sub_column_name = "editor",
                            sort = 20,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "" }, { "notes", "" } })
                        },


                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "product",
                            model = "ProductSet",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "product",
                              model = "ProductSet",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          },



                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "product",
                        model = "ProductSet",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void advertisingBanner(ModelBuilder builder)
        {
            string uri = "Banner";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "Advertising",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                /*new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = uri,
                         model = "ArticleCategory",
                         column_name = "AdvertisingCategoryid",
                         sub_column_name = "",
                         sort = ++sortIndex,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"AdvertisingCategory\",\"uri\":\"\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },
                new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "圖片",
                           uri = uri,
                           model = "Advertising",
                           column_name = "pic",
                           sub_column_name = "pic",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 1920 x 750" } })
                       },*/
                new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "內容",
                           uri = uri,
                           model = "Advertising",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },
                new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "Advertising",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }
                );


            uri = "report";
            sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "標題",
                    uri = uri,
                    model = "Advertising",
                    column_name = "title",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },
              
              
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "相關連結",
                    uri = uri,
                    model = "Advertising",
                    column_name = "details",
                    sub_column_name = "link",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "發佈時間",
                        uri = uri,
                        model = "Advertising",
                        column_name = "start_at",
                        sub_column_name = "",
                        sort = 39,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                    },

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "結束日期",
                                uri = uri,
                                model = "Advertising",
                                column_name = "end_at",
                                sub_column_name = "",
                                sort = 40,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "adv",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                            },
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "啟用狀態",
                    uri = uri,
                    model = "Advertising",
                    column_name = "active",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "adv",
                    options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                }
                );


            uri = "ServiceAd";
            sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "標題",
                    uri = uri,
                    model = "Advertising",
                    column_name = "title",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },
                
                new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "圖片",
                           uri = uri,
                           model = "Advertising",
                           column_name = "pic",
                           sub_column_name = "pic",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 800 x 480" } })
                       },


                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "Icon",
                    uri = uri,
                    model = "Advertising",
                    column_name = "pic",
                    sub_column_name = "icon",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 44 x 44" } })
                },
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "列表敘述",
                    uri = uri,
                    model = "Advertising",
                    column_name = "details",
                    sub_column_name = "notes",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "required" }, { "notes", "" } })
                },
                 new Columns()
                 {
                     id = Guid.NewGuid().ToString(),
                     lang = "zh-Hant",
                     title = "相關連結",
                     uri = uri,
                     model = "Advertising",
                     column_name = "details",
                     sub_column_name = "link",
                     sort = ++sortIndex,
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     layout = "main",
                     options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                 },
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "啟用狀態",
                    uri = uri,
                    model = "Advertising",
                    column_name = "active",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "adv",
                    options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                }
                );




            uri = "NumEditor";
            sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "標題",
                    uri = uri,
                    model = "Advertising",
                    column_name = "title",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },
                  new Columns()
                  {
                      id = Guid.NewGuid().ToString(),
                      lang = "zh-Hant",
                      title = "內容",
                      uri = uri,
                      model = "Advertising",
                      column_name = "details",
                      sub_column_name = "editor",
                      sort = ++sortIndex,
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now,
                      layout = "main",
                      options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                  },


                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "啟用狀態",
                    uri = uri,
                    model = "Advertising",
                    column_name = "active",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "adv",
                    options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                }
                );



            uri = "MainEditor";
            sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "標題",
                    uri = uri,
                    model = "Advertising",
                    column_name = "title",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },
                  new Columns()
                  {
                      id = Guid.NewGuid().ToString(),
                      lang = "zh-Hant",
                      title = "內容",
                      uri = uri,
                      model = "Advertising",
                      column_name = "details",
                      sub_column_name = "editor",
                      sort = ++sortIndex,
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now,
                      layout = "main",
                      options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                  },


                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "啟用狀態",
                    uri = uri,
                    model = "Advertising",
                    column_name = "active",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "adv",
                    options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                }
                );




            uri = "MainAd";
            sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "標題",
                    uri = uri,
                    model = "Advertising",
                    column_name = "title",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },

                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "圖片",
                    uri = uri,
                    model = "Advertising",
                    column_name = "pic",
                    sub_column_name = "pic",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 800 x 800" } })
                },

                  new Columns()
                  {
                      id = Guid.NewGuid().ToString(),
                      lang = "zh-Hant",
                      title = "內容",
                      uri = uri,
                      model = "Advertising",
                      column_name = "details",
                      sub_column_name = "editor",
                      sort = ++sortIndex,
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now,
                      layout = "main",
                      options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                  },
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "啟用狀態",
                    uri = uri,
                    model = "Advertising",
                    column_name = "active",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "adv",
                    options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                }
                );




            uri = "DownAd";
            sortIndex = 0;

            builder.Entity<Columns>().HasData(
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "標題",
                    uri = uri,
                    model = "Advertising",
                    column_name = "title",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                },

                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "圖片",
                    uri = uri,
                    model = "Advertising",
                    column_name = "pic",
                    sub_column_name = "pic",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "main",
                    options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 800 x 475" } })
                },

                  new Columns()
                  {
                      id = Guid.NewGuid().ToString(),
                      lang = "zh-Hant",
                      title = "相關連結",
                      uri = uri,
                      model = "Advertising",
                      column_name = "details",
                      sub_column_name = "link",
                      sort = ++sortIndex,
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now,
                      layout = "main",
                      options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                  },
                new Columns()
                {
                    id = Guid.NewGuid().ToString(),
                    lang = "zh-Hant",
                    title = "啟用狀態",
                    uri = uri,
                    model = "Advertising",
                    column_name = "active",
                    sub_column_name = "",
                    sort = ++sortIndex,
                    active = true,
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    layout = "adv",
                    options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                }
                );
        }

        public void advertisingIndex(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = "main",
                        model = "Advertising",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                       /*   new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "內容",
                              uri = "index",
                              model = "Advertising",
                              column_name = "details",
                              sub_column_name = "link",
                              sort = 5,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "main",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                          },*/

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "內容",
                           uri = "main",
                           model = "Advertising",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },




                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "main",
                        model = "Advertising",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void advertisingFooter(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = "footer",
                        model = "Advertising",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                       /*    new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "內容",
                               uri = "footer",
                               model = "Advertising",
                               column_name = "details",
                               sub_column_name = "link",
                               sort = 5,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                           },*/

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "內容",
                           uri = "footer",
                           model = "Advertising",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },




                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "footer",
                        model = "Advertising",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void advertisingPathBanner(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = "pathBanner",
                        model = "Advertising",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "圖片",
                        uri = "pathBanner",
                        model = "Advertising",
                        column_name = "pic",
                        sub_column_name = "",
                        sort = 6,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 1920 x 750" } })
                    },


                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "內容",
                           uri = "pathBanner",
                           model = "Advertising",
                           column_name = "details",
                           sub_column_name = "editor",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                       },




                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "pathBanner",
                        model = "Advertising",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" } })
                    }




                );




        }


        public void member(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "帳號",
                          uri = "member",
                          model = "Member",
                          column_name = "username",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "username" }, { "required", "required" }, { "notes", "" } })
                      },

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "密碼",
                          uri = "member",
                          model = "Member",
                          column_name = "password",
                          sub_column_name = "",
                          sort = 2,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "password" }, { "required", "required" }, { "notes", "請輸入4~8碼數字，修改時不改密碼請留空白" } })
                      },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "姓名",
                        uri = "member",
                        model = "Member",
                        column_name = "name",
                        sub_column_name = "",
                        sort = 3,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "name" }, { "required", "required" }, { "notes", "" } })
                    },



                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "E-Mail",
                           uri = "member",
                           model = "Member",
                           column_name = "contact",
                           sub_column_name = "email",
                           sort = 5,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "email" }, { "required", "required" }, { "notes", "" }, { "encode", "Y" } })
                       },




                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "member",
                        model = "Member",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );





        }


        public void systemGroup(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "群組名稱:",
                          uri = "",
                          model = "SiteParameterGroup",
                          column_name = "title",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      }






                );




        }
        public void systemItem(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "參數名稱:",
                          uri = "",
                          model = "SiteParameterItem",
                          column_name = "title",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      },
                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "群組",
                             uri = "",
                             model = "SiteParameterItem",
                             column_name = "category_id",
                             sub_column_name = "",
                             sort = 2,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"SiteParameterGroup\",\"uri\":\"\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"2\"}"
                         },

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "Email",
                                uri = "",
                                model = "SiteParameterItem",
                                column_name = "details",
                                sub_column_name = "email",
                                sort = 3,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "main",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                            },

               new Columns()
               {
                   id = Guid.NewGuid().ToString(),
                   lang = "zh-Hant",
                   title = "啟用狀態",
                   uri = "",
                   model = "SiteParameterItem",
                   column_name = "active",
                   sub_column_name = "",
                   sort = 50,
                   active = true,
                   created_at = DateTime.Now,
                   updated_at = DateTime.Now,
                   layout = "adv",
                   options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
               }







                );




        }






        public void smtp(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "名稱",
                          uri = "smtp",
                          model = "SystemItem",
                          column_name = "title",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      },

                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "HOST",
                         uri = "smtp",
                         model = "SystemItem",
                         column_name = "details",
                         sub_column_name = "host",
                         sort = 8,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                     },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "PORT",
                        uri = "smtp",
                        model = "SystemItem",
                        column_name = "details",
                        sub_column_name = "port",
                        sort = 9,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "帳號",
                        uri = "smtp",
                        model = "SystemItem",
                        column_name = "details",
                        sub_column_name = "username",
                        sort = 10,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "密碼",
                        uri = "smtp",
                        model = "SystemItem",
                        column_name = "details",
                        sub_column_name = "password",
                        sort = 11,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "驗證方式",
                        uri = "smtp",
                        model = "SystemItem",
                        column_name = "details",
                        sub_column_name = "encryption",
                        sort = 12,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "TLS或SSL" } })
                    },


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "寄件者信箱",
                        uri = "smtp",
                        model = "SystemItem",
                        column_name = "details",
                        sub_column_name = "fromAddress",
                        sort = 13,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "寄件者",
                        uri = "smtp",
                        model = "SystemItem",
                        column_name = "details",
                        sub_column_name = "fromName",
                        sort = 12,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },



               new Columns()
               {
                   id = Guid.NewGuid().ToString(),
                   lang = "zh-Hant",
                   title = "啟用狀態",
                   uri = "smtp",
                   model = "SystemItem",
                   column_name = "active",
                   sub_column_name = "",
                   sort = 50,
                   active = true,
                   created_at = DateTime.Now,
                   updated_at = DateTime.Now,
                   layout = "adv",
                   options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
               }

                );

        }

        public void webData(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "網站名稱",
                          uri = "web_data",
                          model = "WebData",
                          column_name = "title",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      },


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "網址",
                        uri = "web_data",
                        model = "WebData",
                        column_name = "contact",
                        sub_column_name = "url",
                        sort = 2,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                    },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "電話",
                          uri = "web_data",
                          model = "WebData",
                          column_name = "contact",
                          sub_column_name = "tel",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" }, { "encode", "N" } })
                      },


                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "E-Mail",
                           uri = "web_data",
                           model = "WebData",
                           column_name = "contact",
                           sub_column_name = "email",
                           sort = 3,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" }, { "encode", "N" } })
                       },




                               new Columns()
                               {
                                   id = Guid.NewGuid().ToString(),
                                   lang = "zh-Hant",
                                   title = "Icon",
                                   uri = "web_data",
                                   model = "WebData",
                                   column_name = "pic",
                                   sub_column_name = "icon",
                                   sort = 4,
                                   active = true,
                                   created_at = DateTime.Now,
                                   updated_at = DateTime.Now,
                                   layout = "main",
                                   options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 56 x 66，第一張為前景圖，第二張為滑鼠效果圖片" } })
                               },

                                 new Columns()
                                 {
                                     id = Guid.NewGuid().ToString(),
                                     lang = "zh-Hant",
                                     title = "Facebook",
                                     uri = "web_data",
                                     model = "WebData",
                                     column_name = "social",
                                     sub_column_name = "facebook",
                                     sort = 20,
                                     active = true,
                                     created_at = DateTime.Now,
                                     updated_at = DateTime.Now,
                                     layout = "main",
                                     options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                                 },

                                   new Columns()
                                   {
                                       id = Guid.NewGuid().ToString(),
                                       lang = "zh-Hant",
                                       title = "Instagram",
                                       uri = "web_data",
                                       model = "WebData",
                                       column_name = "social",
                                       sub_column_name = "ig",
                                       sort = 21,
                                       active = true,
                                       created_at = DateTime.Now,
                                       updated_at = DateTime.Now,
                                       layout = "main",
                                       options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                                   },
                                      new Columns()
                                      {
                                          id = Guid.NewGuid().ToString(),
                                          lang = "zh-Hant",
                                          title = "Line",
                                          uri = "web_data",
                                          model = "WebData",
                                          column_name = "social",
                                          sub_column_name = "line",
                                          sort = 22,
                                          active = true,
                                          created_at = DateTime.Now,
                                          updated_at = DateTime.Now,
                                          layout = "main",
                                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "notes", "" } })
                                      },


                        //meta
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "SEO敘述",
                            uri = "web_data",
                            model = "WebData",
                            column_name = "seo",
                            sub_column_name = "description",
                            sort = 1,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "meta",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Description) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                        },

                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "SEO關鍵字",
                              uri = "web_data",
                              model = "WebData",
                              column_name = "seo",
                              sub_column_name = "keywords",
                              sort = 2,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "meta",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "(Metadata Keywords) 設定搜尋關鍵字將有助於搜尋引擎精準的找到網頁內容" } })
                          }




                );




        }


        public void Firewall(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "平台",
                             uri = "firewall",
                             model = "Firewall",
                             column_name = "platform",
                             sub_column_name = "",
                             sort = 1,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = "{\"view\":\"selectFirewall\",\"required\":\"required\",\"notes\":\"\"}"
                         },




                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "名稱",
                          uri = "firewall",
                          model = "Firewall",
                          column_name = "title",
                          sub_column_name = "",
                          sort = 2,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      },


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "說明",
                          uri = "firewall",
                          model = "Firewall",
                          column_name = "details",
                          sub_column_name = "description",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "" } })
                      },


                      new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "IP 位址",
                        uri = "firewall",
                        model = "Firewall",
                        column_name = "ip",
                        sub_column_name = "",
                        sort = 4,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "IP 規則範例：192.168.1.10 或 192.168.1.10~192.168.1.50。" } })
                    },

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "規則",
                          uri = "firewall",
                          model = "Firewall",
                          column_name = "rule",
                          sub_column_name = "",
                          sort = 50,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "adv",
                          options = "{\"view\":\"radio\",\"required\":\"required\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"允許\",\"False\":\"禁止\"}}"
                      },



                          //adv
                          new Columns()
                          {
                              id = Guid.NewGuid().ToString(),
                              lang = "zh-Hant",
                              title = "發佈時間",
                              uri = "firewall",
                              model = "Firewall",
                              column_name = "start_at",
                              sub_column_name = "",
                              sort = 39,
                              active = true,
                              created_at = DateTime.Now,
                              updated_at = DateTime.Now,
                              layout = "adv",
                              options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "required" }, { "notes", "" } })
                          },

                            new Columns()
                            {
                                id = Guid.NewGuid().ToString(),
                                lang = "zh-Hant",
                                title = "結束日期",
                                uri = "firewall",
                                model = "Firewall",
                                column_name = "end_at",
                                sub_column_name = "",
                                sort = 40,
                                active = true,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now,
                                layout = "adv",
                                options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "date" }, { "required", "" }, { "notes", "" } })
                            },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "firewall",
                        model = "Firewall",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );





        }

        public void InboxNotify(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "通知標題",
                          uri = "inbox_notify",
                          model = "InboxNotify",
                          column_name = "title",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      },

              new Columns()
              {
                  id = Guid.NewGuid().ToString(),
                  lang = "zh-Hant",
                  title = "收件信箱",
                  uri = "inbox_notify",
                  model = "InboxNotify",
                  column_name = "details",
                  sub_column_name = "email",
                  sort = 2,
                  active = true,
                  created_at = DateTime.Now,
                  updated_at = DateTime.Now,
                  layout = "main",
                  options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "收件者為複數時，請使用逗號區隔" }, { "encode", "N" } })
              },


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "用戶信件主旨",
                        uri = "inbox_notify",
                        model = "InboxNotify",
                        column_name = "details",
                        sub_column_name = "subject",
                        sort = 3,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                  new Columns()
                  {
                      id = Guid.NewGuid().ToString(),
                      lang = "zh-Hant",
                      title = "用戶信件簡述",
                      uri = "inbox_notify",
                      model = "InboxNotify",
                      column_name = "details",
                      sub_column_name = "notes",
                      sort = 4,
                      active = true,
                      created_at = DateTime.Now,
                      updated_at = DateTime.Now,
                      layout = "main",
                      options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                  },

                 new Columns()
                 {
                     id = Guid.NewGuid().ToString(),
                     lang = "zh-Hant",
                     title = "用戶信件內容",
                     uri = "inbox_notify",
                     model = "InboxNotify",
                     column_name = "details",
                     sub_column_name = "editor",
                     sort = 8,
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     layout = "main",
                     options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                 }








                );




        }


        public void contact(ModelBuilder builder)
        {
            string uri = "Contact";
            int sortIndex = 0;
            builder.Entity<Columns>().HasData(



                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "姓名",
                        uri = uri,
                        model = "Contact",
                        column_name = "name",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "name" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" } })
                    },



                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "電子郵件",
                           uri = uri,
                           model = "Contact",
                           column_name = "email",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "email" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "Y" } })
                       },
                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "電話",
                             uri = uri,
                             model = "Contact",
                             column_name = "contact",
                             sub_column_name = "tel",
                             sort = ++sortIndex,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "Y" } })
                         },


                           new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "國家/地區",
                               uri = uri,
                               model = "Contact",
                               column_name = "contact",
                               sub_column_name = "Country",
                               sort = ++sortIndex,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "N" } })
                           },



                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "公司名稱",
                             uri = uri,
                             model = "Contact",
                             column_name = "contact",
                             sub_column_name = "Company",
                             sort = ++sortIndex,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "N" } })
                         },

                           new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "類別",
                               uri = uri,
                               model = "Contact",
                               column_name = "options",
                               sub_column_name = "ProductCategory",
                               sort = ++sortIndex,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "show" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "N" } })
                           },

                        /*   new Columns()
                           {
                               id = Guid.NewGuid().ToString(),
                               lang = "zh-Hant",
                               title = "Model Name",
                               uri = uri,
                               model = "Contact",
                               column_name = "options",
                               sub_column_name = "ModelName",
                               sort = ++sortIndex,
                               active = true,
                               created_at = DateTime.Now,
                               updated_at = DateTime.Now,
                               layout = "main",
                               options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "show" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "N" } })
                           },*/
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "主旨",
                            uri = uri,
                            model = "Contact",
                            column_name = "contact",
                            sub_column_name = "Subject",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "N" } })
                        },


                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "洽詢內容",
                             uri = uri,
                             model = "Contact",
                             column_name = "contact",
                             sub_column_name = "Description",
                             sort = ++sortIndex,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "readonly", "readonly" }, { "notes", "" }, { "encode", "N" } })
                         },

                       
                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "回覆內容",
                            uri = uri,
                            model = "Contact",
                            column_name = "contact",
                            sub_column_name = "re_content",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "textarea" }, { "required", "" }, { "notes", "" }, { "encode", "N" } })
                        },


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "處理狀態",
                        uri = "contact",
                        model = "Contact",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"已處理\",\"False\":\"未處理\"}}"
                    }




                );





        }


    

        public void questionnaire(ModelBuilder builder)
        {
            string uri = "Questionnaire";
            int sortIndex = 0;
            builder.Entity<Columns>().HasData(



                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "問卷內容",
                        uri = uri,
                        model = "Contact",
                        column_name = "contact",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "questionnaire" }, { "required", "" }, { "readonly", "" }, { "notes", "" } })
                    }



                );





        }
        public void AspNetRole(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "群組名稱",
                          uri = "admin",
                          model = "IdentityRole",
                          column_name = "Name",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                      },

                   new Columns()
                   {
                       id = Guid.NewGuid().ToString(),
                       lang = "zh-Hant",
                       title = "群組ID",
                       uri = "admin",
                       model = "IdentityRole",
                       column_name = "NormalizedName",
                       sub_column_name = "",
                       sort = 1,
                       active = true,
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       layout = "main",
                       options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                   }





                );





        }


        public void AspNetUsers(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "帳號",
                          uri = "admin",
                          model = "IdentityUser",
                          column_name = "NormalizedUserName",
                          sub_column_name = "",
                          sort = 1,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "username" }, { "required", "required" }, { "notes", "" } })
                      },

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "密碼",
                          uri = "admin",
                          model = "IdentityUser",
                          column_name = "PasswordHash",
                          sub_column_name = "",
                          sort = 2,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "password" }, { "required", "required" }, { "notes", "請輸入6~12碼包含英文數字，修改時不改密碼請留空白" } })
                      },


                         new Columns()
                         {
                             id = Guid.NewGuid().ToString(),
                             lang = "zh-Hant",
                             title = "群組",
                             uri = "news",
                             model = "IdentityUser",
                             column_name = "role",
                             sub_column_name = "",
                             sort = 2,
                             active = true,
                             created_at = DateTime.Now,
                             updated_at = DateTime.Now,
                             layout = "main",
                             options = "{\"view\":\"selectRole\",\"required\":\"required\",\"notes\":\"\",\"model\":\"IdentityRole\",\"uri\":\"\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                         },





                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "姓名",
                        uri = "admin",
                        model = "IdentityUser",
                        column_name = "UserName",
                        sub_column_name = "",
                        sort = 10,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "name" }, { "required", "required" }, { "notes", "" }, { "encode", "Y" } })
                    },



                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "E-Mail",
                           uri = "admin",
                           model = "IdentityUser",
                           column_name = "Email",
                           sub_column_name = "",
                           sort = 11,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "email" }, { "required", "required" }, { "notes", "" }, { "encode", "Y" } })
                       },




                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "admin",
                        model = "IdentityUser",
                        column_name = "LockoutEnabled",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"False\",\"value\":{\"False\":\"啟用\",\"True\":\"停用\"}}"
                    }




                );





        }


        public void finance(ModelBuilder builder)
        {


            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = "finance",
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = 1,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "西元年",
                        uri = "finance",
                        model = "ArticleDownload",
                        column_name = "year",
                        sub_column_name = "",
                        sort = 2,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                    },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "月份",
                          uri = "finance",
                          model = "ArticleDownload",
                          column_name = "month",
                          sub_column_name = "",
                          sort = 3,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "month" }, { "required", "required" }, { "notes", "" } })
                      },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "檔案",
                          uri = "finance",
                          model = "ArticleDownload",
                          column_name = "files",
                          sub_column_name = "file",
                          sort = 7,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "N" }, { "required", "required" }, { "notes", "" } })
                      },
                
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = "finance",
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = 50,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void quarterly(ModelBuilder builder)
        {
            string uri = "quarterly";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "西元年",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "year",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                    },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "季",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "month",
                          sub_column_name = "",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "season" }, { "required", "required" }, { "notes", "" } })
                      },
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "Youtube",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "video",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "video" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                       },

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "檔案",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "files",
                          sub_column_name = "file",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                      },
              
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void filings(ModelBuilder builder)
        {
            string uri = "filings";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                  /*  new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "西元年",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "year",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                    },*/
                    
                      /* new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "Youtube",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "video",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "video" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                       },*/

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "20F",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "files",
                          sub_column_name = "file",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                      },

                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "Conflict Mineral Report",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "files",
                           sub_column_name = "file2",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                       },
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "背景圖",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "pic",
                           sub_column_name = "pic",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 800 x 545" } })
                       },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void annual(ModelBuilder builder)
        {
            string uri = "annual";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "西元年",
                            uri = uri,
                            model = "ArticleDownload",
                            column_name = "year",
                            sub_column_name = "",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                        },

                      /* new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "Youtube",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "video",
                           sub_column_name = "",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "video" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                       },*/


                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "檔案",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "files",
                           sub_column_name = "file",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                       },
                       new Columns()
                       {
                           id = Guid.NewGuid().ToString(),
                           lang = "zh-Hant",
                           title = "背景圖",
                           uri = uri,
                           model = "ArticleDownload",
                           column_name = "pic",
                           sub_column_name = "pic",
                           sort = ++sortIndex,
                           active = true,
                           created_at = DateTime.Now,
                           updated_at = DateTime.Now,
                           layout = "main",
                           options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 285 x 325" } })
                       },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

      


        public void consolidated(ModelBuilder builder)
        {
            string uri = "consolidated";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "西元年",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "year",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                    },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "季",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "month",
                          sub_column_name = "",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "season" }, { "required", "required" }, { "notes", "" } })
                      },


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "檔案",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "files",
                          sub_column_name = "file",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "N" }, { "required", "required" }, { "notes", "" } })
                      },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );





        }

        public void certification(ModelBuilder builder)
        {
            string uri = "certification";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "分類",
                         uri = uri,
                         model = "ArticleDownload",
                         column_name = "category_id",
                         sub_column_name = "",
                         sort = ++sortIndex,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"ArticleCategory\",\"uri\":\"certification\",\"parent_id\":\"certification\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                     },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "副標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "details",
                        sub_column_name = "sub_title",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                     new Columns()
                     {
                         id = Guid.NewGuid().ToString(),
                         lang = "zh-Hant",
                         title = "圖片",
                         uri = uri,
                         model = "ArticleDownload",
                         column_name = "pic",
                         sub_column_name = "pic",
                         sort = ++sortIndex,
                         active = true,
                         created_at = DateTime.Now,
                         updated_at = DateTime.Now,
                         layout = "main",
                         options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "pic" }, { "plural", "N" }, { "required", "required" }, { "notes", "建議大小 351 x 451" } })
                     },

                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "檔案",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "files",
                          sub_column_name = "file",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "Y" }, { "required", "required" }, { "notes", "" } })
                      },
                   new Columns()
                   {
                       id = Guid.NewGuid().ToString(),
                       lang = "zh-Hant",
                       title = "內容敘述",
                       uri = uri,
                       model = "ArticleDownload",
                       column_name = "details",
                       sub_column_name = "editor",
                       sort = ++sortIndex,
                       active = true,
                       created_at = DateTime.Now,
                       updated_at = DateTime.Now,
                       layout = "main",
                       options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "editor" }, { "required", "required" }, { "notes", "" } })
                   },
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void reports(ModelBuilder builder)
        {
            string uri = "reports";
            int sortIndex = 0;

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "西元年",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "year",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "year" }, { "required", "required" }, { "notes", "" } })
                    },
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "季",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "month",
                          sub_column_name = "",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "season" }, { "required", "required" }, { "notes", "" } })
                      },
                  
                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "檔案",
                          uri = uri,
                          model = "ArticleDownload",
                          column_name = "files",
                          sub_column_name = "file",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "file" }, { "plural", "N" }, { "required", "required" }, { "notes", "" } })
                      },
                
                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = "ArticleDownload",
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }


        public void QuestionnaireGroup(ModelBuilder builder)
        {
            string uri = "";
            int sortIndex = 0;

            string model = "QuestionnaireGroup";

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = model,
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "分類",
                        uri = uri,
                        model = model,
                        column_name = "parent_id",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "hidden" }, { "required", "" }, { "notes", "" }, { "value", "responsibility" } })
                    },


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = model,
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public void QuestionnaireItem(ModelBuilder builder)
        {
            string uri = "";
            int sortIndex = 0;

            string model = "QuestionnaireItem";

            builder.Entity<Columns>().HasData(


                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "標題",
                        uri = uri,
                        model = model,
                        column_name = "title",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "main",
                        options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "text" }, { "required", "required" }, { "notes", "" } })
                    },
                    


                      new Columns()
                      {
                          id = Guid.NewGuid().ToString(),
                          lang = "zh-Hant",
                          title = "欄位位置",
                          uri = uri,
                          model = model,
                          column_name = "category_id",
                          sub_column_name = "",
                          sort = ++sortIndex,
                          active = true,
                          created_at = DateTime.Now,
                          updated_at = DateTime.Now,
                          layout = "main",
                          options = "{\"view\":\"select\",\"required\":\"required\",\"notes\":\"\",\"model\":\"QuestionnaireGroup\",\"uri\":\"\",\"parent_id\":\"\",\"getCategory\":\"1\",\"limit\":\"1\"}"
                      },

                 

                        new Columns()
                        {
                            id = Guid.NewGuid().ToString(),
                            lang = "zh-Hant",
                            title = "設計區塊",
                            uri = uri,
                            model = model,
                            column_name = "options",
                            sub_column_name = "",
                            sort = ++sortIndex,
                            active = true,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            layout = "main",
                            options = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "view", "formOptions" }, { "required", "required" }, { "notes", "" } })
                        },

                    new Columns()
                    {
                        id = Guid.NewGuid().ToString(),
                        lang = "zh-Hant",
                        title = "啟用狀態",
                        uri = uri,
                        model = model,
                        column_name = "active",
                        sub_column_name = "",
                        sort = ++sortIndex,
                        active = true,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now,
                        layout = "adv",
                        options = "{\"view\":\"radio\",\"required\":\"\",\"notes\":\"\",\"checked\":\"True\",\"value\":{\"True\":\"啟用\",\"False\":\"停用\"}}"
                    }




                );




        }

        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
