﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;

namespace WebApp.Seeder
{
    public class LanguageResourceSeeder
    {
        public void run(ModelBuilder builder, List<LanguageResource> resources)
        {
            builder.Entity<LanguageResource>().HasData(resources);
        }
           

    
    }
}
