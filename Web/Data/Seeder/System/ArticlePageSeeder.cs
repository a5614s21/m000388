﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;
using WebApp.Services;

namespace WebApp.Seeder
{
    public class ArticlePageSeeder
    {
        public List<LanguageResource> run(ModelBuilder builder)
        {
            return this.data(builder);
        }



        public List<LanguageResource> data(ModelBuilder builder)
        {

            List<string> ids = new List<string>()
            {
                "Overview",
                "Organization",
                "Vision",
                "Stakeholder",
                "Security",
                "Contact",
                "Legal",
                "Logo",
                "Privacy",

                "Awards",
                "Recruitment",
                "JobApply",
                "TrainingDevelopment",
                "IncentivesBenefits",
                "HRSystem",
                "HealthManagement",

                "Careers1",
                "Careers2",
                "Careers3",

                "AMessageFromTheCEO",

                "SustainableManagement",
                "InnovationsandServices",
                "ResponsibleSupplyChain",
                "Environmentfriendly",
                "SocialProsperity",
                "HappyWorkplace",

                "StakeholderCommunication",
                "CSRQuestionnaire",

                "CertificationOverview",


            };

            List<string> uris = new List<string>()
            {
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",

                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",

                "Careers",
                "Careers",
                "Careers",

                "Sustainability",

                "CS",
                "CS",
                "CS",
                "CS",
                "CS",
                "CS",

                "Sustainability",
                "Sustainability",

                "About",

            };


            List<string> category_id = new List<string>()
            {
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",
                "About",

                 "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",
                "Careers",


                 "Sustainability",

                "CS",
                "CS",
                "CS",
                "CS",
                "CS",
                "CS",

                "Sustainability",
                "Sustainability",

                "About",
            };


            List<ArticlePage> articlePage = new List<ArticlePage>();
            int i = 1;
            foreach (string val in ids)
            {
                articlePage.Add(new ArticlePage()
                {
                    id = val,
                    uri = uris[(i - 1)],
                    category_id = category_id[(i - 1)],
                    sort = i,
                    active = true                  
                });
                i++;
            }


            builder.Entity<ArticlePage>().HasData(
                articlePage
              );


            List<string> title = new List<string>
            {
                "SPIL Overview & Fact Sheet",
                "Organization",
                "Vision & Core Value",
                "Stakeholder Engagement",
                "Business Continuity and Security Policy",
                "Contact",
                "Legal",
                "Logo",
                "Privacy",


                "Awards",
                "Recruitment",
                "Job Apply",
                "Training & Development",
                "Incentives & Benefits",
                "HR System",
                "Health Management",

                "研發成果與專利",
                "研發佈局",
                "我要應徵",


                "A Message From The CEO",

                "Sustainable Management",
                "Innovations and Services",
                "Responsible Supply Chain",
                "Environment friendly",
                "Social Prosperity",
                "Happy Workplace",

                "Stakeholder Communication",
                "CSR Questionnaire",

                "Certification Overview",
            };

            List<string> twTitle = new List<string>
            {
                "SPIL Overview & Fact Sheet",
                "Organization",
                "Vision & Core Value",
                "Stakeholder Engagement",
                "Business Continuity and Security Policy",
                "Contact",
                "Legal",
                "Logo",
                "Privacy",

                "Awards",
                  "Recruitment",
                "Job Apply",
                "Training & Development",
                "Incentives & Benefits",
                "HR System",
                "Health Management",

                "研發成果與專利",
                "研發佈局",
                "我要應徵",

                 "A Message From The CEO",
                "Sustainable Management",
                "Innovations and Services",
                "Responsible Supply Chain",
                "Environment friendly",
                "Social Prosperity",
                "Happy Workplace",
                "Stakeholder Communication",
                "CSR Questionnaire",

                "Certification Overview",
            };

            List<string> jpTitle = new List<string>
            {
                "SPIL Overview & Fact Sheet",
                "Organization",
                "Vision & Core Value",
                "Stakeholder Engagement",
                "Business Continuity and Security Policy",
                "Contact",
                "Legal",
                "Logo",
                "Privacy",

                "得獎紀錄",
                "人才招募說明",
                "線上人才招募系統",
                "人才發展",
                "薪酬福利",
                "人力資源發展系統",
                "健康管理",

                "研發成果與專利",
                "研發佈局",
                "我要應徵",

                 "A Message From The CEO",
                "Sustainable Management",
                "Innovations and Services",
                "Responsible Supply Chain",
                "Environment friendly",
                "Social Prosperity",
                "Happy Workplace",
                "Stakeholder Communication",
                "CSR Questionnaire",

                "Certification Overview",
            };


            List<string> editor = new List<string>
            {
                 MapPath("Content/Templates/About/AboutOverview.html"),
                 MapPath("Content/Templates/About/AboutOrganization.html"),
                 MapPath("Content/Templates/About/AboutVision.html"),
                 MapPath("Content/Templates/About/AboutStakeholder.html"),
                 MapPath("Content/Templates/About/AboutSecurity.html"),
                 MapPath("Content/Templates/About/Contact.html"),

                 MapPath("Content/Templates/About/Legal.html"),
                 MapPath("Content/Templates/About/Logo.html"),
                 MapPath("Content/Templates/About/Privacy.html"),


                  MapPath("Content/Templates/Careers/Awards.html"),
                  MapPath("Content/Templates/Careers/Recruitment.html"),
                  MapPath("Content/Templates/Careers/JobApply.html"),
                  MapPath("Content/Templates/Careers/TrainingDevelopment.html"),
                  MapPath("Content/Templates/Careers/IncentivesBenefits.html"),
                  MapPath("Content/Templates/Careers/HRSystem.html"),
                  MapPath("Content/Templates/Careers/HealthManagement.html"),

                  MapPath("Content/Templates/Careers/Careers1.html"),
                  MapPath("Content/Templates/Careers/Careers2.html"),
                  MapPath("Content/Templates/Careers/Careers3.html"),

                  MapPath("Content/Templates/Sustainability/1.html"),
                  MapPath("Content/Templates/Sustainability/2.html"),
                  MapPath("Content/Templates/Sustainability/3.html"),
                  MapPath("Content/Templates/Sustainability/4.html"),
                  MapPath("Content/Templates/Sustainability/5.html"),
                  MapPath("Content/Templates/Sustainability/6.html"),
                  MapPath("Content/Templates/Sustainability/7.html"),
                  MapPath("Content/Templates/Sustainability/8.html"),
                  MapPath("Content/Templates/Sustainability/9.html"),

                  "",

            };



            List<string> twEditor = new List<string>
            {
                 MapPath("Content/Templates/About/AboutOverview.html"),
                 MapPath("Content/Templates/About/AboutOrganization.html"),
                 MapPath("Content/Templates/About/AboutVision.html"),
                 MapPath("Content/Templates/About/AboutStakeholder.html"),
                 MapPath("Content/Templates/About/AboutSecurity.html"),
                 MapPath("Content/Templates/About/Contact.html"),
                 MapPath("Content/Templates/About/Legal.html"),
                 MapPath("Content/Templates/About/Logo.html"),
                 MapPath("Content/Templates/About/Privacy.html"),

                  MapPath("Content/Templates/Careers/Awards.html"),
                  MapPath("Content/Templates/Careers/RecruitmentTw.html"),
                  MapPath("Content/Templates/Careers/JobApplyTw.html"),
                  MapPath("Content/Templates/Careers/TrainingDevelopmentTw.html"),
                  MapPath("Content/Templates/Careers/IncentivesBenefitsTw.html"),
                  MapPath("Content/Templates/Careers/HRSystemTw.html"),
                  MapPath("Content/Templates/Careers/HealthManagementTw.html"),

                  MapPath("Content/Templates/Careers/Careers1.html"),
                  MapPath("Content/Templates/Careers/Careers2.html"),
                  MapPath("Content/Templates/Careers/Careers3.html"),

                  MapPath("Content/Templates/Sustainability/1.html"),
                  MapPath("Content/Templates/Sustainability/2.html"),
                  MapPath("Content/Templates/Sustainability/3.html"),
                  MapPath("Content/Templates/Sustainability/4.html"),
                  MapPath("Content/Templates/Sustainability/5.html"),
                  MapPath("Content/Templates/Sustainability/6.html"),
                  MapPath("Content/Templates/Sustainability/7.html"),
                  MapPath("Content/Templates/Sustainability/8.html"),
                  MapPath("Content/Templates/Sustainability/9.html"),

                  "",

            };



            List<LanguageResource> resources = new List<LanguageResource>();

            i = 0;
            foreach (ArticlePage item in articlePage)
            {

                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticlePage",
                    ArticlePageId = item.id,
                    language_id = "en",
                    category_id = category_id[i],
                    title = title[i].ToString(),              
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,         
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticlePage",
                    ArticlePageId = item.id,
                    language_id = "zh-TW",
                    category_id = category_id[i],
                    title = twTitle[i].ToString(),
                 
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", twEditor[i].ToString() }}),
                    active = true,
               
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                resources.Add(new LanguageResource()
                {
                    id = (new Helper()).GenerateIntID(),
                    model_type = "ArticlePage",
                    ArticlePageId = item.id,
                    language_id = "zh-CN",
                    category_id = category_id[i],
                    title = jpTitle[i].ToString(),
                
                    details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "editor", editor[i].ToString() } }),
                    active = true,
                  
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                });


                i++;
            }


            return resources;


        }


        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
