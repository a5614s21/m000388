﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Newtonsoft.Json;
using System.Web;
using Microsoft.Extensions.Hosting;

namespace WebApp.Seeder
{
    public class SystemItemSeeder
    {
        public void run(ModelBuilder builder)
        {
            this.data(builder);
        }



        public void data(ModelBuilder builder)
        {

         

            builder.Entity<SystemItem>().HasData(

                 new SystemItem()
                 {
                     id = "smtp",
                     lang = "zh-Hant",
                     title = "電子郵件伺服器",
                     uri = "smtp",
                     active = true,
                     created_at = DateTime.Now,
                     updated_at = DateTime.Now,
                     sort = 1,
                     details = JsonConvert.SerializeObject(new Dictionary<string, object>() {  
                         { "host", "smtp.mailtrap.io" },
                         { "port" , "2525" },
                         { "username" , "b17fd995f4ed71" },
                         { "password" , "482b512a814c4e" },
                         { "encryption" , "tls" },
                         { "fromAddress" , "test@minmax.biz" },
                         { "fromName" , "Minmax" },

                     }),
                 }

                

                );


           

        }

    

        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
