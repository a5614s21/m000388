$(function () {

    jQuery.validator.setDefaults({
        // This will ignore all hidden elements alongside `contenteditable` elements
        // that have no `name` attribute
        ignore: ":hidden, [contenteditable='true']:not([name])"
    });

    //後台表單驗證
    $("#saveForm").each(function () {

        $("#saveForm").validate({


            submitHandler: function (form) {

                //有圖片檔案上傳-----------------------------------------
                let picArr = new Array();
                let columns = new Array();

                $('.uploadFilePic').each(function () {
                    let obj = {};
                    let title = $(this).find($('.' + $(this).data('filed') + '_title')).val() ?? '';
                    let youtube = $(this).find($('.' + $(this).data('filed') + '_youtube')).val() ?? '';
                    obj = { path: $(this).data('filename'), title: title, youtube: youtube, column: $(this).data('filed') };

                    picArr.push(obj);

                    if (columns.indexOf($(this).data('filed')) == -1) {
                        columns.push($(this).data('filed'));
                    }
                });


                let picData = {};

                if (columns.length > 0) {
                    columns.forEach(item => {

                        let data = new Array();
                        let i = 0;
                        picArr.forEach(pic => {
                            if (pic.column == item) {
                                let temp = pic;
                                delete temp.column;
                                data[i] = temp;
                                i++;
                            }
                        });

                        let col = item.split('_');
                        if (item != "pic_path_pic") {
                            picData[col[1]] = data;
                        }
                        else {
                            picData["path_pic"] = data;
                        }


                    });
                }
                $('#pic').val(JSON.stringify(picData));
                //END----------------------------------------------
           
                //有檔案上傳-----------------------------------------
                let fileArr = new Array();
                let fileColumns = new Array();

                $('.uploadFile').each(function () {
                    let obj = {};
                    let title = $(this).find($('.' + $(this).data('filed') + '_title')).val() ?? '';
                    //let youtube = $(this).find($('.' + $(this).data('filed') + '_youtube')).val() ?? '';
                    obj = { path: $(this).data('filename'), title: title, column: $(this).data('filed') };

                    fileArr.push(obj);

                    if (fileColumns.indexOf($(this).data('filed')) == -1) {
                        fileColumns.push($(this).data('filed'));
                    }
                });

                let fileData = {};

                if (fileColumns.length > 0) {
                    fileColumns.forEach(item => {

                        let data = new Array();
                        let i = 0;
                        fileArr.forEach(file => {
                            if (file.column == item) {
                                let temp = file;
                                delete temp.column;
                                data[i] = temp;
                                i++;
                            }
                        });

                        let col = item.split('_');

                        fileData[col[1]] = data;

                    });
                }
                $('#files').val(JSON.stringify(fileData));
                //END----------------------------------------------
                console.log(fileData);
                console.log(form);

                $('#alltitle').each(function () {
                    console.log(document.getElementById('alltitle').innerHTML);
                });
          
         

                //有影片-----------------------------------------
                let videoArr = new Array();
                let videoColumns = new Array();

                $('.uploadVideo').each(function () {
                    let obj = {};
                    let title = $(this).find($('.' + $(this).data('filed') + '_title')).val() ?? '';
                    let sub_title = $(this).find($('.' + $(this).data('filed') + '_sub_title')).val() ?? '';
                    let path = $(this).find($('.' + $(this).data('filed') + '_path')).val() ?? '';
                    obj = { path: path, column: $(this).data('filed'), title: title, sub_title: sub_title };

                    videoArr.push(obj);

                    if (videoColumns.indexOf($(this).data('filed')) == -1) {
                        videoColumns.push($(this).data('filed'));
                    }
                });
            
                let videoData = {};

                if (videoColumns.length > 0) {
                    videoColumns.forEach(item => {

                        let data = new Array();
                        let i = 0;
                        videoArr.forEach(video => {
                            if (video.column == item) {
                                let temp = video;
                                delete temp.column;
                                data[i] = temp;
                                i++;
                            }
                        });

                        let col = item.split('_');

                        videoData = data;

                    });
                }
                $('#video').val(JSON.stringify(videoData));
                //END----------------------------------------------
              
                //權限使用
                $('#roles').each(function () {

                    let roles = new Array();
                    $('.permission-item').each(function () {

                        if ($(this).prop("checked")) {

                            let obj = {};
                            //obj = { $(this).attr('id') };
                            roles.push($(this).attr('id'));
                        }
                    });

                    if (roles.length > 0) {
                        $('#roles').val(JSON.stringify(roles));
                    }
                });

                //判斷欄位ID


                $('#Spec').each(function () {

             

                    let needVal = new FormData();
                    // let $this = this;

                    needVal.append('spec', $('#Spec').val());
                    needVal.append('id', $("input[name='id']").val());


                    $.ajax({
                        type: 'POST',
                        url: '/Siteadmin/QuestionnaireItem/CheckSpec',
                        data: needVal,
                        // dataType: 'json',
                        cache: false,
                        contentType: false,
                        processData: false,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (json) {

                            if (json == "N") {

                                swal({
                                    title: "系統資訊",
                                    text: "此欄位ID已被使用!",
                                    type: "warning",
                                    showCancelButton: false,
                                    confirmButtonColor: "#e7505a",
                                    confirmButtonText: "確定!",
                                    closeOnConfirm: false,
                                    cancelButtonText: "取消",
                                });

                                return false;

                            }
                            if (json == "blank") {

                                swal({
                                    title: "系統資訊",
                                    text: "欄位ID不可以有空格!",
                                    type: "warning",
                                    showCancelButton: false,
                                    confirmButtonColor: "#e7505a",
                                    confirmButtonText: "確定!",
                                    closeOnConfirm: false,
                                    cancelButtonText: "取消",
                                });

                                return false;

                            }


                         
                        },
                    });



                });

              




                try {
 if (document.getElementById('alltitle').innerHTML == "每月營收") {
                        console.log(fileData.file);
                        if (fileData.file) {
                            swal({
                                title: "系統資訊",
                                text: "確定新增或異動資料?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#e7505a",
                                confirmButtonText: "確定!",
                                closeOnConfirm: false,
                                cancelButtonText: "取消",
                            },
                                function () {

                                    $('.submitBut').attr("disabled", true);

                                    form.submit();
                                    return false;
                                });
                        }
                        else {
                            swal({
                                title: "系統資訊",
                                text: "請選擇檔案",
                                type: "warning",
                                showCancelButton: false,
                                showConfirmButton: false,
                                buttons: false,
                                timer: 1000
                            });
                        }
                    }
                    else if (document.getElementById('alltitle').innerHTML == "每季營運報告") {
                        console.log(fileData.file);
                        if (fileData.file) {
                            swal({
                                title: "系統資訊",
                                text: "確定新增或異動資料?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#e7505a",
                                confirmButtonText: "確定!",
                                closeOnConfirm: false,
                                cancelButtonText: "取消",
                            },
                                function () {

                                    $('.submitBut').attr("disabled", true);

                                    form.submit();
                                    return false;
                                });
                        }
                        else {
                            swal({
                                title: "系統資訊",
                                text: "請選擇檔案",
                                type: "warning",
                                showCancelButton: false,
                                showConfirmButton: false,
                                buttons: false,
                                timer: 1000
                            });
                        }
                    }
                    else if (document.getElementById('alltitle').innerHTML == "合併財務報表") {
                        console.log(fileData.file);
                        if (fileData.file) {
                            swal({
                                title: "系統資訊",
                                text: "確定新增或異動資料?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#e7505a",
                                confirmButtonText: "確定!",
                                closeOnConfirm: false,
                                cancelButtonText: "取消",
                            },
                                function () {

                                    $('.submitBut').attr("disabled", true);

                                    form.submit();
                                    return false;
                                });
                        }
                        else {
                            swal({
                                title: "系統資訊",
                                text: "請選擇檔案",
                                type: "warning",
                                showCancelButton: false,
                                showConfirmButton: false,
                                buttons: false,
                                timer: 1000
                            });
                        }
                    }
                    else if (document.getElementById('alltitle').innerHTML == "財務報告") {
                        console.log(fileData.file);
                        if (fileData.file) {
                            swal({
                                title: "系統資訊",
                                text: "確定新增或異動資料?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#e7505a",
                                confirmButtonText: "確定!",
                                closeOnConfirm: false,
                                cancelButtonText: "取消",
                            },
                                function () {

                                    $('.submitBut').attr("disabled", true);

                                    form.submit();
                                    return false;
                                });
                        }
                        else {
                            swal({
                                title: "系統資訊",
                                text: "請選擇檔案",
                                type: "warning",
                                showCancelButton: false,
                                showConfirmButton: false,
                                buttons: false,
                                timer: 1000
                            });
                        }
                    }
                    else if (document.getElementById('alltitle').innerHTML == "公司年報") {
                        console.log(fileData.file);
                        if (fileData.file) {
                            swal({
                                title: "系統資訊",
                                text: "確定新增或異動資料?",
                                type: "warning",
                                showCancelButton: true,
                                confirmButtonColor: "#e7505a",
                                confirmButtonText: "確定!",
                                closeOnConfirm: false,
                                cancelButtonText: "取消",
                            },
                                function () {

                                    $('.submitBut').attr("disabled", true);

                                    form.submit();
                                    return false;
                                });
                        }
                        else {
                            swal({
                                title: "系統資訊",
                                text: "請選擇檔案",
                                type: "warning",
                                showCancelButton: false,
                                showConfirmButton: false,
                                buttons: false,
                                timer: 1000
                            });
                        }
                    }
                    else {
                        swal({
                            title: "系統資訊",
                            text: "確定新增或異動資料?",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#e7505a",
                            confirmButtonText: "確定!",
                            closeOnConfirm: false,
                            cancelButtonText: "取消",
                        },
                            function () {

                                $('.submitBut').attr("disabled", true);

                                form.submit();
                                return false;
                            });
                    }
                }
                catch (e) {
                    swal({
                        title: "系統資訊",
                        text: "確定新增或異動資料?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#e7505a",
                        confirmButtonText: "確定!",
                        closeOnConfirm: false,
                        cancelButtonText: "取消",
                    },
                        function () {

                            $('.submitBut').attr("disabled", true);

                            form.submit();
                            return false;
                        });
                }
                   

              
               
             

            },
            errorPlacement: function (error, element) {
                element.attr('style', 'border:#FF0000 1px solid;');
                element.parents('.form-group').find("._formErrorMsg").html('<div style="color: #FF0000; padding-bottom: 10px; padding-left: 10px;">' + error.text() + '</div>');

            },
            success: function (error, element) {
               // element.attr('style', '');
                //console.log(element);
                return false;
            }
        });



    });


    //編輯器
    // Summernote
  /*  $('.editor').summernote({
        placeholder: '' +
            '',
        height: 350,
        lang: 'zh-TW',
        content_css: ['/style/style.css'],
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['insert', ['link', 'image', 'elfinder', 'video', 'hr']],
            ['view', ['fullscreen', 'codeview']],
        ],
        callbacks: {
            onInit: function (container) {
                $('.btn-submit').on('click', function () {
                    $(".editor").each(function (index) {
                        if ($(this).summernote('codeview.isActivated')) {
                            $(this).summernote('codeview.deactivate');
                        }
                    });
                });
            }
        }
    });*/


    const mceElf = new tinymceElfinder({
        // connector URL (Use elFinder Demo site's connector for this demo)
        url: TinyMCE_ELFINDER_POPUP_URL,
        // upload target folder hash for this tinyMCE
        uploadTargetHash: 'l1_lw', // l3 MCE_Imgs on elFinder Demo site for this demo
        // elFinder dialog node id
        nodeId: 'elfinder'
    });


    tinymce.init({
        selector: '.editor',
        language: "zh_TW",
        // base_url: '/static/admin/modules/tinymce',
        plugins: [
            'preview', 'searchreplace', 'autolink', 'directionality', 'code',
            'visualblocks', 'visualchars', 'fullscreen', 'image', 'link',
            'media', 'template', 'table', 'charmap', 'hr',
            'pagebreak', 'nonbreaking', 'anchor', 'insertdatetime', 'lists',
            'imagetools', 'noneditable'
        ],
        toolbar: 'fontselect fontsizeselect'
            + ' | bold italic strikethrough underline forecolor backcolor'
            + ' | link image media'
            + ' | alignleft aligncenter alignright alignjustify'
            + ' | numlist bullist outdent indent'
            + ' | removeformat',
        menubar: 'edit insert format table view',
        importcss_append: true,
        image_advtab: true,
        relative_urls: false,
        convert_urls: false,
        cleanup_on_startup: false,
        trim_span_elements: false,
        cleanup: false,
        verify_html: false,
        valid_children: '+a[div|h1|h2|h3|h4|h5|h6|p|ul|ol]',
        extended_valid_elements: "style,script[src],link[href|rel],i[id|class|style]",
        custom_elements: "style,~style,script,~script,link,~i",
        noneditable_noneditable_class: 'mceNonEditable',
        branding: false,
        font_formats: '微軟正黑=microsoft jhenghei'
            + '; 微軟雅黑=microsoft yahei'
            + '; 新細明體=times new roman,times,pmingliu'
            + '; 黑體=heiti tc,heiti sc; Arial=arial,helvetica,sans-serif'
            + '; Georgia=georgia,palatino; Helvetica=helvetica'
            + '; Tahoma=tahoma,arial,helvetica,sans-serif'
            + '; Verdana=verdana,geneva',
        fontsize_formats: '12px 14px 16px 18px 24px 36px 48px',
        height: '350px',
        content_css: ['/styles/css/style.css'],
        templates: null,
        //language: 'en',
        file_picker_callback: mceElf.browser,
        images_upload_handler: mceElf.uploadHandler,
        imagetools_cors_hosts: [thisUrl] // set CORS for this demo

    });



    //刪除圖片
    $("body").delegate(".delFile", "click", function () {
        $(this).parents('.imgLayOut').remove();
    });

  //刪除資料
    $("body").delegate(".delItem", "click", function () {

        let id = $(this).attr('data-id');

        swal({
            title: "系統資訊",
            text: "確定刪除此筆資料?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#e7505a",
            confirmButtonText: "確定!",
            closeOnConfirm: false,
            cancelButtonText: "取消",
        },
            function () {

                delData(id);
                return false;
            });

      
    });

    //全選
    $("body").delegate(".selAll", "click", function () {

        let ckeched = $(this).prop('checked');


            $('.delId').each(function () {
                $(this).prop('checked', ckeched);
            });
          
      
        });

    //刪除全部資料
    $("body").delegate(".delAllItem", "click", function () {

        let id = new Array();

        $('.delId').each(function () {
            if ($(this).prop('checked')) {
                id.push($(this).val());
            }
        });
        if (id.length == 0) {
            swal('請選擇一項需刪除的項目', '', 'error');
            return false;
        }

        swal({
            title: "系統資訊",
            text: "確定刪除資料?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#e7505a",
            confirmButtonText: "確定!",
            closeOnConfirm: false,
            cancelButtonText: "取消",
        },
            function () {

                delData(id);
                return false;
            });

      
    });


    //異動狀態
    $("body").delegate(".changeCActive", "click", function () {

        let status = 'False';
        let statusText = '未處理';
        let statusClass = "badge-secondary";



        if ($(this).attr("data-active") == "False") {
            status = 'True';
            statusText = '已處理';
            statusClass = "badge-danger";
        }

        let needVal = new FormData();

        let id = $(this).data('id');
        needVal.append('id', id);
        needVal.append('active', status);


        $.ajax({
            type: 'POST',
            url: '/Siteadmin/' + $('#ControllerName').val() + '/ChangeActive',
            data: needVal,
            // dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (json) {

            
                if (json != "") {
                    console.log(json);
                     status = 'True';
                     statusText = '未處理';
                     statusClass = "badge-secondary";

                    if (json == "False") {
                        status = 'False';
                        statusText = '已處理';
                        statusClass = "badge-danger";
                    }


                    $('.status' + id + '_text').text(statusText);

                    $('.status' + id + '_text').removeClass("badge-danger");
                    $('.status' + id + '_text').removeClass("badge-secondary");

                    $('.status' + id + '_text').addClass(statusClass);

                    $('.status' + id + '_text').attr("data-active", status);

                }
                else {
                    $('.status' + id + '_text').text(statusText);

                    $('.status' + id + '_text').removeClass("badge-danger");
                    $('.status' + id + '_text').removeClass("badge-secondary");

                    $('.status' + id + '_text').addClass(statusClass);

                    $('.status' + id + '_text').attr("data-active", status);
                }
            },
        });


    });

    //異動狀態
    $("body").delegate(".changeActive", "click", function () {

        let status = 'False';
        let statusText = '停用';
        let statusClass = "badge-secondary";



        if ($(this).attr("data-active") == "False") {
            status = 'True';
            statusText = '啟用';
            statusClass = "badge-danger";
        }

        let needVal = new FormData();

        let id = $(this).data('id');
        needVal.append('id', id);
        needVal.append('active', status);


        $.ajax({
            type: 'POST',
            url: '/Siteadmin/' + $('#ControllerName').val() + '/ChangeActive',
            data: needVal,
            // dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (json) {


                if (json != "") {
                    console.log(json);
                    status = 'True';
                    statusText = '停用';
                    statusClass = "badge-secondary";

                    if (json == "False") {
                        status = 'False';
                        statusText = '啟用';
                        statusClass = "badge-danger";
                    }


                    $('.status' + id + '_text').text(statusText);

                    $('.status' + id + '_text').removeClass("badge-danger");
                    $('.status' + id + '_text').removeClass("badge-secondary");

                    $('.status' + id + '_text').addClass(statusClass);

                    $('.status' + id + '_text').attr("data-active", status);

                }
                else {
                    $('.status' + id + '_text').text(statusText);

                    $('.status' + id + '_text').removeClass("badge-danger");
                    $('.status' + id + '_text').removeClass("badge-secondary");

                    $('.status' + id + '_text').addClass(statusClass);

                    $('.status' + id + '_text').attr("data-active", status);
                }
            },
        });


    });

    //異動置頂
    $("body").delegate(".changeTop", "click", function () {

        let status = 'False';
        let statusText = '停用';
        let statusClass = "badge-secondary";



        if ($(this).attr("data-active") == "False") {
            status = 'True';
            statusText = '啟用';
            statusClass = "badge-danger";
        }

        let needVal = new FormData();

        let id = $(this).data('id');
        needVal.append('id', id);
        needVal.append('active', status);


        $.ajax({
            type: 'POST',
            url: '/Siteadmin/' + $('#ControllerName').val() + '/ChangeTop',
            data: needVal,
            // dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (json) {


                if (json != "") {
                    console.log(json);
                    status = 'True';
                    statusText = '停用';
                    statusClass = "badge-secondary";

                    if (json == "False") {
                        status = 'False';
                        statusText = '啟用';
                        statusClass = "badge-danger";
                    }


                    $('.statustop' + id + '_text').text(statusText);

                    $('.statustop' + id + '_text').removeClass("badge-danger");
                    $('.statustop' + id + '_text').removeClass("badge-secondary");

                    $('.statustop' + id + '_text').addClass(statusClass);

                    $('.statustop' + id + '_text').attr("data-active", status);

                }
                else {
                    $('.statustop' + id + '_text').text(statusText);

                    $('.statustop' + id + '_text').removeClass("badge-danger");
                    $('.statustop' + id + '_text').removeClass("badge-secondary");

                    $('.statustop' + id + '_text').addClass(statusClass);

                    $('.statustop' + id + '_text').attr("data-active", status);
                }
            },
        });


    });

    //異動排序
    $("body").delegate(".sortValue", "change", function () {


        let needVal = new FormData();



        let id = $(this).data('id');
        needVal.append('id', id);
        needVal.append('sort', $(this).val());

        if (parseInt($(this).val(), 10)) {
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/' + $('#ControllerName').val() + '/ChangeSort',
                data: needVal,
                // dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {




                },
            });

        }
        else {
            swal('請輸入純數字!', '', 'error');
            $(this).val($(this).attr('data-sort'));
            return false;
        }

    });


    $("body").delegate(".changeSort", "click", function () {


        let type = $(this).attr('data-type');
        let sortValue = $('.' + $(this).attr('data-id') + "_sortValue").val();
        if (type == "up" && parseInt(sortValue) > 1) {
            sortValue = parseInt(sortValue) - 1;
        }
        if (type == "down") {
            sortValue = parseInt(sortValue) + 1;
        }


        $('.' + $(this).attr('data-id') + "_sortValue").val(sortValue);

        let needVal = new FormData();



        let id = $(this).data('id');
        needVal.append('id', id);
        needVal.append('sort', sortValue);

        
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/' + $('#ControllerName').val() + '/ChangeSort',
                data: needVal,
                // dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {




                },
            });


    });


    //查詢
    $("body").delegate(".table-search-input", "change", function () {

        searchVal["keyword"] = $(this).val();
        searchVal["col"] = $('.bs-select :selected').val();
        searchJson = JSON.stringify(searchVal);
        $("#tableList").DataTable().destroy();
        runDataTable(searchJson);

    });


    //篩選
    $("body").delegate(".findData", "change", function () {

     
        searchVal["keyword"] = $(this).val();
        searchVal["col"] = $(this).attr('data-col');        
       
        searchJson = JSON.stringify(searchVal);
        $("#tableList").DataTable().destroy();
        runDataTable(searchJson);
        return false;

    });


    $("body").delegate(".delVideo", "click", function () {

        $(this).parents('.showVideo').remove();

    });

});

////const { createApp } = Vue

//let videoLay = Vue.createApp({
//    //el: '#videoLay',
//    data() {
//        return {
//            items: [],
//            // videoNum: parseInt($('.addVideoBut').attr('data-num')),
//            videoNum: 0,
//        }
//    },
//    methods: {
//        //取得活動
//        addVideo(videoId) {

//            this.videoNum++;

//        },
//        delVideo() {
//            this.videoNum--;
//        }
//    },
//    watch: {


//        items: function () {

//            this.$nextTick(function () {



//            });

//        }


//    },
//}).mount('#videoLay');

//let videoLay = new Vue({
//    el: '#videoLay',
//    data: {
//        items: [],
//       // videoNum: parseInt($('.addVideoBut').attr('data-num')),
//        videoNum:0,
//    },
//    methods: {
//        //取得活動
//        addVideo(videoId) {

            
//          /*  for (let i = 0; i < this.videoNum ; i++)
//            {
//                let obj = {};

//                obj.path = $('#' + videoId + '_path').val();
//                obj.path = $('#' + videoId + '_title').val();
//                this.items.push(obj);
                
//            }

//            console.log(this.items);*/

//            this.videoNum++;
           
            

//          /*  let needVal = new FormData();
//            let $this = this;
//            needVal.append('guid', guid);
//            needVal.append('rentalsId', rentalsId);
//            needVal.append('nowDate', info.dateStr);

//            $.ajax({
//                type: 'POST',
//                url: '/admin/carRental/getCarData',
//                data: needVal,
//                // dataType: 'json',
//                cache: false,
//                contentType: false,
//                processData: false,
//                headers: {
//                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//                },
//                success: function (json) {

                  

//                },
//            });*/
//        },
//        delVideo() {
//            this.videoNum--;
//        }
//    },
//    watch: {


//        items: function () {

//            this.$nextTick(function () {



//            });

//        },
       

//    },
//    mounted: function () {

//    },
//});




function delData(id) {

    let needVal = new FormData();
    needVal.append('id', id);
 
    $.ajax({
        type: 'POST',
        url: '/Siteadmin/' + $('#ControllerName').val() + '/Delete',
        data: needVal,
        // dataType: 'json',
        cache: false,
        contentType: false,
        processData: false,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (json) {

            if (json == "HaveUser") {
                swal({
                    title: "系統資訊",
                    text: "尚有使用者使用該群組，請優先刪除使用者!",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#e7505a",
                    confirmButtonText: "確定!",
                    closeOnConfirm: false,
                    cancelButtonText: "取消",
                });
            }
            else if (json == "HaveNextData")
            {

                swal({
                    title: "系統資訊",
                    text: "尚有細項資訊，請優先刪除該細項資訊!",
                    type: "error",
                    showCancelButton: false,
                    confirmButtonColor: "#e7505a",
                    confirmButtonText: "確定!",
                    closeOnConfirm: false,
                    cancelButtonText: "取消",
                });

            }
            else {


                $("#tableList").DataTable().destroy();
                runDataTable(searchJson);
                swal.close();

               

            }





       
        
            return false;

        },
    });


}



function elfinderDialog(context) {
    // trigger the reveal modal with elfinder inside
    // context.$note[0].id is the field id
    let triggerUrl = ELFINDER_POPUP_URL.replace('__INPUT_ID__', context.$note[0].id);
    $.colorbox({
        href: triggerUrl,
        fastIframe: true,
        iframe: true,
        width: '70%',
        height: '50%'
    });
}

// function to update the file selected by elfinder
function processSelectedFile(filePath, requestingField) {
    $('#' + requestingField).summernote('editor.insertImage', filePath, function ($image) {
        $image.attr('style', '');
    });
}


$(document).on('click', '.popup_selector', function (event) {
    event.preventDefault();
    var updateID = $(this).attr('data-field'); // Btn id clicked
    var elfinderUrl = ELFINDER_POPUP_URL.replace('__INPUT_ID__', '');

    // trigger the reveal modal with elfinder inside
    var triggerUrl = elfinderUrl + updateID + '?type=' + $(this).attr('data-type');

    $.colorbox({
        href: triggerUrl,
        fastIframe: true,
        iframe: true,
        width: '70%',
        height: '50%'
    });

});











var $ajaxBody = $("body");

/**
 * Switch form data language
 */
$ajaxBody.on('click', '.form-local-option', function () {
    var $this = $(this);
    $.ajax({
        url: $this.attr('data-url'), type: 'PUT', data: { language: $this.attr('data-code') },
        success: function () { window.location.reload(true); }
    });
});

/**
 * Switch form data language active
 */
$ajaxBody.on('click', '#form-local-switch', function () {
    var $this = $(this);
    var switchTo = $this.attr('data-status') === '1' ? 0 : 1;
    $.ajax({
        url: $this.attr('data-url'), type: 'PATCH', data: { id: $this.attr('data-id'), switchTo: switchTo },
        success: function () {
            $this.attr('data-status', switchTo);
            if (switchTo === 1) {
                $this.text($this.attr('data-text-enable'));
                $this.removeClass('badge-secondary').addClass('badge-danger');
            } else {
                $this.text($this.attr('data-text-disable'));
                $this.removeClass('badge-danger').addClass('badge-secondary');
            }
        },
        error: function () {
            swal('Switch failed.', 'Switching language status get failed.', 'error');
        }
    });
});

/**
 * Datatable search field events
 */
$ajaxBody
    .on('change', '#sch_column', function () { $(".datatables").DataTable().draw(); })
    .on('change', 'select.sch_select', function () { $(".datatables").DataTable().draw(); })
    .on('change search', '#sch_keyword', function () { $(".datatables").DataTable().draw(); });

/**
 * Datatable bind event actions
 * - switch status
 * - change sort (after change number call update sort function)
 * - delete row
 */
$('#tableList')
    .on('click', '.badge-switch', function (e) {
        e.preventDefault();

        var $this = $(this);
        var url = $this.attr('data-url'),
            status = parseInt($this.attr('data-value')),
            id = $this.attr('data-id'),
            column = $this.attr('data-column');
        var switchTo = status === 1 ? 0 : 1;

        $.ajax({
            url: url, type: 'PATCH', dataType: 'json',
            data: { id: id, column: column, oriValue: status, switchTo: switchTo },
            success: function (result) {
                $this.attr('data-value', switchTo);
                if (result.hasOwnProperty('oriClass')) $this.removeClass(result.oriClass);
                if (result.hasOwnProperty('newClass')) $this.addClass(result.newClass);
            },
            complete: function () { $('#tableList').DataTable().draw(false); }
        });
        return false;
    })
    .on('click', '.updateSort', function (e) {
        e.preventDefault();

        var $this = $(this);
        var id = $this.attr('data-guid'), column = $this.attr('data-column');
        var $input = $('#' + column + '_' + id);
        var index = parseInt($input.val());

        switch ($this.attr('data-act')) {
            case 'up':
                $input.val(index - 1); break;
            case 'down':
                $input.val(index + 1); break;
        }
        $input.change();
        return false;
    })
    .on('click', '.delItem', function (e) {
        e.preventDefault();

        var $thisForm = $(this).parents('form');
        var trashItem = $(this).hasClass('trashItem');

        swal({
            title: getLanguage(sweetAlertLanguage, trashItem ? 'single_trash.title' : 'single_delete.title'),
            text: getLanguage(sweetAlertLanguage, trashItem ? 'single_trash.text' : 'single_delete.text'),
            type: "info",
            cancelButtonText: getLanguage(sweetAlertLanguage, trashItem ? 'single_trash.cancelButtonText' : 'single_delete.cancelButtonText'),
            confirmButtonText: getLanguage(sweetAlertLanguage, trashItem ? 'single_trash.confirmButtonText' : 'single_delete.confirmButtonText'),
            confirmButtonClass: "btn-danger",
            showCancelButton: true,
            closeOnConfirm: false
        }, function (result) {
            if (result) $thisForm.submit();
        });
    });



/**
 * SystemDraft 草稿
 */
$('.draft-area').each(function () {
    var $draftArea = $(this);
    var $draftMenu = $('.draft-menu', $draftArea);
    var formId = $draftArea.attr('data-form-id');
    var url = $('.draft-url', $draftArea).val();
    var previewUrl = $('.draft-preview-url', $draftArea).val();
    var autoSaver;

    var getDrafts = function () {
        $.post($('.draft-get-url', $draftArea).val(), { 'url': url }, function (response) {
            $('div.dropdown-item, div.dropdown-divider', $draftMenu).remove();
            if (response.data.length > 0) {
                $draftMenu.append('<div class="dropdown-divider"></div>');
                for (var row of response.data) {
                    if (row.hasOwnProperty('id') && row.hasOwnProperty('title')) {
                        $draftMenu.append(
                            '<div class="dropdown-item d-flex justify-content-between" style="cursor: default" title="' + row.time + '">' +
                            '<div class="mr-3"><i class="icon-time mr-2 text-muted"></i>' + row.title + '</div>' +
                            '<div>' +
                            (row.title === row.time ? '<button class="btn btn-outline-default btn-sm destroy-btn" type="button" data-id="' + row.id + '" title="Remove"><i class="icon-bin"></i></button>' : '') +
                            '<a class="btn btn-outline-default btn-sm" href="' + url + '?draft=' + row.id + '" title="Load"><i class="icon-pencil"></i></a>' +
                            (previewUrl === '' ? '' : '<a class="btn btn-outline-default btn-sm" href="' + previewUrl + '?draft=' + row.id + '" target="_blank" title="Preview"><i class="icon-eye"></i></a>') +
                            '</div></div>'
                        );
                    }
                }
            }
        }, 'json');
    };

    var storeDraft = function (type = 'auto') {
        if (typeof CKEDITOR !== 'undefined') {
            if (Object.keys(CKEDITOR.instances).length > 0) {
                for (var k in CKEDITOR.instances) {
                    if (CKEDITOR.instances.hasOwnProperty(k)) {
                        CKEDITOR.instances[k].updateElement();
                    }
                }
            }
        }

        if (typeof tinymce !== 'undefined') {
            tinymce.triggerSave();
        }

        $.post($('.draft-store-url', $draftArea).val(),
            { 'type': type, 'url': url, 'attributes': $('#' + formId).serializeArray() },
            function (response) {
                getDrafts(formId, url);
                if (type === 'manual') {
                    swal(response.msg, 'success');
                }
            }, 'json')
            .fail(function (response) {
                if (type === 'manual') {
                    swal(response.msg, 'error');
                }
            });
    };

    $draftArea.on('click', '.store-btn', function () {
        storeDraft('manual');
    });

    $draftArea.on('click', '.destroy-btn', function () {
        $.post($('.draft-destroy-url', $draftArea).val(), { 'id': $(this).attr('data-id') },
            function () {
                getDrafts(formId, url);
            }, 'json');
    });

    $('#' + formId).on('change', function () {
        clearTimeout(autoSaver);
        autoSaver = setTimeout(function () { storeDraft('auto'); }, 1500);
    });

    getDrafts();
});

/**
 * Datatable single row update sort
 * @param {string} column
 * @param {*} id
 */
function updateSort(column, id) {
    var $input = $('#' + column + '_' + id);
    var url = $input.attr('data-url'), index = parseInt($input.val());
    if (index < 0) index = 0;

    $.ajax({
        url: url, type: 'PATCH', dataType: 'json',
        data: { id: id, column: column, index: index },
        success: function () { $('#tableList').DataTable().draw(false); },
        error: function (response) { console.log(response); }
    });
}

/**
 * Datatable batch delete rows
 * @param {string} url
 * @param {string} column
 * @param {*} status
 */
function multiSwitch(url, column, status) {
    var checkedSet = [];
    $('#tableList tbody .checkboxes input').each(function () {
        if ($(this).prop('checked')) checkedSet.push($(this).val());
    });
    if (checkedSet.length > 0) {
        swal({
            title: getLanguage(sweetAlertLanguage, 'multi_switch.title'),
            text: getLanguage(sweetAlertLanguage, 'multi_switch.text'),
            type: "info",
            showCancelButton: true,
            cancelButtonText: getLanguage(sweetAlertLanguage, 'multi_switch.cancelButtonText'),
            confirmButtonText: getLanguage(sweetAlertLanguage, 'multi_switch.confirmButtonText'),
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        }, function (result) {
            if (result) {
                $.ajax({
                    url: url, type: 'PATCH', dataType: 'json',
                    data: { selected: checkedSet, column: column, switchTo: status },
                    success: function (result) {
                        $('#tableList').DataTable().draw(false);
                        $('#tableList .group-checkable').prop('checked', false);
                        swal.close();
                    },
                    error: function (response) {
                        swal(getLanguage(sweetAlertLanguage, 'multi_switch.error_title'),
                            getLanguage(sweetAlertLanguage, 'multi_switch.error_text'),
                            'error');
                    }
                });
            }
        });
    } else {
        swal(getLanguage(sweetAlertLanguage, 'selected_none.title'),
            getLanguage(sweetAlertLanguage, 'selected_none.text'),
            'error');
    }
}

/**
 * Datatable batch delete rows
 * @param {string} url
 * @param {boolean} trashcan
 */
function multiDelete(url, trashcan = false) {
    var checkedSet = [];
    $('#tableList tbody .checkboxes input').each(function () {
        if ($(this).prop('checked')) checkedSet.push($(this).val());
    });
    if (checkedSet.length > 0) {
        swal({
            title: getLanguage(sweetAlertLanguage, trashcan ? 'multi_trash.title' : 'multi_delete.title'),
            text: getLanguage(sweetAlertLanguage, trashcan ? 'multi_trash.text' : 'multi_delete.text'),
            type: "info",
            showCancelButton: true,
            cancelButtonText: getLanguage(sweetAlertLanguage, trashcan ? 'multi_trash.cancelButtonText' : 'multi_delete.cancelButtonText'),
            confirmButtonText: getLanguage(sweetAlertLanguage, trashcan ? 'multi_trash.confirmButtonText' : 'multi_delete.confirmButtonText'),
            confirmButtonClass: "btn-danger",
            closeOnConfirm: false
        }, function (result) {
            if (result) {
                $.ajax({
                    url: url, type: 'DELETE', dataType: 'json',
                    data: { selected: checkedSet },
                    success: function (result) {
                        $('#tableList').DataTable().draw(false);
                        $('#tableList .group-checkable').prop('checked', false);
                        swal.close();
                    },
                    error: function (response) {
                        swal(getLanguage(sweetAlertLanguage, trashcan ? 'multi_trash.error_title' : 'multi_delete.error_title'),
                            getLanguage(sweetAlertLanguage, trashcan ? 'multi_trash.error_text' : 'multi_delete.error_text'),
                            'error');
                    }
                });
            }
        });
    } else {
        swal(getLanguage(sweetAlertLanguage, 'selected_none.title'),
            getLanguage(sweetAlertLanguage, 'selected_none.text'),
            'error');
    }
}

/**
 * Form field Address.
 *
 * @param {string} id
 * @param {Object} urls
 * @param {Object} defaults
 */
function initAddress(id, urls, defaults) {
    var $country = $('#' + id + '-country');
    $country.on('change', function () {
        var $countrySelect = $(this);
        var $stateSelect = $('#' + id + '-state');
        var $emptyOption = $('option:first-child', $stateSelect);
        $stateSelect.empty().append($emptyOption);
        if ($countrySelect.val() === '' || !urls.hasOwnProperty('states')) {
            $stateSelect.selectpicker('refresh');
            $stateSelect.change();
        } else {
            var url = urls.states.replace(/\/id\//, '/' + $countrySelect.val() + '/');
            $.get(url, function (response) {
                for (var item of response) {
                    if (item.hasOwnProperty('id') && item.hasOwnProperty('name')) {
                        $stateSelect.append('<option value="' + item.id + '">' + item.name + '</option>');
                    }
                }
                $stateSelect.selectpicker('refresh');
                if (defaults.state !== '' && defaults.country === $countrySelect.val()) {
                    $stateSelect.selectpicker('val', defaults.state).change();
                } else {
                    $stateSelect.change();
                }
            });
        }
    });
    $('#' + id + '-state').on('change', function () {
        var $stateSelect = $(this);
        var $countySelect = $('#' + id + '-county');
        var $emptyOption = $('option:first-child', $countySelect);
        $countySelect.empty().append($emptyOption);
        if ($stateSelect.val() === '' || !urls.hasOwnProperty('counties')) {
            $countySelect.selectpicker('refresh');
            $countySelect.change();
        } else {
            var url = urls.counties.replace(/\/id\//, '/' + $stateSelect.val() + '/');
            $.get(url, function (response) {
                for (var item of response) {
                    if (item.hasOwnProperty('id') && item.hasOwnProperty('name')) {
                        $countySelect.append('<option value="' + item.id + '">' + item.name + '</option>');
                    }
                }
                $countySelect.selectpicker('refresh');
                if (defaults.county !== '' && defaults.state === $stateSelect.val()) {
                    $countySelect.selectpicker('val', defaults.county).change();
                } else {
                    $countySelect.change();
                }
            });
        }
    });
    $('#' + id + '-county').on('change', function () {
        var $countySelect = $(this);
        var $citySelect = $('#' + id + '-city');
        var $emptyOption = $('option:first-child', $citySelect);
        $citySelect.empty().append($emptyOption);
        if ($countySelect.val() === '' || !urls.hasOwnProperty('cities')) {
            $citySelect.selectpicker('refresh');
            $citySelect.change();
        } else {
            var url = urls.cities.replace(/\/id\//, '/' + $countySelect.val() + '/');
            $.get(url, function (response) {
                for (var item of response) {
                    if (item.hasOwnProperty('id') && item.hasOwnProperty('zip') && item.hasOwnProperty('name')) {
                        $citySelect.append('<option value="' + item.id + '" data-zip="' + item.zip + '">' + item.name + '</option>');
                    }
                }
                $citySelect.selectpicker('refresh');
                if (defaults.city !== '' && defaults.county === $countySelect.val()) {
                    $citySelect.selectpicker('val', defaults.city).change();
                } else {
                    $citySelect.change();
                }
            });
        }
    });
    $('#' + id + '-city').on('change', function () {
        var $citySelect = $(this);
        var $zipText = $('#' + id + '-zip');
        if ($citySelect.val() !== '') {
            $zipText.val($('option:selected', $citySelect).attr('data-zip'));
        } else {
            $zipText.val('');
        }
    });

    if (defaults.country !== '') {
        $country.selectpicker('val', defaults.country).change();
    }
}




