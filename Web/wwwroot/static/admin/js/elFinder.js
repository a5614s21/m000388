var fileUpload = Array();
fileUpload["field"] = "";
fileUpload["types"] = "";
fileUpload["lay"] = "";
fileUpload["multiple"] = "";
$(document).ready(function () {
    /*取得圖片*/
    $("body").delegate(".getImages", "click", function () {


        fileUpload["field"] = $(this).attr('data-field');
        fileUpload["types"] = $(this).attr('data-type');
        fileUpload["lay"] = $(this).attr('data-lay');
        fileUpload["multiple"] = $(this).attr('data-multiple');

        //檔案管理器



        elFinder.prototype.i18.zh_TW.messages['cmdimportBut'] = '嵌入';
        elFinder.prototype._options.commands.push('importBut');
        // 
        elFinder.prototype.commands.importBut = function () {

            this.exec = function (hashes) {
                //do whatever

                var file = this.files(hashes);


                //圖檔直接加入
                if (file[0].mime != 'directory') {
                    // console.log(fileUpload["field"]);

                    for (var p = 0; p < file.length; p++) {

                        if (file[p].mime != 'directory') {

                            var hash = decodeURI(file[p].hash);
                            var fm = this.fm;
                            var url = fm.url(hash);

                            console.log(fileUpload);

                            if (fileUpload["lay"] == 'area') {

                                var addImg = "";
                                var i = 1;
                                setUploadFile(fileUpload["types"], fileUpload["lay"], url, fileUpload["field"], hash, fileUpload["multiple"]);

                            }
                            else {

                                if ($('#siteadminArea').val().indexOf($('#projeftNo').val()) != -1) {
                                    url = url.replace($('#webPath').val(), "") + '"';
                                }
                                url = $('#AppURL').val() + "/" + url;

                                addImg = '<img src="' + url + '"  >';
                                var oEditor = CKEDITOR.instances[fileUpload["field"]];
                                var html = addImg;

                                var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
                                oEditor.insertElement(newElement);

                            }



                        }
                        else {

                            this.exec('quicklook');//開啟資料夾

                        }



                    }
                }
                else {

                    this.exec('quicklook');//開啟資料夾

                }
            }
            this.getstate = function () {
                //return 0 to enable, -1 to disable icon access
                return 0;
            }
        };


        var myCommands = elFinder.prototype._options.commands;
        var disabled = ['extract', 'archive', 'help', 'select'];
        $.each(disabled, function (i, cmd) {
            (idx = $.inArray(cmd, myCommands)) !== -1 && myCommands.splice(idx, 1);
        });
        var selectedFile = null;
        var options = {
            url: $('#fileMegRoot').val() + "/"+ fileUpload["field"],
            commands: myCommands,
            lang: 'zh_TW',
            // onlyMimes: ['application/pdf'], 
            locale: 'utf-8',
            uiOptions: {
                toolbar: [
                    ['back', 'forward'],
                    ['reload'],
                    ['home', 'up'],
                    ['mkdir', 'mkfile', 'upload'],
                    ['open', 'download'],
                    ['info'],
                    ['quicklook'],
                    ['copy', 'cut', 'paste'],
                    ['rm'],
                    ['duplicate', 'rename', 'edit', 'resize'],
                    ['view', 'sort', 'importBut']
                ]
            },
            contextmenu: {

                // current directory file menu
                files: [
                    'open', 'quicklook', 'sharefolder', '|', 'download', '|', 'copy', 'cut', 'paste', 'rm', '|', 'rename', '|', 'importBut', '|', 'info'
                ]
            },
            reloadClearHistory: true,
            rememberLastDir: false,

            resizable: false,
            height: '560px',
            handlers: {
                select: function (event, elfinderInstance) {

                    if (event.data.selected.length == 1) {

                        var item = $('#' + event.data.selected[0]);
                        if (!item.hasClass('directory')) {
                            selectedFile = event.data.selected[0];
                            $('#elfinder-selectFile').show();
                            return;
                        }

                    }
                    $('#elfinder-selectFile').hide();
                    selectedFile = null;
                },


                //上傳檔案監聽
                upload: function (event, elfinderInstance) {





                    if (event.data.removed != null) {

                        console.log('add');

                        var arr = Array();

                        for (var i = 0; i < event.data.added.length; i++) {

                            var $this = event.data.added[i];


                            var encPath = $this.hash.substr($this.hash.indexOf('_') + 1);

                            var tests = decode64(encPath).split('\\');


                            var url = '';

                            for (var s = 1; s < (tests.length - 1); s++) {
                                url = url + "/" + tests[s];
                            }

                            url = url + '/' + $this.name
                            arr[i] = url;


                        }

                        if (arr.length > 0) {
                            var vals = JSON.stringify(arr);
                            useFTP("Upload", vals);
                        }


                    }


                    return false;
                },
                //移除時
                rm: function (event, elfinderInstance) {

                },

                //判斷點選兩下時
                dblclick: function (event, elfinderInstance) {

                    event.preventDefault();
                    elfinderInstance.exec('importBut');
                    return false;


                },


            }
        };
        $('#elfinder').elfinder(options).elfinder('instance');
        $('#modal_file').on('shown.bs.modal', function () { $(window).resize(); });
    

        /* $('#elfinder-selectFile').click(function () {
             if (selectedFile != null)
                 $.post('files/selectFile', { target: selectedFile }, function (response) {
                     alert(response);
                 });
    
         });*/





    });




    $("body").delegate(".movePic", "click", function () {


        var picDataArr = $(this).attr('data-id').split('.');



        var li = $('#' + picDataArr[0]);
        var act = $(this).attr('data-act');

        if (act == "left") { li.prev().before(li); }
        else { li.next().after(li); }

        return false;

    });

    //移除檔案
    $("body").delegate(".delFiles", "click", function () {

        $('#' + $(this).attr('data-id')).remove();

    });
});


/**
 * 建置上傳圖片或檔案
 */
function setUploadFile(types, lay, url, field, hash, multiple) {




    if (types.indexOf("image") != -1) {

        if (lay == "area") {
            addImg = "";
            addImg += '<div class="card uploadFile" id="' + field + '_img' + hash + '" style="float:left;" data-filename="' + url + '" data-filed="' + field+'">';
            addImg += ' <input class="form-control form-control-sm mb-1 ' + field + '_alt" type="text" value="" placeholder="圖片敘述"  />';
            addImg += '<a class="thumb" href="' + url + '" data-fancybox=""><span class="imgFill imgLiquid_bgSize imgLiquid_ready" style="background-image: url(&quot;' + url + '&quot;); background-size: cover; background-position: center center; background-repeat: no-repeat;"><img src="' + url + '" class="imgData fileData"  data-file="' + url + '" style="display: none;"></span></a>';
            addImg += '<div class="btn-group btn-group-sm justify-content-center">';
            addImg += '<button class="btn btn-outline-default movePic" type="button" title="排列往前" data-act="left" data-id="' + field + '_img' + hash + '"><i class="icon-chevron-thin-left"></i></button>';
            // addImg += '<button class="btn btn-outline-default open_modal_picname" type="button" title="設定" data-target="#modal_picname" data-toggle="modal" data-access="alt_' + field + '_' + field + '_img' + hash + '" data-filename="' + url + '"><i class="icon-wrench"></i></button>';
            addImg += '<button class="btn btn-outline-default delFiles" type="button" data-guid="" data-id="' + field + '_img' + hash + '" data-field=\"' + field + '\" data-filename="' + url + '"><i class="icon-trash2"></i></button>';
            addImg += '<button class="btn btn-outline-default movePic" type="button" title="排列往後" data-act="right" data-id="' + field + '_img' + hash + '"><i class="icon-chevron-thin-right"></i></button>';
            addImg += '</div>';
            addImg += '<input type="hidden" name="alt_' + field + '_' + field + '_img' + hash + '" id="alt_' + field + '_' + field + '_img' + hash + '" class="form-control altData" placeholder="描述(alt)" >';
            addImg += '</div>';

            if (multiple == "Y") {
                $('#' + field + "_img").append(addImg);
            }
            else {
                $('#' + field + "_img").html(addImg);
            }


        }
        else {

            //  console.log(url.replace($('#webPath').val(), ""));

            addImg = '<img src="../../../' + url.replace($('#webPath').val(), "") + '"  >';
            var oEditor = CKEDITOR.instances[field];
            var html = addImg;

            var newElement = CKEDITOR.dom.element.createFromHtml(html, oEditor.document);
            oEditor.insertElement(newElement);
        }
    }
    //檔案
    else {

        var temp = url.split('/');
        var fileExName = temp[temp.length - 1];
        var id = guid();
        $('#' + field + '_img').html('<div class="fileData" data-file="' + url + '" id="file_' + id + '"><a href="' + url + '" target="_blank">' + decodeURI(fileExName) + '</a> <button type="button" class="delFiles" data-field="' + field + '" data-id="file_' + id + '"><i class="icon-trash"></i></button></div>');

    }
}



function guid() {
    function s4() {
        return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
        s4() + '-' + s4() + s4() + s4();
}