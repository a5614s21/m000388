let GaScript = new Vue({
    el: '#ga',
    data: {
        pathViewCount: 0,
        socialCount: 0,
        totalWebViewNum: 0,
        userType: ['0', '0'],
        searchEngine: null,
        keywords: null,
        year: null,

    },
    methods: {
        //年度瀏覽次數
        getPathViewCount() {

            let needVal = new FormData();
         
            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GetPathViewCount',
                data: needVal,
                // dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                    $this.pathViewCount = json;
                },
            });

        },

        //社群網站來源數
        getSocialCount() {

            let needVal = new FormData();
      
            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GetSocialCount',
                data: needVal,
                // dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                    $this.socialCount = json;
                },
            });

        },

        //總瀏覽數
        getTotalWebViewNum() {

            let needVal = new FormData();

            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GetTotalWebViewNum',
                data: needVal,
                // dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                    $this.totalWebViewNum = json;
                },
            });

        },

        //新/舊訪問者
        getUserType() {

            let needVal = new FormData();

            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GetUserType',
                data: needVal,
                 dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                    $this.userType = json;
                },
            });

        },

        //搜尋引擎來源數量
        getOrganicSearches() {

            let needVal = new FormData();

            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GetOrganicSearches',
                data: needVal,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                   //$this.userType = json;
                    if (json != "null") {
                        $this.searchEngine = json;
                    }
                },
            });

        },


        //網站來源關鍵字
        getKeywords() {

            let needVal = new FormData();

            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GaKeywords',
                data: needVal,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                    //$this.userType = json;
                    if (json != "null") {
                        $this.keywords = json;
                    }
                },
            });

        },


        //每月資訊
        getYear() {

            let needVal = new FormData();

            let $this = this;
            $.ajax({
                type: 'POST',
                url: '/Siteadmin/Ga/GaYear',
                data: needVal,
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (json) {

                    $this.year = json;
                },
            });

        },

    },
    watch: {
        year: function () {

            this.$nextTick(function () {

                var chart = AmCharts.makeChart("analyticsAge", {
                    "theme": "light",
                    "type": "serial",
                    "theme": "light",
                    "dataProvider": this.year,
                    "valueAxes": [{
                        "gridColor": "#878787",
                        "gridAlpha": 0.2,
                        "dashLength": 0,
                        "color": "#878787"
                    }],
                    "gridAboveGraphs": true,
                    "startDuration": 1,
                    "graphs": [{
                        "balloonText": "[[category]]: <b>[[value]]</b>",
                        "fillAlphas": 0.8,
                        "lineAlpha": 0.2,
                        "type": "column",
                        "valueField": "visits",
                        "color": "#878787"
                    }],
                    "chartCursor": {
                        "categoryBalloonEnabled": false,
                        "cursorAlpha": 0,
                        "zoomable": false
                    },
                    "categoryField": "age",
                    "categoryAxis": {
                        "gridPosition": "start",
                        "gridAlpha": 0,
                        "tickPosition": "start",
                        "tickLength": 20,
                        "color": "#878787"
                    },
                    "export": {
                        "enabled": false
                    }
                });

            });

        },

        districts1: function () {


        },
    },
    mounted: function () {



    },

});