$(function() {
    $('.selectpicker').selectpicker({
    });
    $('.contact-select').selectpicker({
        noneSelectedText : '',
    });



    var w = $(window).width();
    var h = $(window).height();
    var $window = $(window);
    window.onresize=changeScreen;
    function changeScreen(){
        if(w > 800){
            location.reload();
        }
    }
        if(w > 1000){
            $('.menu-block').mouseenter(function(){
                $('.menu-class-block').stop(true, false).slideDown();
                $('.menu-class-mask').addClass('show');
            });
            $('.menu-block').mouseleave(function(){
                $('.menu-class-block').stop(true, false).slideUp();
                $('.menu-class-mask').removeClass('show');
            });
            setTimeout(function() {
                $('.menu-list').each(function(){
                    var menu_class=$(this).data('menu');
                    var menu_width=$(this).width();
                    $('.menu-class-box').find(menu_class).css('width',menu_width);
                });
            }, 500);

            $('.menu-list,.menu-class-list').mouseenter(function(){
                var menu_open = $(this).data('menu');
                $('.menu-box').find(menu_open).addClass('active');
                $('.menu-class-box').find(menu_open).addClass('active');
            });
            $('.menu-list,.menu-class-list').mouseleave(function(){
                var menu_open = $(this).data('menu');
                $('.menu-box').find(menu_open).removeClass('active');
                $('.menu-class-box').find(menu_open).removeClass('active');

            });
        }else{
            $('.menu-title').click(function(e){
                e.preventDefault();
                var num = $(this).parent('.menu-list').index();
                $(this).parent('.menu-list').toggleClass('active').find('.rwd-menu-class-box').slideToggle();
                $('.menu-list').not(':eq('+num+')').removeClass('active').find('.rwd-menu-class-box').slideUp();
            });
            $('.rwd-menu').click(function(){
                $(this).toggleClass('active');
                $('.menu-list').removeClass('active');
                $('.rwd-menu-class-box').slideUp();
                $('.menu-block').slideToggle();
            })
    }
    $('.search-button-box').click(function(){
        $('.search-box').toggleClass('search-active');
    })


    // 首頁slick
    $('.banner-box').slick({
        slidesToShow: 1,
        arrows: true,
        fade: true,
        dots: true,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
            {
              breakpoint: 1000,
              settings: {
                arrows: false,

              }
            }
          ]
    });
    $('.financial-new-box').slick({
        slidesToShow: 1,
        arrows: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 4000,
    });
    $('.invest-slick-box').slick({
        slidesToShow: 2,
        arrows: true,
        infinite: true,
        dots: false,
        autoplay: true,
        autoplaySpeed: 4000,
        responsive: [
            {
              breakpoint: 800,
              settings: {
                slidesToShow: 1,
                arrows: false,
                autoplay: true,
              }
            }
          ]
    });
    $('.invest-slick-box').on('beforeChange', function(event, slick, currentSlide, nextSlide) {
        $('.invest-slick').removeClass('show');
    });

    $('.invest-slick-box').on('afterChange', function(event, slick, currentSlide, nextSlide) {
        $('.slick-active').addClass('show');
    });
    // 數字跑動
    $('.run-thousand').each(function() {
        var check = true,
            $this = $(this),
            thisNumber = $this.data('number');
        var comma_separator_number_step = $.animateNumber.numberStepFactories.separator(',');
        $(window).scroll(function() {
            var windowTop = $(window).scrollTop() + $(window).height(),
                thisTop = $this.offset().top;
            if (windowTop >= thisTop && $this.hasClass('decimal') && check) {
                var decimal_places = 0,
                    decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);
                $this.animateNumber({
                    number: thisNumber * decimal_factor,
                    numberStep: comma_separator_number_step
                }, 500);
                check = false;
            } else if (windowTop >= thisTop && !$this.hasClass('decimal') && check) {
                $this.animateNumber({
                    number: thisNumber,
                    numberStep: comma_separator_number_step
                }, 500);
                check = false;
            }
        });
    });
    $('.language-box').click(function(){
        $('.lang-list-box').stop(true,false).slideToggle();
    })
    // 動畫觸發
    $('.fade-up,.fade,.fade-left,.fade-right').waypoint(function(direction) {
      if (direction === 'down') {
        $(this.element).addClass('a-play')
      }
    }, {
      offset: '85%'
    });
    // rwd分類標籤
    var class_title =  $('.finance-class-box').find('.active a').text();
    $('.rwd-finance-box').text(class_title);
    if(w <= 800){
        $('.rwd-finance-box').click(function(){
            $('.finance-class-box').stop(true,false).slideToggle();
        })
    }
    // 拉霸
    $(".document-block").mCustomScrollbar({
        theme: "dark",
    });
    // 彈跳視窗
    $('.advertise-close,.advertise-mask').click(function(){
        $('.advertise-block').removeClass('open');
        $('.advertise-video-block').removeClass('open');
        $('.advertise-show-box').removeClass('close');
        $('.video-iframe').find('iframe').attr("src", "")
    })
    $('.video-close').click(function(){
        $('.advertise-video-block').removeClass('open');
        $('.advertise-show-box').removeClass('close');
        $('.video-iframe').find('iframe').attr("src", "")
    })
    $('.document-button').click(function(){
        $('.advertise-block').addClass('open');
    })
    $('.document-video').click(function(){
        var youtube = $(this).data("video");
        $('.advertise-show-box').addClass('close');
        $('.advertise-video-block').addClass('open');
        $('.video-iframe').find('iframe').attr("src", youtube);
    })
    // 產品分類
    $('.class-title').click(function(){
        $(this).parents('.more-class').toggleClass('active');
        $(this).parent('.first-class-title-box').next('.second-class-title-box').stop(true,false).slideToggle();
    })

    // top
    $('.gotop-box').click(function() {
        var $body = (window.opera) ? (document.compatMode == "CSS1Compat" ? $('html') : $('body')) : $('html,body');
        $body.animate({
            scrollTop: 0
        }, 0);
        return false;
    });

  // about
    $('.about-more-class').click(function(){
        var index = $(this).parent('.about-class').index();
        $(this).toggleClass('active');
        $(this).next('.about-class-more').slideToggle();
    })
    var class_title = $('.about-class-box').find('.about-class.active').find('.about-class-title').text();
    $('.rwd-about-class-title').text(class_title);
    $('.rwd-about-class-title').click(function(){
        $('.about-class-box').slideToggle();
    })
    // 浮動header
    $(window).scroll(function() {
        if ($(this).scrollTop() > 0) {
            $('header').addClass('header2');
        } else {
            $('header').removeClass('header2');
        }
    });
    $('.chart-list').each(function(){
        var proportion = $(this).find('.chart-num').text();
        $(this).find('.chart-line-num').css('width',proportion);
    })

    $('.bottom-privacy-button').click(function(){
        $('.bottom-privacy-block').remove();
    })

})

AOS.init({
    easing: 'ease-out-back',
    duration: 2000,
    once: true
});
onElementHeightChange(document.body, function() {
    AOS.refresh();
});

function onElementHeightChange(elm, callback) {
    var lastHeight = elm.clientHeight
    var newHeight;

    (function run() {
        newHeight = elm.clientHeight;
        if (lastHeight !== newHeight) callback();
        lastHeight = newHeight;

        if (elm.onElementHeightChangeTimer) {
            clearTimeout(elm.onElementHeightChangeTimer);
        }

        elm.onElementHeightChangeTimer = setTimeout(run, 200);
    })();
}


window.onscroll = () => {
    let cHeight = document.documentElement.clientHeight;
    let sHeight = document.documentElement.scrollHeight;
    let sTop = document.documentElement.scrollTop;
    let all_Height = cHeight + sTop;
    let footer_height = sHeight - 50;
    let bottom_top = document.getElementsByClassName('gotop-box')[0];
    if (all_Height >= footer_height) {
        bottom_top.classList.add("gottop_bottom");
    } else {
        bottom_top.classList.remove("gottop_bottom");
    }
}