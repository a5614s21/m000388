﻿using Google.Apis.AnalyticsReporting;
using Google.Apis.AnalyticsReporting.v4;
using Google.Apis.AnalyticsReporting.v4.Data;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using System.Globalization;
using WebApp.Models;

namespace WebApp.Services
{
    public class GaService
    {
        private string GaCode = "";
        private GoogleCredential googleCredential;
        private AnalyticsReportingService analyticsReporting = new AnalyticsReportingService();

        public GaService()
        {
            var appSeting = (new Helper()).GetAppSettings();
            GaCode = appSeting["GaCode"].ToString();
            googleCredential = GoogleCredential.FromFile(Path.GetFullPath("Areas/Siteadmin/Ga/m000366-51469e468c72.json")).CreateScoped(AnalyticsReportingService.Scope.Analytics);

            analyticsReporting = new AnalyticsReportingService(
                                        new BaseClientService.Initializer
                                        {
                                            HttpClientInitializer = googleCredential,
                                            ApplicationName = "GA Lab"
                                        });
        }

        public dynamic GetGaData(DateRange dateRange, string dimensionStr, string metricStr)
        {
            //var dimension = new Dimension { Name = "ga:pagePath" };
            //var metric = new Metric { Expression = "ga:pageviews" };

            var dimension = new Dimension { Name = dimensionStr };
            var metric = new Metric { Expression = metricStr };

            var dimensionFilter = new DimensionFilter
            {
                DimensionName = dimensionStr,
                Expressions = new List<string> { "" }
            };

            var dimensionFilterClause = new DimensionFilterClause { Filters = new List<DimensionFilter> { dimensionFilter } };

            //var dateRange = new DateRange { StartDate = "2022-01-01", EndDate = "2022-01-31" };

            var reportRequest = new ReportRequest
            {
                ViewId = GaCode,
                Metrics = new List<Metric> { metric },
                Dimensions = new List<Dimension> { dimension },
                DimensionFilterClauses = new List<DimensionFilterClause> { dimensionFilterClause },
                DateRanges = new List<DateRange> { dateRange }
            };

            var reportGetRequest = new GetReportsRequest { ReportRequests = new List<ReportRequest> { reportRequest } };

            var response = analyticsReporting.Reports.BatchGet(reportGetRequest).Execute();

            return response;
        }

        /// <summary>
        /// 瀏覽數量
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaPageCount(DateRange dateRange)
        {          
            return GetGaData(dateRange, "ga:pagePath", "ga:pageviews");
        }

        /// <summary>
        /// 搜尋引擎來源數量
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaOrganicSearches(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:fullReferrer", "ga:organicSearches");

            List<GaModel> gaModel = new List<GaModel>();

            TextInfo textInfo = new CultureInfo("es-ES", false).TextInfo;

            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel.Add( new GaModel{ source = textInfo.ToTitleCase(item.Dimensions[0]) , count = int.Parse(item.Metrics[0].Values[0]) });
            }

            return gaModel;
        }

        /// <summary>
        /// 關鍵字
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaKeywords(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:keyword", "ga:organicSearches");

            List<GaModel> gaModel = new List<GaModel>();

            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel.Add(new GaModel { source = item.Dimensions[0], count = int.Parse(item.Metrics[0].Values[0]) });
            }

            return gaModel;
        }

        /// <summary>
        /// 社群網站來源數
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaSocialCount(DateRange dateRange)
        {
            return GetGaData(dateRange, "ga:socialNetwork", "ga:organicSearches");
        }

        /// <summary>
        /// 總瀏覽次數
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaTotalWebViewNum(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:year", "ga:visits");
            int num = 0;
            foreach (var item in response.Reports[0].Data.Rows)
            {
                num = num + int.Parse(item.Metrics[0].Values[0]);
            }

            return num;
        }

        /// <summary>
        /// 新/舊訪問者
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaUserType(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:userType", "ga:users");

            List<GaModel> gaModel = new List<GaModel>();

            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel.Add(new GaModel { source = item.Dimensions[0], count = int.Parse(item.Metrics[0].Values[0]) });
            }

            return gaModel;
        }

        /// <summary>
        /// 每月數量
        /// </summary>
        /// <param name="dateRange"></param>
        /// <returns></returns>
        public dynamic GaYear(DateRange dateRange)
        {
            var response = GetGaData(dateRange, "ga:month", "ga:visits");

            List<GaMonthModel> gaModel = new List<GaMonthModel>();

           
            for(int i=0;i<12;i++)
            {
                gaModel.Add(new GaMonthModel { age = (i+1) + "月", visits = "0" });
            }


            int s = 0;
            foreach (var item in response.Reports[0].Data.Rows)
            {
                gaModel[s].visits = item.Metrics[0].Values[0];
                s++;

            }

            return gaModel;
        }

    }


}
