﻿using Microsoft.Extensions.Localization;
using System.Reflection;
using WebApp.Models;

namespace WebApp.Services
{
    public class LocalService
    {

        private readonly IStringLocalizer _localizer;

        public LocalService(IStringLocalizerFactory factory)
        {
            var type = typeof(ShareResources);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _localizer = factory.Create("ShareResources", assemblyName.Name);



        }

        public LocalizedString GetLocalizedHtmlString(string key)
        {
            return _localizer[key];
        }

    }
}
