﻿using WebApp.Data;
using WebApp.Models;
using Web.Repositories.Admin;
using System.Linq.Dynamic.Core;
using Newtonsoft.Json;
using WebApp.Services;

namespace WebApp.Services
{
    public class DataTable
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();


        //


        /// <summary>
        /// DataTable資料整理
        /// </summary>
        /// <param name="model"></param>
        /// <param name="ControllerName"></param>
        /// <param name="_repository"></param>
        /// <param name="uri"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public List<Object> columns(dynamic model, string ControllerName, dynamic _repository, dynamic uri, string id, string lang = null)
        {

            List<Languages> languages = DB.Languages.Where(m => m.active == true).OrderBy(m => m.sort).ToList();//目前啟用的語系
            if (lang == null)
            {
                lang = languages[0].language_id;
            }


            List<Object> re = new List<Object>();

            Dictionary<String, Object> dataTableColnum = _repository.dataTableColnum(id);//取得欄位資訊

            var Json = Newtonsoft.Json.Linq.JArray.Parse(model);//第一層列表轉json陣列

            CipherService cipherService = new CipherService();

            string del = "Y";
            string edit = "Y";
            SystemMenu systemMenu = DB.SystemMenu.Where(m => m.model == ControllerName).FirstOrDefault();
            if (!string.IsNullOrEmpty(id))
            {
                string thisUri = id.ToString();
                SystemMenu systemMenuTemp = DB.SystemMenu.Where(m => m.model == ControllerName).Where(m => m.uri == thisUri.ToString()).FirstOrDefault();

                if (systemMenuTemp != null)
                {
                    systemMenu = systemMenuTemp;
                }
            }


            if (systemMenu.options != null && !string.IsNullOrEmpty(systemMenu.options.ToString()))
            {
                var SystemMenuDataOptions = Newtonsoft.Json.Linq.JArray.Parse("[" + systemMenu.options.ToString() + "]");
                del = SystemMenuDataOptions[0]["del"].ToString();

                if (SystemMenuDataOptions[0]["edit"] != null)
                {
                    edit = SystemMenuDataOptions[0]["edit"].ToString();
                }
            }

            // var test = Json[0]["NormalizedUserName"];



            foreach (var dataList in Json)
            {

                Dictionary<String, Object> rowData = new Dictionary<string, object>();

                foreach (KeyValuePair<string, object> item in dataTableColnum)
                {
                    dynamic tempJsonArray = null;

                    if (dataList["LangData"] != null)
                    {
                        tempJsonArray = Newtonsoft.Json.Linq.JArray.Parse(dataList["LangData"].ToString());//第一層列表轉json陣列
                    }





                    try
                    {
                        dynamic valJsonArray = null;
                        if (tempJsonArray != null)
                        {
                            valJsonArray = tempJsonArray[0];
                        }


                        switch (item.Key)
                        {

                            //核取按鈕
                            case "id":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/checkbox.cshtml");
                                    content = content.Replace("{id}", dataList["id"].ToString());
                                    rowData.Add(item.Key, content);
                                }




                                break;


                            //核取按鈕(管理者)
                            case "Id":

                                if (!string.IsNullOrEmpty(dataList["Id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/checkbox.cshtml");
                                    content = content.Replace("{id}", dataList["Id"].ToString());
                                    rowData.Add(item.Key, content);
                                }




                                break;

                            //特殊使用ID
                            case "useId":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    rowData.Add(item.Key, dataList["id"].ToString());
                                }

                                break;


                            //圖片
                            case "pic":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = "";

                                    if (!string.IsNullOrEmpty(valJsonArray[item.Key.ToString()].ToString()))
                                    {
                                        content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/pic.cshtml");

                                        try
                                        {
                                            var temp = valJsonArray["PicData"].pic[0];
                                            if (temp != null)
                                            {
                                                content = content.Replace("{pic}", temp.path.ToString());
                                            }

                                        }
                                        catch { }


                                    }

                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //取得關聯數量
                            case "qty":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    int qty = _repository.FindListCount(dataList["id"].ToString());
                                    rowData.Add(item.Key, qty);
                                }

                                break;

                            //下層數量
                            case "nextCategory":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {

                                    dynamic options = null;

                                    if (dataList["options"] != null && !string.IsNullOrEmpty(dataList["options"].ToString()))
                                    {
                                        options = Newtonsoft.Json.Linq.JArray.Parse("[" + dataList["options"].ToString() + "]");
                                        options = options[0];
                                    }
                                    //lang
                                    int qty = _repository.FindListCount(dataList["id"].ToString() , lang);



                                    string content = "";

                                    if (options == null || (options["add"] == null && options["add"].ToString() != "N"))
                                    {
                                        content = "<a class=\"btn btn-info\" role =\"button\" title=\"細項\" href=\"/Siteadmin/" + ControllerName + "/List/" + dataList["id"].ToString() + "\">( " + qty + " )</a>";
                                    }


                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //分類
                            case "category_id":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string title = "";
                                    title = _repository.GetCategoryTitle(dataList[item.Key.ToString()].ToString(), lang);
                                    try
                                    {


                                    }
                                    catch { }

                                    rowData.Add(item.Key, title);
                                }

                                break;
                            //欄位型態
                            case "type":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string title = "";


                                    dynamic options = Newtonsoft.Json.Linq.JArray.Parse("[" + valJsonArray["options"].ToString() + "]");

                                    switch (options[0]["type"].ToString())
                                    {
                                        case "text":
                                            title = "文字欄位";

                                            break;
                                        case "checkbox":
                                            title = "複選";

                                            break;
                                        case "radio":
                                            title = "單選";

                                            break;
                                    }

                                    try
                                    {
                                        // Dictionary<string, string> options = JsonConvert.DeserializeObject<Dictionary<string, string>>(valJsonArray["options"].ToString());


                                    }
                                    catch { }

                                    rowData.Add(item.Key, title);
                                }

                                break;


                            //日期
                            case "created_at":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = "";
                                    if (valJsonArray != null)
                                    {
                                        if (!string.IsNullOrEmpty(valJsonArray[item.Key.ToString()].ToString()))
                                        {
                                            content = DateTime.Parse(valJsonArray[item.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm");
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(dataList[item.Key.ToString()].ToString()))
                                        {
                                            content = DateTime.Parse(dataList[item.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm");
                                        }
                                    }


                                    rowData.Add(item.Key, content);
                                }

                                break;


                            case "start_at":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = "";
                                    if (valJsonArray != null)
                                    {
                                        string temptable = valJsonArray["model_type"];
                                        if (!string.IsNullOrEmpty(valJsonArray[item.Key.ToString()].ToString()))
                                        {
                                            if (temptable == "ArticleNews")
                                            {
                                                content = DateTime.Parse(valJsonArray[item.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                            }
                                            else
                                            {
                                                content = DateTime.Parse(valJsonArray[item.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (!string.IsNullOrEmpty(dataList[item.Key.ToString()].ToString()))
                                        {
                                            string temptable = dataList["uri"];
                                            if (temptable == "news")
                                            {
                                                content = DateTime.Parse(dataList[item.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                            }
                                            else
                                            {
                                                content = DateTime.Parse(dataList[item.Key.ToString()].ToString()).ToString("yyyy-MM-dd HH:mm");
                                            }
                                        }
                                    }


                                    rowData.Add(item.Key, content);
                                }

                                break;


                            //平台
                            case "platform":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {

                                    string content = "";
                                    if (dataList[item.Key.ToString()].ToString() == "admin")
                                    {
                                        content = "後台";
                                    }
                                    else
                                    {
                                        content = "前台";

                                    }



                                    rowData.Add(item.Key, content);
                                }

                                break;
                            //狀態
                            case "rule":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {

                                    string content = "";
                                    if (bool.Parse(dataList[item.Key.ToString()].ToString()))
                                    {
                                        content = "允許";
                                    }
                                    else
                                    {
                                        content = "禁止";

                                    }



                                    rowData.Add(item.Key, content);
                                }

                                break;
                            //狀態
                            case "active":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/active.cshtml");


                                    content = content.Replace("{id}", dataList["id"].ToString());
                                    content = content.Replace("{active}", dataList["active"].ToString());

                                    if (bool.Parse(dataList[item.Key.ToString()].ToString()))
                                    {
                                        content = content.Replace("{title}", "啟用");
                                        content = content.Replace("{class}", "badge-danger");
                                    }
                                    else
                                    {
                                        content = content.Replace("{title}", "停用");
                                        content = content.Replace("{class}", "badge-secondary");

                                    }



                                    rowData.Add(item.Key, content);
                                }

                                break;
                            //置頂
                            case "top":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/top.cshtml");

                                    content = content.Replace("{id}", dataList["id"].ToString());
                                    if(dataList["top"].ToString() == null || dataList["top"].ToString() == "")
                                    {
                                        dataList["top"] = false;
                                        content = content.Replace("{active}", "false");
                                    }
                                    else
                                    {
                                        content = content.Replace("{active}", dataList["top"].ToString());
                                    }
                                    

                                    if (bool.Parse(dataList[item.Key.ToString()].ToString()))
                                    {
                                        content = content.Replace("{title}", "啟用");
                                        content = content.Replace("{class}", "badge-danger");
                                    }
                                    else
                                    {
                                        content = content.Replace("{title}", "停用");
                                        content = content.Replace("{class}", "badge-secondary");

                                    }



                                    rowData.Add(item.Key, content);
                                }

                                break;
                            //排序
                            case "sort":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/sort.cshtml");
                                    content = content.Replace("{sort}", dataList[item.Key.ToString()].ToString());
                                    content = content.Replace("{id}", dataList["id"].ToString());

                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //動作
                            case "act":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = "";

                                    switch (dataList[item.Key.ToString()].ToString())
                                    {
                                        case "login":
                                            content = "登入動作";
                                            break;
                                        case "add":
                                            content = "新增";
                                            break;
                                        case "edit":
                                            content = "修改";
                                            break;
                                        case "del":
                                            content = "刪除";
                                            break;
                                    }

                                    rowData.Add(item.Key, content);
                                }

                                break;


                            //異動
                            case "modify":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {

                                    string content = "";

                                    content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify.cshtml");

                                    if (del == "N")
                                    {
                                        content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify_only_edit.cshtml");
                                    }
                                    if (edit == "N")
                                    {
                                        content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify_only_del.cshtml");
                                    }


                                    content = content.Replace("{id}", dataList["id"].ToString());

                                    content = content.Replace("{editUrl}", "/Siteadmin/" + ControllerName + "/Edit/" + dataList["id"].ToString() + (!string.IsNullOrEmpty(id) ? "/" + id : ""));
                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //異動
                            case "modifyNoUri":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {

                                    string content = "";
                                    content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify.cshtml");

                                    if (del == "N")
                                    {
                                        content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify_only_edit.cshtml");
                                    }

                                    content = content.Replace("{id}", dataList["id"].ToString());

                                    content = content.Replace("{editUrl}", "/Siteadmin/" + ControllerName + "/Edit/" + dataList["id"].ToString());
                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //異動
                            case "modifyAdmin":

                                if (!string.IsNullOrEmpty(dataList["Id"].ToString()))
                                {

                                    string content = "";

                                    content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify.cshtml");

                                    if (del == "N")
                                    {
                                        content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/modify_only_edit.cshtml");
                                    }




                                    content = content.Replace("{id}", dataList["Id"].ToString());

                                    content = content.Replace("{editUrl}", "/Siteadmin/" + ControllerName + "/Edit/" + dataList["Id"].ToString() + (!string.IsNullOrEmpty(id) ? "/" + id : ""));
                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //會員帳號
                            case "username":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = dataList[item.Key.ToString()];
                                    rowData.Add(item.Key, content);
                                }

                                break;
                            //會員姓名
                            case "name":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = cipherService.Decrypt(dataList[item.Key.ToString()].ToString());
                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //管理員姓名
                            case "UserName":

                                if (!string.IsNullOrEmpty(dataList["Id"].ToString()))
                                {
                                    string content = cipherService.Decrypt(dataList[item.Key.ToString()].ToString());
                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //管理員姓名
                            case "NormalizedUserName":

                                if (!string.IsNullOrEmpty(dataList["Id"].ToString()))
                                {
                                    string content = dataList[item.Key.ToString()].ToString();
                                    rowData.Add(item.Key, content);
                                }

                                break;


                            //客服用狀態
                            case "activeContact":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/activeContact.cshtml");

                                    content = content.Replace("{id}", dataList["id"].ToString());
                                    content = content.Replace("{active}", dataList["active"].ToString());

                                    if (bool.Parse(dataList["active"].ToString()))
                                    {
                                        content = content.Replace("{title}", "已處理");
                                        content = content.Replace("{class}", "badge-danger");
                                    }
                                    else
                                    {
                                        content = content.Replace("{title}", "未處理");
                                        content = content.Replace("{class}", "badge-secondary");

                                    }

                                    rowData.Add(item.Key, content);
                                }

                                break;


                            //狀態
                            case "LockoutEnabled":

                                if (!string.IsNullOrEmpty(dataList["Id"].ToString()))
                                {
                                    string content = MapPath("Areas/Siteadmin/Views/Shared/DataTable/active.cshtml");

                                    content = content.Replace("{id}", dataList["Id"].ToString());
                                    content = content.Replace("{active}", dataList["LockoutEnabled"].ToString());

                                    if (!bool.Parse(dataList[item.Key.ToString()].ToString()))
                                    {
                                        content = content.Replace("{title}", "啟用");
                                        content = content.Replace("{class}", "badge-danger");
                                    }
                                    else
                                    {
                                        content = content.Replace("{title}", "停用");
                                        content = content.Replace("{class}", "badge-secondary");

                                    }



                                    rowData.Add(item.Key, content);
                                }

                                break;

                            //年
                            case "year":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    rowData.Add(item.Key, dataList["year"].ToString());
                                }

                                break;
                            //月
                            case "month":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    rowData.Add(item.Key, dataList["month"].ToString());
                                }

                                break;

                            //lang
                            case "lang":

                                if (!string.IsNullOrEmpty(dataList["id"].ToString()))
                                {
                                    string language_id = dataList["lang"].ToString();
                                    string content = DB.Languages.Where(m => m.language_id == language_id).FirstOrDefault().title ?? "";


                                    rowData.Add(item.Key, content);
                                }




                                break;


                            //不須處理輸出
                            default:

                                if (dataList["LangData"] != null && !string.IsNullOrEmpty(dataList["LangData"].ToString()))
                                {
                                    string val = "";
                                    if (valJsonArray[item.Key.ToString()] != null)
                                    {
                                        val = valJsonArray[item.Key.ToString()].ToString();
                                    }

                                    if(val.Length > 20)
                                    {
                                        val = val.Substring(0, 20) + "...";
                                    }

                                    rowData.Add(item.Key, val);
                                }
                                else
                                {
                                    rowData.Add(item.Key, dataList[item.Key.ToString()]);
                                }


                                break;
                        }
                    }
                    catch
                    {

                    }

                }
                if (rowData.Count > 0)
                {
                    re.Add(rowData);
                }

            }

            return re;
        }


        public int GetTotalNum(dynamic model)
        {
            int re = 0;


            foreach (var item in model)
            {
                if (item.LangData.Count > 0)
                {
                    re++;
                }

            }

            return re;
        }



        public string MapPath(string seedFile)
        {

            var path = Path.Combine(seedFile);
            //FileStream fileStream = new FileStream(seedFile ,FileMode.Open);
            using (StreamReader sr = new StreamReader(path))
            {
                // Read the stream to a string, and write the string to the console.
                String line = sr.ReadToEnd();
                return line;
            }
        }
    }
}
