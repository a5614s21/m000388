﻿using MailKit.Net.Smtp;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using Newtonsoft.Json;
using System.Globalization;
using System.Text;
using WebApp.Data;
using WebApp.Models;
using Web.Repositories.Admin;
using System.Net;
using X.PagedList;
using Newtonsoft.Json.Linq;
using NPOI.Util;

namespace WebApp.Services
{
    public class Helper
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();

        /// <summary>
        /// 後台選單
        /// </summary>
        /// <param name="parent_id"></param>
        /// <returns></returns>
        public List<SystemMenu> menu(string parent_id)
        {
            List<SystemMenu> SystemMenus = DB.SystemMenu.Where(m => m.parent_id == parent_id).Where(m => m.active).OrderBy(m => m.sort).ToList();

            return SystemMenus;
        }

        public double versionid()
        {
            //DateTime temp =  File.GetLastWriteTime(path);
            double timestamp = new DateTimeOffset(DateTime.UtcNow).ToUnixTimeSeconds();
            return timestamp;
        }

        /// <summary>
        /// 取得欄位資訊
        /// </summary>
        /// <param name="model"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public Dictionary<String, Object> editColumns(string model, string uri)
        {
            List<Columns> temp = new List<Columns>();

            if (string.IsNullOrEmpty(uri))
            {
                temp = DB.Columns
                       .Where(m => m.active == true)
                       .Where(m => m.model == model)
                       .OrderBy(m => m.sort)
                       .ToList();
            }
            else
            {
                temp = DB.Columns
                      .Where(m => m.active == true)
                      .Where(m => m.uri == uri)
                      .Where(m => m.model == model)
                      .OrderBy(m => m.sort)
                      .ToList();
            }

            Dictionary<String, Object> columns = new Dictionary<String, Object>();

            foreach (Columns item in temp)
            {
                List<Columns> col = new List<Columns>();

                if (columns.ContainsKey(item.layout))
                {
                    col = (List<Columns>)columns[item.layout];
                }

                col.Add(item);

                columns.Remove(item.layout);
                columns.Add(item.layout, col);
            }

            return columns;
        }

        /// <summary>
        /// 取得分類
        /// </summary>
        /// <param name="mJObj"></param>
        /// <param name="uri"></param>
        /// <returns></returns>
        public dynamic GetCategory(Newtonsoft.Json.Linq.JToken mJObj, string uri, string lang, string parent_id = "")
        {

            if (mJObj["getCategory"] != null && !string.IsNullOrEmpty(mJObj["getCategory"].ToString()))
            {
                switch (mJObj["model"].ToString())
                {
                    case "ArticleCategory":

                        try
                        {
                            var model = DB.ArticleCategory
                                 .Where(m => (!string.IsNullOrEmpty(uri) ? m.uri == uri : string.IsNullOrEmpty(uri)))
                                 .Where(m => m.parent_id == mJObj["parent_id"].ToString())
                                 .OrderBy(m => m.sort)
                                 .Include(x => x.LangData
                                   .Where(m => m.language_id == lang)
                                    ).ToList();

                            Dictionary<int, Object> data = new Dictionary<int, Object>();
                            int i = 0;
                            foreach (ArticleCategory topItem in model)
                            {
                                foreach (LanguageResource item in topItem.LangData.ToList())
                                {
                                    Categories categories = new Categories();
                                    categories.id = item.ArticleCategoryId;
                                    categories.title = item.title;
                                    categories.parent_id = item.ArticleCategoryId;
                                    data.Add(i, categories);

                                    i++;
                                }
                            }

                            return data;


                        }
                        catch
                        {
                            return null;
                        }

                        break;

                    case "AdvertisingCategory":

                        try
                        {
                            var model = DB.AdvertisingCategory
                                 .Where(m => m.uri == uri)
                                 .Where(m => m.parent_id == mJObj["parent_id"].ToString())
                                 .OrderBy(m => m.sort)
                                 .Include(x => x.LangData
                                   .Where(m => m.language_id == lang)
                                    ).ToList();

                            Dictionary<int, Object> data = new Dictionary<int, Object>();
                            int i = 0;
                            foreach (AdvertisingCategory topItem in model)
                            {
                                foreach (LanguageResource item in topItem.LangData.ToList())
                                {
                                    Categories categories = new Categories();
                                    categories.id = item.AdvertisingCategoryId;
                                    categories.title = item.title;
                                    categories.parent_id = item.ArticleCategoryId;
                                    data.Add(i, categories);

                                    i++;
                                }
                            }

                            return data;


                        }
                        catch
                        {
                            return null;
                        }

                        break;

                    case "IdentityRole":
                        return DB.IdentityRole.ToList();

                        break;


                    case "ProductCategory":

                        try
                        {



                            var model = DB.ProductCategory
                                  .Where(m => !string.IsNullOrEmpty(uri) ? m.uri == uri : m.id != "")
                                 .Where(m => !string.IsNullOrEmpty(parent_id) ? m.parent_id == parent_id : m.parent_id == mJObj["parent_id"].ToString())
                                 .OrderBy(m => m.sort)
                                 .Include(x => x.LangData
                                   .Where(m => m.language_id == lang)
                                    ).ToList();

                            Dictionary<int, Object> data = new Dictionary<int, Object>();
                            int i = 0;
                            foreach (ProductCategory topItem in model)
                            {
                                foreach (LanguageResource item in topItem.LangData.ToList())
                                {
                                    Categories categories = new Categories();
                                    categories.id = item.ProductCategoryId;
                                    categories.title = item.title;
                                    categories.parent_id = item.parent_id;
                                    categories.uri = topItem.uri;


                                    var sub_categories = DB.ProductCategory
                                         // .Where(m => m.uri == uri)
                                         .Where(m => m.parent_id == item.ProductCategoryId)
                                         .OrderBy(m => m.sort)
                                         .Include(x => x.LangData
                                           .Where(m => m.language_id == lang)
                                            ).ToList();

                                    List<Categories> nextCategories = new List<Categories>();

                                    foreach (ProductCategory subItem in sub_categories)
                                    {
                                        foreach (LanguageResource subitem in subItem.LangData.ToList())
                                        {
                                            Categories sub_category = new Categories();
                                            sub_category.id = subitem.ProductCategoryId;
                                            sub_category.title = subitem.title;
                                            sub_category.parent_id = subitem.parent_id;
                                            sub_category.uri = subItem.uri;


                                            var the_categories = DB.ProductCategory
                                       // .Where(m => m.uri == uri)
                                       .Where(m => m.parent_id == subitem.ProductCategoryId)
                                       .OrderBy(m => m.sort)
                                       .Include(x => x.LangData
                                         .Where(m => m.language_id == lang)
                                          ).ToList();

                                            List<Categories> theCategories = new List<Categories>();

                                            foreach (ProductCategory theItem in the_categories)
                                            {
                                                foreach (LanguageResource theitem in theItem.LangData.ToList())
                                                {
                                                    Categories the_category = new Categories();
                                                    the_category.id = theitem.ProductCategoryId;
                                                    the_category.title = theitem.title;
                                                    the_category.parent_id = theitem.parent_id;
                                                    the_category.uri = theItem.uri;


                                                    theCategories.Add(the_category);
                                                }
                                            }
                                            sub_category.sub_categories = theCategories;


                                            nextCategories.Add(sub_category);
                                        }
                                    }

                                    categories.sub_categories = nextCategories;


                                    data.Add(i, categories);

                                    i++;
                                }
                            }

                            return data;


                        }
                        catch
                        {
                            return null;
                        }




                        break;


                    case "ProductSpecCategory":

                        try
                        {
                            var model = DB.ProductSpecCategory
                                 //.Where(m => m.uri == uri)
                                 // .Where(m => m.parent_id == mJObj["parent_id"].ToString())
                                 .OrderBy(m => m.sort)
                                 .Include(x => x.LangData
                                   .Where(m => m.language_id == lang)
                                    ).ToList();

                            Dictionary<int, Object> data = new Dictionary<int, Object>();
                            int i = 0;
                            foreach (ProductSpecCategory topItem in model)
                            {
                                foreach (LanguageResource item in topItem.LangData.ToList())
                                {
                                    Categories categories = new Categories();
                                    categories.id = item.ProductSpecCategoryId;
                                    categories.title = item.title;
                                    // categories.parent_id = item.ArticleCategoryId;
                                    data.Add(i, categories);

                                    i++;
                                }
                            }

                            return data;


                        }
                        catch
                        {
                            return null;
                        }




                        break;

                    case "ProductSpec":

                        try
                        {
                            var model = DB.ProductSpec
                                 .Where(m => m.uri == mJObj["uri"].ToString())
                                 // .Where(m => m.parent_id == mJObj["parent_id"].ToString())
                                 .OrderBy(m => m.sort)
                                 .Include(x => x.LangData
                                   .Where(m => m.language_id == lang)
                                    ).ToList();

                            Dictionary<int, Object> data = new Dictionary<int, Object>();
                            int i = 0;
                            foreach (ProductSpec topItem in model)
                            {
                                foreach (LanguageResource item in topItem.LangData.ToList())
                                {
                                    Categories categories = new Categories();
                                    categories.id = item.ProductSpecId;
                                    categories.title = item.title;
                                    categories.pic = item.pic;
                                    // categories.parent_id = item.ArticleCategoryId;
                                    data.Add(i, categories);

                                    i++;
                                }
                            }

                            return data;


                        }
                        catch
                        {
                            return null;
                        }




                        break;

                    case "SiteParameterGroup":

                        try
                        {
                            var model = DB.SiteParameterGroup
                                 .OrderBy(m => m.sort)
                                 .Include(x => x.LangData
                                   .Where(m => m.language_id == lang)
                                    ).ToList();

                            Dictionary<int, Object> data = new Dictionary<int, Object>();
                            int i = 0;
                            foreach (SiteParameterGroup topItem in model)
                            {
                                foreach (LanguageResource item in topItem.LangData.ToList())
                                {
                                    Categories categories = new Categories();
                                    categories.id = topItem.id;
                                    categories.title = item.title;
                                    //categories.parent_id = item.ArticleCategoryId;
                                    data.Add(i, categories);

                                    i++;
                                }
                            }

                            return data;


                        }
                        catch
                        {
                            return null;
                        }

                        break;

                    default:
                        return null;

                        break;
                }

            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// 分類名稱
        /// </summary>
        /// <param name="Model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public string GetCategoryTitle(string Model, string id, string lang)
        {

            switch (Model)
            {
                case "ArticleCategory":


                    return DB.ArticleCategory.Where(x => x.id == id)
                        .Include(x => x.LangData
                        .Where(l => l.model_id == id)
                         ).FirstOrDefault()?
                        .LangData.Where(m => m.language_id == lang)
                        .FirstOrDefault()?.title;

                    break;


                case "ProductCategory":


                    return DB.ProductCategory.Where(x => x.id == id)
                      .Include(x => x.LangData
                      .Where(l => l.model_id == id)
                       ).FirstOrDefault()?
                      .LangData.Where(m => m.language_id == lang)
                      .FirstOrDefault()?.title;

                    break;

                default:
                    return "";

                    break;
            }



        }


        public List<ProductCategory> SubProductCategory(string parent_id)
        {
            return DB.ProductCategory
                          .Where(m => m.parent_id == parent_id)
                          .OrderBy(m => m.sort)
                          .ToList();
        }

        /// <summary>
        /// 修改資料格式化
        /// </summary>
        /// <param name="Columns"></param>
        /// <param name="temp"></param>
        /// <returns></returns>
        public Dictionary<string, string> FormatViewData(Dictionary<String, Object> Columns, dynamic temp)
        {
            Dictionary<string, string> viewData = new Dictionary<string, string>();
            foreach (KeyValuePair<string, object> col in Columns)
            {
                foreach (Columns item in (List<Columns>)Columns[col.Key])
                {
                    if (string.IsNullOrEmpty(item.sub_column_name))
                    {
                        viewData.Add(item.column_name, (temp[item.column_name] != null && temp[item.column_name].ToString() != null) ? temp[item.column_name].ToString() : "");




                        if (item.column_name == "files")
                        {
                            if (!string.IsNullOrEmpty(temp[item.column_name].ToString()))
                            {
                                var subTemp = Newtonsoft.Json.Linq.JArray.Parse("[" + temp[item.column_name] + "]");

                                JObject obj = JObject.Parse(temp[item.column_name].ToString());

                                if (obj.ContainsKey("spec"))
                                {

                                    foreach (JObject parsedObject in subTemp.Children<JObject>())
                                    {
                                        foreach (JProperty parsedProperty in parsedObject.Properties())
                                        {
                                            if (parsedProperty.Name != "spec")
                                            {
                                                viewData.Add(item.column_name + "_" + parsedProperty.Name, parsedProperty.Value.ToString());
                                            }


                                        }
                                    }



                                }
                            }

                        }


                    }
                    else
                    {
                        var subTemp = Newtonsoft.Json.Linq.JArray.Parse("[" + temp[item.column_name] + "]");

                        try
                        {
                            viewData.Add(item.column_name + "_" + item.sub_column_name, (subTemp.Count > 0 && subTemp[0][item.sub_column_name].ToString() != null) ? subTemp[0][item.sub_column_name].ToString() : "");

                        }
                        catch (Exception ex)
                        {

                            viewData.Add(item.column_name + "_" + item.sub_column_name, null);

                        }

                    }
                }

            }

            viewData.Add("sort", (temp != null && temp.ContainsKey("sort")) ? temp["sort"].ToString() : "1");
            viewData.Add("created_at", (temp != null && temp.ContainsKey("created_at")) ? temp["created_at"].ToString() : DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));


            return viewData;
        }

        /// <summary>
        /// 檢視標題
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public string SearchSubject(string type)
        {
            switch (type)
            {
                case "active":
                    return "所有狀態";
                    break;
                case "category_id":
                    return "分類";
                    break;
                default:
                    return "";
                    break;
            }
        }

        /// <summary>
        /// 取得排序鍵值
        /// </summary>
        /// <param name="dataTableColnum"></param>
        /// <param name="OrderBy"></param>
        /// <returns></returns>
        public string GetSortIndex(Dictionary<String, Object> dataTableColnum, Dictionary<string, string> OrderBy)
        {
            string sort = "";
            int index = 0;
            foreach (KeyValuePair<string, object> item in dataTableColnum)
            {
                if (OrderBy["key"].ToString() == item.Key)
                {
                    sort = "'" + index.ToString() + "','" + OrderBy["type"].ToString() + "'";
                }

                index++;
            }

            return sort;
        }

        /// <summary>
        /// SEO資訊
        /// </summary>
        /// <param name="webData"></param>
        /// <param name="Json"></param>
        /// <param name="pic"></param>
        /// <returns></returns>
        public Dictionary<string, string> Seo(dynamic webData, string Json, string pic)
        {
            Dictionary<string, string> seo = new Dictionary<string, string>();



            var temp = Newtonsoft.Json.Linq.JArray.Parse("[" + webData.seo + "]");

            if (!string.IsNullOrEmpty(Json))
            {
                temp = Newtonsoft.Json.Linq.JArray.Parse("[" + Json + "]");
            }

            string picPath = "/styles/images/common/logo.svg";
            if (!string.IsNullOrEmpty(pic))
            {
                var tempPic = Newtonsoft.Json.Linq.JArray.Parse(pic.ToString());
                picPath = tempPic[0]["path"].ToString();
            }

            seo.Add("keywords", temp[0]["keywords"].ToString());
            seo.Add("description", temp[0]["description"].ToString());
            seo.Add("pic", picPath);


            return seo;
        }


        /// <summary>
        /// 格式化輸出資料
        /// </summary>
        /// <param name="dbData"></param>
        /// <param name="_repository"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public List<ListModel> FormatListData(dynamic dbData, dynamic _repository, string table)
        {
            List<ListModel> data = new List<ListModel>();


            foreach (var item in dbData)
            {

                ListModel temp = new ListModel()
                {
                    id = item.id ?? "",
                    title = item.title ?? "",
                    details = !string.IsNullOrEmpty(item.details) ? JsonConvert.DeserializeObject<Dictionary<string, string>>(item.details) : null,
                    pic = (new WebApp.Services.Helper()).FormatJson(item.pic),
                    active = item.active,
                    sort = item.sort,
                    category_title = _repository.GetCategoryTitle(item.category_id),
                    path = item.path,
                    created_at = item.created_at.ToString() != "" ? item.created_at.ToString() : ""
                };


                if (table == "ArticleNews")
                {
                    temp.start_at = item.start_at != null ? item.start_at.ToString() : "";
                }
                if (table == "ArticleCategory" || table == "ProductCategory" || table == "ArticleColumn")
                {
                    temp.icon = (new WebApp.Services.Helper()).FormatJson(item.icon);
                }
                if (table == "ArticleDownload" || table == "ProductSet")
                {
                    temp.files = (new WebApp.Services.Helper()).FormatJson(item.files);
                }

                data.Add(temp);

            }
            return data;
        }

        /// <summary>
        /// 格式化輸出資料(單筆)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="_repository"></param>
        /// <param name="table"></param>
        /// <returns></returns>
        public ListModel FormatSingleData(dynamic item, dynamic _repository, string table)
        {

            ListModel data = new ListModel
            {
                id = item.id ?? "",
                title = item.title ?? "",
                details = !string.IsNullOrEmpty(item.details) ? JsonConvert.DeserializeObject<Dictionary<string, string>>(item.details) : null,
                pic = (new WebApp.Services.Helper()).FormatJson(item.pic),
                active = item.active,
                sort = item.sort,
                path = !string.IsNullOrEmpty(item.path) ? item.path : ""
            };

            if (table == "ArticleNews")
            {
                data.start_at = !string.IsNullOrEmpty(item.start_at.ToString()) ? item.start_at.ToString() : "";
            }
            if (table != "ArticleCategoryColumn")
            {
                data.category_title = _repository.GetCategoryTitle(item.category_id);
            }

            if (table == "ArticleCategory" || table == "ProductCategory" || table == "ArticleColumn")
            {
                data.icon = (new WebApp.Services.Helper()).FormatJson(item.icon);
            }
            if (table == "ArticleDownload" || table == "ProductSet")
            {
                data.files = (new WebApp.Services.Helper()).FormatJson(item.files);

            }

            if (table == "ProductSet")
            {

                var list_pic = (new WebApp.Services.Helper()).FormatJson(item.list_pic);
                data.list_pic = new Dictionary<string, string>() { { "path", list_pic[0].path } };
            }

            return data;
        }

        /// <summary>
        /// 取得appsettings.json資訊
        /// </summary>
        /// <returns></returns>
        public dynamic GetAppSettings()
        {
            var builder = new ConfigurationBuilder()
                 .SetBasePath(Directory.GetCurrentDirectory())
                 .AddJsonFile("appsettings.json");
            return builder.Build();
        }

        /// <summary>
        /// Json格式化
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public dynamic FormatJson(string json)
        {

            string temp = "[{\"path\":\"/styles/images/nopic.jpg\",\"alt\":\"\",\"en_title\":\"\",\"notes\":\"\",\"editor\":\"\"},{\"path\":\"\",\"alt\":\"\",\"en_title\":\"\",\"notes\":\"\",\"editor\":\"\"}]";


            List<JsonModel> data = new List<JsonModel>();

            if (json != null && !string.IsNullOrEmpty(json))
            {
                data = JsonConvert.DeserializeObject<List<JsonModel>>(json).ToList();

                if (data.Count > 0)
                {
                    if (data.Count == 1)
                    {
                        data.Add(new JsonModel
                        {

                            path = "/styles/images/nopic.jpg",
                            alt = "",
                            en_title = "",
                            notes = "",
                            editor = "",
                        });


                    }
                    return data;
                }
                else
                {
                    return JsonConvert.DeserializeObject<List<JsonModel>>(temp).ToList();
                }
            }
            else
            {
                return JsonConvert.DeserializeObject<List<JsonModel>>(temp).ToList();
            }
        }

        /// <summary>
        /// 取得權限
        /// </summary>
        /// <param name="systemMenu"></param>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public string GetPermission(SystemMenu systemMenu, string RoleId)
        {
            string permission = "N";
            List<string> nextMenus = DB.SystemMenu.Where(m => m.parent_id == systemMenu.id.ToString()).Select(m => m.id).ToList();

            List<string> menuIds = new List<string>();

            if (nextMenus.Count == 0)
            {
                menuIds = new List<string> { systemMenu.id };
            }
            else
            {
                menuIds.AddRange(nextMenus);

                foreach (string nextMenusId in nextMenus)
                {
                    menuIds.AddRange(DB.SystemMenu.Where(m => m.parent_id == nextMenusId).Select(m => m.id).ToList());
                }
            }

            //取得
            List<RolePermission> rolePermissions = DB.RolePermission.Where(m => m.RoleId == RoleId).Where(m => menuIds.Contains(m.MenuId)).ToList();

            foreach (RolePermission item in rolePermissions)
            {
                permission = item.Permission;
            }

            if (systemMenu.id == "default" || systemMenu.parent_id == "default")
            {
                permission = "Y";
            }

            return permission;
        }


        /// <summary>
        /// 判斷防火牆
        /// </summary>
        /// <param name="ip"></param>
        /// <param name="platform"></param>
        /// <returns></returns>
        public Dictionary<string, bool> FirewallCheck(string ip, string platform)
        {
            if (ip == "::1")
            {
                return new Dictionary<string, bool> { { "ruleTrue", true }, { "ruleFalse", false } };
            }


            //取得平台
            List<Firewall> firewalls = DB.Firewall.Where(m => m.platform == platform).Where(m => m.active == true).ToList();

            bool ruleTrue = true;//允許
            bool ruleFalse = false;//禁止

            long ipNum = IP2Long(ip);

            foreach (Firewall firewall in firewalls)
            {
                if (!string.IsNullOrEmpty(GetValidatedIP(firewall.ip.ToString())))
                {
                    //允許
                    if (firewall.rule == true)
                    {
                        if (firewall.ip.IndexOf("~") != -1)
                        {
                            List<string> tempIp = firewall.ip.Split('~').ToList();

                            long ipStart = IP2Long(tempIp[0]);
                            long ipEnd = IP2Long(tempIp[1]);

                            if (ipStart > ipNum || ipEnd < ipNum)
                            {
                                ruleTrue = false;
                            }
                        }
                        else
                        {
                            long ipStart = IP2Long(firewall.ip);
                            if (ipStart != ipNum)
                            {
                                ruleTrue = false;
                            }

                        }
                    }
                    //禁止
                    else
                    {
                        if (firewall.ip.IndexOf("~") != -1)
                        {
                            List<string> tempIp = firewall.ip.Split('~').ToList();

                            long ipStart = IP2Long(tempIp[0]);
                            long ipEnd = IP2Long(tempIp[1]);

                            if (ipStart <= ipNum && ipEnd >= ipEnd)
                            {
                                ruleFalse = true;
                            }
                        }
                        else
                        {
                            long ipStart = IP2Long(firewall.ip);
                            if (ipStart == ipNum)
                            {
                                ruleFalse = true;
                            }

                        }

                    }

                }
            }

            return new Dictionary<string, bool> { { "ruleTrue", ruleTrue }, { "ruleFalse", ruleFalse } };

        }

        /// <summary>
        /// IP轉數字
        /// </summary>
        /// <param name="ip"></param>
        /// <returns></returns>
        public static long IP2Long(string ip)
        {
            string[] ipBytes;
            double num = 0;
            if (!string.IsNullOrEmpty(ip))
            {
                ipBytes = ip.Split('.');
                for (int i = ipBytes.Length - 1; i >= 0; i--)
                {
                    num += ((int.Parse(ipBytes[i]) % 256) * Math.Pow(256, (3 - i)));
                }
            }
            return (long)num;
        }

        /// <summary>
        /// IP判斷
        /// </summary>
        /// <param name="ipStr"></param>
        /// <returns></returns>
        private string GetValidatedIP(string ipStr)
        {
            string validatedIP = string.Empty;
            IPAddress ip;
            if (IPAddress.TryParse(ipStr, out ip))
            {
                validatedIP = ip.ToString();
            }
            return validatedIP;
        }

        /// <summary>
        /// 發信
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="data"></param>
        /// <param name="NowURL"></param>
        /// <param name="lang"></param>
        /// <param name="cc"></param>
        public void SendMail(string uri, dynamic data, string NowURL, string lang, List<string> addMailList = null, bool cc = false)
        {

            // InboxNotify inboxNotify = DB.InboxNotify.Where(m => m.uri == uri).FirstOrDefault();

            InboxNotify inboxNotify = DB.InboxNotify.Where(x => x.uri == uri)
                        .Where(m => m.active == true)
                        .OrderBy(m => m.sort)
                        .Include(x => x.LangData.Where(l => l.language_id == lang)).FirstOrDefault();

            LanguageResource LangData = inboxNotify.LangData.FirstOrDefault();



            var webData = DB.WebData.Where(m => m.id == "web").Include(x => x.LangData).FirstOrDefault()?
                .LangData.Where(m => m.language_id == lang)
           .FirstOrDefault();

            // var WebDataContact = Newtonsoft.Json.Linq.JArray.Parse("[" + webData.contact.ToString() + "]");


            // var details = Newtonsoft.Json.Linq.JArray.Parse("[" + inboxNotify.details.ToString() + "]");

            SystemItem systemItem = DB.SystemItem.Where(m => m.id == "smtp").FirstOrDefault();

            var smtp = Newtonsoft.Json.Linq.JArray.Parse("[" + systemItem.details.ToString() + "]");

            // 建立郵件
            var message = new MimeMessage();

            // 添加寄件者
            message.From.Add(new MailboxAddress(smtp[0]["fromName"].ToString(), smtp[0]["fromAddress"].ToString()));

            // 添加收件者
            // message.To.Add(new MailboxAddress("test", "servie@minmax.biz"));

            List<string> mailList = new List<string>();
            if (addMailList != null && addMailList.Count > 0)
            {
                mailList.AddRange(addMailList);

            }
            else
            {

                string emails = LangData.DetailsData?.email.ToString();

                if (emails.Contains(","))
                {
                    mailList = emails.Split(',').ToList();
                }
                else
                {
                    mailList.Add(LangData.DetailsData?.email.ToString());
                }



            }


            foreach (string mail in mailList)
            {
                message.To.Add(new MailboxAddress(mail, mail));
            }
            //有需要發副本時
            if (cc)
            {
                message.To.Add(new MailboxAddress(data.email, data.email));
            }
            //


            // 設定郵件標題
            message.Subject = LangData.DetailsData?.subject.ToString();

            // 使用 BodyBuilder 建立郵件內容
            var bodyBuilder = new BodyBuilder();

            // 設定文字內容
            // bodyBuilder.TextBody = "文字內容";

            //處理資訊
            string body = LangData.DetailsData?.editor.ToString();

            body = body.Replace("{[websiteName]}", webData.title);
            body = body.Replace("{[websitePhone]}", (webData.ContactData.tel != null) ? webData.ContactData.tel.ToString() : "");
            body = body.Replace("{[websiteEmail]}", webData.ContactData.email.ToString() ?? "");

            body = body.Replace("{[websiteUrl]}", NowURL);
            body = body.Replace("{[formDate]}", DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            body = body.Replace("{[nowYear]}", DateTime.Now.ToString("yyyy"));

            try
            {
                body = body.Replace("{[formName]}", data.name);
                body = body.Replace("{[formEmail]}", data.email);
            }
            catch
            {

            }


            TextInfo textInfo = new CultureInfo("es-ES", false).TextInfo;
            try
            {
                foreach (KeyValuePair<string, string> item in data)
                {
                    body = body.Replace("{[form" + textInfo.ToTitleCase(item.Key) + "]}", item.Value);
                }
            }
            catch
            {

            }

            try
            {
                foreach (KeyValuePair<string, string> item in data.contact)
                {
                    body = body.Replace("{[form" + textInfo.ToTitleCase(item.Key) + "]}", item.Value);
                }

                foreach (KeyValuePair<string, List<string>> item in data.options)
                {

                    string value = String.Join("<br>", item.Value);

                    switch (item.Key)
                    {
                        case "AboutUs":

                            if (value == "Other")
                            {

                                string AboutUsOther = String.Join("<br>", data.options["AboutUsOther"]);
                                value = "Other：" + AboutUsOther;
                            }

                            break;

                        case "ProductCategory":

                            List<string> tempvalue = new List<string>();
                            value = "";
                            foreach (var citem in item.Value)
                            {
                                if (uri == "re_contact")
                                {
                                    tempvalue.Add((from a in DB.LanguageResource
                                                   where a.SiteParameterItemId == citem && a.language_id == lang
                                                   select a.title).FirstOrDefault());
                                }
                                else
                                {
                                    tempvalue.Add(citem);
                                }
                            }

                            value = String.Join("<br>", tempvalue);

                            break;
                    }

                    body = body.Replace("{[form" + item.Key + "]}", value);
                }

            }
            catch
            {

            }



            // 設定 HTML 內容
            bodyBuilder.HtmlBody = body;

            // 設定附件
            //bodyBuilder.Attachments.Add("檔案路徑");

            // 設定郵件內容
            message.Body = bodyBuilder.ToMessageBody();


            using (var client = new SmtpClient())
            {
                var hostUrl = smtp[0]["host"].ToString();
                var port = int.Parse(smtp[0]["port"].ToString());
                var useSsl = false;
                if (smtp[0]["encryption"].ToString().ToUpper() == "SSL")
                {
                    useSsl = true;
                }


                // 連接 Mail Server (郵件伺服器網址, 連接埠, 是否使用 SSL)
                client.Connect(hostUrl, port, useSsl);

                // 驗證
                client.Authenticate(smtp[0]["username"].ToString(), smtp[0]["password"].ToString());

                // 寄出郵件
                client.Send(message);

                // 中斷連線
                client.Disconnect(true);
            }

        }
        /// <summary>
        /// 儲存紀錄
        /// </summary>
        /// <param name="data"></param>
        public void SaveLog(Dictionary<string, string> data)
        {
            if (data["username"] != "sysadmin")
            {
                SystemLog entity = new SystemLog
                {
                    id = Guid.NewGuid().ToString(),
                    username = data["username"],
                    act = data["act"],
                    title = data["title"],
                    ip = data["ip"],
                    created_at = DateTime.Now,
                    updated_at = DateTime.Now,
                    details = data.ContainsKey("details") ? data["details"] : "",
                    uri = data["uri"],

                };

                DB.SystemLog.Add(entity);
                DB.SaveChanges();
            }

        }



        /// <summary>
        /// 短GUID
        /// </summary>
        /// <returns></returns>
        public string GenerateIntID()
        {
            long i = 1;
            foreach (byte b in Guid.NewGuid().ToByteArray())
            {
                i *= ((int)b + 1);
            }
            return string.Format("{0:x}", i - DateTime.Now.Ticks);
        }



        /// <summary>
        /// 下層分類數量
        /// </summary>
        /// <param name="parent_id"></param>
        /// <returns></returns>
        public int NextCategoryCount(string parent_id)
        {
            return DB.ProductCategory.Where(m => m.parent_id == parent_id).Where(m => m.active == true).Count();
        }




        /// <summary>
        /// 下層分類
        /// </summary>
        /// <param name="parent_id"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<List<ProductCategory>> NextCategory(string parent_id, string lang)
        {
            return DB.ProductCategory.Where(m => m.active == true)
                .Where(m => m.parent_id == parent_id).OrderBy(m => m.sort)
                 .Include(x => x.LangData.Where(l => l.language_id == lang))
                .ToList();
        }

        /// <summary>
        /// 檔案格式化
        /// </summary>
        /// <param name="product"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<List<PicDetailModel>> FileFormat(ProductSet product, string lang, Dictionary<string, List<ProductSet>> LangProductSet)
        {

            dynamic appSeting = GetAppSettings();

            List<ProductSet> productSets = LangProductSet[lang];

            //若語系為無資料，使用預設語系
            if (productSets.Count == 0)
            {
                productSets = LangProductSet[appSeting["Lang"].ToString()];
            }

            var thisProduct = productSets.Where(m => m.id == product.id).FirstOrDefault();


            List<PicDetailModel> responseFiles = new List<PicDetailModel>();
            string filesJson = thisProduct.LangData.FirstOrDefault()?.files.ToString();

            if (!string.IsNullOrEmpty(filesJson))
            {
                var temp = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, JArray>>(filesJson);

                foreach (var kv in temp)
                {
                    if (kv.Key == "spec")
                    {

                        foreach (JObject item in kv.Value)
                        {
                            string path = item.GetValue("path").ToString();
                            var pathList = path.Split('/').ToList();

                            responseFiles.Add(new PicDetailModel
                            {
                                link = path,
                                path = path,
                                title = pathList[(pathList.Count - 1)].ToString()

                            });

                        }
                    }
                }

            }
            if (!string.IsNullOrEmpty(filesJson))
            {
                var temp = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<string, JArray>>(filesJson);

                foreach (var kv in temp)
                {
                    if (kv.Key != "spec")
                    {

                        foreach (JObject item in kv.Value)
                        {
                            string path = item.GetValue("path").ToString();
                            var pathList = path.Split('/').ToList();

                            responseFiles.Add(new PicDetailModel
                            {
                                link = path,
                                path = path,
                                title = pathList[(pathList.Count - 1)].ToString()

                            });

                        }
                    }
                }

            }
            return responseFiles;
        }

        /// <summary>
        /// 圖片處理
        /// </summary>
        /// <param name="LangData"></param>
        /// <returns></returns>
        public string FormatPic(LanguageResource LangData, string col, string type, int index = 0)
        {
            string re = "";
            try
            {
                if (LangData != null && LangData.pic != null)
                {
                    switch (col)
                    {
                        case "pic":

                            if (!string.IsNullOrEmpty(LangData.pic) && LangData.pic != "{}")
                            {
                                string path = LangData?.PicData.pic[index]?.path;

                                string fileRoot = Path.GetFullPath("wwwroot" + path);

                                if (!System.IO.File.Exists(fileRoot))
                                {
                                    path = "/static/admin/images/common/com_logo.png";
                                }

                                if (type == "image") re = path;
                                else re = LangData?.PicData.pic[index]?.title;

                            }
                            else
                            {
                                if (type == "image") re = "/static/admin/images/common/com_logo.png";
                                else re = LangData?.title;
                            }
                            break;

                        case "icon":

                            if (!string.IsNullOrEmpty(LangData.pic) && LangData.pic != "{}" && LangData?.PicData.icon != null)
                            {


                                if (type == "image") re = LangData?.PicData.icon[index]?.path;
                                else re = LangData?.PicData.icon[index]?.title;

                            }
                            else
                            {
                                if (type == "image") re = "";
                                else re = LangData?.title;
                            }
                            break;


                        case "logo":

                            if (!string.IsNullOrEmpty(LangData.pic) && LangData.pic != "{}")
                            {
                                if (type == "image") re = LangData?.PicData.logo[index]?.path;
                                else re = LangData?.PicData.logo[index]?.title;

                            }
                            else
                            {
                                if (type == "image") re = "/static/admin/images/common/com_logo.png";
                                else re = LangData?.title;
                            }
                            break;

                        case "path_pic":

                            if (!string.IsNullOrEmpty(LangData.pic) && LangData.pic != "{}")
                            {
                                if (type == "image") re = LangData?.PicData.path_pic[index]?.path;
                                else re = LangData?.PicData.path_pic[index]?.title;

                            }
                            else
                            {
                                if (type == "image") re = "/static/admin/images/common/com_logo.png";
                                else re = LangData?.title;
                            }
                            break;

                    }
                }

            }
            catch
            {

            }


            return re;
        }

        /// <summary>
        /// 取得麵包屑
        /// </summary>
        /// <param name="category"></param>
        /// <param name="categories"></param>
        /// <returns></returns>
        public List<Dictionary<string, string>> GetBreadcrumbs(ProductCategory category, List<ProductCategory> categories)
        {
            List<Dictionary<string, string>> Breadcrumbs = new List<Dictionary<string, string>>();


            List<string> topId = new List<string> {
                "ICPackaging" , "Service" , "Technology" ,
            };

            string parent_id = category.parent_id;



            while (!topId.Contains(parent_id))
            {

                ProductCategory nowCategory = categories.Where(m => m.id == parent_id).FirstOrDefault();


                LanguageResource LangData = nowCategory.LangData.FirstOrDefault();

                parent_id = nowCategory.parent_id;

                Dictionary<string, string> data = new Dictionary<string, string>();

                data.Add("id", LangData?.ProductCategoryId);
                data.Add("title", LangData?.title);

                Breadcrumbs.Add(data);




            }


            return Breadcrumbs;
        }

        /// <summary>
        /// 問卷(checkbox)
        /// </summary>
        /// <param name="source"></param>
        /// <param name="contacts"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public async Task<string> Calculate(string source, List<Contact> contacts, string type)
        {
            //string re = "";
            float nowNum = 0;
            foreach (Contact item in contacts)
            {
                string json = item.contact;
                dynamic obj = null;
                if (!string.IsNullOrEmpty(json))
                {
                    var serializerOptions = new System.Text.Json.JsonSerializerOptions
                    {
                        Converters = { new Web.Services.DynamicJsonConverter() },
                        WriteIndented = true
                    };

                    obj = System.Text.Json.JsonSerializer.Deserialize<dynamic>(json, serializerOptions);

                    foreach (var data in (IDictionary<String, Object>)obj)
                    {
                        QuestionnaireItem questionnaireItem = await (new Helper()).GetQuestionnaireItem(data.Key);

                        if (questionnaireItem != null)
                        {


                            LanguageResource LangData = questionnaireItem.LangData.FirstOrDefault();

                            string showVal = "";

                            FormModel model = JsonConvert.DeserializeObject<FormModel>(LangData.options);

                            if (model.type == type)
                            {

                                foreach (string propertyInfo in (IList<object>)data.Value)
                                {
                                    // do stuff here
                                    // showVal += propertyInfo + "<br>";

                                    if (propertyInfo.Trim().ToLower() == source.Trim().ToLower())
                                    {
                                        nowNum++;
                                    }

                                }

                            }
                        }
                    }
                }

            }


            float contactsCount = nowNum / contacts.Count;

            return decimal.Round(  decimal.Parse( (contactsCount * 100).ToString() )   , 1).ToString() + "%";
        }


        public async Task<string> CalculateSource(string source, List<Contact> contacts, string type)
        {
            //string re = "";
            float nowNum = 0;
            foreach (Contact item in contacts)
            {
                string json = item.contact;
                dynamic obj = null;
                if (!string.IsNullOrEmpty(json))
                {
                    var serializerOptions = new System.Text.Json.JsonSerializerOptions
                    {
                        Converters = { new Web.Services.DynamicJsonConverter() },
                        WriteIndented = true
                    };

                    obj = System.Text.Json.JsonSerializer.Deserialize<dynamic>(json, serializerOptions);

                    foreach (var data in (IDictionary<String, Object>)obj)
                    {

                        if (data.Key == source)
                        {
                            int number;
                            bool success = int.TryParse(data.Value.ToString(), out number);

                            int addQty = 0;

                            if (success)
                            {
                                addQty = int.Parse(data.Value.ToString());
                            }
                           

                            nowNum = nowNum + addQty;

                        }

                    }
                }

            }

            float contactsCount = nowNum / contacts.Count;

            return decimal.Round(decimal.Parse((contactsCount).ToString()), 1).ToString();
            //return ((float)(nowNum / contacts.Count)).ToString();
        }

        public async Task<List<string>> GetSiteParameterItem(List<string> id)
        {
            return DB.LanguageResource.Where(m => id.Contains(m.SiteParameterItemId))
                                   .Where(l => l.language_id == "en")
                                   .Select(m => m.title)
                                   .ToList();

        }

        /// <summary>
        /// 取得問卷問題
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<QuestionnaireItem> GetQuestionnaireItem(string id)
        {
            return DB.QuestionnaireItem.Where(m => m.spec == id)
                .Include(x => x.LangData.Where(l => l.language_id == "en"))
                                   .FirstOrDefault();

        }


        public QuestionnaireItem GetQuestionnaireItemExport(string id)
        {
            return DB.QuestionnaireItem.Where(m => m.spec == id)
                .Include(x => x.LangData.Where(l => l.language_id == "en"))
                                   .FirstOrDefault();

        }


        /// <summary>
        /// 全文檢索調整
        /// </summary>
        /// <param name="data"></param>
        /// <param name="culturePath"></param>
        /// <param name="dbData"></param>
        /// <returns></returns>
        public async Task<string> GetSearchResultUrl(LanguageResource data, string culturePath, dynamic dbData)
        {
            string url = "";

            switch (data.model_type)
            {
                case "ProductCategory":
                    try
                    {
                        List<ProductCategory> model = dbData;

                        string uri = model.Where(m => m.id == data.ProductCategoryId).Select(m => m.uri)?.FirstOrDefault() ?? "";

                        List<ProductCategory> SubProductCategory = model.Where(m => m.parent_id == data.ProductCategoryId).ToList();

                        switch (uri)
                        {
                            case "ICPackaging":

                                url = culturePath + "IC_PackagingData/" + data.ProductCategoryId;
                                if (SubProductCategory != null && SubProductCategory.Count > 0)
                                {
                                    url = culturePath + "IC_PackagingSecond/" + data.ProductCategoryId;
                                }

                                if (data.parent_id != "ICPackaging")
                                {
                                    url = culturePath + "IC_PackagingThird/" + data.ProductCategoryId;
                                }

                                break;
                            case "Service":
                                url = culturePath + "ServiceData/" + data.ProductCategoryId;

                                if (SubProductCategory != null && SubProductCategory.Count > 0)
                                {
                                    url = culturePath + "ServiceSecond/" + data.ProductCategoryId;
                                }
                                if (data.parent_id != "Service")
                                {
                                    url = culturePath + "ServiceThird/" + data.ProductCategoryId;
                                }
                                var temp = (from a in DB.ProductCategory
                                            where a.id == data.ProductCategoryId
                                            select a.parent_id).FirstOrDefault(); 
                                if (temp != null && temp == "TopService")
                                {
                                    url = culturePath + "Service";
                                }

                                break;
                            case "Technology":

                                url = culturePath + "TechnologyData/" + data.ProductCategoryId;
                                break;
                        }
                    }
                    catch
                    {

                    }


                    break;

                case "ArticleCategory":
                    try
                    {
                        List<ArticleCategory> model = dbData;

                        string uri = model.Where(m => m.id == data.ArticleCategoryId).Select(m => m.uri)?.FirstOrDefault() ?? "";

                        if (data.ArticleCategoryId == "awards")
                        {
                            url = culturePath + "Careers/" + data.ArticleCategoryId;
                        }
                        else if (data.ArticleCategoryId == "video")
                        {
                            url = culturePath + "About/" + data.ArticleCategoryId;
                        }
                        else
                        {
                            switch (uri)
                            {
                                case "news":

                                    url = culturePath + "News/" + data.ArticleCategoryId;

                                    break;
                                case "location":
                                    url = culturePath + "About/Locations";


                                    break;
                                case "certification":

                                    url = culturePath + "About/Certification/" + data.ArticleCategoryId;
                                    break;
                            }
                        }
                      
                    }
                    catch { }
                    break;

                case "ArticlePage":
                    try
                    {
                        List<ArticlePage> model = dbData;

                        ArticlePage articlePage = model.Where(m => m.id == data.ArticlePageId).FirstOrDefault();

                        switch (articlePage.uri.ToString())
                        {
                            case "CS":
                                url = culturePath + "Sustainability/" + data.ArticlePageId;

                                break;
                            case "About":


                                url = culturePath + "About/" + data.ArticlePageId;

                                if(articlePage.id == "Legal")
                                {
                                    url = culturePath + "Terms/Legal";
                                }

                                if (articlePage.id == "Logo")
                                {
                                    url = culturePath + "Terms/Logo";
                                }
                                if (articlePage.id == "Privacy")
                                {
                                    url = culturePath + "Privacy";
                                }

                                break;
                            case "Careers":
                                url = culturePath + "Careers/" + data.ArticlePageId;


                                break;
                            case "Sustainability":

                                url = culturePath + "Sustainability/" + data.ArticlePageId;
                                break;
                        }
                    }
                    catch { }
                    break;

                case "Advertising":
                    try
                    {
                        List<Advertising> model = dbData;

                        Advertising advertisingPage = model.Where(m => m.id == data.AdvertisingId).FirstOrDefault();

                        switch (advertisingPage.uri.ToString())
                        {
                            case "MainAd":
                                if (advertisingPage.LangData.FirstOrDefault().title == "Career")
                                {
                                    url = culturePath + "Careers/Awards";
                                }
                                else if (advertisingPage.LangData.FirstOrDefault().title == "Corporate Sustainability")
                                {
                                    url = culturePath + "Sustainability/AMessageFromTheCEO";
                                }


                                
                                break;
                        }
                    }
                    catch { }
                    break;
            }

            return url;

        }

        /// <summary>
        /// 移除htmlTag
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public string removeHtmlTag(string str)
        {
            return System.Text.RegularExpressions.Regex.Replace(str, "<.*?>", string.Empty);
        }
    }


}
