﻿using Microsoft.AspNetCore.DataProtection;
using NETCore.Encrypt;

namespace WebApp.Services
{
    public class CipherService
    {     
        private string Key = "";       

        public CipherService()
        {          
            var appSeting = (new Helper()).GetAppSettings();
            Key = appSeting["APP_KEY"].ToString();

        }
        public string Encrypt(string input)
        {
            return (!string.IsNullOrEmpty(input)) ? EncryptProvider.AESEncrypt(input, Key) : "";
        }
        public string Decrypt(string cipherText)
        {
            return (!string.IsNullOrEmpty(cipherText)) ? EncryptProvider.AESDecrypt(cipherText, Key) : "";

        }
    }

   
}
