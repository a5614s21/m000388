﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class WebData
    {
        [Key]       
        public string id { get; set; }

        public string? path { get; set; }
        public string? uri { get; set; }

        /*   public string lang { get; set; }

           public string title { get; set; }

           public string? url { get; set; }

           public string? email { get; set; }

           public string? contact { get; set; }


           public string? details { get; set; }

           public string? social { get; set; }


           public string? logo { get; set; }
           public string? options { get; set; }

           public string? seo { get; set; }

           public string? ga { get; set; }


           public DateTime? created_at{ get; set; }
           public DateTime? updated_at { get; set; }
           */


        public List<LanguageResource> LangData { get; set; }

        //public virtual ICollection<ModelColumnConfig> columns { get; set; }
    }
}