﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class SpecData
    {
        [Key]
        public string id { get; set; }

        public string? spec_val { get; set; }
    
        public string? ProductSpecCategoryId { get; set; }

        public string? ProductSpecId { get; set; }

        public string? ProductSetId { get; set; }

    }
}