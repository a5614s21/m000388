﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class Contact
    {
        [Key]
        public string id { get; set; }

        public string? uri { get; set; }

        public string? lang { get; set; }

        public string? name { get; set; }
        public string? email { get; set; }

        public string? contact { get; set; }       

        public Boolean? active { get; set; }     

        public string? options { get; set; }

        public Boolean? send { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}