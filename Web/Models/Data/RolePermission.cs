﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class RolePermission
    {
        [Key]       
        public string id { get; set; }       
    
        public string RoleId { get; set; }

        public string MenuId { get; set; }

        public string? Permission { get; set; }

    }

}