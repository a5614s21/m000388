﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class Member
    {
        [Key]
        public string id { get; set; }

        public string? lang { get; set; }

        public string? username { get; set; }
        public string? password { get; set; }
       
        public string? name { get; set; }

        public string? contact { get; set; }       

        public Boolean? active { get; set; }     

        public string? options { get; set; }


        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}