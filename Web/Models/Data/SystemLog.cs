﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class SystemLog
    {
        [Key]
        public string id { get; set; }

        public string? lang { get; set; }

        public string? uri { get; set; }

        public string? title { get; set; }

        public string? username { get; set; }

        public string? act { get; set; }

        public string? details { get; set; }

        public Boolean? active { get; set; }
        public string? ip { get; set; }


        public string? options { get; set; }
        public int? sort { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}