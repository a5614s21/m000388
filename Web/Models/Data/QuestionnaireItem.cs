﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class QuestionnaireItem
    {
        [Key]
        public string id { get; set; }

        public string? path { get; set; }
        public string? uri { get; set; }

        public string? spec { get; set; }
        public string? category_id { get; set; }

        public string? type { get; set; }

        public string? options { get; set; }
        public dynamic OptionsData
        {
            get
            {
                if (!string.IsNullOrEmpty(options))
                {
                    return JsonConvert.DeserializeObject<FormModel>(options);
                }
                else
                {
                    return null;
                }

            }
        }
        public Boolean? active { get; set; }

        public int? sort { get; set; }

        public List<LanguageResource> LangData { get; set; }

    }
}