﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class SystemItem
    {
        [Key]
        public string id { get; set; }

        public string? lang { get; set; }

        public string? uri { get; set; }

        public string? title { get; set; }

        public string? details { get; set; }

        public Boolean? active { get; set; }     

        public string? options { get; set; }
        public int? sort { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}