﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class LanguageResource
    {
        [Key]
        public string? id { get; set; }

        public string? model_type { get; set; }

        public string? model_id { get; set; }

        public string? language_id { get; set; }

        public string? title { get; set; }

        public string? parent_id { get; set; }
        public string? category_id { get; set; }
        public string? application_id { get; set; }
        public string? input_type { get; set; }
        public string? spec { get; set; }

        public string? year { get; set; }

        public string? month { get; set; }

        public string? pic { get; set; }



        public dynamic PicData
        {
            get
            {
                if (!string.IsNullOrEmpty(pic))
                {
                    return JsonConvert.DeserializeObject<PicModel>(pic);
                }
                else
                {
                    return null;
                }

            }
        }


        public string? files { get; set; }


        public dynamic FilesData
        {
            get
            {
                if (!string.IsNullOrEmpty(files))
                {
                    return JsonConvert.DeserializeObject<FileModel>(files);
                }
                else
                {
                    return null;
                }

            }
        }


        public string? details { get; set; }

        public int? sort { get; set; }

        public dynamic DetailsData
        {
            get
            {
                if (!string.IsNullOrEmpty(details))
                {
                    return JsonConvert.DeserializeObject<JsonModel>(details);
                }
                else
                {
                    return null;
                }

            }
        }

        public Boolean? active { get; set; }

        public DateTime? start_at { get; set; }
        public DateTime? end_at { get; set; }

        public string? seo { get; set; }

        public dynamic SeoData
        {
            get
            {
                if (!string.IsNullOrEmpty(seo))
                {
                    return JsonConvert.DeserializeObject<JsonModel>(seo);
                }
                else
                {
                    return null;
                }

            }
        }

        public string? tags { get; set; }

        public string? series { get; set; }

        public string? options { get; set; }
        public dynamic OptionsData
        {
            get
            {
               if(!string.IsNullOrEmpty(options))
                {
                    return JsonConvert.DeserializeObject<JsonModel>(options);
                }
                else
                {
                    return null;
                }
             
            }
        }
        public Boolean? top { get; set; }

        public string? social { get; set; }

        public dynamic SocialData
        {
            get
            {
                if (!string.IsNullOrEmpty(social))
                {
                    return JsonConvert.DeserializeObject<Social>(social);
                }
                else
                {
                    return null;
                }

            }
        }

        public string? video { get; set; }
        public dynamic VideoData
        {
            get
            {
                if (!string.IsNullOrEmpty(video))
                {
                    return JsonConvert.DeserializeObject<List<VideoModel>>(video);
                }
                else
                {
                    return null;
                }

            }
        }

        public string? contact { get; set; }

        public dynamic ContactData
        {
            get
            {
                if (!string.IsNullOrEmpty(contact))
                {
                    return JsonConvert.DeserializeObject<JsonModel>(contact);
                }
                else
                {
                    return null;
                }

            }
        }

        public string? ga { get; set; }
        public string? specs { get; set; }

        public string? feature { get; set; }
        public string? product_tags { get; set; }

        public string? safety { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }


        public string? ArticlePageId { get; set; }
        public string? ArticleNewsId { get; set; }
        public string? ArticleCategoryId { get; set; }

        public string? AdvertisingCategoryId { get; set; }

        public string? AdvertisingId { get; set; }

        public string? ArticleColumnId { get; set; }

        public string? ArticleDownloadId { get; set; }
        public string? ArticleLocationId { get; set; }

        public string? SystemMenuId { get; set; }

        public string? WebDataId { get; set; }

        public string? ProductCategoryId { get; set; }
        public string? ProductSetId { get; set; }


        public string? ProductSpecCategoryId { get; set; }
        public string? ProductSpecId { get; set; }

        public string? SiteParameterGroupId { get; set; }

        public string? SiteParameterItemId { get; set; }


        public string? QuestionnaireGroupId { get; set; }

        public string? QuestionnaireItemId { get; set; }


        public string? InboxNotifyId { get; set; }

    }
}