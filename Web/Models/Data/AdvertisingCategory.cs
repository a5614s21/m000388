﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class AdvertisingCategory
    {
        [Key]
        public string id { get; set; }

        public string? path { get; set; }
        public string? uri { get; set; }

        public string? parent_id { get; set; }

        public Boolean? active { get; set; }

        public int? sort { get; set; }

        public DateTime? start_at { get; set; }
        public DateTime? end_at { get; set; }
        public List<LanguageResource> LangData { get; set; }

        //public List<Advertising> Advertising { get; set; }
    }
}