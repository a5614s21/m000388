﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class DataText
    {
        [Key]
        public string id { get; set; }

        public string model_type { get; set; }

        public string TestNewsid { get; set; }

        public string language_id { get; set; }

        public string title { get; set; }

        public string content { get; set; }

        public string active { get; set; }

        public string options { get; set; }

        public dynamic OptionJson
        {
            get => JsonConvert.DeserializeObject<List<JsonModel>>(options); 
        }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }
    }
}