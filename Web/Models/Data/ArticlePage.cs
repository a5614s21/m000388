﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class ArticlePage
    {
        [Key]
        public string id { get; set; }

        public string? path { get; set; }
        public string? uri { get; set; }


        public string? category_id { get; set; }

        public Boolean? active { get; set; }

        public int? sort { get; set; }


        public List<LanguageResource> LangData { get; set; }
    }
}