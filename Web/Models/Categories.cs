﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class Categories
    {
        public string id { get; set; }

        public string? lang { get; set; }

        public string? title { get; set; }

        public string? uri { get; set; }

        public string? link { get; set; }

        public string? parent_id { get; set; }

        public string? pic { get; set; }

        public dynamic PicData
        {
            get
            {
                if (!string.IsNullOrEmpty(pic))
                {
                    return JsonConvert.DeserializeObject<PicModel>(pic);
                }
                else
                {
                    return null;
                }

            }
        }


        public string? icon { get; set; }

        public string? details { get; set; }

        public Boolean? active { get; set; }

        public int? sort { get; set; }

        public string? options { get; set; }

        public string? seo { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }

        public dynamic? sub_categories { get; set; }

    }


}