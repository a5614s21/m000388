﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class JsonModel
    {
        public string? path { get; set; }
        public string? alt { get; set; }
        public string? en_title { get; set; }
        public string? notes { get; set; }
        public string? editor { get; set; }
        public string? no { get; set; }

        public string? youtube { get; set; }

        public string? video_info { get; set; }
        public string? address { get; set; }
        public string? map { get; set; }

        public string? tel { get; set; }

        public string? fax { get; set; }

        public string? post_code { get; set; }

        public string? email { get; set; }

        public string? company { get; set; }

        public string? PowerModuleSolutions { get; set; }

        public string? subject { get; set; }
        public string? name { get; set; }

        public string? color { get; set; }

        public string? link { get; set; }

        public string? sub_title { get; set; }

        public string? Country { get; set; }

        public string? Subject { get; set; }
        public string? Description { get; set; }

        public string? Company { get; set; }

    }

    public class OptionsData
    {
        public List<string>? ModelName { get; set; }

        public List<string>? ProductCategory { get; set; }
    }


    public class VideoModel
    {
        public string? path { get; set; }
        public string? title { get; set; }
        public string? sub_title { get; set; }
    }


    public class Social
    {
        public string? facebook { get; set; }
        public string? ig { get; set; }
        public string? line { get; set; }
    }

    public class PicModel
    {
        public List<PicDetailModel>? pic { get; set; }
        public List<PicDetailModel>? icon { get; set; }
        public List<PicDetailModel>? logo { get; set; }
        public List<PicDetailModel>? path_pic { get; set; }


    }

    public class PicDetailModel
    {
        public string? path { get; set; }
        public string? title { get; set; }
        public string? link { get; set; }
        public string? video { get; set; }

    }



    public class FileModel
    {
        public List<FileDetailModel>? file { get; set; }

        public List<FileDetailModel>? file2 { get; set; }
    }

    public class FileDetailModel
    {
        public string? path { get; set; }
        public string? title { get; set; }
        public string? link { get; set; }
        public string? video { get; set; }

    }

    public class SpecModel
    {
        public string? id { get; set; }
        public string? title { get; set; }

        public string? input_type { get; set; }

        public string? val { get; set; }

        public List<ProductSpec> ProductSpec { get; set; }
    }

    /// <summary>
    /// 季報表Media
    /// </summary>
    public class MediaModel
    {
        public string? path { get; set; }

        public string? title { get; set; }

        public string? sub_title { get; set; }

        public string? type { get; set; }

    }

    public class SearchTags
    {
        public string? id { get; set; }
        public string? title { get; set; }
        public string? url { get; set; }
    }

    /// <summary>
    /// 搜尋結果用
    /// </summary>
    public class SearchResult
    {
        public string? id { get; set; }
        public string? model_type { get; set; }


        public string? language_id { get; set; }

        public string? title { get; set; }

        public string? parent_id { get; set; }
        public string? category_id { get; set; }
        public string? application_id { get; set; }

        public string? pic { get; set; }


        public dynamic PicData
        {
            get
            {
                if (!string.IsNullOrEmpty(pic))
                {
                    return JsonConvert.DeserializeObject<PicModel>(pic);
                }
                else
                {
                    return null;
                }

            }
        }


        public string? files { get; set; }



        public string? details { get; set; }

        public int? sort { get; set; }

        public dynamic DetailsData
        {
            get
            {
                if (!string.IsNullOrEmpty(details))
                {
                    return JsonConvert.DeserializeObject<JsonModel>(details);
                }
                else
                {
                    return null;
                }

            }
        }


        public string? tags { get; set; }

        public string? series { get; set; }

       
        public Boolean? top { get; set; }
             



        public string? specs { get; set; }

        public string? feature { get; set; }
        public string? product_tags { get; set; }

        public string? safety { get; set; }

        public DateTime? created_at { get; set; }
        public DateTime? updated_at { get; set; }


        public string? ProductCategoryId { get; set; }
        public string? ProductSetId { get; set; }




    }


    public class FormModel
    {
        public string? type { get; set; }
        public string? row_id { get; set; }
        public string? required { get; set; }
        public string? value { get; set; }
    }
}