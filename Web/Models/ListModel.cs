﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class ListModel
    {
        public string? id { get; set; }
        public string? lang { get; set; }
        public string? title { get; set; }
        public string? path { get; set; }
        public string? uri { get; set; }
        public string? link { get; set; }

        public string? parent_id { get; set; }
        public string? category_id { get; set; }
        public string? category_title { get; set; }
        public List<JsonModel>? pic { get; set; }

        public Dictionary<string, string>? details { get; set; }
        public bool? active { get; set; }

        public Dictionary<string, string>? options { get; set; }
        public Dictionary<string, string>? seo { get; set; }

        public int? sort { get; set; }

        public string? created_at { get; set; }
        public string? updated_at { get; set; }
        public string? start_at { get; set; }
        public string? end_at { get; set; }

        public List<JsonModel>? icon { get; set; }

        public Dictionary<string, string>? list_pic { get; set; }

        public Dictionary<string, string>? index_pic { get; set; }

        public List<JsonModel>? files { get; set; }


    }
}