﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApp.Models
{
    public class FormRequest
    {
        public string? id { get; set; }
        public string? lang { get; set; }
        public string? title { get; set; }
        public string? uri { get; set; }

        public string? path { get; set; }
        public string? link { get; set; }
        public string? url { get; set; }
        public string? parent_id { get; set; }
        public string? category_id { get; set; }
        public string? application_id { get; set; }

        public string? pic { get; set; }

        public string? video { get; set; }


        public string? files { get; set; }
        public string? input_type { get; set; }

        public List<string>? spec { get; set; }

        public List<string>? feature { get; set; }
        public List<string>? product_tags { get; set; }

        public List<string>? safety { get; set; }

        public List<string>? series { get; set; }

        public Dictionary<string, string>? specs { get; set; }

        public Dictionary<string, string>? details { get; set; }
        public bool? active { get; set; }
        public bool? top { get; set; }
        public Dictionary<string, string>? options { get; set; }
        public Dictionary<string, string>? seo { get; set; }

        public Dictionary<string, string>? social { get; set; }
        public int? sort { get; set; }

        public DateTime? updated_at { get; set; }

        public string? created_at { get; set; }
        public string? start_at { get; set; }
        public string? end_at { get; set; }

        public string? icon { get; set; }

        public string? list_pic { get; set; }

        public string? index_pic { get; set; }

        public string? header_pic { get; set; }

        public string? index_right_pic { get; set; }
        public Dictionary<string, string>? contact { get; set; }

        public string? name { get; set; }
        public string? username { get; set; }

        public string? password { get; set; }

        public string? email { get; set; }

        public string? year { get; set; }

        public string? month { get; set; }

        public string? tags { get; set; }

        public string? ip { get; set; }
        public bool? rule { get; set; }

        public string? platform { get; set; }

        public List<string>? value1 { get; set; }
        public List<string>? value2 { get; set; }
        public string? Spec { get; set; }
    }
}