﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;

namespace Web.Repositories.Web
{
    public class ArticlePageRepository
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();


        public async Task<ArticlePage> FindArticlePage(string uri, string lang)
        {
         
            return DB.ArticlePage.Where(x => x.path == uri || x.id == uri)
                       .Include(x => x.LangData.Where(l => l.language_id == lang)).FirstOrDefault();


        }

        public async Task<ArticleCategory> FindArticleCategory(string uri, string lang)
        {

            return DB.ArticleCategory.Where(x => x.path == uri || x.id == uri)
                       .Include(x => x.LangData.Where(l => l.language_id == lang)).FirstOrDefault();


        }

        public async Task<List<ArticlePage>> FindArticlePages(string lang , string uri)
        {

            return DB.ArticlePage
                .Where(m=>m.uri == uri)
                .Where(m=>m.active == true)
                .OrderBy(x => x.sort)
                       .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();


        }

        public async Task<List<ArticlePage>> FindAllArticlePage(string lang)
        {

            return DB.ArticlePage
                .OrderBy(x => x.sort)
                       .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();


        }

        public async Task<List<ArticleLocation>> FindArticleLocations(string lang)
        {

            return DB.ArticleLocation.Where(x => x.active == true)
                .OrderBy(x => x.sort)
                       .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();


        }

        public List<ArticleCategory> FindArticleCategories(string parent_id , string lang)
        {

            return DB.ArticleCategory.Where(x => x.active == true)
                .Where(x => x.parent_id == parent_id)
                .OrderBy(x => x.sort)
                       .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();


        }


        public async Task<dynamic> FindArticleColumnMilestones(string uri, string lang)
        {

            var model =  DB.ArticleColumn.Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)
                       )
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.year)
                       .ThenBy(m => m.sort)
                       .Select(m => m.year).Distinct()
                       .OrderByDescending(q => q);


            List< ArticleColumn > data = DB.ArticleColumn.Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)
                       )
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.year)   
                       .ToList();


            Dictionary<string, List<ArticleColumn>> re = new Dictionary<string, List<ArticleColumn>>();

            foreach (string year in model)
            {
                re.Add(year, data.Where(m => m.year == year).OrderByDescending(m=>int.Parse(m.month)).ToList());

            }


            return re;



        }



        /// <summary>
        /// 計算頁碼用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindUsePage(string id, string lang , string uri = "")
        {
            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();


            return DB.ArticleDownload
              .Where(m => m.category_id == id)
              .Where(m => ids.Contains(m.id))
                  .Where(m => m.uri == uri)
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)

                       )
                       .Where(m => m.active == true)
                       .OrderBy(m => m.sort);
        }

        /// <summary>
        /// 依照分頁取得資料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindList(string id, string lang, int pageSize, string uri = "", int? page = 1, string tagId = "")
        {
            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();


            return DB.ArticleDownload
                .Where(m =>  m.category_id == id)
                .Where(m => ids.Contains(m.id))
                .Where(m => m.uri == uri)
                         .Include(x => x.LangData
                            .Where(l => l.language_id == lang)
                         )                      
                            .Where(m => m.active == true)
                         .OrderBy(m => m.sort).Skip<ArticleDownload>(pageSize * ((page ?? 1) - 1)).Take(pageSize).ToListAsync();

        }

    }
}
