﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Web
{
    public class AdvertisingCategoryRepository
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();

        /// <summary>
        /// 廣告列表
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public List<Advertising> FindAdvertising(string uri, string lang)
        {
            /* var AdvertisingId = DB.AdvertisingCategory
                  .Where(m => m.uri == uri).FirstOrDefault()?.id;

             return DB.Advertising.Where(x => x.category_id == AdvertisingId).Where(m => m.active == true).OrderBy(m => m.sort)
                        .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();*/

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "Advertising")
                .Where(l => l.language_id == lang).Select(m => m.AdvertisingId).ToList();




            return DB.Advertising.Where(x => x.uri == uri).Where(m => m.active == true)
                   .Where(m => ids.Contains(m.id))
                .OrderBy(m => m.sort)
                      .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();

        }

        /// <summary>
        /// 廣告列表  含 分類標題
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public dynamic FindCategoryAdvertising(string uri, string lang)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "Advertising")
                .Where(l => l.language_id == lang).Select(m => m.AdvertisingId).ToList();

            var category = DB.AdvertisingCategory
           .Include(x => x.LangData.Where(l => l.language_id == lang))
           .Where(m => m.uri == "FinancialReport").FirstOrDefault();

            LanguageResource LangData = category.LangData.FirstOrDefault();
            try
            {
                Dictionary<string, List<Advertising>> data = new Dictionary<string, List<Advertising>>
                {
                    {
                        LangData?.title ,
                        DB.Advertising.Where(x => x.uri == uri).Where(m => ids.Contains(m.id)).Where(m=>m.active == true).OrderBy(m => m.sort)
                           .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList()
                    }
                };

                return data;
            }
            catch
            {
                return null;
            }



        }

    }
}
