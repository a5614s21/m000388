﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Web
{
    public class ProductCategoryRepository
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();

        /// <summary>
        /// 計算頁碼用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindUsePage(string id, string lang, string tagId = "")
        {

            return DB.ProductSet
                .Where(m => ((id != "") ? m.category_id == id : m.id != ""))
                .Where(m => ((tagId != "") ? m.product_tags.Contains(tagId) : m.id != ""))
                       .Include(x => x.LangData.Where(l => l.language_id == lang))
                       .OrderBy(m => m.sort);
        }

      
        /// <summary>
        /// 依照分頁取得資料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindProductSetList(string id, string lang, int pageSize, int? page = 1, string tagId = "")
        {

            return DB.ProductSet
                .Where(m => ((id != "") ? m.category_id == id : m.id != ""))
                .Where(m => ((tagId != "") ? m.product_tags.Contains(tagId) : m.id != ""))
                         .Include(x => x.LangData.Where(l => l.language_id == lang))
                         .OrderBy(m => m.sort).Skip<ProductSet>(pageSize * ((page ?? 1) - 1)).Take(pageSize).ToListAsync();

        }

        /// <summary>
        /// 計算頁碼用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindUsePageSearch(string id, string lang, string keywords)
        {


            List<string> modelId = DB.LanguageResource.Where(m => m.model_type == "ProductSet")              
                .Where(m => m.active == true)
                .Where(m => m.title.Contains(keywords) || m.details.Contains(keywords))
                .Where(m => m.language_id == lang)
                .Select(m=>m.ProductSetId)
                .ToList();
           

            return DB.ProductSet
                .Where(m=>m.active == true)
                .Where(m => modelId.Contains(m.id))              
                       .Include(x => x.LangData.Where(l => l.language_id == lang))
                       .OrderBy(m => m.sort);
        }

        public dynamic FindUsePageSearchCount(string id, string lang, string keywords)
        {

            List<string> modelId = DB.LanguageResource.Where(m => m.model_type == "ProductSet")              
                .Where(m => m.active == true)
                .Where(m => m.title.Contains(keywords) || m.details.Contains(keywords))
                .Where(m => m.language_id == lang)
                .Select(m=>m.ProductSetId)
                .ToList();
           

            return DB.ProductSet
                .Where(m=>m.active == true)
                .Where(m => modelId.Contains(m.id))              
                       .Include(x => x.LangData.Where(l => l.language_id == lang))
                       .OrderBy(m => m.sort).Count();
        }

        public dynamic FindProductSetSearchList(string id, string lang, int pageSize, string keywords, int? page = 1)
        {

            List<string> modelId = DB.LanguageResource.Where(m => m.model_type == "ProductSet")
              .Where(m => m.active == true)
               .Where(m => m.title.Contains(keywords) || m.details.Contains(keywords))
              .Where(m => m.language_id == lang)
              .Select(m => m.ProductSetId)
              .ToList();


            return DB.ProductSet
                .Where(m => m.active == true)
                .Where(m => modelId.Contains(m.id))
                         .Include(x => x.LangData.Where(l => l.language_id == lang))
                         .OrderBy(m => m.sort).Skip<ProductSet>(pageSize * ((page ?? 1) - 1)).Take(pageSize).ToListAsync();

        }


        /// <summary>
        /// 目前分類
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public ProductCategory FindProductCategory(string id, string lang)
        {
            var model = DB.ProductCategory
                    .Where(m => m.path == id || m.id == id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .FirstOrDefault();

            return model;
        }

   



    }
}
