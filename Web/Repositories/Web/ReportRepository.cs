﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Web
{
    public class ReportRepository
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();


        /// <summary>
        /// 計算頁碼用
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public dynamic FindUsePage(string uri, string lang , bool distinct = true  )
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();

            if (distinct)
            {
                return DB.ArticleDownload
                .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                .Where(m => ids.Contains(m.id))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)
                       )
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.year)
                       .ThenBy(m => m.sort)
                       .Select(m => m.year).Distinct()
                       .OrderByDescending(q => q);
            }
            else
            {
                return DB.ArticleDownload
                .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                .Where(m => ids.Contains(m.id))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)
                       )
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.year)
                       .ThenBy(m => m.sort)
                       .OrderByDescending(q => q);
            }
            



        }

        /// <summary>
        /// 依照分頁取得資料
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindList(string uri, string lang, int pageSize, int? page = 1, bool distinct = true, string tagId = "")
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();

            if (distinct)
            {
                return DB.ArticleDownload
                .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                .Where(m => ids.Contains(m.id))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)
                       )
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.year)
                       .ThenBy(m => m.sort)
                       .Select(m => m.year).Distinct()
                       .OrderByDescending(q => q)
                       .Skip(pageSize * ((page ?? 1) - 1)).Take(pageSize)
                       .ToListAsync();
            }
            else
            {


                return DB.ArticleDownload
               .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
               .Where(m => ids.Contains(m.id))
                      .Include(
                           x => x.LangData
                           .Where(l => l.language_id == lang)
                      )
                      .Where(m => m.active == true)
                      .OrderByDescending(m => m.year)
                      .ThenBy(m => m.sort)                   
                      .Skip(pageSize * ((page ?? 1) - 1)).Take(pageSize)
                      .ToListAsync();


            }

        }


        public dynamic FindListFilings(string uri, string lang, int pageSize, int? page = 1, bool distinct = true, string tagId = "")
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();

           

                return DB.ArticleDownload
               .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
               .Where(m => ids.Contains(m.id))
                      .Include(
                           x => x.LangData
                           .Where(l => l.language_id == lang)
                      )
                      .Where(m => m.active == true)
                      .OrderByDescending(m => m.LangData.FirstOrDefault().title)
                      .ThenBy(m => m.sort)
                      .Skip(pageSize * ((page ?? 1) - 1)).Take(pageSize)
                      .ToListAsync();



        }


        public dynamic FindListAll(string uri, string lang, bool distinct = true)
        {
            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();


            if (distinct)
            {
                return DB.ArticleDownload
                .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                .Where(m => ids.Contains(m.id))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)
                       )
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.year)
                       .ThenBy(m => m.sort)
                       .Select(m => m.year).Distinct()
                       .OrderByDescending(q => q)
                       .ToList();
            }
            else
            {
                return DB.ArticleDownload
               .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
               .Where(m => ids.Contains(m.id))
                      .Include(
                           x => x.LangData
                           .Where(l => l.language_id == lang)
                      )
                      .Where(m => m.active == true)
                      .OrderByDescending(m => m.year)
                      .ThenBy(m => m.sort)
                      .ToList();
            }

        }



        /// <summary>
        /// 取得ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public dynamic FindAllReportByYearMonth(string uri, string lang)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();


            List<ArticleDownload> model =  DB.ArticleDownload
                 .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                 .Where(m => ids.Contains(m.id))
                        .Include(
                             x => x.LangData
                             .Where(l => l.language_id == lang)
                        )
                        .Where(m => m.active == true)
                        .OrderByDescending(m => m.year)
                        .ThenBy(m => m.sort)                        
                        .ToList();

            Dictionary<string, List<LanguageResource>> data = new Dictionary<string, List<LanguageResource>>();

            foreach(ArticleDownload item in model)
            {
                LanguageResource LangData = item.LangData.FirstOrDefault();



                if(!data.ContainsKey(item.year + "-" + item.month))
                {
                    data.Add(item.year + "-" + item.month, new List<LanguageResource> { LangData });
                }
                else
                {
                    List<LanguageResource> temp = data[item.year + "-" + item.month];
                    temp.Add(LangData);
                    data[item.year + "-" + item.month] = temp;

                }
                

            }

            return data;
        }


        public dynamic FindAllReportQuarterly(string uri, string lang)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == lang).Select(m => m.ArticleDownloadId).ToList();

            List<ArticleDownload> model = DB.ArticleDownload
                   .Where(m => ((uri != "") ? m.uri == uri : m.uri != ""))
                   .Where(m => ids.Contains(m.id))
                          .Include(
                               x => x.LangData
                               .Where(l => l.language_id == lang)
                          )
                          .Where(m => m.active == true)
                          .OrderByDescending(m => m.year)
                          .ThenBy(m => m.sort)
                          .ToList();

            Dictionary<string, List<LanguageResource>> data = new Dictionary<string, List<LanguageResource>>();

            foreach (ArticleDownload item in model)
            {
                LanguageResource LangData = item.LangData.FirstOrDefault();


                if(!data.ContainsKey(item.year + "-" + item.month))
                {
                    data.Add(item.year + "-" + item.month, new List<LanguageResource> { LangData });
                }
                else
                {           

                    data[item.year + "-" + item.month].Add(LangData);
                }
              

            }

            return data;
        }




        /// <summary>
        /// Top資訊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public dynamic FindTop(string id, string lang)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleNews")
                .Where(l => l.language_id == lang).Select(m => m.ArticleNewsId).ToList();


            return DB.ArticleNews
                .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
                .Where(m => ids.Contains(m.id))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)

                       )
                        .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
                            .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
                       .Where(m => m.active == true)
                       .Where(m => m.top == true)
                       .OrderByDescending(m => m.top)
                       .ThenBy(m => m.sort).ToList();
        }

        /// <summary>
        /// 目前分類
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<ArticleCategory> FindCategory(string id, string lang)
        {
            var model = DB.ArticleCategory
                    .Where(m => m.path == id || m.id == id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .FirstOrDefault();

            return model;
        }

        /// <summary>
        /// 所有分類
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<List<ArticleCategory>> FindCategories(string lang)
        {
            var model = DB.ArticleCategory
                .Where(m => m.active == true)
                .Where(m => m.parent_id != "")
                .Where(m => m.uri == "news")
                .OrderBy(m => m.sort)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .ToList();

            return model;
        }

        /// <summary>
        /// 新聞內容
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<ArticleDownload> FindDetail(string id, string lang)
        {
            var model = DB.ArticleDownload
                    .Where(m => m.path == id || m.id == id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .FirstOrDefault();

            return model;
        }


    }

   
}
