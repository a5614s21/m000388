﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Web
{
    public class ArticleNewsRepository
    {
        protected ApplicationDbContext DB = new ApplicationDbContext();


        /// <summary>
        /// 計算頁碼用
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindUsePage(string id, string lang, string uri)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleNews")
                .Where(l => l.language_id == lang).Select(m => m.ArticleNewsId).ToList();


            return DB.ArticleNews
                .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
                .Where(m => ids.Contains(m.id))
                  .Where(m => m.uri == uri)
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)

                       )
                        .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
                            .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
                       .Where(m => m.active == true)
                       .Where(m => m.LangData.Count > 0)
                       .OrderByDescending(m => m.top)
                       .ThenByDescending(m => m.LangData.FirstOrDefault().created_at);
        }

        /// <summary>
        /// 依照分頁取得資料
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <param name="pageSize"></param>
        /// <param name="page"></param>
        /// <param name="tagId"></param>
        /// <returns></returns>
        public dynamic FindList(string id, string lang, int pageSize, string uri, int? page = 1, string tagId = "")
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleNews")
                .Where(l => l.language_id == lang).Select(m => m.ArticleNewsId).ToList();

            var rearticlenews = DB.ArticleNews
                .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
                .Where(m => ids.Contains(m.id))
                .Where(m => m.uri == uri)
                         .Include(x => x.LangData
                            .Where(l => l.language_id == lang)


                         )
                             .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
                            .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
                            .Where(m => m.active == true)
                            .Where(m => m.LangData.Count > 0)
                        // .OrderByDescending(m => m.top)
                        .OrderByDescending(m => !string.IsNullOrEmpty(m.LangData.FirstOrDefault().start_at.ToString()) ? m.LangData.FirstOrDefault().start_at : m.LangData.FirstOrDefault().created_at)
                         //.ThenByDescending(m => m.LangData.FirstOrDefault().created_at)
                         .ToList();

            List<ArticleNews> resultArticleNews = new List<ArticleNews>();

            foreach (var item in rearticlenews)
            {
                string tempcid = item.category_id;
                var judgecategory = (from a in DB.ArticleCategory
                                     where a.id == tempcid
                                     select a).FirstOrDefault();
                if (judgecategory.active == true)
                {
                    resultArticleNews.Add(item);
                }
            }

            //return DB.ArticleNews
            //    .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
            //    .Where(m => ids.Contains(m.id))
            //    .Where(m => m.uri == uri)
            //             .Include(x => x.LangData
            //                .Where(l => l.language_id == lang)


            //             )
            //                 .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
            //                .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
            //                .Where(m => m.active == true)
            //                .Where(m => m.LangData.Count > 0)
            //            // .OrderByDescending(m => m.top)
            //            .OrderByDescending(m => !string.IsNullOrEmpty(m.LangData.FirstOrDefault().start_at.ToString()) ? m.LangData.FirstOrDefault().start_at : m.LangData.FirstOrDefault().created_at)
            //           //.ThenByDescending(m => m.LangData.FirstOrDefault().created_at)
            //             .Skip<ArticleNews>(pageSize * ((page ?? 1) - 1)).Take(pageSize).ToListAsync();
            return resultArticleNews.Skip<ArticleNews>(pageSize * ((page ?? 1) - 1)).Take(pageSize).ToList();

        }
        /// <summary>
        /// 取得ID
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public dynamic FindDetailOnlyId(string id, string lang, string uri)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleNews")
                .Where(l => l.language_id == lang).Select(m => m.ArticleNewsId).ToList();


            //return DB.ArticleNews
            //    .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
            //     .Where(m => ids.Contains(m.id))
            //    .Where(m => m.uri == uri)
            //           .Include(
            //                x => x.LangData
            //                .Where(l => l.language_id == lang)

            //           )
            //            .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
            //                .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
            //           .Where(m => m.active == true)
            //             .OrderByDescending(m => m.top)
            //           .ThenByDescending(m => m.LangData.FirstOrDefault().created_at).Select(m => m.id).ToList();
            return DB.ArticleNews
                .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
                 .Where(m => ids.Contains(m.id))
                .Where(m => m.uri == uri)
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)

                       )
                        .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
                            .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
                       .Where(m => m.active == true)
                       .OrderByDescending(m => m.LangData.FirstOrDefault().created_at).Select(m => m.id).ToList();
        }

        /// <summary>
        /// Top資訊
        /// </summary>
        /// <param name="id"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public dynamic FindTop(string id, string lang)
        {
            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleNews")
                .Where(l => l.language_id == lang).Select(m => m.ArticleNewsId).ToList();

            return DB.ArticleNews
                   .Where(m => ids.Contains(m.id))
                .Where(m => ((id != "All") ? m.category_id == id : m.id != ""))
                       .Include(
                            x => x.LangData
                            .Where(l => l.language_id == lang)

                       )
                        .Where(m => m.start_at <= DateTime.Now || string.IsNullOrEmpty(m.start_at.ToString()))
                            .Where(m => m.end_at >= DateTime.Now || string.IsNullOrEmpty(m.end_at.ToString()))
                       .Where(m => m.active == true)
                       .Where(m => m.top == true)
                       .OrderByDescending(m => !string.IsNullOrEmpty(m.LangData.FirstOrDefault().start_at.ToString()) ? m.LangData.FirstOrDefault().start_at : m.LangData.FirstOrDefault().created_at)
                       //.OrderByDescending(m => m.top)
                       //.ThenByDescending(m => m.LangData.FirstOrDefault().created_at).Skip(0).Take(4).ToList();
                       .Skip(0).Take(4).ToList();
        }

        /// <summary>
        /// 目前分類
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<ArticleCategory> FindCategory(string id, string lang)
        {
            var model = DB.ArticleCategory
                    .Where(m => m.path == id || m.id == id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .FirstOrDefault();

            return model;
        }

        /// <summary>
        /// 所有分類
        /// </summary>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<List<ArticleCategory>> FindCategories(string lang)
        {
            var model = DB.ArticleCategory
                .Where(m => m.active == true)
                .Where(m => m.parent_id == "news")
                // .Where(m => m.uri == "news")
                .OrderBy(m => m.sort)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .ToList();

            return model;
        }

        public async Task<List<ArticleCategory>> FindAllCategories(string lang)
        {
            var model = DB.ArticleCategory
                .Where(m => m.active == true)
                .Where(m => m.parent_id != "")
                // .Where(m => m.uri == "news")
                .OrderBy(m => m.sort)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .ToList();

            return model;
        }

        /// <summary>
        /// 新聞內容
        /// </summary>
        /// <param name="uri"></param>
        /// <param name="lang"></param>
        /// <returns></returns>
        public async Task<ArticleNews> FindDetail(string id, string lang)
        {
            var model = DB.ArticleNews
                    .Where(m => m.path == id || m.id == id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .FirstOrDefault();

            return model;
        }
    }
}
