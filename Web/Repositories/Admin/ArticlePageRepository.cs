﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class ArticlePageRepository : IRepository<ArticlePage, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ArticlePageRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(ArticlePage entity, LanguageResource entityLang)
        {
            _context.ArticlePage.Add(entity);

            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();
            return entity.id;

        }

        public void Update(ArticlePage entity, LanguageResource entityLang)
        {
            var data = _context.ArticlePage.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
              .LangData.Where(m => m.language_id == entityLang.language_id)
              .FirstOrDefault();

            var model = _context.ArticlePage.Where(x => x.id == entity.id).FirstOrDefault();

            model.active = entity.active;
            model.uri = entity.uri;

            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;


                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }

        }

        public void Delete(string id)
        {
            _context.ArticlePage.Remove(_context.ArticlePage.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.model_id == id));
            _context.SaveChanges();
        }

        public IEnumerable<ArticlePage> Find(Expression<Func<ArticlePage, bool>> expression)
        {
            return _context.ArticlePage.Where(expression);
        }

        public ArticlePage FindById(string id)
        {
            return _context.ArticlePage.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.ArticlePage.Where(x => x.id == id)
                  .Include(x => x.LangData
                  .Where(m => m.language_id == lang)
                   ).FirstOrDefault()?
                  .LangData
                  .FirstOrDefault();
        }

        public IQueryable<ArticlePage> FindList(string parent_id)
        {
            return _context.ArticlePage.Where(m=>m.category_id == parent_id);
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.ArticlePage.Where(x => x.id == parent_id)
                  .Include(x => x.LangData
                  .Where(l => l.ArticlePageId == parent_id)
                   ).FirstOrDefault()?
                  .LangData.ToList();
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.ArticleCategory.Where(x => x.id == id)
                  .Include(x => x.LangData
                  .Where(l => l.model_id == id)
                   ).FirstOrDefault()?
                  .LangData.Where(m => m.language_id == lang)
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.ArticlePage.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("title", new Dictionary<string, string>() { { "title", "類別名稱" }, { "type", "text" } });
            re.Add("created_at", new Dictionary<string, string>() { { "title", "建立日期" }, { "type", "text" } });
            if(uri == "CS")
            {
                re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "text" } });
                re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            }
            //
            re.Add("modify", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify_only_edit" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }


        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            if (uri == "CS")
            {
                return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
            }
            else
            {
                return new Dictionary<string, string> { { "key", "title" }, { "type", "asc" } };
            }
               
        }

    }
}
