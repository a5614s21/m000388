﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class ArticleNewsRepository : IRepository<ArticleNews, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ArticleNewsRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(ArticleNews entity, LanguageResource entityLang)
        {
            _context.ArticleNews.Add(entity);

            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(ArticleNews entity, LanguageResource entityLang)
        {

            var data = _context.ArticleNews.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
                .LangData.Where(m => m.language_id == entityLang.language_id)
                .FirstOrDefault();

            var model = _context.ArticleNews.Where(x => x.id == entity.id).FirstOrDefault();
            model.start_at = entity.start_at != null ? DateTime.Parse(entity.start_at.ToString()) : null;
            model.end_at = entity.end_at != null ? DateTime.Parse(entity.end_at.ToString()) : null;
            model.top = entity.top;
            model.active = entity.active;
            model.category_id = entity.category_id;


            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;
                data.files = entityLang.files != null ? entityLang.files : null;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;

                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }


        }

        public void Delete(string id)
        {
            _context.ArticleNews.Remove(_context.ArticleNews.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.ArticleNewsId == id));
            _context.SaveChanges();
        }

        public IEnumerable<ArticleNews> Find(Expression<Func<ArticleNews, bool>> expression)
        {
            return _context.ArticleNews.Where(expression);
        }

        public ArticleNews FindById(string id)
        {
            return _context.ArticleNews.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.ArticleNews.Where(x => x.id == id)
             .Include(x => x.LangData
             .Where(m => m.language_id == lang)
              ).FirstOrDefault()?
             .LangData
             .FirstOrDefault();

        }

        public IQueryable<ArticleNews> FindList(string parent_id)
        {
            return _context.ArticleNews.Where(m => m.uri == parent_id);
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.ArticleNews.Where(x => x.id == parent_id)
                   .Include(x => x.LangData
                   .Where(l => l.ArticleNewsId == parent_id)
                    ).FirstOrDefault()?
                   .LangData.ToList();
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.ArticleCategory.Where(x => x.id == id)
                  .Include(x => x.LangData.Where(m => m.language_id == lang)
                   ).FirstOrDefault()?
                  .LangData
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.ArticleNews.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("pic", new Dictionary<string, string>() { { "title", "圖片" }, { "type", "pic" } });
            re.Add("title", new Dictionary<string, string>() { { "title", "類別名稱" }, { "type", "text" } });
            re.Add("category_id", new Dictionary<string, string>() { { "title", "分類" }, { "type", "text" } });
            // re.Add("created_at", new Dictionary<string, string>() { { "title", "建立日期" }, { "type", "text" } });
            re.Add("start_at", new Dictionary<string, string>() { { "title", "發佈日期" }, { "type", "text" } });
            re.Add("top", new Dictionary<string, string>() { { "title", "置頂" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
          //  re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "sort" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            //取得分類
            ArticleCategory top = _context.ArticleCategory.Where(m => m.uri == uri).FirstOrDefault();

            //  List<ArticleCategory> categoryData = _context.ArticleCategory.Where(m=>m.parent_id == top.id).ToList();

            /*List<LanguageResource> categoryData = _context.ArticleCategory.Where(m => m.uri == "news").Where(m => !string.IsNullOrEmpty(m.parent_id)).Include(x => x.LangData)
            .FirstOrDefault()?
              .LangData.Where(m => m.language_id == lang)
             .ToList();*/

            //取得對應的語系ID
            List<string> ids = _context.LanguageResource.Where(m => m.model_type == "ArticleCategory")
                .Where(l => l.language_id == lang).Select(m => m.ArticleCategoryId).ToList();



            List<ArticleCategory> categoryData = _context.ArticleCategory
                  .Where(m => ids.Contains(m.id))
                .Where(m => string.IsNullOrEmpty(m.parent_id))
                .Where(m => m.uri == "news" || m.uri == "video")
                .Include(x => x.LangData.Where(l => l.language_id == lang)).OrderBy(m => m.sort).ToList();


            Dictionary<string, string> categories = new Dictionary<string, string>();

            foreach (ArticleCategory item in categoryData)
            {
                LanguageResource LangData = item.LangData?.FirstOrDefault();

                categories.Add(item.id , LangData?.title);

                List<ArticleCategory> nextCategoryData = _context.ArticleCategory
                   .Where(m => m.parent_id == item.id)
                   .Include(x => x.LangData.Where(l => l.language_id == lang))
                   .OrderBy(m => m.sort)
                   .ToList();

                foreach (ArticleCategory subItem in nextCategoryData)
                {
                    LanguageResource subLangData = subItem.LangData?.FirstOrDefault();

                    if(!string.IsNullOrEmpty(subLangData?.title))
                    {
                        categories.Add(subItem.id, "└" + subLangData?.title);
                    }
                   
                }


            }

            find.Add("category_id", categories);


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "start_at" }, { "type", "desc" } };
        }
    }
}
