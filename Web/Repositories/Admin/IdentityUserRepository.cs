﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Admin
{
    public class IdentityUserRepository : IRepository<IdentityUser, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public IdentityUserRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(IdentityUser entity, LanguageResource entityLang)
        {
            _context.IdentityUser.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public void Update(IdentityUser entity, LanguageResource entityLang)
        {
            var data = _context.IdentityUser.Single(x => x.Id == entity.Id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            _context.IdentityUser.Remove(_context.IdentityUser.Single(x => x.Id == id));
            _context.SaveChanges();
        }

        public IEnumerable<IdentityUser> Find(Expression<Func<IdentityUser, bool>> expression)
        {
            return _context.IdentityUser.Where(expression);
        }

        public IdentityUser FindById(string id)
        {
            return _context.IdentityUser.SingleOrDefault(x => x.Id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return null;
        }
        public IQueryable<IdentityUser> FindList(string parent_id)
        {
            return _context.IdentityUser;
        }
        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return null;
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var temp = _context.ArticleCategory.Where(x => x.id == id)
                   .Include(x => x.LangData
                   .Where(l => l.model_id == id)
                    ).FirstOrDefault()?
                   .LangData.Where(m => m.language_id == lang)
                   .FirstOrDefault();
                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.IdentityUser.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("Id", "Y");
            re.Add("NormalizedUserName", new Dictionary<string, string>() { { "title", "帳號" }, { "type", "text" } });
            re.Add("UserName", new Dictionary<string, string>() { { "title", "類別名稱" }, { "type", "text" } });
            re.Add("LockoutEnabled", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("modifyAdmin", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("UserName", "姓名");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }


        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }

    }
}
