﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class WebDataRepository : IRepository<WebData, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public WebDataRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(WebData entity, LanguageResource entityLang)
        {
            _context.WebData.Add(entity);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(WebData entity, LanguageResource entityLang)
        {

            var data = _context.WebData.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
               .LangData.Where(m => m.language_id == entityLang.language_id)
               .FirstOrDefault();

            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.contact = entityLang.contact;
                data.social = entityLang.social;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;


                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }
        }

        public void Delete(string id)
        {
            /* _context.WebData.Remove(_context.WebData.Single(x => x.id == id));
             _context.SaveChanges();*/

            _context.WebData.Remove(_context.WebData.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.WebDataId == id));
            _context.SaveChanges();
        }

        public IEnumerable<WebData> Find(Expression<Func<WebData, bool>> expression)
        {
            return _context.WebData.Where(expression);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.WebData.Where(x => x.id == id)
            .Include(x => x.LangData
            .Where(m => m.language_id == lang)
             ).FirstOrDefault()?
            .LangData
            .FirstOrDefault();
        }

        public WebData FindById(string id)
        {
            return _context.WebData.SingleOrDefault(x => x.id == id);
        }
        public IQueryable<WebData> FindList(string parent_id)
        {
            return _context.WebData;
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.WebData.Where(x => x.id == parent_id)
                  .Include(x => x.LangData
                  .Where(l => l.WebDataId == parent_id)
                   ).FirstOrDefault()?
                  .LangData.ToList();
        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.WebData.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("title", new Dictionary<string, string>() { { "title", "類別名稱" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "text" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "操作" }, { "type", "text" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "啟用", "True" }, { "停用", "False" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }
    }
}
