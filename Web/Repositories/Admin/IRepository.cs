﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Web.Repositories.Admin
{
    public interface IRepository<TEntity, TLang, TKey>
         where TEntity : class
         where TLang : class
    {
        TKey Create(TEntity entity, TLang entityLang);

        void Update(TEntity entity, TLang entityLang);


        void Delete(TKey id);

        TEntity FindById(TKey id);

        TLang FindByIdLang(TKey id, string lang);

        IQueryable<TEntity> FindList(TKey parent_id);

        List<TLang> FindListLang(TKey parent_id, string lang);

        int FindListCount(TKey parent_id , string lang = null);

        IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> expression);



        Dictionary<string, object> dataTableColnum(string uri = "");

        Dictionary<string, object> dataTableSearch(TKey uri, string lang);

        Dictionary<string, string> orderBy(string uri = "");



    }
}
