﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class ArticleCategoryRepository : IRepository<ArticleCategory, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ArticleCategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(ArticleCategory entity, LanguageResource entityLang)
        {
            _context.ArticleCategory.Add(entity);

            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();


            return entity.id;
        }

        public void Update(ArticleCategory entity, LanguageResource entityLang)
        {
            var data = _context.ArticleCategory.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
               .LangData.Where(m => m.language_id == entityLang.language_id)
               .FirstOrDefault();

            var model = _context.ArticleCategory.Where(x => x.id == entity.id).FirstOrDefault();
            model.active = entity.active;
            model.uri = entity.uri;
            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;
                data.created_at = entityLang.created_at != null ? DateTime.Parse(entityLang.created_at.ToString()) : null;

                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }

        }

        public void Delete(string id)
        {
            _context.ArticleCategory.Remove(_context.ArticleCategory.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.ArticleCategoryId == id));
            _context.SaveChanges();
        }

        public IEnumerable<ArticleCategory> Find(Expression<Func<ArticleCategory, bool>> expression)
        {
            return _context.ArticleCategory.Where(expression);
        }

        public ArticleCategory FindById(string id)
        {
            return _context.ArticleCategory.SingleOrDefault(x => x.id == id);
        }
        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.ArticleCategory.Where(x => x.id == id)
              .Include(x => x.LangData
              .Where(m => m.language_id == lang)
               ).FirstOrDefault()?
              .LangData
              .FirstOrDefault();
        }

        public IQueryable<ArticleCategory> FindList(string parent_id)
        {
            List<string> notIn = new List<string>
            {
                "Awards","experience","history","video",
            };


            return _context.ArticleCategory.Where(m => m.parent_id == parent_id).Where(m=> !notIn.Contains(m.id) );
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.ArticleCategory.Where(x => x.id == parent_id)
              .Include(x => x.LangData
              .Where(l => l.ArticleCategoryId == parent_id)
               ).FirstOrDefault()?
              .LangData.ToList();
        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.ArticleCategory.Where(m => m.parent_id == parent_id).Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("title", new Dictionary<string, string>() { { "title", "類別名稱" }, { "type", "text" } });
            //re.Add("qty", new Dictionary<string, string>() { { "title", "數量" }, { "type", "text" } } );
            re.Add("nextCategory", new Dictionary<string, string>() { { "title", "動作" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "text" } });
            re.Add("useId", new Dictionary<string, string>() { { "title", "" }, { "type", "text" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("啟用狀態", new Dictionary<string, string>() { { "啟用", "1" }, { "停用", "0" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }


        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }
    }
}
