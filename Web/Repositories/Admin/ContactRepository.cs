﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Admin
{
    public class ContactRepository : IRepository<Contact, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ContactRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(Contact entity, LanguageResource entityLang)
        {
            _context.Contact.Add(entity);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(Contact entity, LanguageResource entityLang)
        {
            var data = _context.Contact.Single(x => x.id == entity.id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            _context.Contact.Remove(_context.Contact.Single(x => x.id == id));
            _context.SaveChanges();
        }

        public IEnumerable<Contact> Find(Expression<Func<Contact, bool>> expression)
        {
            return _context.Contact.Where(expression);
        }

        public Contact FindById(string id)
        {
            return _context.Contact.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return null;
        }

        public IQueryable<Contact> FindList(string parent_id)
        {
            return _context.Contact.Where(m=>m.uri == parent_id);
        }
        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return null;
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.ArticleCategory.Where(x => x.id == id)
                  .Include(x => x.LangData
                  .Where(l => l.model_id == id)
                   ).FirstOrDefault()?
                  .LangData.Where(m => m.language_id == lang)
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.Contact.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("name", new Dictionary<string, string>() { { "title", "姓名" }, { "type", "text" } });
            re.Add("created_at", new Dictionary<string, string>() { { "title", "建立日期" }, { "type", "text" } });
            if(uri != "Subscribe" && uri != "Questionnaire")
            {
                re.Add("activeContact", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            }
            if (uri == "Questionnaire")
            {
                re.Add("lang", new Dictionary<string, string>() { { "title", "來源語系" }, { "type", "text" } });
            }

            re.Add("modify", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("name", "姓名");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }


        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "created_at" }, { "type", "desc" } };
        }

    }
}
