﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class QuestionnaireItemRepository : IRepository<QuestionnaireItem, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public QuestionnaireItemRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(QuestionnaireItem entity, LanguageResource entityLang)
        {
            _context.QuestionnaireItem.Add(entity);

            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();


            return entity.id;
        }

        public void Update(QuestionnaireItem entity, LanguageResource entityLang)
        {
            var data = _context.QuestionnaireItem.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
                .LangData.Where(m => m.language_id == entityLang.language_id)
                .FirstOrDefault();


            var model = _context.QuestionnaireItem.Where(x => x.id == entity.id).FirstOrDefault();

            model.category_id = entity.category_id;
            model.type = entity.type;
            model.spec = entity.spec;
            model.active = entity.active;

            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;
                data.options = entityLang.options != null ? entityLang.options : null;

                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }
        }

        public void Delete(string id)
        {
            _context.QuestionnaireItem.Remove(_context.QuestionnaireItem.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.ArticleNewsId == id));
            _context.SaveChanges();
        }

        public IEnumerable<QuestionnaireItem> Find(Expression<Func<QuestionnaireItem, bool>> expression)
        {
            return _context.QuestionnaireItem.Where(expression);
        }

        public QuestionnaireItem FindById(string id)
        {
            return _context.QuestionnaireItem.SingleOrDefault(x => x.id == id);
        }
        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.QuestionnaireItem.Where(x => x.id == id)
            .Include(x => x.LangData
            .Where(m => m.language_id == lang)
             ).FirstOrDefault()?
            .LangData
            .FirstOrDefault();
        }
        public IQueryable<QuestionnaireItem> FindList(string parent_id)
        {
            //return _context.QuestionnaireItem.Where(m => m.uri == parent_id);
            return _context.QuestionnaireItem;
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.QuestionnaireItem.Where(x => x.id == parent_id)
             .Include(x => x.LangData
             .Where(l => l.QuestionnaireItemId == parent_id)
              ).FirstOrDefault()?
             .LangData.ToList();
        }

        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.QuestionnaireItem.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("title", new Dictionary<string, string>() { { "title", "參數名稱" }, { "type", "text" } });
            re.Add("type", new Dictionary<string, string>() { { "title", "欄位型態" }, { "type", "text" } });
            re.Add("category_id", new Dictionary<string, string>() { { "title", "類別" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "text" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "操作" }, { "type", "text" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }


        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.QuestionnaireGroup.Where(x => x.id == id)
                  .Include(x => x.LangData.Where(m => m.language_id == lang)
                   ).FirstOrDefault()?
                  .LangData
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }
    }
}
