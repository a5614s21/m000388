﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class ArticleDownloadRepository : IRepository<ArticleDownload, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ArticleDownloadRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(ArticleDownload entity, LanguageResource entityLang)
        {

            _context.ArticleDownload.Add(entity);

            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(ArticleDownload entity, LanguageResource entityLang)
        {
            /*
            var data = _context.ArticleDownload.Single(x => x.id == entity.id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
            */

            var data = _context.ArticleDownload.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
                .LangData.Where(m => m.language_id == entityLang.language_id)
                .FirstOrDefault();


            var model = _context.ArticleDownload.Where(x => x.id == entity.id).FirstOrDefault();
            model.year = entity.year;
            model.month = entity.month;
            model.active = entity.active;


            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;
                data.year = entityLang.year;
                data.month = entityLang.month;
                data.video = entityLang.video;
                data.files = entityLang.files;
                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }

        }

        public void Delete(string id)
        {
            _context.ArticleDownload.Remove(_context.ArticleDownload.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.ArticleDownloadId == id));
            _context.SaveChanges();
        }

        public IEnumerable<ArticleDownload> Find(Expression<Func<ArticleDownload, bool>> expression)
        {
            return _context.ArticleDownload.Where(expression);
        }

        public ArticleDownload FindById(string id)
        {
            return _context.ArticleDownload.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.ArticleDownload.Where(x => x.id == id)
             .Include(x => x.LangData
             .Where(m => m.language_id == lang)
              ).FirstOrDefault()?
             .LangData
             .FirstOrDefault();
        }

        public IQueryable<ArticleDownload> FindList(string parent_id)
        {
            return _context.ArticleDownload.Where(m=>m.uri == parent_id);
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.ArticleDownload.Where(x => x.id == parent_id)
                  .Include(x => x.LangData
                  .Where(l => l.ArticleDownloadId == parent_id)
                   ).FirstOrDefault()?
                  .LangData.ToList();
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.ArticleCategory.Where(x => x.id == id)
                  .Include(x => x.LangData.Where(m => m.language_id == lang)
                   ).FirstOrDefault()?
                  .LangData
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.ArticleDownload.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();


            re.Add("id", "Y");
            //re.Add("pic", new Dictionary<string, string>() { { "title", "圖片" }, { "type", "pic" } });
            re.Add("title", new Dictionary<string, string>() { { "title", "標題" }, { "type", "text" } });
            if(uri == "finance")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "年" }, { "type", "text" } });
                re.Add("month", new Dictionary<string, string>() { { "title", "月" }, { "type", "text" } });
            }
            if (uri == "quarterly")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "年" }, { "type", "text" } });
                re.Add("month", new Dictionary<string, string>() { { "title", "季" }, { "type", "text" } });
            }
            if (uri == "annual")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "年" }, { "type", "text" } });
            }

            if (uri == "consolidated")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "年" }, { "type", "text" } });
                re.Add("month", new Dictionary<string, string>() { { "title", "季" }, { "type", "text" } });
            }
            if (uri == "reports")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "年" }, { "type", "text" } });
                re.Add("month", new Dictionary<string, string>() { { "title", "季" }, { "type", "text" } });
            }

            if (uri == "Sustainability")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "年" }, { "type", "text" } });
            }

            // re.Add("category_id", new Dictionary<string, string>() { { "title", "分類" }, { "type", "text" } });

            re.Add("created_at", new Dictionary<string, string>() { { "title", "建立日期" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "sort" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });
            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
           // search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            //取得分類

            Dictionary<string, string> categories = new Dictionary<string, string>();

          /*  ArticleCategory top = _context.ArticleCategory.Include(x => x.LangData.Where(m => m.language_id == lang)).FirstOrDefault();
            categories.Add(top.LangData.FirstOrDefault().title, top.id);


            List<LanguageResource> categoryData = _context.ArticleCategory.Where(m => m.parent_id == top.id).Include(x => x.LangData)
                .FirstOrDefault()?
                  .LangData.Where(m => m.language_id == lang)
                 .ToList();



            if (categoryData != null && categoryData.Count > 0)
            {
                foreach (LanguageResource item in categoryData)
                {
                    categories.Add(item.id, item.title);
                }
            }
          */
        //    find.Add("category_id", categories);


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {

            List<string> isUri = new List<string>
            {
                "finance",
                "quarterly",
                "annual",
                "consolidated",
                "reports",
            };

            if(isUri.Contains(uri))
            {
                return new Dictionary<string, string> { { "key", "year" }, { "type", "desc" } };
            }
            else
            {
                if(uri == "filings")
                {
                    return new Dictionary<string, string> { { "key", "title" }, { "type", "desc" } };
                }
                else
                {
                    return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
                }
                
            }
           

        }
    }
}
