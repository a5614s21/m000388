﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Admin
{
    public class IdentityRoleRepository : IRepository<IdentityRole, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public IdentityRoleRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(IdentityRole entity, LanguageResource entityLang)
        {
            _context.IdentityRole.Add(entity);
            _context.SaveChanges();
            return entity.Id;
        }

        public void Update(IdentityRole entity, LanguageResource entityLang)
        {
            var data = _context.IdentityRole.Single(x => x.Id == entity.Id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            _context.IdentityRole.Remove(_context.IdentityRole.Single(x => x.Id == id));
            _context.SaveChanges();
        }

        public IEnumerable<IdentityRole> Find(Expression<Func<IdentityRole, bool>> expression)
        {
            return _context.IdentityRole.Where(expression);
        }

        public IdentityRole FindById(string id)
        {
            return _context.IdentityRole.SingleOrDefault(x => x.Id == id);
        }
        public LanguageResource FindByIdLang(string id, string lang)
        {
            return null;
        }

        public IQueryable<IdentityRole> FindList(string parent_id)
        {
            return _context.IdentityRole;
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return null;
        }
        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var temp = _context.ArticleCategory.Where(x => x.id == id)
                 .Include(x => x.LangData
                 .Where(l => l.model_id == id)
                  ).FirstOrDefault()?
                 .LangData.Where(m => m.language_id == lang)
                 .FirstOrDefault();
                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.IdentityRole.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("Id", "Y");
            re.Add("Name", new Dictionary<string, string>() { { "title", "群組名稱" }, { "type", "text" } });
            re.Add("modifyAdmin", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("Name", "群組名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            //find.Add("啟用狀態", new Dictionary<string, string>() { { "啟用", "1" }, { "停用", "0" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }


        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "id" }, { "type", "asc" } };
        }

    }
}
