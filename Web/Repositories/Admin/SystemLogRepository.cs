﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Admin
{
    public class SystemLogRepository : IRepository<SystemLog, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public SystemLogRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(SystemLog entity, LanguageResource entityLang)
        {
            _context.SystemLog.Add(entity);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(SystemLog entity, LanguageResource entityLang)
        {
            var data = _context.SystemLog.Single(x => x.id == entity.id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            _context.SystemLog.Remove(_context.SystemLog.Single(x => x.id == id));
            _context.SaveChanges();
        }

        public IEnumerable<SystemLog> Find(Expression<Func<SystemLog, bool>> expression)
        {
            return _context.SystemLog.Where(expression);
        }

        public SystemLog FindById(string id)
        {
            return _context.SystemLog.SingleOrDefault(x => x.id == id);
        }
        public LanguageResource FindByIdLang(string id, string lang)
        {
            return null;
        }

        public IQueryable<SystemLog> FindList(string parent_id)
        {
            return _context.SystemLog.Where(m => m.uri == parent_id);
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return null;
        }
        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.SystemLog.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("username", new Dictionary<string, string>() { { "title", "使用者" }, { "type", "text" } });
            re.Add("act", new Dictionary<string, string>() { { "title", "動作" }, { "type", "text" } });
            re.Add("title", new Dictionary<string, string>() { { "title", "敘述" }, { "type", "text" } });
            re.Add("ip", new Dictionary<string, string>() { { "title", "IP" }, { "type", "text" } });
            re.Add("created_at", new Dictionary<string, string>() { { "title", "日期" }, { "type", "text" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "操作" }, { "type", "text" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "created_at" }, { "type", "desc" } };
        }
    }
}
