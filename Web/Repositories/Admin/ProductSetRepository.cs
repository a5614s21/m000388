﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class ProductSetRepository : IRepository<ProductSet, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ProductSetRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(ProductSet entity, LanguageResource entityLang)
        {
            _context.ProductSet.Add(entity);

            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(ProductSet entity, LanguageResource entityLang)
        {
            var data = _context.ProductSet.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
                  .LangData.Where(m => m.language_id == entityLang.language_id)
                  .FirstOrDefault();


            var model = _context.ProductSet.Where(x => x.id == entity.id).FirstOrDefault();
            model.feature = entity.feature;
            model.product_tags = entity.product_tags;
            model.safety = entity.safety;
            model.series = entity.series;
            model.category_id = entity.category_id;
            model.application_id = entity.application_id;
            model.path = entity.path;
            model.active = entity.active;
            model.uri = entity.uri;

            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.application_id = entityLang.application_id;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;
                data.feature = entityLang.feature;
                data.product_tags = entityLang.product_tags;
                data.safety = entityLang.safety;
                data.series = entityLang.series;
                data.files = entityLang.files;
                data.ProductSetId = entityLang.ProductSetId;
                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }


        }

        public void Delete(string id)
        {
            _context.ProductSet.Remove(_context.ProductSet.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.ArticleNewsId == id));
            _context.SaveChanges();
        }

        public IEnumerable<ProductSet> Find(Expression<Func<ProductSet, bool>> expression)
        {
            return _context.ProductSet.Where(expression);
        }

        public ProductSet FindById(string id)
        {
            return _context.ProductSet.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.ProductSet.Where(x => x.id == id)
             .Include(x => x.LangData
             .Where(m => m.language_id == lang)
              ).FirstOrDefault()?
             .LangData
             .FirstOrDefault();
        }

        public IQueryable<ProductSet> FindList(string parent_id)
        {
            return _context.ProductSet;
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.ProductSet.Where(x => x.id == parent_id)
                   .Include(x => x.LangData
                   .Where(l => l.ProductSetId == parent_id)
                    ).FirstOrDefault()?
                   .LangData.ToList();
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.ProductCategory.Where(x => x.id == id)
                  .Include(x => x.LangData.Where(m => m.language_id == lang)
                   ).FirstOrDefault()?
                  .LangData
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }

        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.ProductSet.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("pic", new Dictionary<string, string>() { { "title", "圖片" }, { "type", "pic" } });
            re.Add("title", new Dictionary<string, string>() { { "title", "標題" }, { "type", "text" } });
            re.Add("category_id", new Dictionary<string, string>() { { "title", "分類" }, { "type", "text" } });
            re.Add("created_at", new Dictionary<string, string>() { { "title", "建立日期" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "sort" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            //取得分類
            // ProductCategory top = _context.ProductCategory.Where(m => m.uri == uri).FirstOrDefault();

            List<ProductCategory> categoryData = _context.ProductCategory.Where(m => m.parent_id == "Product").Include(x => x.LangData.Where(l => l.language_id == lang)).OrderBy(m => m.sort).ToList();


            Dictionary<string, string> categories = new Dictionary<string, string>();

            foreach (ProductCategory item in categoryData)
            {
                LanguageResource LangData = item.LangData?.FirstOrDefault();
                categories.Add(item.id, LangData?.title);

                List<ProductCategory> nextCategoryData = _context.ProductCategory
                    .Where(m => m.parent_id == item.id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .OrderBy(m => m.sort)
                    .ToList();

                foreach (ProductCategory subItem in nextCategoryData)
                {
                    LanguageResource subLangData = subItem.LangData?.FirstOrDefault();

                    categories.Add(subItem.id, "└" + subLangData?.title);


                    List<ProductCategory> threeCategoryData = _context.ProductCategory
                    .Where(m => m.parent_id == subItem.id)
                    .Include(x => x.LangData.Where(l => l.language_id == lang))
                    .OrderBy(m => m.sort)
                    .ToList();

                    foreach (ProductCategory threeItem in threeCategoryData)
                    {
                        LanguageResource threeLangData = threeItem.LangData?.FirstOrDefault();

                        categories.Add(threeItem.id, "　└" + threeLangData?.title);
                    }

                }

            }

            find.Add("category_id", categories);


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }
    }
}
