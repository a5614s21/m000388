﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Admin
{
    public class FirewallRepository : IRepository<Firewall, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public FirewallRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(Firewall entity, LanguageResource entityLang)
        {
            _context.Firewall.Add(entity);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(Firewall entity, LanguageResource entityLang)
        {
            var data = _context.Firewall.Single(x => x.id == entity.id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            _context.Firewall.Remove(_context.Firewall.Single(x => x.id == id));
            _context.SaveChanges();
        }

        public IEnumerable<Firewall> Find(Expression<Func<Firewall, bool>> expression)
        {
            return _context.Firewall.Where(expression);
        }
        public LanguageResource FindByIdLang(string id, string lang)
        {
            return null;
        }


        public Firewall FindById(string id)
        {
            return _context.Firewall.SingleOrDefault(x => x.id == id);
        }
        public IQueryable<Firewall> FindList(string parent_id)
        {
            return _context.Firewall;
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return null;
        }

        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.Firewall.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("platform", new Dictionary<string, string>() { { "title", "平台" }, { "type", "text" } });
            re.Add("title", new Dictionary<string, string>() { { "title", "名稱" }, { "type", "text" } });
            re.Add("rule", new Dictionary<string, string>() { { "title", "規則" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "text" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "操作" }, { "type", "text" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }
    }
}
