﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;

namespace Web.Repositories.Admin
{
    public class ArticleColumnRepository : IRepository<ArticleColumn, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public ArticleColumnRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(ArticleColumn entity, LanguageResource entityLang)
        {
            _context.ArticleColumn.Add(entity);
            entityLang.id = new Helper().GenerateIntID();
            _context.LanguageResource.Add(entityLang);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(ArticleColumn entity, LanguageResource entityLang)
        {

            var data = _context.ArticleColumn.Where(x => x.id == entity.id).Include(x => x.LangData).FirstOrDefault()?
               .LangData.Where(m => m.language_id == entityLang.language_id)
               .FirstOrDefault();

            var model = _context.ArticleColumn.Where(x => x.id == entity.id).FirstOrDefault();
            model.year = entity.year;
            model.month = entity.month;
            model.uri = entity.uri;
            model.active = entity.active;
             model.category_id = entity.category_id;

            if (data == null)
            {
                entityLang.id = new Helper().GenerateIntID();
                _context.LanguageResource.Add(entityLang);
                _context.SaveChanges();
            }
            else
            {

                data.title = entityLang.title;
                data.details = entityLang.details;
                data.active = entityLang.active;
                data.updated_at = DateTime.Now;
                data.seo = entityLang.seo;
                data.parent_id = entityLang.parent_id;
                data.category_id = entityLang.category_id;
                data.start_at = entityLang.start_at != null ? DateTime.Parse(entityLang.start_at.ToString()) : null;
                data.end_at = entityLang.end_at != null ? DateTime.Parse(entityLang.end_at.ToString()) : null;
                data.tags = entityLang.tags != null ? entityLang.tags.ToString() : null;
                data.top = entityLang.top != null ? entityLang.top : null;
                data.sort = entityLang.sort != null ? entityLang.sort : null;
                data.pic = entityLang.pic != null ? entityLang.pic : null;
                data.files = entityLang.files;
                data.year = entityLang.year;
                data.month = entityLang.month;
                data.video = entityLang.video;
                _context.LanguageResource.Attach(data);
                _context.SaveChanges();
            }


        }

        public void Delete(string id)
        {

            _context.ArticleColumn.Remove(_context.ArticleColumn.Single(x => x.id == id));
            _context.LanguageResource.RemoveRange(_context.LanguageResource.Where(x => x.ArticleColumnId == id));
            _context.SaveChanges();

        }

        public IEnumerable<ArticleColumn> Find(Expression<Func<ArticleColumn, bool>> expression)
        {
            return _context.ArticleColumn.Where(expression);
        }

        public ArticleColumn FindById(string id)
        {
            return _context.ArticleColumn.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return _context.ArticleColumn.Where(x => x.id == id)
             .Include(x => x.LangData
             .Where(m => m.language_id == lang)
              ).FirstOrDefault()?
             .LangData
             .FirstOrDefault();
        }

        public IQueryable<ArticleColumn> FindList(string parent_id)
        {
            return _context.ArticleColumn.Where(m=>m.uri == parent_id);
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return _context.ArticleColumn.Where(x => x.id == parent_id)
                   .Include(x => x.LangData
                   .Where(l => l.ArticleColumnId == parent_id)
                    ).FirstOrDefault()?
                   .LangData.ToList();
        }

        public string GetCategoryTitle(string id, string lang)
        {
            if (!string.IsNullOrEmpty(id))
            {

                var temp = _context.ArticleCategory.Where(x => x.id == id)
                  .Include(x => x.LangData.Where(m => m.language_id == lang)
                   ).FirstOrDefault()?
                  .LangData
                  .FirstOrDefault();


                if (temp != null)
                {
                    return temp.title;
                }
                else
                {
                    return "";
                }

            }
            else
            {
                return "";
            }

        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.ArticleColumn.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            //re.Add("pic", new Dictionary<string, string>() { { "title", "圖片" }, { "type", "pic" } });
            re.Add("title", new Dictionary<string, string>() { { "title", "標題" }, { "type", "text" } });

            re.Add("category_id", new Dictionary<string, string>() { { "title", "分類" }, { "type", "text" } });

            if(uri == "history")
            {
                re.Add("year", new Dictionary<string, string>() { { "title", "西元年" }, { "type", "text" } });
            }


            re.Add("created_at", new Dictionary<string, string>() { { "title", "建立日期" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "sort" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "異動" }, { "type", "modify" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            //取得分類
            ArticleCategory top = _context.ArticleCategory.Where(m=>m.uri == uri).Include(x => x.LangData.Where(m => m.language_id == lang)).FirstOrDefault();
            Dictionary<string, string> categories = new Dictionary<string, string>();

            categories.Add(top.LangData.FirstOrDefault().title, top.id);


            List<LanguageResource> categoryData = _context.ArticleCategory.Where(m => m.parent_id == top.id).Include(x => x.LangData)
                .FirstOrDefault()?
                  .LangData.Where(m => m.language_id == lang)
                 .ToList();



            if (categoryData != null && categoryData.Count > 0)
            {
                foreach (LanguageResource item in categoryData)
                {
                    categories.Add(item.id, item.title);
                }
            }


            find.Add("category_id", categories);


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            if (uri == "history")
            {
                return new Dictionary<string, string> { { "key", "year" }, { "type", "desc" } };
            }
            else
            {
                return new Dictionary<string, string> { { "key", "sort" }, { "type", "desc" } };
            }
            
        }
    }
}
