﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using WebApp.Data;
using WebApp.Models;

namespace Web.Repositories.Admin
{
    public class SystemItemRepository : IRepository<SystemItem, LanguageResource, string>
    {
        private readonly ApplicationDbContext _context;

        public SystemItemRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public string Create(SystemItem entity, LanguageResource entityLang)
        {
            _context.SystemItem.Add(entity);
            _context.SaveChanges();
            return entity.id;
        }

        public void Update(SystemItem entity, LanguageResource entityLang)
        {
            var data = _context.SystemItem.Single(x => x.id == entity.id);
            _context.Entry(data).CurrentValues.SetValues(entity);
            _context.SaveChanges();
        }

        public void Delete(string id)
        {
            _context.SystemItem.Remove(_context.SystemItem.Single(x => x.id == id));
            _context.SaveChanges();
        }

        public IEnumerable<SystemItem> Find(Expression<Func<SystemItem, bool>> expression)
        {
            return _context.SystemItem.Where(expression);
        }

        public SystemItem FindById(string id)
        {
            return _context.SystemItem.SingleOrDefault(x => x.id == id);
        }

        public LanguageResource FindByIdLang(string id, string lang)
        {
            return null;
        }

        public IQueryable<SystemItem> FindList(string parent_id)
        {
            return _context.SystemItem.Where(m => m.uri == parent_id);
        }

        public List<LanguageResource> FindListLang(string parent_id, string lang)
        {
            return null;
        }


        public int FindListCount(string parent_id, string lang = null)
        {
            return _context.SystemItem.Count();
        }

        /// <summary>
        /// DataTable欄位
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableColnum(string uri = "")
        {
            Dictionary<string, object> re = new Dictionary<string, object>();

            re.Add("id", "Y");
            re.Add("title", new Dictionary<string, string>() { { "title", "類別名稱" }, { "type", "text" } });
            re.Add("active", new Dictionary<string, string>() { { "title", "狀態" }, { "type", "text" } });
            re.Add("sort", new Dictionary<string, string>() { { "title", "排序" }, { "type", "text" } });
            re.Add("modify", new Dictionary<string, string>() { { "title", "操作" }, { "type", "text" } });

            return re;
        }

        /// <summary>
        /// 查詢使用參數
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, object> dataTableSearch(string uri, string lang)
        {

            Dictionary<string, object> search = new Dictionary<string, object>();
            search.Add("title", "類別名稱");

            Dictionary<string, object> find = new Dictionary<string, object>();
            find.Add("active", new Dictionary<string, string>() { { "True", "啟用" }, { "False", "停用" } });


            Dictionary<string, object> re = new Dictionary<string, object>();
            re.Add("search", search);
            re.Add("find", find);

            return re;
        }

        /// <summary>
        /// 排序
        /// </summary>
        /// <returns></returns>
        public Dictionary<string, string> orderBy(string uri = "")
        {
            return new Dictionary<string, string> { { "key", "sort" }, { "type", "asc" } };
        }
    }
}
