﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Linq.Dynamic.Core;
using BCrypt.Net;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Web.Repositories.Admin;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]
    
    public class IdentityRoleController : HomeController
    {
        private readonly IRepository<IdentityRole, LanguageResource, string> _repository;
        protected Dictionary<String, Object> dataTableColnum;
        private dynamic appSeting;

        public IdentityRoleController(IRepository<IdentityRole, LanguageResource, string> repository , IConfiguration config )
        {
            _repository = repository;
            dataTableColnum = _repository.dataTableColnum();//取得欄位資訊
            appSeting = (new Helper()).GetAppSettings();
            ViewBag.appSeting = appSeting;
        
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult List(string id)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;

            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";

            //麵包屑
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.Data = _repository.FindById(id);
            }

            Dictionary<String, Object> dataTableSearch = _repository.dataTableSearch("", adminNowLang.language_id);

            ViewBag.Search = dataTableSearch["search"];

            ViewBag.dataTableColnum = dataTableColnum;

            ViewBag.orderBy = _repository.orderBy();

            //取得排序鍵值
            ViewBag.orderBy = Helper.GetSortIndex(dataTableColnum, _repository.orderBy());

            //使用於最後一個若為"操作"，設定為不可排序
            ViewBag.LastCloseSortIndex = (dataTableColnum.ContainsKey("modify") ? "," + (dataTableColnum.Count - 1).ToString() : "");

            return View(!string.IsNullOrEmpty(id) ? "ListUri" : "List");
        }

        /// <summary>
        /// 新增修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult Edit(string id, string uri)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;

            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";
            ViewBag.Uri = !string.IsNullOrEmpty(uri) ? uri : "";

            //取得編輯欄位
            Dictionary<String, Object> Columns = Helper.editColumns("IdentityRole", uri);

            ViewBag.Columns = Columns;


            ViewBag.RolePermission = null;

            //取得資料
            if (id != "add")
            {
                IdentityRole data = _repository.FindById(id);
                var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                var temp = Newtonsoft.Json.Linq.JArray.Parse("["+json+"]");

                ViewBag.Data = Helper.FormatViewData(Columns , temp[0]);


                ViewBag.RolePermission = DB.RolePermission.Where(m=>m.RoleId == id).ToList();

            }

            return View();
        }

        /// <summary>
        /// 寫入與修改
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Save(FormUser form)
        {
            CipherService cipherService = new CipherService();

            string key = appSeting["APP_KEY"].ToString();


            string masterId = Guid.NewGuid().ToString();

            IdentityRole entity = new IdentityRole()
            {
                Id = masterId,
                Name = form.Name,
              
                NormalizedName = form.NormalizedName,         
            };


            Dictionary<string, string> newLog = new Dictionary<string, string> {
                { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                { "uri" , "modify" },
                { "username" , ViewBag.User.NormalizedUserName },
                { "title" ,   "[ " + ViewBag.SystemMenuData.title + " - " + form.Name + " ]"}
            };


            if (form.Id == "add")
            {               
                entity.Id = masterId;
                _repository.Create(entity, null);

                newLog.Add("act", "add");
            }
            else
            {                
                entity.Id = form.Id;      
                _repository.Update(entity, null);

                //先行移除原本的權限

                DB.RolePermission.RemoveRange(DB.RolePermission.Where(m=>m.RoleId == form.Id));
                DB.SaveChanges();

                masterId = form.Id;

                newLog.Add("act", "edit");
            }

            (new Helper()).SaveLog(newLog);

            //處理權限
            var roles = JsonConvert.DeserializeObject<List<string>>(form.roles.ToString());
            foreach (string id in roles)
            {
                string MenuId = id.Replace("_F", "").Replace("_O", "");
                string Permission = "F";
                if (id.IndexOf("O") != -1)
                {
                    Permission = "O";
                }

                RolePermission role = new RolePermission
                {
                    id = Guid.NewGuid().ToString(),
                    RoleId = masterId,
                    MenuId = MenuId,
                    Permission = Permission,
                };

                DB.RolePermission.Add(role);
                DB.SaveChanges();
            }
                    

            Response.Redirect("/Siteadmin/"+ ControllerName + "/List");
        }

  


        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task Delete(IFormCollection form)
        {
            List<string> idList = form["id"].ToString().Split(',').ToList();

            foreach (string id in idList)
            {

                //判斷是否有子項目
                

                IdentityRole data = _repository.FindById(id);

                int identityUserRole = DB.IdentityUserRole.Where(m => m.RoleId == id).Count();

                if(identityUserRole == 0)
                {

                    Dictionary<string, string> newLog = new Dictionary<string, string> {
                    { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                    { "uri" , "modify" },
                    { "username" , ViewBag.User.NormalizedUserName },
                    { "act" , "del" },
                    { "title" , "[ " + ViewBag.SystemMenuData.title + " - " + data.Name + " ]" },
                };

                    (new Helper()).SaveLog(newLog);

                    _repository.Delete(id);

                    Response.WriteAsync("OK");

                }
                else
                {
                    Response.WriteAsync("HaveUser");
                }

                
            }

        }


        /// <summary>
        /// DataTable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public string dataTable(string id , IFormCollection form)
        {
            IQueryable<IdentityRole> model = _repository.FindList(id ?? "");          

            string orderByKey = form["columns["+ form["order[0][column]"] +"][data]"].ToString();
            string orderByType = form["order[0][dir]"].ToString();
            string orderBy = orderByKey + " "+ orderByType.ToUpper();
            int skip = int.Parse(form["start"].ToString());
            int take = int.Parse(form["length"].ToString());

            Dictionary<String, Object> list = new Dictionary<String, Object>();

            list.Add("iTotalDisplayRecords", model.Count());

            CipherService cipherService = new CipherService();

            //查詢
            if (!string.IsNullOrEmpty(form["searchJson"].ToString()))
            {
                Newtonsoft.Json.Linq.JToken sSearch = Newtonsoft.Json.Linq.JArray.Parse("["+form["searchJson"].ToString()+"]");
                sSearch = sSearch[0];


                string keyword = cipherService.Encrypt(sSearch["keyword"].ToString());



                if (sSearch["col"].ToString() == "all")
                {
                    model = model.Where("name.Contains(@0) or contact.Contains(@0)", keyword);
                   
                }
                else
                {
                    model = model.Where(sSearch["col"].ToString() + ".Contains(@0)", keyword);
                }

            }

            dynamic output = model.OrderBy(orderBy).ToList().Skip(skip).Take(take);
            

            //整理輸出資料
            var data = (new DataTable()).columns(JsonConvert.SerializeObject(output) , ControllerName, _repository , uri , id);          

            list.Add("data", data);   

            return JsonConvert.SerializeObject(list, Formatting.Indented);
        }




        public class FormUser
        {
            public string? Id { get; set; }
            public string? Name { get; set; }
            public string? NormalizedName { get; set; }
            public string? roles { get; set; }

        }
    }
}