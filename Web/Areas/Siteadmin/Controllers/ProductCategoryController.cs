﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Linq.Dynamic.Core;
using Web.Repositories.Admin;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]

    public class ProductCategoryController : HomeController
    {
        private readonly IRepository<ProductCategory, LanguageResource, string> _repository;
        protected Dictionary<String, Object> dataTableColnum;

        public ProductCategoryController(IRepository<ProductCategory, LanguageResource, string> repository)
        {
            _repository = repository;
            dataTableColnum = _repository.dataTableColnum();//取得欄位資訊
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult List(string id)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;

            ViewBag.FirstId = !string.IsNullOrEmpty(id) ? id : "";

            id = !string.IsNullOrEmpty(id) ? id.Replace("Top", "") : "";


            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";
            ViewBag.prevData = null;
            ViewBag.theData = null;

            int lay = 1;
            ViewBag.showLay = 2;

            List<string> topId = new List<string> {
                          "ICPackaging" , "Service" , "Technology" ,
                          };

            //麵包屑
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.Data = _repository.FindByIdLang(id, adminNowLang.language_id);

                var data = _repository.FindById(id);

                //是否有上層
                if (!string.IsNullOrEmpty(data.parent_id))
                {
                    ViewBag.prevData = _repository.FindByIdLang(data.parent_id, adminNowLang.language_id);

                    if (ViewBag.prevData != null)
                    {
                        //string prevDataId = ViewBag.prevData.parent_id ?? "TopTechnology";
                        string prevDataId = ViewBag.prevData.parent_id ?? "TopService";
                        if (data.parent_id == "ICPackaging")
                        {
                            prevDataId = "ICPackaging";
                        }
                        else if (data.parent_id == "Technology")
                        {
                            prevDataId = "TopTechnology";
                        }
                        else if(data.parent_id == "Service")
                        {
                            prevDataId = "TopService";
                        }

                        ViewBag.SystemMenuData = DB.SystemMenu.Where(m => m.model == ControllerName).Where(m => m.uri == prevDataId).FirstOrDefault();

                        lay++;

                        if (ViewBag.prevData != null)
                        {


                            if (!topId.Contains(ViewBag.prevData.ProductCategoryId))
                            {
                                lay++;

                                ViewBag.theData = _repository.FindByIdLang(ViewBag.prevData.parent_id, adminNowLang.language_id);

                                string thePrevid = ViewBag.prevData.parent_id;
                                prevDataId = DB.ProductCategory.Where(m => m.id == thePrevid).FirstOrDefault().parent_id ?? "";

                                //prevDataId = ViewBag.theData.parent_id;

                                ViewBag.SystemMenuData = DB.SystemMenu.Where(m => m.model == ControllerName).Where(m => m.uri == prevDataId).FirstOrDefault();



                            }

                            if (ViewBag.prevData.ProductCategoryId == "Service" && lay < 3)
                            {
                                ViewBag.showLay = 3;
                            }




                        }

                    }

                }

            }

            ViewBag.lay = lay;




            Dictionary<String, Object> dataTableSearch = _repository.dataTableSearch("", adminNowLang.language_id);


            ViewBag.Search = dataTableSearch["search"];
            ViewBag.Find = dataTableSearch["find"];

            if (!string.IsNullOrEmpty(id))
            {
                dataTableColnum.Remove("nextCategory");
            }


            ViewBag.dataTableColnum = dataTableColnum;

            //取得排序鍵值
            ViewBag.orderBy = Helper.GetSortIndex(dataTableColnum, _repository.orderBy());

            ViewBag.BreadCrumbs = true;

            //使用於最後一個若為"操作"，設定為不可排序
            ViewBag.LastCloseSortIndex = (dataTableColnum.ContainsKey("modify") ? "," + (dataTableColnum.Count - 1).ToString() : "");


            ViewBag.PrevSystemMenuData = DB.SystemMenu.Where(m => m.uri == "product").FirstOrDefault();


            if (id == "Service")
            {
                ViewBag.showLay = 3;
            }

            return View(!string.IsNullOrEmpty(id) ? "ListUri" : "List");
        }

        /// <summary>
        /// 新增修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult Edit(string id, string uri)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;

            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";
            ViewBag.Uri = !string.IsNullOrEmpty(uri) ? uri : "";

            //取得編輯欄位
            Dictionary<String, Object> Columns = Helper.editColumns("ProductCategory", "");

            if (!string.IsNullOrEmpty(uri))
            {
                Columns.Remove("icon");
            }

            ViewBag.Columns = Columns;



            //取得資料
            if (id != "add")
            {
                var data = _repository.FindByIdLang(id, adminNowLang.language_id);

                if (data != null)
                {
                    // data.ArticleCategory = null;
                    var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                    var temp = Newtonsoft.Json.Linq.JArray.Parse("[" + json + "]");

                    ViewBag.Data = Helper.FormatViewData(Columns, temp[0]);

                }
            }


            dynamic prevData = null;

            if (!string.IsNullOrEmpty(uri))
            {
                prevData = _repository.FindById(uri.Replace("Top", ""));

                //ViewBag.Uri = prevData.uri;


                var topData = _repository.FindById(prevData.uri);

                ViewBag.parent_id = topData.parent_id;

                //是否有上層
                if (!string.IsNullOrEmpty(prevData.parent_id))
                {
                    ViewBag.prevData = _repository.FindByIdLang(prevData.parent_id, adminNowLang.language_id);



                    if (ViewBag.prevData != null)
                    {
                        string prevDataId = ViewBag.prevData.parent_id ?? "TopTechnology";

                        ViewBag.SystemMenuData = DB.SystemMenu.Where(m => m.model == ControllerName).Where(m => m.uri == prevDataId).FirstOrDefault();



                        if (ViewBag.prevData != null)
                        {
                            List<string> topId = new List<string> {
                          "ICPackaging" , "Service" , "Technology" ,
                          };

                            if (!topId.Contains(ViewBag.prevData.ProductCategoryId))
                            {
                                //ViewBag.prevData = _repository.FindByIdLang(ViewBag.prevData.parent_id, adminNowLang.language_id);
                                //prevDataId = ViewBag.prevData.parent_id;


                                string thePrevid = ViewBag.prevData.parent_id;
                                prevDataId = DB.ProductCategory.Where(m => m.id == thePrevid).FirstOrDefault().parent_id ?? "";

                                ViewBag.SystemMenuData = DB.SystemMenu.Where(m => m.model == ControllerName).Where(m => m.uri == prevDataId).FirstOrDefault();

                            }


                        }

                    }

                }

                ViewBag.PrevSystemMenuData = DB.SystemMenu.Where(m => m.uri == "product").FirstOrDefault();
            }





            return View();
        }

        /// <summary>
        /// 寫入與修改
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Save(FormRequest form)
        {

            string masterId = (new Helper()).GenerateIntID();
            string tempuri = DB.ProductCategory.Where(m => m.id == form.uri).Select(m=>m.uri).FirstOrDefault();
            string tempparentid = null;

            if (form.uri == "TopService")
            {
                tempparentid = "Service";
            }
            else if (form.uri == "TopTechnology")
            {
                tempparentid = "Technology";
            }
            else
            {
                tempparentid = form.uri;
            }

            ProductCategory entity = new ProductCategory()
            {
                id = masterId,
                //parent_id = form.parent_id,
                //uri = form.uri,
                parent_id = tempparentid,
                uri = tempuri == null ? tempparentid : tempuri,
                active = form.active,
                spec = JsonConvert.SerializeObject(form.spec),
                sort = form.sort,

            };



            LanguageResource entityLang = new LanguageResource()
            {
                // id = (new Helper()).GenerateIntID(),
                model_type = "ProductCategory",
                language_id = form.lang,
                title = form.title,
                details = JsonConvert.SerializeObject(form.details),
                options = JsonConvert.SerializeObject(form.options),
                seo = JsonConvert.SerializeObject(form.seo),
                category_id = form.category_id,
                //parent_id = form.parent_id,
                parent_id = tempparentid,
                spec = JsonConvert.SerializeObject(form.spec),
                pic = form.pic,
                active = form.active,
                updated_at = DateTime.Now,
                sort = form.sort,
                //start_at = !string.IsNullOrEmpty(form.start_at) ? DateTime.Parse(form.start_at.ToString()) : null,
                //end_at = !string.IsNullOrEmpty(form.end_at) ? DateTime.Parse(form.end_at.ToString()) : null,
                created_at = DateTime.Parse(form.created_at.ToString()),
                tags = form.tags,
                top = form.top,
                ProductCategoryId = masterId,
            };


            Dictionary<string, string> newLog = new Dictionary<string, string> {
                { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                { "uri" , "modify" },
                { "username" , ViewBag.User.NormalizedUserName },
                { "title" ,   "[ " + ViewBag.SystemMenuData.title + " - " + form.title + " ]"}
            };

            string id = masterId;
            if (form.id == "add")
            {
                _repository.Create(entity, entityLang);

                newLog.Add("act", "add");
            }
            else
            {
                entity.id = form.id;
                entityLang.ProductCategoryId = form.id;
                _repository.Update(entity, entityLang);
                newLog.Add("act", "edit");

                id = form.id;

            }

             (new Helper()).SaveLog(newLog);

            string uriPath = "";

            if (form.uri != null && !string.IsNullOrEmpty(form.uri.ToString()))
            {
                uriPath = "/" + form.uri.ToString();
            }

            string parent_id = form.parent_id;

            List<string> topId = new List<string> {
                          "ICPackaging" , "Service" , "Technology" ,
                          };

            //if (topId.Contains(parent_id) && parent_id != "ICPackaging")
            //{
            //    parent_id = "Top" + parent_id;
            //}

            if (tempuri == "Technology")
            {
                parent_id = "Top" + tempuri;
            }
            else if (tempuri == "Service")
            {
                parent_id = "Top" + tempuri;
            }
            else if(tempuri == "ICPackaging")
            {
                parent_id = tempuri;
            }

            //Response.Redirect("/Siteadmin/" + ControllerName + "/List/" + parent_id);
            Response.Redirect("/Siteadmin/" + ControllerName + "/List" + uriPath);
        }

        /// <summary>
        /// 修改狀態
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task ChangeActive(IFormCollection form)
        {
            ProductCategory entity = new ProductCategory() { id = form["id"].ToString() };
            using (var db = new ApplicationDbContext())
            {
                entity.active = Boolean.Parse(form["active"]);
                db.ProductCategory.Attach(entity);
                db.Entry(entity).Property(x => x.active).IsModified = true;
                db.SaveChanges();
            }


            var resources = _repository.FindListLang(form["id"].ToString(), adminNowLang.language_id);

            foreach (var item in resources)
            {
                LanguageResource entityLang = new LanguageResource() { id = item.id };
                using (var db = new ApplicationDbContext())
                {
                    entityLang.active = Boolean.Parse(form["active"]);
                    entityLang.updated_at = DateTime.Now;
                    db.LanguageResource.Attach(entityLang);
                    db.Entry(entityLang).Property(x => x.active).IsModified = true;
                    db.Entry(entityLang).Property(x => x.updated_at).IsModified = true;
                    db.SaveChanges();
                }
            }


        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task ChangeSort(IFormCollection form)
        {

            ProductCategory entity = new ProductCategory() { id = form["id"].ToString() };
            using (var db = new ApplicationDbContext())
            {
                entity.sort = int.Parse(form["sort"]);
                //entity.updated_at = DateTime.Now;
                db.ProductCategory.Attach(entity);
                db.Entry(entity).Property(x => x.sort).IsModified = true;
                // db.Entry(entity).Property(x => x.updated_at).IsModified = true;
                db.SaveChanges();
            }

        }

        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task Delete(IFormCollection form)
        {
            List<string> idList = form["id"].ToString().Split(',').ToList();

            foreach (string id in idList)
            {

                int subDataNum = DB.LanguageResource.Where(m => m.parent_id == id).Where(m => m.model_type == "ProductCategory").Count();

                int productDataNum = DB.LanguageResource.Where(m => m.category_id == id).Where(m => m.model_type == "ProductSet").Count();

                if (subDataNum <= 0 && productDataNum <= 0)
                {
                    LanguageResource data = _repository.FindByIdLang(id, adminNowLang.language_id);

                    Dictionary<string, string> newLog = new Dictionary<string, string> {
                    { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                    { "uri" , "modify" },
                    { "username" , ViewBag.User.NormalizedUserName },
                    { "act" , "del" },
                    { "title" , "[ " + ViewBag.SystemMenuData.title + " - " + data.title + " ]" },
                };

                    (new Helper()).SaveLog(newLog);

                    _repository.Delete(id);

                    Response.WriteAsync("OK");
                }
                else
                {
                    Response.WriteAsync("HaveNextData");
                }


            }

        }

        /// <summary>
        /// DataTable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public string dataTable(string id, IFormCollection form)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ProductCategory")
                .Where(l => l.language_id == adminNowLang.language_id).Select(m => m.ProductCategoryId).ToList();


            IQueryable<ProductCategory> model = _repository.FindList(id ?? "")
                .Where(m => ids.Contains(m.id))
              .Include(x => x.LangData.Where(l => l.language_id == adminNowLang.language_id));


            string orderByKey = form["columns[" + form["order[0][column]"] + "][data]"].ToString();
            string orderByType = form["order[0][dir]"].ToString();
            string orderBy = orderByKey + " " + orderByType.ToUpper();
            int skip = int.Parse(form["start"].ToString());
            int take = int.Parse(form["length"].ToString());

            Dictionary<String, Object> list = new Dictionary<String, Object>();

            //list.Add("iTotalDisplayRecords", model.Count());

            //查詢
            if (!string.IsNullOrEmpty(form["searchJson"].ToString()))
            {
                Newtonsoft.Json.Linq.JToken sSearch = Newtonsoft.Json.Linq.JArray.Parse("[" + form["searchJson"].ToString() + "]");
                sSearch = sSearch[0];

                switch (sSearch["col"].ToString())
                {
                    case "active":

                        if (!string.IsNullOrEmpty(sSearch["keyword"].ToString()))
                        {
                            // model = model.Where(m => m.active == bool.Parse(sSearch["keyword"].ToString()));
                            model = model.Where(m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().active == bool.Parse(sSearch["keyword"].ToString()));

                        }


                        break;
                    /* case "category_id":

                         if (!string.IsNullOrEmpty(sSearch["keyword"].ToString()))
                         {
                             model = model.Where(m => m.parent_id == sSearch["keyword"].ToString());
                         }


                         break;*/

                    default:

                        if (sSearch["col"].ToString() == "all")
                        {
                            //model = model.Where("LangData.title.Contains(@0) or LangData.details.Contains(@0)", sSearch["keyword"].ToString());

                            model = model.Where(

                                m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().title.Contains(sSearch["keyword"].ToString())

                                ||
                                m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().details.Contains(sSearch["keyword"].ToString())
                                );
                        }
                        else
                        {
                            if (sSearch["col"].ToString() == "title")
                            {
                                model = model.Where(
                                    m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().title.Contains(sSearch["keyword"].ToString())
                                    );

                            }

                            if (sSearch["col"].ToString() == "details")
                            {
                                model = model.Where(
                                    m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().details.Contains(sSearch["keyword"].ToString())
                                    );

                            }


                            //model = model.Where("LangData."+sSearch["col"].ToString() + ".Contains(@0)", sSearch["keyword"].ToString());
                        }
                        break;
                }

            }

            list.Add("iTotalDisplayRecords", (new DataTable()).GetTotalNum(model.ToList()));

            List<string> langOrderByKey = new List<string> {
             "title" , "created_at",
            };

            dynamic output = null;

            if (langOrderByKey.IndexOf(orderByKey) != -1)
            {
                switch (orderByKey)
                {
                    case "title":
                        if (orderByType == "asc")
                        {
                            output = model.OrderBy(m => m.LangData.FirstOrDefault().title).ToList().Skip(skip).Take(take);
                        }
                        else
                        {
                            output = model.OrderByDescending(m => m.LangData.FirstOrDefault().title).ToList().Skip(skip).Take(take);
                        }



                        break;
                    case "created_at":

                        if (orderByType == "asc")
                        {
                            output = model.OrderBy(m => m.LangData.FirstOrDefault().created_at).ToList().Skip(skip).Take(take);
                        }
                        else
                        {
                            output = model.OrderByDescending(m => m.LangData.FirstOrDefault().created_at).ToList().Skip(skip).Take(take);
                        }

                        break;
                }

            }
            else
            {
                output = model.OrderBy(orderBy).ToList().Skip(skip).Take(take);
            }


            //整理輸出資料
            var data = (new DataTable()).columns(JsonConvert.SerializeObject(output), ControllerName, _repository, uri, id);

            list.Add("data", data);

            return JsonConvert.SerializeObject(list, Formatting.Indented);
        }




    }
}