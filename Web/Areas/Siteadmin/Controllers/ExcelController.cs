﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Linq.Dynamic.Core;
using Npoi.Mapper;
using System.ComponentModel.DataAnnotations.Schema;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using NPOI.HSSF.UserModel;
using BCrypt.Net;
using Web.Services;
using NPOI.POIFS.Crypt.Dsig;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]

    public class ExcelController : HomeController
    {
        private CipherService cipherService = new CipherService();



        public IActionResult Import(string id)
        {
            ViewBag.showResult = "N";

            if (HttpContext.Session.GetString("numOK") != null)
            {
                ViewBag.showResult = "Y";
                ViewBag.numOK = HttpContext.Session.GetString("numOK");
                ViewBag.numErr = HttpContext.Session.GetString("numErr");

                HttpContext.Session.Remove("numOK");
                HttpContext.Session.Remove("numErr");
            }


            return View();
        }


        #region 匯出
        [HttpPost]
        public FileResult ExportSave(IFormCollection form, List<string> category = null)
        {

            XSSFWorkbook hssfworkbook = new XSSFWorkbook(); //建立活頁簿
            ISheet sheet = hssfworkbook.CreateSheet(form["uri"].ToString()); //建立sheet


            //建立Excel

            XSSFCellStyle headStyle = (XSSFCellStyle)hssfworkbook.CreateCellStyle();
            headStyle.Alignment = NPOI.SS.UserModel.HorizontalAlignment.Center;
            headStyle.VerticalAlignment = NPOI.SS.UserModel.VerticalAlignment.Center;
            XSSFFont font = (XSSFFont)hssfworkbook.CreateFont();
            font.FontHeightInPoints = 11;
            font.Boldweight = (short)NPOI.SS.UserModel.FontBoldWeight.Bold;
            font.FontName = "微軟正黑體";
            headStyle.SetFont(font);

            Dictionary<int, List<string>> topData = new Dictionary<int, List<string>>();


            switch (form["uri"].ToString())
            {
                //收件匣
                case "Contact":

                    topData = ContactExport(form, category);


                    break;

                //問卷
                case "Questionnaire":

                    topData = QuestionnaireExport(form, category);


                    break;




                //訂閱
                case "Subscribe":

                    topData = SubscribeExport(form, category);


                    break;
            }


            //填入資料

            if (form["uri"].ToString() == "Subscribe")
            {
                for (int rowIndex = 0; rowIndex < topData.Count; rowIndex++)
                {
                    //int c = 0;
                    int c = 1;

                    foreach (string val in topData[rowIndex])
                    {
                        //if (c == 0)
                        //{
                        //    sheet.CreateRow(rowIndex).CreateCell(c).SetCellValue(val);
                        //}
                        //else
                        //{
                        //    sheet.GetRow(rowIndex).CreateCell(c).SetCellValue(val);
                        //}
                        if (rowIndex == 0)
                        {
                            sheet.CreateRow(0).CreateCell(0).SetCellValue(val);
                        }
                        else if (rowIndex == 1)
                        {
                            sheet.CreateRow(c).CreateCell(0).SetCellValue(val);
                        }
                        else if (rowIndex == 2)
                        {
                            sheet.GetRow(0).CreateCell(1).SetCellValue(val);
                        }
                        else if (rowIndex == 3)
                        {
                            sheet.GetRow(c).CreateCell(1).SetCellValue(val);
                        }


                        sheet.SetColumnWidth(c, 5000);  //寬度調整

                        c++;
                    }

                }
            }
            else
            {
                for (int rowIndex = 0; rowIndex < topData.Count; rowIndex++)
                {
                    int c = 0;

                    foreach (string val in topData[rowIndex])
                    {
                        if (c == 0)
                        {
                            sheet.CreateRow(rowIndex).CreateCell(c).SetCellValue(val);
                        }
                        else
                        {
                            sheet.GetRow(rowIndex).CreateCell(c).SetCellValue(val);
                        }

                        sheet.SetColumnWidth(c, 5000);  //寬度調整

                        c++;
                    }

                }
            }
        


            var excelDatas = new MemoryStream();
            hssfworkbook.Write(excelDatas);

            return File(excelDatas.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", string.Format(form["uri"].ToString() + ".xlsx"));






        }

        /// <summary>
        /// 收件匣
        /// </summary>
        /// <param name="form"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public Dictionary<int, List<string>> ContactExport(IFormCollection form, List<string> category = null)
        {

            //新增標題列
            Dictionary<int, List<string>> topData = new Dictionary<int, List<string>>();

            List<string> strList = new List<string> { "Name", "E-mail", "Phone Number", "Country / Area", "Company", "Product Category", "Model name", "Subject", "Description", "Status" };

            topData.Add(0, strList);

            string lang = form["lang"].ToString();

            List<Contact> contacts = DB.Contact.Where(m => m.uri == "Contact")
                 .Where(m => m.lang == lang)
                 .Where(!string.IsNullOrEmpty(form["start_at"].ToString()) ? (m => m.created_at >= DateTime.Parse(form["start_at"].ToString())) : m => m.id != "")
                 .Where(!string.IsNullOrEmpty(form["end_at"].ToString()) ? (m => m.created_at <= DateTime.Parse(form["end_at"].ToString())) : m => m.id != "")
                 .Where(m => (form["active"].ToString() != "All") ? m.active == bool.Parse(form["active"].ToString()) : m.id != "")
                 .ToList();


            List<SiteParameterItem> siteParameterItem = DB.SiteParameterItem
                   .Where(m => m.active == true)
                   .Where(m => m.category_id == "ProductCategory")
                   .OrderBy(m => m.sort)
                   .Include(x => x.LangData.Where(l => l.language_id == lang)).ToList();


            int index = 1;



            foreach (Contact item in contacts)
            {
                List<string> data = new List<string>();

                JsonModel ContactData = JsonConvert.DeserializeObject<JsonModel>(item.contact);
                OptionsData optionData = JsonConvert.DeserializeObject<OptionsData>(item.options);

                List<string> optionCategory = optionData.ProductCategory;

                var CheckCategory = category.Where(i => !optionCategory.Contains(i)).ToList();

                if (category.Count == 0 || CheckCategory.Count > 0)
                {
                    data.Add(cipherService.Decrypt(item.name));
                    data.Add(cipherService.Decrypt(item.email));




                    data.Add(cipherService.Decrypt(ContactData.tel));

                    data.Add(ContactData.Country);
                    data.Add(ContactData.company);

                    List<string> pro = new List<string>();

                    List<SiteParameterItem> temp = siteParameterItem.Where(m => optionCategory.Contains(m.id)).ToList();

                    foreach (SiteParameterItem subItem in temp)
                    {
                        LanguageResource langData = subItem.LangData.FirstOrDefault();
                        pro.Add(langData.title);
                    }


                    data.Add((pro.Count > 0) ? string.Join(", ", pro) : "");


                    data.Add((optionData.ModelName != null && optionData.ModelName.Count > 0) ? optionData.ModelName[0] : "");
                    data.Add(ContactData.Subject);
                    data.Add(ContactData.Description);

                    data.Add((item.active == true) ? "已處理" : "未處理");

                    topData.Add(index, data);

                    index++;
                }

            }

            return topData;
        }


        /// <summary>
        /// 問卷
        /// </summary>
        /// <param name="form"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public Dictionary<int, List<string>> QuestionnaireExport(IFormCollection form, List<string> category = null)
        {

            try
            {
                //新增標題列
                Dictionary<int, List<string>> topData = new Dictionary<int, List<string>>();

                string lang = form["lang"].ToString();

                //取得項目

                List<QuestionnaireItem> questionnaireTitle = DB.QuestionnaireItem.Where(m => m.active == true).OrderBy(m => m.sort).Include(
                             x => x.LangData
                             .Where(l => l.language_id == lang)
                        ).ToList();

                List<string> strList = new List<string>();

                strList.Add("語系");
                strList.Add("日期");

                foreach (QuestionnaireItem item in questionnaireTitle.Where(m => m.category_id == "information").ToList())
                {
                    LanguageResource LangData = item.LangData?.FirstOrDefault();

                    strList.Add(LangData.title);
                }

                foreach (QuestionnaireItem item in questionnaireTitle.Where(m => m.category_id == "relation").ToList())
                {
                    LanguageResource LangData = item.LangData?.FirstOrDefault();

                    strList.Add(LangData.title);
                }


                //評分表分類
                List<string> questionnaireGroup = DB.QuestionnaireGroup.Where(m => m.parent_id == "responsibility").OrderBy(m => m.sort).Include(
                         x => x.LangData
                         .Where(l => l.language_id == lang)
                    ).Select(m => m.id).ToList();



                foreach (string questionnaireId in questionnaireGroup)
                {

                    foreach (QuestionnaireItem item in questionnaireTitle.Where(m => m.category_id == questionnaireId).ToList())
                    {
                        LanguageResource LangData = item.LangData?.FirstOrDefault();

                        strList.Add(LangData.title);
                    }

                }



                strList.Add("others");



                topData.Add(0, strList);


                List<Contact> contactsen = DB.Contact.Where(m => m.uri == "Questionnaire")
                    .Where(m => m.lang == "en")
                     .Where(!string.IsNullOrEmpty(form["start_at"].ToString()) ? (m => m.created_at >= DateTime.Parse(form["start_at"].ToString())) : m => m.id != "")
                     .Where(!string.IsNullOrEmpty(form["end_at"].ToString()) ? (m => m.created_at <= DateTime.Parse(form["end_at"].ToString())) : m => m.id != "")
                     //.Where(m => (form["active"].ToString() != "All") ? m.active == bool.Parse(form["active"].ToString()) : m.id != "")
                     .ToList();

                List<Contact> contactstw = DB.Contact.Where(m => m.uri == "Questionnaire")
    .Where(m => m.lang == "zh-TW")
     .Where(!string.IsNullOrEmpty(form["start_at"].ToString()) ? (m => m.created_at >= DateTime.Parse(form["start_at"].ToString())) : m => m.id != "")
     .Where(!string.IsNullOrEmpty(form["end_at"].ToString()) ? (m => m.created_at <= DateTime.Parse(form["end_at"].ToString())) : m => m.id != "")
     //.Where(m => (form["active"].ToString() != "All") ? m.active == bool.Parse(form["active"].ToString()) : m.id != "")
     .ToList();

                List<Contact> contactscn = DB.Contact.Where(m => m.uri == "Questionnaire")
    .Where(m => m.lang == "zh-CN")
     .Where(!string.IsNullOrEmpty(form["start_at"].ToString()) ? (m => m.created_at >= DateTime.Parse(form["start_at"].ToString())) : m => m.id != "")
     .Where(!string.IsNullOrEmpty(form["end_at"].ToString()) ? (m => m.created_at <= DateTime.Parse(form["end_at"].ToString())) : m => m.id != "")
     //.Where(m => (form["active"].ToString() != "All") ? m.active == bool.Parse(form["active"].ToString()) : m.id != "")
     .ToList();

                List<Contact> contacts = new List<Contact>();
                foreach (var itemen in contactsen)
                {
                    contacts.Add(itemen);
                }
                foreach (var itemtw in contactstw)
                {
                    contacts.Add(itemtw);
                }
                foreach (var itemcn in contactscn)
                {
                    contacts.Add(itemcn);
                }

                int index = 1;



                foreach (Contact item in contacts)
                {
                    dynamic obj = null;

                    var serializerOptions = new System.Text.Json.JsonSerializerOptions
                    {
                        Converters = { new DynamicJsonConverter() },
                        WriteIndented = true
                    };

                    obj = System.Text.Json.JsonSerializer.Deserialize<dynamic>(item.contact, serializerOptions);

                    List<string> opt = new List<string>();

                    switch (item.lang)
                    {
                        case "en":
                            opt.Add("English");
                            break;
                        case "zh-TW":
                            opt.Add("繁體中文");
                            break;
                        case "zh-CN":
                            opt.Add("简体中文");
                            break;
                    }

                    opt.Add(DateTime.Parse(item.created_at.ToString()).ToString("yyyy-MM-dd HH:mm"));


                    //information
                    foreach (QuestionnaireItem questionnaireItem in questionnaireTitle.Where(m => m.category_id == "information").ToList())
                    {
                        LanguageResource LangData = questionnaireItem.LangData?.FirstOrDefault();

                        FormModel model = JsonConvert.DeserializeObject<FormModel>(LangData.options);

                        int iscolumn = 0;

                        foreach (var data in (IDictionary<String, Object>)obj)
                        {
                            if (questionnaireItem != null && data.Key == questionnaireItem.spec && data.Key != "lang")
                            {
                                iscolumn = 1;
                                string showVal = "";

                                if (model.type == "checkbox")
                                {

                                    foreach (var propertyInfo in (IList<object>)data.Value)
                                    {
                                        // do stuff here
                                        showVal += propertyInfo + ",";

                                    }

                                }
                                else
                                {
                                    if (data.Value.ToString().IndexOf("System.Object") == -1)
                                    {
                                        showVal = data.Value.ToString();
                                    }
                                    else
                                    {
                                        showVal = "";
                                    }

                                }


                                opt.Add(showVal);
                            }
                        }
                        if (iscolumn == 0)
                        {
                            opt.Add("");
                        }
                        iscolumn = 0;
                    }

                    //relation
                    foreach (QuestionnaireItem questionnaireItem in questionnaireTitle.Where(m => m.category_id == "relation").ToList())
                    {
                        LanguageResource LangData = questionnaireItem.LangData?.FirstOrDefault();

                        FormModel model = JsonConvert.DeserializeObject<FormModel>(LangData.options);
                        int spacecount = 0;

                        foreach (var data in (IDictionary<String, Object>)obj)
                        {

                            if (questionnaireItem != null && data.Key == questionnaireItem.spec && data.Key != "lang")
                            {
                                spacecount = 1;
                                string showVal = "";

                                if (model.type == "checkbox")
                                {

                                    foreach (var propertyInfo in (IList<object>)data.Value)
                                    {
                                        // do stuff here
                                        showVal += propertyInfo + ",";

                                    }

                                }
                                else
                                {
                                    if (data.Value.ToString().IndexOf("System.Object") == -1)
                                    {
                                        showVal = data.Value.ToString();
                                    }
                                    else
                                    {
                                        showVal = "";
                                    }
                                }


                                opt.Add(showVal);
                            }
                        }
                        if (spacecount == 0)
                        {
                            opt.Add(" ");
                        }
                    }


                    //responsibility


                    foreach (string questionnaireId in questionnaireGroup)
                    {

                        foreach (QuestionnaireItem questionnaireItem in questionnaireTitle.Where(m => m.category_id == questionnaireId).ToList())
                        {
                            LanguageResource LangData = questionnaireItem.LangData?.FirstOrDefault();

                            FormModel model = JsonConvert.DeserializeObject<FormModel>(LangData.options);
                            int radiocount = 0;

                            foreach (var data in (IDictionary<String, Object>)obj)
                            {

                                if (questionnaireItem != null && data.Key == questionnaireItem.spec && data.Key != "lang")
                                {

                                    string showVal = "";
                                    radiocount = 1;

                                    if (model.type == "checkbox")
                                    {

                                        foreach (var propertyInfo in (IList<object>)data.Value)
                                        {
                                            // do stuff here
                                            showVal += propertyInfo + ",";

                                        }

                                    }
                                    else
                                    {
                                        if (data.Value.ToString().IndexOf("System.Object") == -1)
                                        {
                                            showVal = data.Value.ToString();
                                        }
                                        else
                                        {
                                            showVal = "";
                                        }
                                    }


                                    opt.Add(showVal);
                                }

                            }
                            if (radiocount == 0)
                            {
                                opt.Add(" ");
                            }
                        }

                    }

                    //others
                    foreach (var data in (IDictionary<String, Object>)obj)
                    {
                        if (data.Key == "others")
                        {
                            opt.Add(data.Value.ToString());
                        }
                    }

                    topData.Add(index, opt);

                    index++;


                }

                return topData;
            }
            catch
            {
                List<string> strList = new List<string>();

                strList.Add("匯出資訊取得錯誤，請檢測語系是否對應欄位資料為空!");

                Dictionary<int, List<string>> topData = new Dictionary<int, List<string>>();
                topData.Add(0, strList);

                return topData;
            }

        }

        /// <summary>
        /// 訂閱
        /// </summary>
        /// <param name="form"></param>
        /// <param name="category"></param>
        /// <returns></returns>
        public Dictionary<int, List<string>> SubscribeExport(IFormCollection form, List<string> category = null)
        {

            //新增標題列
            Dictionary<int, List<string>> topData = new Dictionary<int, List<string>>();

            List<string> strList = new List<string> { "E-mail" };

            topData.Add(0, strList);


            List<Contact> contacts = DB.Contact.Where(m => m.uri == "Subscribe")
                 .Where(!string.IsNullOrEmpty(form["start_at"].ToString()) ? (m => m.created_at >= DateTime.Parse(form["start_at"].ToString())) : m => m.id != "")
                 .Where(!string.IsNullOrEmpty(form["end_at"].ToString()) ? (m => m.created_at <= DateTime.Parse(form["end_at"].ToString())) : m => m.id != "")
                 //.Where(m => (form["active"].ToString() != "All") ? m.active == bool.Parse(form["active"].ToString()) : m.id != "")
                 .ToList();




            int index = 1;

            List<string> data = new List<string>();
            List<string> data2 = new List<string>();

            foreach (Contact item in contacts)
            {

                data.Add(cipherService.Decrypt(item.email));
                data2.Add(DateTime.Parse(item.created_at.ToString()).ToString("yyyy-MM-dd HH:mm"));
                //topData.Add(index, data);

                //index++;


            }
            topData.Add(index, data);

            List<string> strList2 = new List<string> { "建立日期" };

            topData.Add(2, strList2);
            topData.Add(3, data2);

            return topData;
        }


        #endregion


        #region 匯入

        [HttpPost]
        public void ImportSave(IFormFile postFile, string uri, string sheetName, string category_id = "")
        {
            switch (uri)
            {
                //新聞稿
                case "news":

                    ArticleNewsImport(postFile, uri, sheetName, category_id);

                    break;

                //月銷售額   
                case "finance":

                    ArticleFinanceImport(postFile, uri, sheetName, category_id);

                    break;

                //季報表      
                case "quarterly":

                    ArticleQuarterlyImport(postFile, uri, sheetName, category_id);

                    break;


                //美國證券交易委員會文件      
                case "filings":

                    ArticleFilingsImport(postFile, uri, sheetName, category_id);

                    break;

                //合併財務報表      
                case "consolidated":

                    ArticleConsolidatedImport(postFile, uri, sheetName, category_id);

                    break;


                //合併財務報表      
                case "reports":

                    ArticleReportsImport(postFile, uri, sheetName, category_id);

                    break;

                //年度報告       
                case "annual":

                    ArticleAnnualImport(postFile, uri, sheetName, category_id);

                    break;
            }



        }

        /// <summary>
        /// 新聞稿
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleNewsImport(IFormFile postFile, string uri, string sheetName, string category_id = "")
        {


            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleNews/Import/news");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ProductSet/Import");
                    return;
                }

                int rowCont = sheet.LastRowNum;


                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }


                        }

                        if (val.Count > 0)
                        {
                            ArticleNews dbData = new ArticleNews()
                            {
                                id = id,
                                path = "",
                                uri = "news",
                                category_id = category_id,
                                active = (val[9] == "" || val[9] == "Y") ? true : false,
                                sort = (val[10] != "") ? int.Parse(val[10]) : 99,
                            };

                            DB.ArticleNews.Add(dbData);

                            string files = "{\"file\":[{\"path\":\"" + val[11] + "\",\"title\":\"" + val[11] + "\"}]}";
                            if (val.Count > 10 && !string.IsNullOrEmpty(val[11]) && !string.IsNullOrEmpty(val[12]))
                            {
                                if (val[12].IndexOf("www.spil.com.tw") == -1)
                                {
                                    files = "{\"file\":[{\"path\":\"/Files/news/file/" + val[11] + "/" + val[12] + "\",\"title\":\"" + val[12] + "\"}]}";
                                }
                                else
                                {
                                    string urlFile = val[12].Replace("https://www.spil.com.tw", "/Files");

                                    files = "{\"file\":[{\"path\":\"" + urlFile + "\",\"title\":\"" + val[12] + "\"}]}";
                                }
                                //files = "{\"file\":[{\"path\":\"/Files/news/file/" + val[11] + "/" + val[12] + "\",\"title\":\"" + val[12] + "\"}]}";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(val[11]))
                                {
                                    files = "{\"file\":[{\"path\":\"" + val[11] + "\",\"title\":\"" + val[11] + "\"}]}";
                                }

                            }
                            string pic = "";
                            if (!string.IsNullOrEmpty(val[1]))
                            {
                                pic = "{\"pic\":[{\"path\":\"" + val[1] + "\",\"title\":\"\"}]}";
                            }


                            //建立日期
                            DateTime created_at = DateTime.Now;
                            if (val.Count >= 14 && !string.IsNullOrEmpty(val[13]))
                            {

                                created_at = DateTime.Parse(val[13]);
                            }


                            LanguageResource LanguageData = new LanguageResource()
                            {
                                id = (new Helper()).GenerateIntID(),
                                language_id = LangId,
                                category_id = category_id,
                                sort = (val[10] != "") ? int.Parse(val[10]) : 99,
                                title = val[0],
                                pic = pic,
                                details = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "notes", val[2] }, { "editor", val[3] } }),//lastCellNum
                                ArticleNewsId = id,
                                seo = JsonConvert.SerializeObject(new Dictionary<string, object>() { { "keywords", val[5] }, { "description", val[4] } }),
                                files = files,
                                start_at = (!string.IsNullOrEmpty(val[6]) && val[6] != "NULL") ? DateTime.Parse(val[6]) : null,
                                end_at = (!string.IsNullOrEmpty(val[7]) && val[7] != "NULL") ? DateTime.Parse(val[7]) : null,
                                top = (val[8] == "" || val[8] == "N") ? false : true,
                                active = (val[9] == "" || val[9] == "Y") ? true : false,

                                created_at = created_at,
                                updated_at = DateTime.Now,
                                model_type = "ArticleNews",
                            };

                            DB.LanguageResource.Add(LanguageData);
                        }





                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleNews/Import/news");
        }

        /// <summary>
        /// 月銷售額
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleFinanceImport(IFormFile postFile, string uri, string sheetName, string category_id = "finance")
        {
            ///////////////電子報訂閱匯入
            
            //List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            //var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            //using (var stream = new FileStream(savePath, FileMode.Create))
            //{
            //    postFile.CopyTo(stream);

            //}

            //DataTable data = new DataTable();
            //FileStream fs;
            //int startRow = 0;

            //int numOK = 0;
            //int numErr = 0;
            //string errorType = "";
            //using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            //{

            //    IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

            //    ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


            //    if (sheet == null)
            //    {
            //        HttpContext.Session.SetString("numOK", numOK.ToString());
            //        HttpContext.Session.SetString("numErr", "1");
            //        HttpContext.Session.SetString("errorType", "查無sheetName");

            //        Response.Redirect("/Siteadmin/ArticleDownload/Import/finance");
            //        return;
            //    }


            //    IRow firstrow = sheet.GetRow(0);//第一行

            //    int firstCellNum = 0;// 行第一個cell的編號,從0開始
            //    int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

            //    try
            //    {
            //        firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
            //        lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
            //    }
            //    catch
            //    {
            //        HttpContext.Session.SetString("numOK", numOK.ToString());
            //        HttpContext.Session.SetString("numErr", "1");
            //        HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

            //        Response.Redirect("/Siteadmin/ArticleDownload/Import/finance");
            //        return;
            //    }

            //    int rowCont = sheet.LastRowNum;


            //    for (int s = 1; s <= rowCont; s++)
            //    {

            //        IRow thisRaw = sheet.GetRow(s);//第N行
            //        ICell topCell = thisRaw.GetCell(0);
            //        topCell.SetCellType(CellType.String);
            //        string email = topCell.StringCellValue;
            //        string encodeemail = cipherService.Encrypt(email);
            //        ICell topCell2 = thisRaw.GetCell(1);
            //        topCell2.SetCellType(CellType.String);
            //        DateTime createdate = DateTime.Parse(topCell2.StringCellValue);

            //        Contact contactData = new Contact()
            //        {
            //            id = (new Helper()).GenerateIntID(),
            //            uri = "Subscribe",
            //            lang = "en",
            //            name = encodeemail,
            //            email = encodeemail,
            //            active = true,
            //            send = false,
            //            created_at = createdate,
            //            updated_at = createdate
            //        };

            //        DB.Contact.Add(contactData);


            //        DB.SaveChanges();

            //    }




            //}

            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/finance");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/finance");
                    return;
                }

                int rowCont = sheet.LastRowNum;


                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }


                        }

                        ArticleDownload dbData = new ArticleDownload()
                        {
                            id = id,
                            path = "",
                            uri = "finance",
                            year = val[1],
                            month = val[2],
                            category_id = category_id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                        };

                        DB.ArticleDownload.Add(dbData);


                        string files = "{\"file\":[{\"path\":\"" + val[3] + "\",\"title\":\"\"}]}";
                        if (val.Count > 6 && !string.IsNullOrEmpty(val[6]))
                        {
                            files = "{\"file\":[{\"path\":\"/Files/reports/" + val[6] + "/" + val[3] + "\",\"title\":\"" + val[3] + "\"}]}";
                        }



                        LanguageResource LanguageData = new LanguageResource()
                        {
                            id = (new Helper()).GenerateIntID(),
                            language_id = LangId,
                            category_id = category_id,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                            title = val[0],
                            files = files,
                            ArticleDownloadId = id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            model_type = "ArticleDownload",
                        };

                        DB.LanguageResource.Add(LanguageData);
                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleDownload/Import/finance");
        }

        /// <summary>
        /// 季報表
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleQuarterlyImport(IFormFile postFile, string uri, string sheetName, string category_id = "quarterly")
        {


            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/quarterly");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/quarterly");
                    return;
                }

                int rowCont = sheet.LastRowNum;



                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }

                        }





                        ArticleDownload dbData = new ArticleDownload()
                        {
                            id = id,
                            path = "",
                            uri = "quarterly",
                            year = val[1],
                            month = val[2],
                            category_id = category_id,
                            active = (val[8] == "" || val[8] == "Y") ? true : false,
                            sort = (val[9] != "") ? int.Parse(val[9]) : 99,
                        };

                        DB.ArticleDownload.Add(dbData);


                        //檔案處理
                        Dictionary<string, List<Dictionary<string, string>>> files = new Dictionary<string, List<Dictionary<string, string>>>();

                        List<string> tempFilePath = val[3].Split(',').ToList();
                        List<string> tempFileTitle = val[4].Split(',').ToList();
                        List<string> tempFileId = val[10].Split(',').ToList();


                        List<Dictionary<string, string>> fileList = new List<Dictionary<string, string>>();

                        int f = 0;
                        foreach (string file in tempFilePath)
                        {
                            Dictionary<string, string> tempFile = new Dictionary<string, string>();

                            if (!string.IsNullOrEmpty(file))
                            {
                                if (val.Count >= 10)
                                {
                                    //"{\"file\":[{\"path\":\"/Files/reports/" + val[6] + "/" + val[3] + "\",\"title\":\""+ val[3] + "\"}]}";
                                    tempFile.Add("path", "/Files/reports/" + tempFileId[f] + "/" + file);
                                }
                                else
                                {
                                    tempFile.Add("path", file);
                                }
                            }
                            else
                            {
                                tempFile.Add("path", "");
                            }




                            if (tempFileTitle.Count > f)
                            {

                                tempFile.Add("title", tempFileTitle[f]);


                            }
                            else
                            {
                                tempFile.Add("title", "");
                            }

                            f++;
                            fileList.Add(tempFile);
                        }


                        files.Add("file", fileList);






                        //影片
                        List<Dictionary<string, string>> videos = new List<Dictionary<string, string>>();

                        List<string> tempVideoPath = val[5].Split(',').ToList();
                        List<string> tempVideoTitle = val[6].Split(',').ToList();
                        List<string> tempVideoSubTitle = val[7].Split(',').ToList();




                        int v = 0;
                        foreach (string video in tempVideoPath)
                        {
                            Dictionary<string, string> tempVideo = new Dictionary<string, string>();

                            tempVideo.Add("path", video);

                            if (tempVideoTitle.Count > v)
                            {
                                tempVideo.Add("title", tempVideoTitle[v]);
                            }
                            else
                            {
                                tempVideo.Add("title", "");
                            }

                            if (tempVideoSubTitle.Count > v)
                            {
                                tempVideo.Add("sub_title", tempVideoSubTitle[v]);
                            }
                            else
                            {
                                tempVideo.Add("sub_title", "");
                            }

                            v++;

                            videos.Add(tempVideo);
                        }






                        LanguageResource LanguageData = new LanguageResource()
                        {
                            id = (new Helper()).GenerateIntID(),
                            language_id = LangId,
                            category_id = category_id,
                            title = val[0],
                            video = JsonConvert.SerializeObject(videos),
                            files = JsonConvert.SerializeObject(files),
                            ArticleDownloadId = id,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            model_type = "ArticleDownload",
                            active = (val[8] == "" || val[8] == "Y") ? true : false,
                            sort = (val[9] != "") ? int.Parse(val[9]) : 99,
                        };

                        DB.LanguageResource.Add(LanguageData);
                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleDownload/Import/quarterly");
        }

        /// <summary>
        /// 美國證券交易委員會文件
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleFilingsImport(IFormFile postFile, string uri, string sheetName, string category_id = "filings")
        {


            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/filings");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/filings");
                    return;
                }

                int rowCont = sheet.LastRowNum;


                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }


                        }

                        ArticleDownload dbData = new ArticleDownload()
                        {
                            id = id,
                            path = "",
                            uri = "filings",
                            category_id = category_id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                        };

                        DB.ArticleDownload.Add(dbData);



                        string files = "{\"file\":[{\"path\":\"" + val[1] + "\"}],\"file2\":[{\"path\":\"" + val[2] + "\"}]}";
                        if (val.Count > 7 && !string.IsNullOrEmpty(val[6]))
                        {
                            string f1 = (!string.IsNullOrEmpty(val[1])) ? "/Files/reports/" + val[6] + "/" + val[1] : "";
                            string f2 = (!string.IsNullOrEmpty(val[2])) ? "/Files/reports/" + val[7] + "/" + val[2] : "";


                            files = "{\"file\":[{\"path\":\"" + f1 + "\"}],\"file2\":[{\"path\":\"" + f2 + "\"}]}";
                        }


                        LanguageResource LanguageData = new LanguageResource()
                        {
                            id = (new Helper()).GenerateIntID(),
                            language_id = LangId,
                            category_id = category_id,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                            title = val[0],
                            files = files,
                            pic = "{\"pic\":[{\"path\":\"" + val[3] + "\",\"title\":\"\"}]}",
                            ArticleDownloadId = id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            model_type = "ArticleDownload",
                        };

                        DB.LanguageResource.Add(LanguageData);
                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleDownload/Import/filings");
        }


        /// <summary>
        /// 合併財務報表
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleConsolidatedImport(IFormFile postFile, string uri, string sheetName, string category_id = "consolidated")
        {


            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/consolidated");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/consolidated");
                    return;
                }

                int rowCont = sheet.LastRowNum;


                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }


                        }

                        ArticleDownload dbData = new ArticleDownload()
                        {
                            id = id,
                            path = "",
                            uri = "consolidated",
                            year = val[1],
                            month = val[2],
                            category_id = category_id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                        };

                        DB.ArticleDownload.Add(dbData);



                        string files = "{\"file\":[{\"path\":\"" + val[3] + "\",\"title\":\"\"}]}";
                        if (val.Count > 6 && !string.IsNullOrEmpty(val[6]))
                        {
                            files = "{\"file\":[{\"path\":\"/Files/reports/" + val[6] + "/" + val[3] + "\",\"title\":\"" + val[3] + "\"}]}";
                        }



                        LanguageResource LanguageData = new LanguageResource()
                        {
                            id = (new Helper()).GenerateIntID(),
                            language_id = LangId,
                            category_id = category_id,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                            title = val[0],
                            files = files,
                            ArticleDownloadId = id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            model_type = "ArticleDownload",
                        };

                        DB.LanguageResource.Add(LanguageData);
                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleDownload/Import/consolidated");

        }

        /// <summary>
        /// 財務報告 
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleReportsImport(IFormFile postFile, string uri, string sheetName, string category_id = "reports")
        {


            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/reports");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/reports");
                    return;
                }

                int rowCont = sheet.LastRowNum;


                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }


                        }

                        ArticleDownload dbData = new ArticleDownload()
                        {
                            id = id,
                            path = "",
                            uri = "reports",
                            year = val[1],
                            month = val[2],
                            category_id = category_id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                        };

                        DB.ArticleDownload.Add(dbData);



                        string files = "{\"file\":[{\"path\":\"" + val[3] + "\",\"title\":\"\"}]}";
                        if (val.Count > 6 && !string.IsNullOrEmpty(val[6]))
                        {
                            files = "{\"file\":[{\"path\":\"/Files/reports/" + val[6] + "/" + val[3] + "\",\"title\":\"" + val[3] + "\"}]}";
                        }



                        LanguageResource LanguageData = new LanguageResource()
                        {
                            id = (new Helper()).GenerateIntID(),
                            language_id = LangId,
                            category_id = category_id,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                            title = val[0],
                            files = files,
                            ArticleDownloadId = id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            model_type = "ArticleDownload",
                        };

                        DB.LanguageResource.Add(LanguageData);
                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleDownload/Import/reports");

        }

        /// <summary>
        /// 年度報告
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="uri"></param>
        /// <param name="sheetName"></param>
        /// <param name="category_id"></param>
        public void ArticleAnnualImport(IFormFile postFile, string uri, string sheetName, string category_id = "annual")
        {


            List<Languages> languages = DB.Languages.OrderBy(m => m.sort).ToList();


            var savePath = Path.GetFullPath("wwwroot/Files/cache/") + postFile.FileName;
            using (var stream = new FileStream(savePath, FileMode.Create))
            {
                postFile.CopyTo(stream);

            }

            DataTable data = new DataTable();
            FileStream fs;
            int startRow = 0;

            int numOK = 0;
            int numErr = 0;
            string errorType = "";
            using (fs = new FileStream(savePath, FileMode.Open, FileAccess.Read))
            {

                IWorkbook workbook = savePath.Contains(".xlsx") ? (IWorkbook)new XSSFWorkbook(fs) : null;//xlsx使用XSSFWorkbook， xls使用HSSFWorkbokk

                ISheet sheet = workbook.GetSheet(sheetName) ?? workbook.GetSheetAt(0);//如果沒有找到指sheetName 對應的sheet，則嘗試獲取第一個sheet


                if (sheet == null)
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無sheetName");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/annual");
                    return;
                }


                IRow firstrow = sheet.GetRow(0);//第一行

                int firstCellNum = 0;// 行第一個cell的編號,從0開始
                int lastCellNum = 0; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）

                try
                {
                    firstCellNum = firstrow.FirstCellNum;// 行第一個cell的編號,從0開始
                    lastCellNum = firstrow.LastCellNum; //  行最後一個cell的編號 即總的列數,（不忽略中間某    列空格）
                }
                catch
                {
                    HttpContext.Session.SetString("numOK", numOK.ToString());
                    HttpContext.Session.SetString("numErr", "1");
                    HttpContext.Session.SetString("errorType", "查無對應資訊或SheetName錯誤");

                    Response.Redirect("/Siteadmin/ArticleDownload/Import/annual");
                    return;
                }

                int rowCont = sheet.LastRowNum;


                for (int s = 1; s <= rowCont; s++)
                {

                    IRow thisRaw = sheet.GetRow(s);//第N行
                    ICell topCell = thisRaw.GetCell(0);
                    topCell.SetCellType(CellType.String);
                    string LangId = topCell.StringCellValue;




                    if (languages.Where(m => m.language_id == LangId) == null)
                    {

                        errorType += "查無該語系ID[" + LangId + "]<br>";
                        numErr++;
                    }
                    else
                    {
                        string id = (new Helper()).GenerateIntID();

                        List<string> val = new List<string>();

                        for (int i = 1; i <= lastCellNum; i++)
                        {

                            ICell cell = thisRaw.GetCell(i);

                            if (cell != null)
                            {
                                try
                                {
                                    cell.SetCellType(CellType.String);
                                    val.Add(cell.StringCellValue);
                                }
                                catch
                                {

                                    val.Add("");
                                }
                            }
                            else
                            {
                                val.Add("");
                            }


                        }

                        ArticleDownload dbData = new ArticleDownload()
                        {
                            id = id,
                            path = "",
                            uri = "annual",
                            year = val[1],
                            category_id = category_id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                        };

                        DB.ArticleDownload.Add(dbData);

                        string files = "";
                        if (!string.IsNullOrEmpty(val[2]))
                        {
                            files = "{\"file\":[{\"path\":\"" + val[2] + "\",\"title\":\"\"}]}";
                            if (val.Count >= 7 && !string.IsNullOrEmpty(val[6]))
                            {
                                files = "{\"file\":[{\"path\":\"/Files/reports/" + val[6] + "/" + val[2] + "\",\"title\":\"\"}]}";
                            }
                        }




                        LanguageResource LanguageData = new LanguageResource()
                        {
                            id = (new Helper()).GenerateIntID(),
                            language_id = LangId,
                            category_id = category_id,
                            sort = (val[5] != "") ? int.Parse(val[5]) : 99,
                            title = val[0],
                            files = files,
                            pic = "{\"pic\":[{\"path\":\"" + val[3] + "\",\"title\":\"\"}]}",
                            ArticleDownloadId = id,
                            active = (val[4] == "" || val[4] == "Y") ? true : false,
                            created_at = DateTime.Now,
                            updated_at = DateTime.Now,
                            model_type = "ArticleDownload",
                        };

                        DB.LanguageResource.Add(LanguageData);
                    }

                    numOK++;
                    DB.SaveChanges();

                }




            }

            System.IO.File.Delete(savePath);


            HttpContext.Session.SetString("numOK", numOK.ToString());
            HttpContext.Session.SetString("numErr", numErr.ToString());
            HttpContext.Session.SetString("errorType", errorType);
            Response.Redirect("/Siteadmin/ArticleDownload/Import/annual");

        }

        #endregion
    }



}