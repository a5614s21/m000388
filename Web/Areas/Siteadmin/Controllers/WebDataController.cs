﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Linq.Dynamic.Core;
using Web.Repositories.Admin;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]

    public class WebDataController : HomeController
    {
        private readonly IRepository<WebData, LanguageResource, string> _repository;
        protected Dictionary<String, Object> dataTableColnum;

        public WebDataController(IRepository<WebData, LanguageResource, string> repository)
        {
            _repository = repository;
            dataTableColnum = _repository.dataTableColnum();//取得欄位資訊
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult List(string id)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;


            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";

            //麵包屑
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.Data = _repository.FindById(id);
            }

            Dictionary<String, Object> dataTableSearch = _repository.dataTableSearch("", adminNowLang.language_id);

            ViewBag.Search = dataTableSearch["search"];

            ViewBag.orderBy = _repository.orderBy();

            ViewBag.BreadCrumbs = true;

            ViewBag.dataTableColnum = dataTableColnum;

            //取得排序鍵值
            ViewBag.orderBy = Helper.GetSortIndex(dataTableColnum, _repository.orderBy());

            return View(!string.IsNullOrEmpty(id) ? "ListUri" : "List");
        }

        /// <summary>
        /// 新增修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult Edit(string id, string uri)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;


            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";
            ViewBag.Uri = !string.IsNullOrEmpty(uri) ? uri : "";

            //取得編輯欄位
            Dictionary<String, Object> Columns = Helper.editColumns("WebData", uri);

            ViewBag.Columns = Columns;

            //取得資料
            if (id != "add")
            {

                /*WebData data = _repository.FindById(id);
                var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                var temp = Newtonsoft.Json.Linq.JArray.Parse("["+json+"]");              

                ViewBag.Data = Helper.FormatViewData(Columns , temp[0]);*/


                var data = _repository.FindByIdLang(id, adminNowLang.language_id);
                if (data != null)
                {
                    //data.ArticleNews = null;
                    var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                    var temp = Newtonsoft.Json.Linq.JArray.Parse("[" + json + "]");

                    ViewBag.Data = Helper.FormatViewData(Columns, temp[0]);
                }

            }

            return View();
        }

        /// <summary>
        /// 寫入與修改
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Save(FormRequest form)
        {
            string masterId = "web";
            WebData entity = new WebData()
            {
                id = masterId,
            };

            LanguageResource entityLang = new LanguageResource()
            {
                // id = (new Helper()).GenerateIntID(),
                model_type = "WebData",
                language_id = form.lang,
                title = form.title,
                contact = JsonConvert.SerializeObject(form.contact),
                social = JsonConvert.SerializeObject(form.social),
                seo = JsonConvert.SerializeObject(form.seo),
                options = JsonConvert.SerializeObject(form.options),
                pic = form.pic,
                active = form.active,
                updated_at = DateTime.Now,
                sort = form.sort,
                start_at = !string.IsNullOrEmpty(form.start_at) ? DateTime.Parse(form.start_at.ToString()) : null,
                end_at = !string.IsNullOrEmpty(form.end_at) ? DateTime.Parse(form.end_at.ToString()) : null,
                created_at = DateTime.Parse(form.created_at.ToString()),
                tags = form.tags,
                top = form.top,
                WebDataId = masterId,
            };


            Dictionary<string, string> newLog = new Dictionary<string, string> {
                { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                { "uri" , "modify" },
                { "username" , ViewBag.User.NormalizedUserName },
                { "title" ,   "[ " + ViewBag.SystemMenuData.title + " - " + form.title + " ]"}
            };

            newLog.Add("act", "edit");
            if (form.id == "add")
            {
                /* entity.created_at = DateTime.Now;
                 entity.id = Guid.NewGuid().ToString();
                 _repository.Create(entity ,null);
                 newLog.Add("act", "add");*/
                _repository.Create(entity, entityLang);
            }
            else
            {
                /* entity.id = form.id;      
                 _repository.Update(entity, null);
                 newLog.Add("act", "edit");*/

                entity.id = form.id;
                entityLang.WebDataId = form.id;
                _repository.Update(entity, entityLang);

            }


                (new Helper()).SaveLog(newLog);


            if (ViewBag.SystemMenuDataOptions[0]["list"] == "Y")
            {
                Response.Redirect("/Siteadmin/" + ControllerName + "/List");
            }
            else
            {
                Response.Redirect(ViewBag.SystemMenuData.link);
            }

        }




        /// <summary>
        /// DataTable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public string dataTable(string id, IFormCollection form)
        {
            IQueryable<WebData> model = _repository.FindList(id ?? "");

            string orderByKey = form["columns[" + form["order[0][column]"] + "][data]"].ToString();
            string orderByType = form["order[0][dir]"].ToString();
            string orderBy = orderByKey + " " + orderByType.ToUpper();
            int skip = int.Parse(form["start"].ToString());
            int take = int.Parse(form["length"].ToString());

            Dictionary<String, Object> list = new Dictionary<String, Object>();

            list.Add("iTotalDisplayRecords", model.Count());

            //查詢
            if (!string.IsNullOrEmpty(form["searchJson"].ToString()))
            {
                Newtonsoft.Json.Linq.JToken sSearch = Newtonsoft.Json.Linq.JArray.Parse("[" + form["searchJson"].ToString() + "]");
                sSearch = sSearch[0];


                if (sSearch["col"].ToString() == "all")
                {
                    model = model.Where("title.Contains(@0) or details.Contains(@0)", sSearch["keyword"].ToString());
                }
                else
                {
                    model = model.Where(sSearch["col"].ToString() + ".Contains(@0)", sSearch["keyword"].ToString());
                }

            }

            // model = model.Where("JSON_VALUE(options, '$.show') IS NOT NULL or JSON_VALUE(options, '$.show') != 'N'");

            dynamic output = model.OrderBy(orderBy).ToList().Skip(skip).Take(take);


            //整理輸出資料
            var data = (new DataTable()).columns(JsonConvert.SerializeObject(output), ControllerName, _repository, uri, id);

            list.Add("data", data);

            return JsonConvert.SerializeObject(list, Formatting.Indented);
        }




    }
}