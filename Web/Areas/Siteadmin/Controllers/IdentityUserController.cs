﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Linq.Dynamic.Core;
using BCrypt.Net;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Web.Repositories.Admin;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]
    
    public class IdentityUserController : HomeController
    {
        private readonly IRepository<IdentityUser, LanguageResource, string> _repository;
        protected Dictionary<String, Object> dataTableColnum;
        private dynamic appSeting;

        public IdentityUserController(IRepository<IdentityUser, LanguageResource, string> repository , IConfiguration config )
        {
            _repository = repository;
            dataTableColnum = _repository.dataTableColnum();//取得欄位資訊
            appSeting = (new Helper()).GetAppSettings();
            ViewBag.appSeting = appSeting;
        
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult List(string id)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;

            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";

            //麵包屑
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.Data = _repository.FindById(id);
            }

            Dictionary<String, Object> dataTableSearch = _repository.dataTableSearch("", adminNowLang.language_id);

            ViewBag.Search = dataTableSearch["search"];

            ViewBag.dataTableColnum = dataTableColnum;

            ViewBag.orderBy = _repository.orderBy();

            //取得排序鍵值
            ViewBag.orderBy = Helper.GetSortIndex(dataTableColnum, _repository.orderBy());

            //使用於最後一個若為"操作"，設定為不可排序
            ViewBag.LastCloseSortIndex = (dataTableColnum.ContainsKey("modify") ? "," + (dataTableColnum.Count - 1).ToString() : "");

            return View(!string.IsNullOrEmpty(id) ? "ListUri" : "List");
        }

        /// <summary>
        /// 新增修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult Edit(string id, string uri)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;


            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";
            ViewBag.Uri = !string.IsNullOrEmpty(uri) ? uri : "";

            //取得編輯欄位
            Dictionary<String, Object> Columns = Helper.editColumns("IdentityUser", uri);

            ViewBag.Columns = Columns;

            //取得資料
            if (id != "add")
            {
                IdentityUser data = _repository.FindById(id);
                var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                var temp = Newtonsoft.Json.Linq.JArray.Parse("["+json+"]");

                ViewBag.RoleId = DB.IdentityUserRole.Where(m=>m.UserId == data.Id).First().RoleId;

                ViewBag.Data = Helper.FormatViewData(Columns , temp[0]);
            }

            return View();
        }

        /// <summary>
        /// 寫入與修改
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Save(FormUser form)
        {
            CipherService cipherService = new CipherService();

            string key = appSeting["APP_KEY"].ToString();
            

            IdentityUser entity = new IdentityUser()
            {
                Id = Guid.NewGuid().ToString(),
                UserName = cipherService.Encrypt(form.UserName),            
                NormalizedUserName = form.NormalizedUserName,
                LockoutEnabled = form.LockoutEnabled,         
                Email = cipherService.Encrypt(form.Email),
                EmailConfirmed = true,
            };

            if(!string.IsNullOrEmpty(form.PasswordHash))
            {
                PasswordHasher<IdentityUser> passwordHasher = new PasswordHasher<IdentityUser>();

                entity.PasswordHash = passwordHasher.HashPassword(entity, form.PasswordHash);
            }
            else
            {
                if(form.Id != "add")
                {
                    entity.PasswordHash = DB.IdentityUser.Where(m => m.Id == form.Id).FirstOrDefault().PasswordHash;
                }
             
            }

            Dictionary<string, string> newLog = new Dictionary<string, string> {
                { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                { "uri" , "modify" },
                { "username" , ViewBag.User.NormalizedUserName },
                { "title" ,   "[ " + ViewBag.SystemMenuData.title + " - " + form.NormalizedUserName + " ]"}
            };



            if (form.Id == "add")
            {               
                entity.Id = Guid.NewGuid().ToString();
                _repository.Create(entity, null);

                newLog.Add("act", "add");

            }
            else
            {                
                entity.Id = form.Id;
           


                _repository.Update(entity, null);

                //設定群組

                DB.IdentityUserRole.Remove(DB.IdentityUserRole.Single(m => m.UserId == entity.Id));
                DB.SaveChanges();

                /*
                var data = DB.IdentityUserRole.Single(x => x.UserId == entity.Id);
                data.RoleId = form.role.ToString();
                DB.IdentityUserRole.Attach(data);
                DB.SaveChanges();
                */

                newLog.Add("act", "edit");

            }
            (new Helper()).SaveLog(newLog);

            //設定群組
            IdentityUserRole<string> role = new IdentityUserRole<string>()
            {
                UserId = entity.Id,
                RoleId = form.role

            };
            DB.IdentityUserRole.Add(role);
            DB.SaveChanges();

            //System.Security.Claims.ClaimsPrincipal currentUser = this.User;

           // string userID = currentUser.Claims.FirstOrDefault().Value;

          //  IdentityUser user = DB.IdentityUser.Where(m => m.Id == userID).FirstOrDefault();

            //所屬群組
            //ViewBag.RoleId = DB.IdentityUserRole.Where(m => m.UserId == userID).FirstOrDefault().RoleId;

            Response.Redirect("/Siteadmin/"+ ControllerName + "/List");
        }

        /// <summary>
        /// 修改狀態
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public  string ChangeActive(IFormCollection form)
        {          

            IdentityUser entity = DB.IdentityUser.Where(m => m.Id == form["id"].ToString()).FirstOrDefault();

            entity.LockoutEnabled = bool.Parse(form["active"]);

            DB.IdentityUser.Attach(entity);

            DB.SaveChanges();

            return form["active"];
        }


        /// <summary>
        /// 刪除
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task Delete(IFormCollection form)
        {
            List<string> idList = form["id"].ToString().Split(',').ToList();

            foreach (string id in idList)
            {

                IdentityUser data = _repository.FindById(id);

                Dictionary<string, string> newLog = new Dictionary<string, string> {
                    { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                    { "uri" , "modify" },
                    { "username" , ViewBag.User.NormalizedUserName },
                    { "act" , "del" },
                    { "title" , "[ " + ViewBag.SystemMenuData.title + " - " + data.NormalizedUserName + " ]" },
                };

                (new Helper()).SaveLog(newLog);

                _repository.Delete(id);
            }

        }


        /// <summary>
        /// DataTable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public string dataTable(string id , IFormCollection form)
        {
            IQueryable<IdentityUser> model = _repository.FindList(id ?? "");          

            string orderByKey = form["columns["+ form["order[0][column]"] +"][data]"].ToString();
            string orderByType = form["order[0][dir]"].ToString();
            string orderBy = orderByKey + " "+ orderByType.ToUpper();
            int skip = int.Parse(form["start"].ToString());
            int take = int.Parse(form["length"].ToString());

            Dictionary<String, Object> list = new Dictionary<String, Object>();

            list.Add("iTotalDisplayRecords", model.Count());

            CipherService cipherService = new CipherService();

            model = model.Where(m => m.NormalizedUserName != "sysadmin");

            //查詢
            if (!string.IsNullOrEmpty(form["searchJson"].ToString()))
            {
                Newtonsoft.Json.Linq.JToken sSearch = Newtonsoft.Json.Linq.JArray.Parse("["+form["searchJson"].ToString()+"]");
                sSearch = sSearch[0];


                string keyword = cipherService.Encrypt(sSearch["keyword"].ToString());



                if (sSearch["col"].ToString() == "all")
                {
                    model = model.Where("name.Contains(@0) or contact.Contains(@0)", keyword);
                   
                }
                else
                {
                    model = model.Where(sSearch["col"].ToString() + ".Contains(@0)", keyword);
                }

            }

            dynamic output = model.OrderBy(orderBy).ToList().Skip(skip).Take(take);
            

            //整理輸出資料
            var data = (new DataTable()).columns(JsonConvert.SerializeObject(output) , ControllerName, _repository , uri , id);          

            list.Add("data", data);   

            return JsonConvert.SerializeObject(list, Formatting.Indented);
        }


        /// <summary>
        /// 判斷帳號
        /// </summary>
        /// <returns></returns>
        public string CheckUsername()
        {
            string NormalizedUserName = HttpContext.Request.Query["NormalizedUserName"];

            IdentityUser IdentityUser = DB.IdentityUser.Where(m => m.NormalizedUserName == NormalizedUserName).FirstOrDefault();

            if(IdentityUser == null)
            {
                return "true";
            }
            else
            {
                return "false";
            }


        }


        public class FormUser
        {
            public string? Id { get; set; }
            public string? UserName { get; set; }
            public string? NormalizedUserName { get; set; }
            public bool LockoutEnabled { get; set; }

            public string? PasswordHash { get; set; }

            public string? Email { get; set; }


            public string? role { get; set; }
        }
    }
}