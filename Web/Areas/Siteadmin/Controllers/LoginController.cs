﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Security.Principal;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;


namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]    
    public class LoginController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        protected dynamic appSeting = null;
        protected ApplicationDbContext DB = new ApplicationDbContext();
        public LoginController(SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /// <summary>
        /// 登入判斷
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Verify(UserRequest request)
        {
            Dictionary<string, string> newLog = new Dictionary<string, string> {
                { "act" , "login" },
                { "ip" , HttpContext.Connection.RemoteIpAddress.ToString() },
                { "uri" , "login" },
                { "username" , request.username }
            };

            string temp1 = HttpContext.Session.GetString("verificationCode");
            string temp2 = temp1.ToUpper();
            string temp3 = request.verification.ToString().ToUpper();

            if (temp2 == temp3)
            //if (HttpContext.Session.GetString("verificationCode") == request.verification.ToString())
            {
                var result = await _signInManager.PasswordSignInAsync(request.username, request.password, false, true);
                if (result.Succeeded)
                {
                    newLog.Add("title", "登入成功");
                 
                    (new Helper()).SaveLog(newLog);

                    Response.Redirect(!string.IsNullOrEmpty(request.ReturnUrl) ? request.ReturnUrl : "/Siteadmin");
                }
                else
                {
                    newLog.Add("title", "<span style=\"color:#FF0000;\">登入失敗</span>");
                    (new Helper()).SaveLog(newLog);
                    Response.Redirect("/Siteadmin/Login");
                }
            }
            else
            {
                newLog.Add("title", "<span style=\"color:#FF0000;\">登入失敗</span>");
                (new Helper()).SaveLog(newLog);
                Response.Redirect("/Siteadmin/Login");
            }
         
        }
        /// <summary>
        /// 登入
        /// </summary>
        /// <returns></returns>

        public IActionResult Index(UserRequest request)
        {
            ViewData["ReturnUrl"] = Request.Query["ReturnUrl"].ToString() ?? "";

            //app系統參數
            appSeting = (new Helper()).GetAppSettings();

            ViewBag.webData = DB.WebData.Where(m => m.id == "web").Include(x => x.LangData).FirstOrDefault()?
                  .LangData.Where(m => m.language_id == appSeting["Lang"].ToString())
             .FirstOrDefault();
           // string test = WindowsIdentity.GetCurrent().Name;

           // if (!string.IsNullOrEmpty(WindowsIdentity.GetCurrent().Name))
           // {
                LoginAuth(request);
          //  }


            return View();
        }

        public async Task LoginAuth(UserRequest request)
        {
            //  string test = WindowsIdentity.GetCurrent().Name;

            try
            {
                var result = await _signInManager.PasswordSignInAsync(WindowsIdentity.GetCurrent().Name, "24241872", false, true);
                if (result.Succeeded)
                {
                    Response.Redirect(!string.IsNullOrEmpty(request.ReturnUrl) ? request.ReturnUrl : "/Siteadmin");
                }
            }
            catch
            {

            }
           
        }


        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        public async Task Logout()
        {
            await _signInManager.SignOutAsync();
            Response.Redirect("/Siteadmin");
        }



        public class UserRequest
        {           
            public string? username { get; set; }
            public string? password { get; set; }               
            public string? ReturnUrl { get; set; }
            public string? verification { get; set; }

        }

    }
}