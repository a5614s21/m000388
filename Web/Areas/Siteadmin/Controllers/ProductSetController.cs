﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Newtonsoft.Json;
using System.Collections.Specialized;
using System.Linq.Dynamic.Core;
using Web.Repositories.Admin;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]
    
    public class ProductSetController : HomeController
    {
        private readonly IRepository<ProductSet, LanguageResource, string> _repository;
        protected Dictionary<String, Object> dataTableColnum;

        public ProductSetController(IRepository<ProductSet, LanguageResource, string> repository)
        {
            _repository = repository;
            dataTableColnum = _repository.dataTableColnum();//取得欄位資訊
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult List(string id)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;

            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";

            //麵包屑
            if (!string.IsNullOrEmpty(id))
            {
                ViewBag.Data = _repository.FindById(id);
            }

            Dictionary<String, Object> dataTableSearch = _repository.dataTableSearch("product", adminNowLang.language_id);

            ViewBag.Search = dataTableSearch["search"];
            ViewBag.Find = dataTableSearch["find"];

            ViewBag.dataTableColnum = dataTableColnum;

            ViewBag.orderBy = _repository.orderBy();

            //取得排序鍵值
            ViewBag.orderBy = Helper.GetSortIndex(dataTableColnum, _repository.orderBy());

            //使用於最後一個若為"操作"，設定為不可排序
            ViewBag.LastCloseSortIndex = (dataTableColnum.ContainsKey("modify") ? "," + (dataTableColnum.Count - 1).ToString() : "");

            return View(!string.IsNullOrEmpty(id) ? "ListUri" : "List");
        }

        /// <summary>
        /// 新增修改
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public IActionResult Edit(string id, string uri)
        {
            //語系
            ViewBag.languages = languages;
            ViewBag.adminNowLang = adminNowLang;


            ViewBag.Id = !string.IsNullOrEmpty(id) ? id : "";
            ViewBag.Uri = !string.IsNullOrEmpty(uri) ? uri : "";

            //取得編輯欄位
            Dictionary<String, Object> Columns = Helper.editColumns("ProductSet", uri);

            ViewBag.Columns = Columns;           

            //取得資料
            if (id != "add")
            {
                var data = _repository.FindByIdLang(id, adminNowLang.language_id);
                if (data != null)
                {
                    //data.ArticleNews = null;
                    var json = JsonConvert.SerializeObject(data, Formatting.Indented);
                    var temp = Newtonsoft.Json.Linq.JArray.Parse("[" + json + "]");

                    ViewBag.Data = Helper.FormatViewData(Columns, temp[0]);
                }
            }

            return View();
        }


        public IActionResult Import()
        {

            ViewBag.ProductCategory = DB.ProductCategory.Where(m => m.active == true).Where(m => m.parent_id == "Product")
                  .Include(x => x.LangData.Where(l => l.language_id == adminNowLang.language_id)).OrderBy(m => m.sort).ToList();


            ViewBag.showResult = "N";

            if (HttpContext.Session.GetString("numOK") != null)
            {
                ViewBag.showResult = "Y";
                ViewBag.numOK = HttpContext.Session.GetString("numOK");
                ViewBag.numErr = HttpContext.Session.GetString("numErr");

                HttpContext.Session.Remove("numOK");
                HttpContext.Session.Remove("numErr");
            }

            return View();
        }

        public IActionResult Sample()
        {
            ViewBag.showResult = "N";

            if (HttpContext.Session.GetString("numOK") != null)
            {
                ViewBag.showResult = "Y";
                ViewBag.numOK = HttpContext.Session.GetString("numOK");
                ViewBag.numErr = HttpContext.Session.GetString("numErr");

                HttpContext.Session.Remove("numOK");
                HttpContext.Session.Remove("numErr");
            }

            return View();
        }

        /// <summary>
        /// 寫入與修改
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Save(FormRequest form)
        {

            string masterId = (new Helper()).GenerateIntID();

            ProductSet entity = new ProductSet()
            {
                id = masterId,
                category_id = form.category_id,
                application_id = form.application_id,
                active = form.active,
                feature = JsonConvert.SerializeObject(form.feature),
                product_tags = JsonConvert.SerializeObject(form.product_tags),
                safety = JsonConvert.SerializeObject(form.safety),
                series = JsonConvert.SerializeObject(form.series),
                uri = form.uri,
                sort = form.sort,
            };

            LanguageResource entityLang = new LanguageResource()
            {
                // id = (new Helper()).GenerateIntID(),
                model_type = "ProductSet",
                language_id = form.lang,
                title = form.title,
                details = JsonConvert.SerializeObject(form.details),
                options = JsonConvert.SerializeObject(form.options),
                seo = JsonConvert.SerializeObject(form.seo),
                //specs = JsonConvert.SerializeObject(form.specs),
                category_id = form.category_id,
                application_id=form.application_id,
                parent_id = form.parent_id,
                pic = form.pic,
                active = form.active,
                updated_at = DateTime.Now,
                sort = form.sort,
                //start_at = !string.IsNullOrEmpty(form.start_at) ? DateTime.Parse(form.start_at.ToString()) : null,
                //end_at = !string.IsNullOrEmpty(form.end_at) ? DateTime.Parse(form.end_at.ToString()) : null,
                created_at = DateTime.Parse(form.created_at.ToString()),
                feature = JsonConvert.SerializeObject(form.feature),
                product_tags = JsonConvert.SerializeObject(form.product_tags),
                safety = JsonConvert.SerializeObject(form.safety),
                series = JsonConvert.SerializeObject(form.series),
                files = form.files,
                tags = form.tags,
                top = form.top,
                ProductSetId = masterId,
            };



            Dictionary<string, string> newLog = new Dictionary<string, string> {
                { "ip" , GetIP() },
                { "uri" , "modify" },
                { "username" , ViewBag.User.NormalizedUserName }
            };


            if (form.id == "add")
            {
                _repository.Create(entity, entityLang);

                newLog.Add("act", "add");
                newLog.Add("title", "[ " + ViewBag.SystemMenuData.title + " - " + form.title + " ]");
            }
            else
            {
                masterId = form.id;
                entity.id = form.id;
                entityLang.ProductSetId = form.id;
                _repository.Update(entity, entityLang);

                newLog.Add("act", "edit");
                newLog.Add("title", "[ " + ViewBag.SystemMenuData.title + " - " + form.title + " ]");

                //移除規格
                DB.SpecData.RemoveRange( DB.SpecData.Where(m => m.ProductSetId == form.id) );
            }

              (new Helper()).SaveLog(newLog);




            string uriPath = (form.uri != null && !string.IsNullOrEmpty(form.uri.ToString()) ) ? "/" + form.uri.ToString() : "";


            Response.Redirect("/Siteadmin/"+ ControllerName + "/List");
        }

        /// <summary>
        /// 修改狀態
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task ChangeActive(IFormCollection form)
        {
            ProductSet entity = new ProductSet() { id = form["id"].ToString() };
            using (var db = new ApplicationDbContext())
            {
                entity.active = Boolean.Parse(form["active"]);
                db.ProductSet.Attach(entity);
                db.Entry(entity).Property(x => x.active).IsModified = true;
                db.SaveChanges();
            }


            var resources = _repository.FindListLang(form["id"].ToString(), adminNowLang.language_id);

            foreach (var item in resources)
            {
                LanguageResource entityLang = new LanguageResource() { id = item.id };
                using (var db = new ApplicationDbContext())
                {
                    entityLang.active = Boolean.Parse(form["active"]);
                    entityLang.updated_at = DateTime.Now;
                    db.LanguageResource.Attach(entityLang);
                    db.Entry(entityLang).Property(x => x.active).IsModified = true;
                    db.Entry(entityLang).Property(x => x.updated_at).IsModified = true;
                    db.SaveChanges();
                }
            }

        }

        /// <summary>
        /// 修改排序
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public async Task ChangeSort(IFormCollection form)
        {

            ProductSet entity = new ProductSet() { id = form["id"].ToString() };
            using (var db = new ApplicationDbContext())
            {
                entity.sort = int.Parse(form["sort"]);
                db.ProductSet.Attach(entity);
                db.Entry(entity).Property(x => x.sort).IsModified = true;
                db.SaveChanges();
            }

        }

        public async Task Delete(IFormCollection form)
        {
            List<string> idList = form["id"].ToString().Split(',').ToList();

            foreach (string id in idList)
            {
                string tempsubid = "";
                LanguageResource subid = DB.LanguageResource.Where(m => m.ProductSetId == id).FirstOrDefault();
                if (subid != null)
                {
                    tempsubid = subid.id;
                }


                //ArticleNews data = _repository.FindById(id);

                LanguageResource data = _repository.FindByIdLang(id, adminNowLang.language_id);

                Dictionary<string, string> newLog = new Dictionary<string, string> {
                    { "ip" , GetIP() },
                    { "uri" , "modify" },
                    { "username" , ViewBag.User.NormalizedUserName },
                    { "act" , "del" },
                    { "title" , "[ " + ViewBag.SystemMenuData.title + " - " + data.title + " ]" },
                };

                (new Helper()).SaveLog(newLog);

                _repository.Delete(id);


                List<LanguageResource> allsub = DB.LanguageResource.Where(m => m.id == tempsubid).ToList();
                foreach (LanguageResource deletesubid in allsub)
                {

                    DB.LanguageResource.Attach(deletesubid);
                    DB.Entry(deletesubid).State = EntityState.Deleted;

                    DB.SaveChanges();
                }
            }

        }



        /// <summary>
        /// DataTable
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        public string dataTable(string id , IFormCollection form)
        {

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ProductSet")
                .Where(l => l.language_id == adminNowLang.language_id).Select(m => m.ProductSetId).ToList();

            IQueryable<ProductSet> model = _repository.FindList(id ?? "")
                .Where(m => ids.Contains(m.id))
                 .Include(x => x.LangData.Where(l => l.language_id == adminNowLang.language_id));

            string orderByKey = form["columns[" + form["order[0][column]"] + "][data]"].ToString();
            string orderByType = form["order[0][dir]"].ToString();
            string orderBy = orderByKey + " " + orderByType.ToUpper();
            int skip = int.Parse(form["start"].ToString());
            int take = int.Parse(form["length"].ToString());

            Dictionary<String, Object> list = new Dictionary<String, Object>();



            //查詢
            if (!string.IsNullOrEmpty(form["searchJson"].ToString()))
            {
                Newtonsoft.Json.Linq.JToken sSearch = Newtonsoft.Json.Linq.JArray.Parse("[" + form["searchJson"].ToString() + "]");
                sSearch = sSearch[0];

                switch (sSearch["col"].ToString())
                {
                    case "active":

                        if (!string.IsNullOrEmpty(sSearch["keyword"].ToString()))
                        {
                            // model = model.Where(m => m.active == bool.Parse(sSearch["keyword"].ToString()));
                            model = model.Where(m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().active == bool.Parse(sSearch["keyword"].ToString()));

                        }


                        break;
                    case "category_id":

                        if (!string.IsNullOrEmpty(sSearch["keyword"].ToString()))
                        {
                            model = model.Where(m => m.category_id == sSearch["keyword"].ToString());
                        }


                        break;
                    default:

                        if (sSearch["col"].ToString() == "all")
                        {
                            //model = model.Where("LangData.title.Contains(@0) or LangData.details.Contains(@0)", sSearch["keyword"].ToString());

                            model = model.Where(

                                m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().title.Contains(sSearch["keyword"].ToString())

                                ||
                                m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().details.Contains(sSearch["keyword"].ToString())
                                );
                        }
                        else
                        {
                            if (sSearch["col"].ToString() == "title")
                            {
                                model = model.Where(
                                    m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().title.Contains(sSearch["keyword"].ToString())
                                    );

                            }

                            if (sSearch["col"].ToString() == "details")
                            {
                                model = model.Where(
                                    m => m.LangData.Where(m => m.language_id == adminNowLang.language_id).FirstOrDefault().details.Contains(sSearch["keyword"].ToString())
                                    );

                            }


                            //model = model.Where("LangData."+sSearch["col"].ToString() + ".Contains(@0)", sSearch["keyword"].ToString());
                        }
                        break;
                }

            }

            list.Add("iTotalDisplayRecords", (new DataTable()).GetTotalNum(model.ToList()));

            List<string> langOrderByKey = new List<string> {
             "title" , "created_at",
            };

            dynamic output = null;

            if (langOrderByKey.IndexOf(orderByKey) != -1)
            {
                switch (orderByKey)
                {
                    case "title":
                        if (orderByType == "asc")
                        {
                            output = model.OrderBy(m => m.LangData.FirstOrDefault().title).ToList().Skip(skip).Take(take);
                        }
                        else
                        {
                            output = model.OrderByDescending(m => m.LangData.FirstOrDefault().title).ToList().Skip(skip).Take(take);
                        }



                        break;
                    case "created_at":

                        if (orderByType == "asc")
                        {
                            output = model.OrderBy(m => m.LangData.FirstOrDefault().created_at).ToList().Skip(skip).Take(take);
                        }
                        else
                        {
                            output = model.OrderByDescending(m => m.LangData.FirstOrDefault().created_at).ToList().Skip(skip).Take(take);
                        }

                        break;
                }

            }
            else
            {
                output = model.OrderBy(orderBy).ToList().Skip(skip).Take(take);
            }


            //整理輸出資料
            var data = (new DataTable()).columns(JsonConvert.SerializeObject(output), ControllerName, _repository, uri, id, adminNowLang.language_id);

            list.Add("data", data);

            return JsonConvert.SerializeObject(list, Formatting.Indented);
        }

        /// <summary>
        /// 取得篩選資訊
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [HttpPost]
        public dynamic GetSpec(IFormCollection form)
        {
            string id = form["ProductSpecCategoryId"].ToString();

            var model = DB.ProductCategory.Where(m => m.id == id).FirstOrDefault();

            List<object> data = new List<object>();
            

            if(!string.IsNullOrEmpty(model.spec))
            {
               var specs = Newtonsoft.Json.Linq.JArray.Parse(model.spec);

                foreach(string specCategoryId in specs)
                {
                    var productSpecCategory = DB.ProductSpecCategory.Where(m => m.id == specCategoryId)
                        .Include(x =>x.LangData.Where(m => m.language_id == "en"))
                        .FirstOrDefault();

                    SpecModel specModel = new SpecModel();

                    specModel.id = productSpecCategory.id;
                    specModel.title = productSpecCategory.LangData[0].title;
                    specModel.input_type = productSpecCategory.LangData[0].input_type;
                    specModel.ProductSpec = DB.ProductSpec.Where(m => m.category_id == productSpecCategory.id).Include(x => x.LangData.Where(m => m.language_id == "en")).ToList();

                    //取得相關資料
                    if(form["ProductSetId"].ToString() != "add")
                    {
                        SpecData specData =  DB.SpecData.Where(m => m.ProductSpecCategoryId == specCategoryId).Where(m => m.ProductSetId == form["ProductSetId"].ToString()).FirstOrDefault();
                    
                        if(specData != null)
                        {
                            if (!string.IsNullOrEmpty(specData.ProductSpecId))
                            {
                                specModel.val = specData.ProductSpecId;
                            }
                            else
                            {
                                specModel.val = specData.spec_val;
                            }
                        }
                        else
                        {
                            specModel.val = "";
                        }

                    }
                    else
                    {
                        specModel.val = "";
                    }


                    data.Add(specModel);
                }


             
            }



            return data;

        }



    }
}