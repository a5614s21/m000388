﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Filters;
using WebApp.Services;
using Web.Repositories.Admin;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Identity;
using Google.Apis.AnalyticsReporting.v4.Data;
using System.Security.Principal;

namespace Web.Controllers.Siteadmin
{

    [Area("Siteadmin")]
    public class HomeController : Controller
    {

        protected ApplicationDbContext DB = new ApplicationDbContext();
        protected Helper Helper = new Helper();
        protected string ControllerName = "Home";
        protected Uri uri = null;
        //protected string ipAddress = "";
        protected List<Languages> languages = new List<Languages>();
        protected Languages adminNowLang = new Languages();
        private dynamic appSeting;


        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string IpAddress = HttpContext.Connection.RemoteIpAddress.ToString();//取得IP

            Dictionary<string, bool> FirewallCheck = (new Helper()).FirewallCheck(IpAddress, "admin");

            if (!FirewallCheck["ruleTrue"] && FirewallCheck["ruleFalse"])
            {
                Response.Redirect("/Siteadmin/Login");
            }

            ViewBag.appSeting = (new Helper()).GetAppSettings();

            //語系
            languages = DB.Languages.Where(m => m.active == true).OrderBy(m => m.sort).ToList();

            ViewBag.languages = languages;

            if (HttpContext.Session.GetString("adminNowLang") == null)
            {
                HttpContext.Session.SetString("adminNowLang", languages[0].language_id);
            }

            adminNowLang = languages.Where(m => m.language_id == HttpContext.Session.GetString("adminNowLang")).FirstOrDefault();
            ViewBag.adminnowlang = adminNowLang;


            //目前Controller名稱
            ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() ?? "Home";
            string action = this.ControllerContext.RouteData.Values.ContainsKey("action") ? this.ControllerContext.RouteData.Values["action"].ToString() : "";
            SystemMenu systemMenu = DB.SystemMenu.Where(m => m.model == ControllerName).FirstOrDefault();

            ViewBag.NowUrl = "";

            if (action == "List")
            {
                ViewBag.NowUrl = this.ControllerContext.RouteData.Values.ContainsKey("id") ? this.ControllerContext.RouteData.Values["id"].ToString() : "";
            }
            if (action == "Edit")
            {
                ViewBag.NowUrl = this.ControllerContext.RouteData.Values.ContainsKey("uri") ? this.ControllerContext.RouteData.Values["uri"].ToString() : "";
            }

            if (!string.IsNullOrEmpty(ViewBag.NowUrl))
            {
                string uri = ViewBag.NowUrl;

                if (DB.SystemMenu.Where(m => m.model == ControllerName).Count() > 1)
                {
                    systemMenu = DB.SystemMenu.Where(m => m.model == ControllerName).Where(m => m.uri == uri).FirstOrDefault();
                }

            }


            //選單名稱


            SystemMenu prevSystemMenu = DB.SystemMenu.Where(m => m.model == ControllerName).FirstOrDefault();
            if (systemMenu != null)
            {
                prevSystemMenu = DB.SystemMenu.Where(m => m.id == systemMenu.parent_id).FirstOrDefault();
            }
           

            string absoluteUrl = UriHelper.BuildAbsolute(Request.Scheme, Request.Host);
            uri = new Uri(absoluteUrl);


            //ViewBag
            ViewBag.SystemMenu = Helper.menu("");
            ViewBag.SystemMenuData = systemMenu;
            ViewBag.SystemMenuDataOptions = null;
            if (systemMenu != null && !string.IsNullOrEmpty(systemMenu.options.ToString()))
            {
                ViewBag.SystemMenuDataOptions = Newtonsoft.Json.Linq.JArray.Parse("[" + systemMenu.options.ToString() + "]");
            }


            ViewBag.PrevSystemMenuData = prevSystemMenu;
            ViewBag.ControllerName = ControllerName;
            ViewBag.Id = "";
            ViewBag.Uri = "";
            ViewBag.Data = null;

            ViewBag.Search = null;
            ViewBag.Find = null;
            ViewBag.RootPath = uri.Authority;
            ViewBag.BreadCrumbs = false;//顯示階層麵包屑
            ViewBag.LastCloseSortIndex = "";//使用於最後一個若為"操作"，設定為不可排序




            System.Security.Claims.ClaimsPrincipal currentUser = this.User;

            string userID = currentUser.Claims.FirstOrDefault().Value;

            IdentityUser user = DB.IdentityUser.Where(m => m.Id == userID).FirstOrDefault();
            ViewBag.UserId = userID;
            ViewBag.User = user;

            //所屬群組
            ViewBag.NowRoleId = DB.IdentityUserRole.Where(m => m.UserId == userID).FirstOrDefault().RoleId;

            //網站資訊
            ViewBag.webData = DB.WebData.Where(m => m.id == "web").Include(x => x.LangData).FirstOrDefault()?
               .LangData.Where(m => m.language_id == adminNowLang.language_id.ToString())
               .FirstOrDefault();

            // ViewBag.username = WindowsIdentity.GetCurrent().Name;


            //可匯入
            ViewBag.CanImport = new List<string>
            {
                "news" , "finance", "quarterly", "filings", "consolidated", "reports" , "annual",
                //"news" , "finance", "quarterly", "filings", "reports" , "annual",
            };

            ViewBag.CanImportModel = new List<string>
            {
                "ArticleNews" , "ArticleDownload",
            };


        }
        [Authorize]

        public IActionResult Index()
        {
            string username = ViewBag.User.NormalizedUserName;
            //上次登入
            List<SystemLog> logins = DB.SystemLog.Where(m => m.uri == "login").Where(m => m.act == "login").Where(m => m.username == username).OrderBy(m => m.created_at).ToList();
            ViewBag.LoginLog = null;
            if (logins.Count > 1)
            {
                ViewBag.LoginLog = logins[1];
            }

            //News
            ViewBag.news = DB.ArticleNews.Include(m => m.LangData).FirstOrDefault()?.LangData.OrderBy(m => m.created_at).Skip(0).Take(4).ToList();


            return View();
        }

        /// <summary>
        /// 設定目前語系
        /// </summary>
        /// <param name="id"></param>
        [Authorize]
        public void SetLang(string id)
        {
            string previousURL = Request.Headers["Referer"].ToString();//上一頁

            HttpContext.Session.SetString("adminNowLang", id);

            Response.Redirect(previousURL);
        }

        public string GetIP()
        {
            string remoteIpAddress = HttpContext.Connection.RemoteIpAddress.MapToIPv4().ToString();
            if (Request.Headers.ContainsKey("X-Forwarded-For"))
            {
                remoteIpAddress = Request.Headers["X-Forwarded-For"];
            }

            return remoteIpAddress;
        }
    }
}