﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.AspNetCore.Mvc.Localization;
using Microsoft.Extensions.Localization;
using System.Reflection;
using Web.Services;
using System.Text.Json;
using Org.BouncyCastle.Asn1.Ocsp;

namespace WebApp.Controllers
{

    public class ContactController : BaseController
    {

        public ContactController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }



        [Route("Contact")]
        [Route("{culture}/Contact")]
        public async Task<IActionResult> Contact(string tag = "")
        {
            ViewBag.tag = tag;
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("ContactUs");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "SiteParameterItem")
                .Where(l => l.language_id == nowLang).Select(m => m.SiteParameterItemId).ToList();



            ViewBag.siteItem = DB.SiteParameterItem
                .Where(m => ids.Contains(m.id))
                        .Where(m => m.active == true)
                        .OrderBy(m => m.sort)
                        .Include(x => x.LangData.Where(l => l.language_id == nowLang)).ToList();


            ArticlePage data = await (new ArticlePageRepository()).FindArticlePage("Contact", nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            return View();

        }


        [Route("ContactSend")]
        [Route("{culture}/ContactSend")]
        public async Task<IActionResult> ContactSend()
        {

            ViewData["Title"] = _localizer.GetLocalizedHtmlString("ContactUs");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");



            return View();

        }

        [Route("Contact/Send")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task Send(ContactRequest request)
        {
            string temp1 = HttpContext.Session.GetString("verificationCode");
            string temp2 = temp1.ToUpper();
            string temp3 = request.verification.ToString().ToUpper();

            if(temp2 == temp3)
            //if (HttpContext.Session.GetString("verificationCode") == request.verification.ToString())
            {
                CipherService cipherService = new CipherService();


                Dictionary<string, string> contact = request.contact;

                contact["tel"] = cipherService.Encrypt(contact["tel"]);
                contact["re_content"] = "";

                Contact entity = new Contact()
                {
                    id = Guid.NewGuid().ToString(),
                    uri = "Contact",
                    lang = request.lang,
                    name = cipherService.Encrypt(request.name),
                    contact = JsonConvert.SerializeObject(contact),
                    email = cipherService.Encrypt(request.email),
                    options = JsonConvert.SerializeObject(request.options),
                    active = false,
                    send = false,
                    updated_at = DateTime.Now,
                    created_at = DateTime.Now,
                };
                DB.Contact.Add(entity);
                DB.SaveChanges();

                List<string> mailList = new List<string>();

                List<string> ProductCategoryText = new List<string>();

                List<SiteParameterItem> SiteParameterItem = DB.SiteParameterItem.Where(m => m.category_id == "ProductCategory")
                    .Include(x => x.LangData.Where(l => l.language_id == request.lang))
                    .ToList();

                foreach (string id in request.options["ProductCategory"])
                {

                    SiteParameterItem category = SiteParameterItem.Where(m=>m.id == id).FirstOrDefault();

                    LanguageResource LangData = category.LangData?.FirstOrDefault();


                    if (!string.IsNullOrEmpty(LangData.DetailsData?.email))
                    {
                        mailList.Add(LangData.DetailsData?.email);                    
                    }

                    ProductCategoryText.Add(LangData?.title);
                }

                request.options["ProductCategory"] = ProductCategoryText;


                //發信
                request.contact["tel"] = cipherService.Decrypt(request.contact["tel"].ToString());

                (new Helper()).SendMail("contact", request, appSeting["APP_FULL_URL"].ToString(), request.lang, mailList);

                HttpContext.Session.SetString("Message", "Y");

                if (request.lang != "en")
                {
                    Response.Redirect(Url.Content("~/" + request.lang) + "/ContactSend");
                }
                else
                {
                    Response.Redirect(Url.Content("~/") + "ContactSend");
                }
               
            }
            else
            {
                HttpContext.Session.SetString("ContactData", JsonConvert.SerializeObject(request));

                HttpContext.Session.SetString("verificationError", "Y");
                Response.Redirect(ViewBag.culturePath + "Contact");
            }
        }



        [Route("Contact/SendQuestionnaire")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task SendQuestionnaire(IFormCollection form)
        {
            
                CipherService cipherService = new CipherService();

            string json = form["sendVal"].ToString();

                Contact entity = new Contact()
                {
                    id = Guid.NewGuid().ToString(),
                    uri = "Questionnaire",
                    lang = form["lang"],
                    //name = cipherService.Encrypt(request.name),
                    contact = form["sendVal"].ToString(),
                    // email = cipherService.Encrypt(request.email),
                   // options = JsonConvert.SerializeObject(request.options),
                    active = false,
                    send = false,
                    updated_at = DateTime.Now,
                    created_at = DateTime.Now,
                };
                DB.Contact.Add(entity);
                DB.SaveChanges();

                List<string> mailList = new List<string>();


                var serializerOptions = new System.Text.Json.JsonSerializerOptions
                {
                    Converters = { new DynamicJsonConverter() },
                    WriteIndented = true
                };

                dynamic obj = System.Text.Json.JsonSerializer.Deserialize<dynamic>(form["sendVal"].ToString(), serializerOptions);

            //發信

            //分類
            List<QuestionnaireGroup> questionnaireGroup = DB.QuestionnaireGroup.Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();

            string sendVal = "";
            foreach (var data in (IDictionary<String, Object>)obj)
            {
                // test = property.Key + ": " + property.Value;
                // Console.WriteLine(property.Key + ": " + property.Value);

                QuestionnaireItem questionnaireItem = DB.QuestionnaireItem.Where(m => m.spec == data.Key)
                    .Include(x => x.LangData.Where(l => l.language_id == nowLang)).FirstOrDefault();

                if(questionnaireItem != null)
                {
                    LanguageResource LangData = questionnaireItem.LangData.FirstOrDefault();
                    string showVal = "";

                    FormModel model = JsonConvert.DeserializeObject<FormModel>(LangData.options);

                    if (model.type == "checkbox")
                    {

                        foreach (var propertyInfo in (IList<object>)data.Value)
                        {
                            // do stuff here
                            showVal += propertyInfo + "<br>";

                        }

                    }
                    else
                    {
                        showVal = data.Value.ToString();
                    }

                    sendVal += "<tr><td style =\"padding: 5px; border: 1px solid #dedcdc; background-color: #fafaf8;\" align=\"left\">" + LangData?.title + "</td><td style =\"padding: 5px; border: 1px solid #dedcdc;\" align=\"left\">" + showVal + "</td> </tr>";


                }

                if (data.Key == "others")
                {
                    sendVal += "<tr><td style =\"padding: 5px; border: 1px solid #dedcdc; background-color: #fafaf8;\" align=\"left\">" + "others" + "</td><td style =\"padding: 5px; border: 1px solid #dedcdc;\" align=\"left\">" + data.Value.ToString() + "</td> </tr>";
                }


            }
            Dictionary<string, string> sendData = new Dictionary<string, string>();

            sendData.Add("val", sendVal);

            (new Helper()).SendMail("Questionnaire", sendData, appSeting["APP_FULL_URL"].ToString(), form["lang"], mailList);

                    HttpContext.Session.SetString("Message", "Y");

            //Response.Redirect(ViewBag.culturePath + "CSRQuestionnaireSend");

            if (form["lang"] != "en")
            {
                Response.Redirect(Url.Content("~/" + form["lang"]) + "/CSRQuestionnaireSend");
            }
            else
            {
                Response.Redirect(Url.Content("~/") + "CSRQuestionnaireSend");
            }

        }


        [Route("CheckVerification")]
        [HttpPost]
        public async Task<string> CheckVerification(IFormCollection form)
        {
            string temp1 = HttpContext.Session.GetString("verificationCode");
            string temp2 = temp1.ToUpper();
            string temp3 = form["verification"].ToString().ToUpper();

            if(temp2 != temp3)
            //if (HttpContext.Session.GetString("verificationCode") != form["verification"].ToString())
            {
                return "verificationError";
            }
            else
            {
                return "OK";
            }
        }


        public class ContactRequest
        {
            public string? name { get; set; }
            public Dictionary<string, string>? contact { get; set; }

            public Dictionary<string, List<string>>? options { get; set; }
            public string? verification { get; set; }
            public string? email { get; set; }

            public string? lang { get; set; }

        }
    }
}