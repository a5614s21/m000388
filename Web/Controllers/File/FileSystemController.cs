﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using elFinder.NetCore.Drivers.FileSystem;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Mvc;
using WebApp.Services;

namespace elFinder.NetCore.Web.Controllers
{
    [Route("el-finder/file-system")]
    public class FileSystemController : Controller
    {
        [Route("connector")]
        public async Task<IActionResult> Connector()
        {
            var connector = GetConnector();
            return await connector.ProcessAsync(Request);
        }


        [Route("addFile")]
        [HttpPost]
        public async Task<string> AddFile(IFormCollection form)
        {
         
            List<string> files =  form["paths"].ToString().Split(',').ToList();

            var path =  Path.GetFullPath("wwwroot") + "\\";

            List<string> imageExt = new List<string>()
            {
                ".jpg" , ".jpeg" , ".png" , ".bmp",".gif",
            };

            List<string> delFile = new List<string>();


            if(files != null && files.Count > 0)
            {
                foreach (string file in files)
                {
                    if (!string.IsNullOrEmpty(file))
                    {
                        string newFile = file.Replace("\\", "");

                        string ext = Path.GetExtension(path + newFile);

                        if (imageExt.IndexOf(ext.ToLower()) != -1)
                        {
                            System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(path + newFile);

                            string fileName = Path.GetFileNameWithoutExtension(path + newFile);

                            Imazen.WebP.SimpleEncoder encoder = new Imazen.WebP.SimpleEncoder();

                            string floder = newFile.Replace(fileName + ext, "");

                            using (var outStream = new System.IO.MemoryStream())
                            {
                                encoder.Encode(bmp, outStream, -1);
                                outStream.Close();

                                var webpBytes = outStream.ToArray();
                                System.IO.File.WriteAllBytes(path + floder + newFile, webpBytes);

                                //System.IO.File.WriteAllBytes(path + floder + fileName + ".webp", webpBytes);
                                //System.IO.File.Delete(path + newFile);
                                // delFile.Add(path + newFile);
                            }
                        }

                    }

                }
            }
          
          

          /*  if(delFile.Count > 0)
            {
                foreach(string file in delFile)
                {

                    using (FileStream fs =  new FileStream(file, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
                    {
                        System.IO.File.Delete(file);
                    }

                      
                }
            }*/

            return "";
        }

        [Route("thumb/{hash}")]
        public async Task<IActionResult> Thumbs(string hash)
        {
            var connector = GetConnector();
            return await connector.GetThumbnailAsync(HttpContext.Request, HttpContext.Response, hash);
        }

        private Connector GetConnector()
        {
            var driver = new FileSystemDriver();
            dynamic appSeting = (new Helper()).GetAppSettings();

            string absoluteUrl = UriHelper.BuildAbsolute(Request.Scheme, Request.Host);
            var uri = new Uri(absoluteUrl);

            //string path = Path.GetFullPath("wwwroot/Files");

            var root = new RootVolume(
                Path.GetFullPath("wwwroot/Files"),
                $"{appSeting["APP_FULL_URL"].ToString()}/Files/",
                $"{appSeting["APP_FULL_URL"].ToString()}/el-finder/file-system/thumb/")
            {
                //IsReadOnly = !User.IsInRole("Administrators")
                IsReadOnly = false, // Can be readonly according to user's membership permission
                IsLocked = false, // If locked, files and directories cannot be deleted, renamed or moved
                Alias = "Files", // Beautiful name given to the root/home folder
                                 //MaxUploadSizeInKb = 2048, // Limit imposed to user uploaded file <= 2048 KB
                                 //LockedFolders = new List<string>(new string[] { "Folder1" })
                MaxUploadSizeInMb = 30,
                
               
            };

            

            driver.AddRoot(root);

            return new Connector(driver)
            {
                // This allows support for the "onlyMimes" option on the client.
                MimeDetect = MimeDetectOption.Internal
            };

        }

       

    }



}