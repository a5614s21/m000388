﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Localization.Routing;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApplicationModels;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Globalization;
using WebApp.Data;
using WebApp.Models;
using WebApp.Services;
using System.Text;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json.Linq;
using Web.Repositories.Web;

namespace WebApp.Controllers
{
    public class BaseController : Controller
    {

        protected ApplicationDbContext DB = new ApplicationDbContext();
        protected Helper helper = new Helper();
        protected Uri uri = null;
        protected dynamic appSeting = null;
        protected string nowLang = "";
        protected LocalService _localizer;
        protected string routerId;
        //
        public override void OnActionExecuting(ActionExecutingContext context)
        {

            ViewBag.languages = DB.Languages.Where(m => m.active == true).OrderBy(m => m.sort).ToList();


            try
            {


                //app系統參數
                appSeting = (new Helper()).GetAppSettings();


                ViewBag.culture = context.RouteData.Values["culture"]?.ToString();//語系連結用
                if (ViewBag.culture == null)
                {
                    if (HttpContext.Session.GetString("turnlang") != null)
                    {
                        //目前語系
                        nowLang = HttpContext.Session.GetString("turnlang").ToString();
                    }
                    else
                    {
                        nowLang = context.RouteData.Values["culture"]?.ToString() ?? appSeting["Lang"].ToString();
                    }
                }
                else
                {
                    //if (ViewBag.culture == "zh-TWIC_PackagingSearch")
                    //{
                    //    //目前語系
                    //    nowLang = "zh-TW";
                    //    ViewBag.culture = "zh-TW/IC_PackagingSearch";
                    //}
                    //else if (ViewBag.culture == "zh-CNIC_PackagingSearch")
                    //{
                    //    //目前語系
                    //    nowLang = "zh-CN";
                    //    ViewBag.culture = "zh-CN/IC_PackagingSearch";
                    //}
                    //else
                    //{
                    //    //目前語系
                    //    nowLang = context.RouteData.Values["culture"]?.ToString() ?? appSeting["Lang"].ToString();
                    //}
                    //目前語系
                    nowLang = context.RouteData.Values["culture"]?.ToString() ?? appSeting["Lang"].ToString();
                }


                context.ActionArguments["culture"] = nowLang;

                //網址前綴
                ViewBag.culturePath = (nowLang == "en") ? appSeting["APP_FULL_URL"].ToString() + "/" : appSeting["APP_FULL_URL"].ToString() + "/" + nowLang + "/";

                ViewBag.currentyear = DateTime.Now.Year;

                routerId = context.RouteData.Values["id"]?.ToString() ?? "";

                //當頁網址
                ViewBag.NowPath = (nowLang == "en") ? appSeting["APP_FULL_URL"].ToString() + "/" : appSeting["APP_FULL_URL"].ToString() + "/" + nowLang + "/" + context.RouteData.Values["controller"]?.ToString() + "/" + context.RouteData.Values["id"]?.ToString();


                var cultureInfo = CultureInfo.GetCultureInfo(nowLang);

                Thread.CurrentThread.CurrentCulture = cultureInfo;
                Thread.CurrentThread.CurrentUICulture = cultureInfo;




                var webData = DB.WebData.Where(m => m.id == "web").Include(x => x.LangData).FirstOrDefault()?
                    .LangData.Where(m => m.language_id == nowLang)
               .FirstOrDefault();


                ViewBag.nowLang = nowLang;//目前語系



                ViewBag.WebData = webData;


                HttpContext.Session.Remove("turnlang");

                /*
                ViewBag.WebDataContact = Newtonsoft.Json.Linq.JArray.Parse("[" + ViewBag.WebData.contact.ToString() + "]");
                ViewBag.WebDataEmail = ViewBag.WebData.email.ToString();
                */

                ViewBag.AllProductCategoryCategory = DB.ProductCategory.Where(m => m.active == true)
                  //.Where(m => m.uri == "ICPackaging")
                  .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();

                ViewBag.ICPackagingCategory = DB.ProductCategory.Where(m => m.active == true)
                    .Where(m => m.uri == "ICPackaging")
                     .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();


                ViewBag.ServiceCategory = DB.ProductCategory.Where(m => m.active == true)
                   .Where(m => m.uri == "Service")
                    .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();



                ViewBag.TechnologyCategory = DB.ProductCategory.Where(m => m.active == true)
                   .Where(m => m.uri == "Technology")
                    .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();


                ViewBag.Id = "";

                ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

                string absoluteUrl = UriHelper.BuildAbsolute(Request.Scheme, Request.Host);
                uri = new Uri(absoluteUrl);

                ViewBag.NowUrl = uri.AbsoluteUri;

                ViewBag.Abouts = DB.ArticlePage.Where(m => m.active == true).Where(m => m.category_id == "about").OrderBy(m => m.sort).ToList();

                ViewBag.NewsCategory = DB.ArticleCategory.Where(m => m.active == true).Where(m => m.parent_id == "news").OrderBy(m => m.sort).ToList();

                ViewBag.Path = "";

                ViewBag.FullUrl = GetCompleteUrl();//完整網址
                ViewBag.LangUrl = LangUrl(ViewBag.languages, appSeting["APP_FULL_URL"].ToString());//所有語系網址



                //檔案下載用
                /*   ViewBag.filesType = DB.ProductSpec.Where(x => x.active == true)
                              .Where(x => x.uri == "files")
                              .Include(x => x.LangData.Where(l => l.language_id == nowLang))
                              .OrderBy(m => m.sort)
                              .ToList();
                */

                List<Advertising> footerAdv = DB.Advertising.Where(m => m.active == true).Where(m => m.uri == "footer").OrderBy(m => m.sort).ToList();

                ViewBag.footer1 = null;
                ViewBag.footer2 = null;


                //目前Controller名稱
                string ControllerName = this.ControllerContext.RouteData.Values["controller"].ToString() ?? "Home";

                Dictionary<string, string> HeaderAvtive = new Dictionary<string, string> {
                {"product" , ( ControllerName == "Product" ? "active" : "" ) },
                {"application" , ( ControllerName == "Application" ? "active" : "" ) },
                {"search" , ( ControllerName == "search" ? "active" : "" ) },
                {"technical" , ( ControllerName == "technical" ? "active" : "" ) },
                {"partners" , ( ControllerName == "partners" ? "active" : "" ) },
                {"history" , ( ControllerName == "history" ? "active" : "" ) },
                {"contact" , ( ControllerName == "contact" ? "active" : "" ) },
            };

                ViewBag.HeaderAvtive = HeaderAvtive;

                ViewBag.isLogin = false;

                if (HttpContext.Session.GetString("Member") != null)
                {
                    ViewBag.isLogin = true;
                }


                ViewBag.certificationCategory = (new ArticlePageRepository()).FindArticleCategories("certification", nowLang);

                string cookieValueFromReq = Request.Cookies["GDPR"];
                ViewBag.Message = cookieValueFromReq;

                var tempaction = context.RouteData.Values["action"]?.ToString();
                FormCollection temparg = (FormCollection)context.ActionArguments["form"];
                string tempargid = temparg["id"];

                ViewBag.tempaction = context.ActionArguments["form"]?.ToString();

                if (tempaction == "GetQuarterlyMedia")
                {
                    ViewBag.requarterlyitem = SetQuarterlyMedia(tempargid);
                }
                //BinaryFormatter bf = new BinaryFormatter();
                //MemoryStream ms = new MemoryStream();
                //bf.Serialize(ms, media);

                HttpContext.Session.Set("requarterlyitem", ViewBag.requarterlyitem);
                base.OnActionExecuting(context);
            }
            catch
            {

            }

        }

        private Dictionary<string, object> SetQuarterlyMedia(string temparg)
        {
            string id = temparg;

            ArticleDownload model = DB.ArticleDownload
        .Where(m => m.path == id || m.id == id)
        .Include(x => x.LangData.Where(l => l.language_id == nowLang))
        .FirstOrDefault();

            LanguageResource LangData = model.LangData.FirstOrDefault();




            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("title", LangData?.title);


            List<MediaModel> media = new List<MediaModel>();

            if (LangData?.VideoData != null)
            {

                foreach (VideoModel item in LangData?.VideoData)
                {
                    if (!string.IsNullOrEmpty(item.path))
                    {

                        string type = "youtube";

                        if (!item.path.Contains("youtube"))
                        {
                            type = "other";
                        }

                        media.Add(new MediaModel
                        {
                            path = item.path,
                            title = item.title,
                            sub_title = item.sub_title,
                            type = type,
                        });
                    }


                }
            }

            data.Add("video", media);

            media = new List<MediaModel>();

            if (LangData?.FilesData != null && LangData?.FilesData.file != null)
            {
                foreach (FileDetailModel item in LangData?.FilesData.file)
                {
                    if (!string.IsNullOrEmpty(item.path))
                    {

                        media.Add(new MediaModel
                        {
                            path = item.path,
                            title = item.title,
                            sub_title = "",
                            type = "",
                        });
                    }
                }

            }

            data.Add("files", media);
            //BinaryFormatter bf = new BinaryFormatter();
            //MemoryStream ms = new MemoryStream();
            //bf.Serialize(ms, media);

            //HttpContext.Session.Set("requarterlyitem", ms.ToArray());


            return data;
        }


        private string GetCompleteUrl()
        {
            return new StringBuilder()
                 //.Append(HttpContext.Request.Scheme)
                 .Append("https")
                 .Append("://")
                 .Append(HttpContext.Request.Host)
                 .Append(HttpContext.Request.PathBase)
                 .Append(HttpContext.Request.Path)
                 //.Append(HttpContext.Request.QueryString)
                 .ToString();
        }

        /// <summary>
        /// 多語系各頁網址
        /// </summary>
        /// <param name="languages"></param>
        /// <returns></returns>
        private Dictionary<string, string> LangUrl(List<Languages> languages, string AppFullUrl)
        {
            Dictionary<string, string> urls = new Dictionary<string, string>();

            string top = "http";
            if (AppFullUrl.Contains("https"))
            {
                top = "https";
            }


            foreach (Languages lang in languages)
            {

                string setUrl = new StringBuilder()
                 .Append(top)
                 .Append("://")
                 .Append(HttpContext.Request.Host).ToString();

                if (lang.language_id != "en")
                {
                    setUrl += "/" + lang.language_id;
                }
                setUrl += new StringBuilder()
                    .Append(HttpContext.Request.PathBase)
                    .ToString();

                string path = new StringBuilder()
                                .Append(HttpContext.Request.Path)
                                .ToString();
                if (path == "/zh-CN" || path == "/zh-TW")
                {
                    path = path + "/";
                }



                foreach (Languages subLang in languages)
                {
                    path = path.Replace(subLang.language_id + "/", "");
                }

                setUrl = setUrl + path;

                urls.Add(lang.language_id, setUrl);
            }

            return urls;
        }



    }
}
