﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class ServiceController : BaseController
    {
        ProductCategoryRepository _repository = new ProductCategoryRepository();

        public ServiceController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }


        [Route("Service")]
        [Route("{culture}/Service")]

        public async Task<IActionResult> Service()
        {

            ViewBag.category = _repository.FindProductCategory("Service", nowLang);

            ViewData["Title"] = ViewBag.category.LangData[0].title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");



            return View();

        }

        [Route("ServiceSecond/{id}")]
        [Route("{culture}/ServiceSecond/{id}")]
        public async Task<IActionResult> ServiceSecond(string id)
        {

            ViewBag.category = _repository.FindProductCategory(id, nowLang);

            if (ViewBag.category.LangData.Count == 0)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("Service");
            }
            else
            {
                ViewData["Title"] = ViewBag.category.LangData[0].title;

                ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");
            }


            return View();

        }

        [Route("ServiceThird/{id}")]
        [Route("{culture}/ServiceThird/{id}")]
        public async Task<IActionResult> ServiceThird(string id)
        {

            ViewBag.category = _repository.FindProductCategory(id, nowLang);

            ViewBag.topCategory = _repository.FindProductCategory(ViewBag.category.parent_id, nowLang);

            if (ViewBag.category.LangData.Count == 0)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("Service");
            }
            else
            {
                ViewData["Title"] = ViewBag.category.LangData[0].title;

                ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");
            }


            return View();

        }


        [Route("ServiceData/{id}")]
        [Route("{culture}/ServiceData/{id}")]

        public async Task<IActionResult> ServiceData(string id)
        {
            var data = (from a in DB.LanguageResource
                        where a.ProductCategoryId == id
                        select a).FirstOrDefault();
            if (data != null)
            {
                var preid = (from a in DB.ProductCategory
                             where a.id == data.ProductCategoryId
                             select a).FirstOrDefault();
                if (preid.parent_id != "Service")
                {
                    var prepreid = (from a in DB.ProductCategory
                                 where a.id == preid.parent_id
                                 select a).FirstOrDefault();
                    if (prepreid.parent_id != "Service")
                    {
                        ViewBag.showType = "serviceproduct";
                    }
                    else
                    {
                        ViewBag.showType = "secondproduct";
                    }
                }
                else
                {
                    ViewBag.showType = "category";
                }
            }
            else
            {
                ViewBag.showType = "category";
            }

            LanguageResource LangData = new LanguageResource();

            var categoryData = _repository.FindProductCategory(id, nowLang);

            LangData = categoryData.LangData.FirstOrDefault();

            if (LangData == null)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("Service");
            }

            ProductCategory category = _repository.FindProductCategory(LangData.parent_id, nowLang);
            ViewBag.categoryLangData = category.LangData.FirstOrDefault();


            ViewBag.Breadcrumbs = (new Helper()).GetBreadcrumbs(categoryData, ViewBag.ServiceCategory);


            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");



            return View();

        }




        protected IPagedList<ProductSet> GetPagedProcess(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id);
            IPagedList<ProductSet> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ProductSet> GetStuffFromDatabase(string id)
        {
            return _repository.FindUsePage(id, nowLang);
        }



    }
}