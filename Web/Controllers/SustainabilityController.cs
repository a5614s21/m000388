﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class SustainabilityController : BaseController
    {
        ArticlePageRepository _repository = new ArticlePageRepository();
        List<ArticlePage> CS = new List<ArticlePage>();

        public SustainabilityController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
            ViewBag.shoLay = "N";

        }


        [Route("Sustainability/CSRQuestionnaire")]
        [Route("{culture}/Sustainability/CSRQuestionnaire")]

        public async Task<IActionResult> CSRQuestionnaire(string id)
        {

            ArticlePage data = await _repository.FindArticlePage("CSRQuestionnaire", nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.SubAvtice = GetActive("CSRQuestionnaire");

            ViewBag.TopAvtice = GetTopActive("CSRQuestionnaire");

            ViewBag.certificationCategory = GetCertificationCategory();

            ViewBag.MenuTitle = LangData?.title;

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "QuestionnaireItem")
                .Where(l => l.language_id == nowLang).Select(m => m.QuestionnaireItemId).ToList();




            //取得問題
            ViewBag.AllQuestionnaireItem = DB.QuestionnaireItem.Where(m => m.active == true)
                   .Where(m => ids.Contains(m.id))
                .OrderBy(m => m.sort).Include(
                        x => x.LangData
                        .Where(l => l.language_id == nowLang)
                   ).ToList();


            //取得對應的語系ID
             ids = DB.LanguageResource.Where(m => m.model_type == "QuestionnaireGroup")
                .Where(l => l.language_id == nowLang).Select(m => m.QuestionnaireGroupId).ToList();



            //問卷評分表分類
            ViewBag.AllQuestionnaireGroup = DB.QuestionnaireGroup.Where(m => m.active == true)
                    .Where(m => ids.Contains(m.id))
                .OrderBy(m => m.sort).Include(
                        x => x.LangData
                        .Where(l => l.language_id == nowLang)
                   ).ToList();



            return View("CSRQuestionnaire");

        }


        [Route("CSRQuestionnaireSend")]
        [Route("{culture}/CSRQuestionnaireSend")]
        public async Task<IActionResult> CSRQuestionnaireSend()
        {

            ArticlePage data = await _repository.FindArticlePage("CSRQuestionnaire", nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");



            return View();

        }


        [Route("StatisticalResults")]
        [Route("{culture}/StatisticalResults")]
        public async Task<IActionResult> StatisticalResults()
        {

            ViewData["Title"] = _localizer.GetLocalizedHtmlString("StatisticalResults");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");



            //取得問題
            ViewBag.AllQuestionnaireItem = DB.QuestionnaireItem.Where(m => m.active == true).OrderBy(m => m.sort).Include(
                        x => x.LangData
                        .Where(l => l.language_id == nowLang)
                   ).ToList();

            //問卷評分表分類
            ViewBag.AllQuestionnaireGroup = DB.QuestionnaireGroup.Where(m => m.active == true).OrderBy(m => m.sort).Include(
                        x => x.LangData
                        .Where(l => l.language_id == nowLang)
                   ).ToList();


            //所有問卷
            ViewBag.Contact = DB.Contact.Where(m => m.uri == "Questionnaire").OrderBy(m => m.created_at).ToList();


            return View();

        }



        [Route("Sustainability/Download")]
        [Route("{culture}/Sustainability/Download")]

        public async Task<IActionResult> Download(string id)
        {
                     

            ViewData["Title"] = _localizer.GetLocalizedHtmlString("Download");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            ViewBag.SubAvtice = GetActive("Download");

            ViewBag.TopAvtice = GetTopActive("Download");

            ViewBag.certificationCategory = GetCertificationCategory();

            ViewBag.MenuTitle = _localizer.GetLocalizedHtmlString("Download");

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleDownload")
                .Where(l => l.language_id == nowLang).Select(m => m.ArticleDownloadId).ToList();


            ViewBag.data = DB.ArticleDownload.Where(m => m.uri == "sustainability").Where(m => m.active == true)
                .Where(m => ids.Contains(m.id))
             .Include(
                        x => x.LangData
                        .Where(l => l.language_id == nowLang)
                   )
            .OrderBy(m => m.sort).ToList();

            return View();

        }

        [Route("Sustainability/{id}")]
        [Route("{culture}/Sustainability/{id}")]

        public async Task<IActionResult> Sustainability(string id)
        {
            ViewBag.id = id;

            ArticlePage data = await _repository.FindArticlePage(id, nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;
            if (LangData == null)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("Sustainability", new { id = "AMessageFromTheCEO" });
            }
            //if (LangData == null)
            //{
            //    HttpContext.Session.SetString("turnlang", nowLang);

            //    return RedirectToAction("News", "News", new { id = "All", lang = nowLang });
            //}

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.SubAvtice = GetActive(routerId);

            ViewBag.TopAvtice = GetTopActive(routerId);

            ViewBag.certificationCategory = GetCertificationCategory();

            ViewBag.MenuTitle = LangData?.title;

            ViewBag.shoLay = "Y";
            ViewBag.shoLayTitle = _localizer.GetLocalizedHtmlString("CorporateSocialResponsibility");

            List<ArticlePage> tempCS = await (new ArticlePageRepository()).FindArticlePages(ViewBag.nowLang, "CS");
            List<ArticlePage> CS = new List<ArticlePage>();
            foreach (var itemcs in tempCS)
            {
                if (itemcs.LangData.Count == 0)
                {

                }
                else
                {
                    CS.Add(itemcs);
                }
            }
            ViewBag.shoLayLlink = ViewBag.culturePath + "Sustainability/" + CS[0].id;

            List<string> noLay = new List<string> { "AMessageFromTheCEO" };
            List<string> LayStakeholder = new List<string> { "StakeholderCommunication" };
        

            if (noLay.Contains(id))
            {
                ViewBag.shoLay = "N";
            }

            if (LayStakeholder.Contains(id))
            {
                ViewBag.shoLayTitle = _localizer.GetLocalizedHtmlString("StakeholderEngagement");
                ViewBag.shoLayLlink = ViewBag.culturePath + "Sustainability/StakeholderCommunication";
            }


            return View("Editor");

        }



        public List<ArticleCategory> GetCertificationCategory()
        {
            return _repository.FindArticleCategories("certification", nowLang);
        }

        public Dictionary<string, string> GetActive(string routerId)
        {
            Dictionary<string, string> active = new Dictionary<string, string>()
            {
                  {"AMessageFromTheCEO" , (routerId == "AMessageFromTheCEO") ? "active" : "" },

                  {"SustainableManagement" , (routerId == "SustainableManagement") ? "active" : "" },
                  {"InnovationsandServices" , (routerId == "InnovationsandServices") ? "active" : "" },
                  {"ResponsibleSupplyChain" , (routerId == "ResponsibleSupplyChain") ? "active" : "" },
                  {"Environmentfriendly" , (routerId == "Environmentfriendly") ? "active" : "" },
                  {"SocialProsperity" , (routerId == "SocialProsperity") ? "active" : "" },
                  {"HappyWorkplace" , (routerId == "HappyWorkplace") ? "active" : "" },

                  {"StakeholderCommunication" , (routerId == "StakeholderCommunication") ? "active" : "" },

                  {"CSRQuestionnaire" , (routerId == "CSRQuestionnaire") ? "active" : "" },
                  {"Download" , (routerId == "Download") ? "active" : "" },
            };

            return active;
        }


        public Dictionary<string, string> GetTopActive(string routerId)
        {
            var temprouteid = DB.ArticlePage
                .Where(m => m.uri == "CS")
                .Where(m => m.active == true).ToList();
            
            List<string> CorporateSustainability = new List<string>();

            foreach (var item in temprouteid)
            {
                CorporateSustainability.Add(item.id);
            }
            //List<string> CorporateSustainability = new List<string>
            //{
            //    "SustainableManagement",
            //    "InnovationsandServices",
            //    "ResponsibleSupplyChain",
            //    "Environmentfriendly",
            //    "SocialProsperity",
            //    "HappyWorkplace",
            //};

            List<string> StakeholderEngagement = new List<string>
            {
                "StakeholderCommunication",
                "CSRQuestionnaire",
            };


            Dictionary<string, string> active = new Dictionary<string, string>()
            {
                  {"CorporateSustainability" , (CorporateSustainability.Contains(routerId)) ? "active" : "" },
                  {"StakeholderEngagement" , (StakeholderEngagement.Contains(routerId)) ? "active" : "" },
            };

            return active;
        }


      


        protected IPagedList<ArticleNews> GetPagedProcess(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id);
            IPagedList<ArticleNews> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleNews> GetStuffFromDatabase(string id)
        {
            return (new Web.Repositories.Web.ArticleNewsRepository()).FindUsePage("All", nowLang, "video");
        }


        protected IPagedList<ArticleDownload> GetPagedProcessDownload(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabaseDownload(id);
            IPagedList<ArticleDownload> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleDownload> GetStuffFromDatabaseDownload(string id)
        {
            return _repository.FindUsePage(id, nowLang, "certification");
        }

    }
}
