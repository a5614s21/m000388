﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class TechnologyController : BaseController
    {
        ProductCategoryRepository _repository = new ProductCategoryRepository();

        public TechnologyController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }


        [Route("Technology")]
        [Route("{culture}/Technology")]

        public async Task<IActionResult> Technology()
        {

            ViewBag.category = _repository.FindProductCategory("Technology", nowLang);

            ViewData["Title"] = ViewBag.category.LangData[0].title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");



            return View();

        }

        [Route("TechnologyList/{id}")]
        [Route("{culture}/TechnologyList/{id}")]
        public async Task<IActionResult> TechnologyList(string id)
        {

            ViewBag.category = _repository.FindProductCategory(id, nowLang);

            if (ViewBag.category.LangData.Count == 0)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("Technology");
            }
            else
            {
            ViewData["Title"] = ViewBag.category.LangData[0].title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");
            }



            return View();

        }



        [Route("TechnologyData/{id}")]
        [Route("{culture}/TechnologyData/{id}")]

        public async Task<IActionResult> TechnologyData(string id)
        {

            var data = (from a in DB.LanguageResource
                        where a.ProductCategoryId == id
                        select a).FirstOrDefault();
            if (data != null)
            {
                var preid = (from a in DB.ProductCategory
                             where a.id == data.ProductCategoryId
                             select a).FirstOrDefault();
                if (preid.parent_id != "Technology")
                {
                    ViewBag.showType = "tproduct";
                }
                else
                {
                    ViewBag.showType = "category";
                }
            }
            else
            {
                ViewBag.showType = "category";
            }

            var categoryData = _repository.FindProductCategory(id, nowLang);

            LanguageResource LangData = categoryData.LangData.FirstOrDefault();

            if (LangData == null)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("Technology");
            }

            //ViewBag.showType = "category";


            ProductCategory category = _repository.FindProductCategory(LangData.parent_id, nowLang);
            ViewBag.categoryLangData = category.LangData.FirstOrDefault();


            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.Breadcrumbs = (new Helper()).GetBreadcrumbs(categoryData, ViewBag.TechnologyCategory);



            return View();

        }




        protected IPagedList<ProductSet> GetPagedProcess(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id);
            IPagedList<ProductSet> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ProductSet> GetStuffFromDatabase(string id)
        {
            return _repository.FindUsePage(id, nowLang);
        }



    }
}