﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;
using System.Runtime.Serialization.Formatters.Binary;

namespace WebApp.Controllers
{

    public class ReportController : BaseController
    {
        ReportRepository _repository = new ReportRepository();

        public ReportController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }

        /// <summary>
        /// 月報表
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("Finance")]
        [Route("{culture}/Finance")]

        public async Task<IActionResult> Finance(int? page = 1)
        {
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("Financials");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            //年月資料
            ViewBag.AllReport = _repository.FindAllReportByYearMonth("finance",nowLang);

            //每頁幾筆            
            const int pageSize = 10;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, "finance");

            ViewBag.page = page;


            return View(await _repository.FindList("finance", nowLang, pageSize, page));

        }

        /// <summary>
        /// 季報表
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("Quarterly")]
        [Route("{culture}/Quarterly")]
        public async Task<IActionResult> Quarterly(int? page = 1)
        {
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("QuarterlyResults");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            //年月資料
            ViewBag.AllReport = _repository.FindAllReportQuarterly("quarterly", nowLang);

            //每頁幾筆            
            const int pageSize = 10;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, "quarterly");

            ViewBag.page = page;          


            return View(await _repository.FindList("quarterly", nowLang, pageSize, page));

        }


        /// <summary>
        /// 美國證券交易委員會文件
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("Filings")]
        [Route("{culture}/Filings")]
        public async Task<IActionResult> Filings(int? page = 1)
        {
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("USSECFilings");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            //年月資料
            ViewBag.AllReport = _repository.FindListAll("filings", nowLang ,false);

            //每頁幾筆            
            const int pageSize = 10;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, "filings" , false);

            ViewBag.page = page;


            return View(await _repository.FindListFilings("filings", nowLang, pageSize, page , false));

        }



        /// <summary>
        /// 財務報表
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("FinancialReports")]
        [Route("{culture}/FinancialReports")]
        public async Task<IActionResult> FinancialReports(int? page = 1)
        {
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("FinancialReport");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            ViewBag.Consolidated = _repository.FindAllReportByYearMonth("consolidated", nowLang);
            ViewBag.AllReport = _repository.FindAllReportByYearMonth("reports", nowLang);

            ViewBag.Model1 = _repository.FindListAll("consolidated", nowLang, true);
            ViewBag.Model2 = _repository.FindListAll("reports", nowLang, true);

            return View();

        }



        /// <summary>
        /// 年度報告
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [Route("Annual")]
        [Route("{culture}/Annual")]
        public async Task<IActionResult> Annual(int? page = 1)
        {
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("AnnualReport");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            //年月資料
            ViewBag.AllReport = _repository.FindAllReportByYearMonth("annual", nowLang);

            //每頁幾筆            
            const int pageSize = 15;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, "annual", false);

            ViewBag.page = page;


            return View(await _repository.FindList("annual", nowLang, pageSize, page, false));

        }


        /// <summary>
        /// 取得季報表資訊
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Report/GetQuarterlyMedia")]
        [HttpPost]
        public async Task<dynamic> GetQuarterlyMedia(IFormCollection form)
        {


            ArticleDownload model = await _repository.FindDetail(form["id"].ToString(), form["lang"].ToString());
                
            LanguageResource LangData = model.LangData.FirstOrDefault();



            Dictionary<string, object> data = new Dictionary<string, object>();
            data.Add("title" , LangData?.title);


            List<MediaModel> media = new List<MediaModel>();

            if (LangData?.VideoData != null)
            {

                foreach (VideoModel item in LangData?.VideoData )
                {
                    if(!string.IsNullOrEmpty(item.path))
                    {

                        //string type = "youtube";
                        string type = "other";

                        if (!item.path.Contains("youtube"))
                        {
                            type = "other";
                        }


                        media.Add(new MediaModel
                        {
                            path = item.path,
                            title = item.title,
                            sub_title = item.sub_title,
                            type = type,
                        });
                    }
                

                }
            }

            data.Add("video", media);

            media = new List<MediaModel>();

            if (LangData?.FilesData != null && LangData?.FilesData.file != null)
            {
                foreach(FileDetailModel item in LangData?.FilesData.file)
                {
                    if (!string.IsNullOrEmpty(item.path))
                    {

                        media.Add(new MediaModel
                        {
                            path = item.path,
                            title = item.title,
                            sub_title = "",
                            type = "",
                        });
                    }
                }

            }

            data.Add("files", media);
            //BinaryFormatter bf = new BinaryFormatter();
            //MemoryStream ms = new MemoryStream();
            //bf.Serialize(ms, media);

            //HttpContext.Session.Set("requarterlyitem", ms.ToArray());


            return data;
        }


        protected IPagedList<dynamic> GetPagedProcess(int? page, int pageSize, string id , bool distinct = true)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id , distinct);
            IPagedList<dynamic> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<dynamic> GetStuffFromDatabase(string id , bool distinct = true)
        {
            return _repository.FindUsePage(id, nowLang , distinct);
        }

    }
}