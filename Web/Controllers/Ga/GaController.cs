﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Authorization;
using Google.Apis.AnalyticsReporting.v4.Data;
using System.Text.Json;

namespace Web.Controllers.Siteadmin
{
    [Area("Siteadmin")]
    public class GaController
    {
        DateRange dateRange = new DateRange { StartDate = DateTime.Now.ToString("yyyy") + "-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" };

        /// <summary>
        /// 年度瀏覽次數
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]        
        public  string GetPathViewCount(GatRequest request)
        {
            var response = (new GaService()).GaPageCount(dateRange);
            if(response != null) { return response.Reports[0].Data.RowCount.ToString(); } else { return "0";}
        }

        /// <summary>
        /// 社群網站來源數
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public string GetSocialCount(GatRequest request)
        {
            var response = (new GaService()).GaSocialCount(dateRange);
            if (response != null) { return response.Reports[0].Data.RowCount.ToString(); } else { return "0"; }
        }

        /// <summary>
        /// 總瀏覽數
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public string GetTotalWebViewNum(GatRequest request)
        {
            var response = (new GaService()).GaTotalWebViewNum(new DateRange { StartDate = "2019-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" });
            if (response != null) { return response.ToString(); } else { return "0"; }
        }

        /// <summary>
        /// 新/舊訪問者
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public string GetUserType(GatRequest request)
        {
            List<GaModel> gaModel = (new GaService()).GaUserType(dateRange);

            List<string> re = new List<string> { "0" , "0" };

            if(gaModel[0].count != null)
            {
                re[0] = gaModel[0].count.ToString();
            }
            if (gaModel[1].count != null)
            {
                re[1] = gaModel[1].count.ToString();
            }

            return JsonSerializer.Serialize(re);
        }

        /// <summary>
        /// 搜尋引擎來源數量
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public string GetOrganicSearches(GatRequest request)
        {
            List<GaModel> organicSearches = (new GaService()).GaOrganicSearches(dateRange);
       
            if (organicSearches.Count > 0)
            {
                return JsonSerializer.Serialize(organicSearches.OrderByDescending(m=>m.count).ToList());
            }
            else
            {
                return "null";
            }
        }

        /// <summary>
        /// 網站來源關鍵字
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [Authorize]
        [HttpPost]
        public string GaKeywords(GatRequest request)
        {
            List<GaModel> keywords = (new GaService()).GaKeywords(dateRange);

            if (keywords.Count > 0)
            {
                return JsonSerializer.Serialize(keywords.OrderByDescending(m => m.count).Skip(0).Take(5).ToList());
            }
            else
            {
                return "null";
            }
        }


        public string GaYear(GatRequest request)
        {
            List<GaMonthModel> year = (new GaService()).GaYear(new DateRange { StartDate = DateTime.Now.ToString("yyyy") + "-01-01", EndDate = DateTime.Now.ToString("yyyy") + "-12-31" });

            if (year.Count > 0)
            {
                return JsonSerializer.Serialize(year);
            }
            else
            {
                return "null";
            }
        }

    }

    public class GatRequest
    {
        public string? name { get; set; }

    }
}