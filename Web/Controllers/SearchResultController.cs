﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;
using System.Text.RegularExpressions;

namespace WebApp.Controllers
{

    public class SearchResultController : BaseController
    {


        public SearchResultController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }

        [Route("SearchResult")]
        [Route("{culture}/SearchResult")]

        public async Task<IActionResult> SearchResult(string keyword, int? page = 1)
        {

            ViewData["Title"] = _localizer.GetLocalizedHtmlString("SearchResult");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            ViewBag.keyword = keyword;

            ViewBag.pageAdd = "&keyword=" + keyword;



            List<string> needModelType = new List<string> {
                "ProductCategory",
                "ProductSet",
                "ArticleNews"
            };

            ViewBag.newsCategories = await (new ArticleNewsRepository()).FindAllCategories(nowLang);
            ViewBag.AllArticlePage = await (new ArticlePageRepository()).FindAllArticlePage(nowLang);
            ViewBag.AllAdvertising = DB.Advertising.OrderBy(x => x.sort).Include(x => x.LangData.Where(l => l.language_id == nowLang)).ToList();

            List<LanguageResource> tempSearchResult = DB.LanguageResource.Where(m => m.language_id == nowLang)
                .Where(m => m.active == true)
                //.Where(m => needModelType.Contains(m.model_type))
                .Where(
                    m => (
                         m.title.Contains(keyword) || m.details.Contains(keyword)
                    //m.title.Contains(keyword)
                    )
                ).ToList();

            List<LanguageResource> searchResult = new List<LanguageResource>();
            foreach (var item in tempSearchResult)
            {

                //if (item.model_type != "QuestionnaireItem" && item.model_type != "ArticleColumn" && item.model_type != "QuestionnaireGroup" && item.category_id != "video")
                //{
                //    searchResult.Add(item);
                //}
                if (item.model_type != "QuestionnaireItem" && item.model_type != "QuestionnaireGroup" && item.category_id != "video" && item.model_type != "SiteParameterItem" && item.model_type != "ArticleDownload")
                {
                    if (item.model_type == "Advertising" && item.title == "CSR")
                    {

                    }
                    else if (item.model_type == "ArticlePage" && item.title == "研發佈局" && item.language_id == "en")
                    {

                    }
                    else if (item.model_type == "ArticlePage" && item.title == "研發佈局" && keyword.ToLower() == "test")
                    {

                    }
                    else if (item.model_type == "ArticlePage" && item.title == "研发布局" && keyword.ToLower() == "test")
                    {

                    }
                    else if (item.id == "b45bc98e62e7594")
                    {

                    }
                    else if (item.id == "e6d0cea3dd28812b")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "We Are SPIL")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "Corporate Sustainability")
                    {

                    }
                    else if (item.model_type == "ArticleColumn")
                    {
                        var tempjudge = (from a in DB.ArticleColumn
                                         where a.id == item.ArticleColumnId
                                         select a).FirstOrDefault();
                        if (tempjudge != null && tempjudge.category_id == "history")
                        {
                            searchResult.Add(item);
                        }
                    }
                    else if (item.model_type == "Advertising" && item.title == "相關數據")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "人力資源")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "企業永續")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "全方位服務 贏得客戶信賴")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "潔淨生產 環境永續")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "聯絡我們")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "Contact Us")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "IC Packaging" && item.language_id == "en")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "Financials")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "財務資訊")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "Services")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "矽品公佈111年6月營收報告")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "Monthly Sales Report--Jun 2022 has been released...")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "Stakeholders")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "利害關係人專區")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "尖端封測技術 釋放晶片算力")
                    {

                    }
                    else if (item.model_type == "Advertising" && item.title == "矽品公佈2022年八月營收")
                    {

                    }


                    else
                    {
                        searchResult.Add(item);
                    }

                }

            }

            // List<SearchResult> searchResult = new List<SearchResult>();
            /*
            foreach (LanguageResource item in searchResultText)
            {
                searchResult.Add(new SearchResult
                {
                    id = item.id,
                    model_type = item.model_type,
                    title = item.title,
                    parent_id = item.parent_id,
                    category_id = item.category_id,
                    application_id = item.application_id,
                    details = item.details,
                    tags = item.tags,
                    series = item.series,
                    feature = item.feature,
                    product_tags = item.product_tags,
                    created_at = item.created_at,
                    ProductCategoryId = item.ProductCategoryId,
                    ProductSetId = item.ProductSetId,
                    sort = item.sort,
                    specs = item.specs,
                });

            }
                */





            ViewBag.searchCount = searchResult.Count;
            //每頁幾筆            
            const int pageSize = 10;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, searchResult);

            ViewBag.page = page;

            var model = searchResult.OrderBy(m => m.sort).Skip<LanguageResource>(pageSize * ((page ?? 1) - 1)).Take(pageSize).ToListAsync();

            return View(await model);

        }



        protected IPagedList<LanguageResource> GetPagedProcess(int? page, int pageSize, List<LanguageResource> searchResult)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = searchResult.OrderBy(m => m.sort);
            IPagedList<LanguageResource> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }



    }
}