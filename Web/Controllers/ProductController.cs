﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class ProductController : BaseController
    {
        ProductCategoryRepository _repository = new ProductCategoryRepository();

        public ProductController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }


        [Route("IC_Packaging")]
        [Route("{culture}/IC_Packaging")]

        public async Task<IActionResult> IC_Packaging()
        {

            ViewBag.category = _repository.FindProductCategory("ICPackaging", nowLang);

            ViewData["Title"] = ViewBag.category.LangData[0].title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");

            ViewBag.Product = DB.ProductSet.Where(m => m.active == true)
             .Where(m => m.uri == "ICPackaging")
             .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();


            return View();

        }

        [Route("IC_PackagingSecond/{id}")]
        [Route("{culture}/IC_PackagingSecond/{id}")]
        public async Task<IActionResult> IC_PackagingSecond(string id)
        {

            ViewBag.category = _repository.FindProductCategory(id, nowLang);
            
            if (ViewBag.category.LangData.Count == 0)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("IC_Packaging");
            }
            else
            {
                ViewData["Title"] = ViewBag.category.LangData[0].title;

                ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");


                ViewBag.Product = DB.ProductSet.Where(m => m.active == true)
             .Where(m => m.uri == "ICPackaging")
             .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();
            }



            return View();

        }

        [Route("IC_PackagingThird/{id}")]
        [Route("{culture}/IC_PackagingThird/{id}")]
        public async Task<IActionResult> IC_PackagingThird(string id)
        {

            ViewBag.category = _repository.FindProductCategory(id, nowLang);

            if (ViewBag.category.LangData.Count == 0)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("IC_Packaging");
            }
            else
            {
                ViewBag.topCategory = _repository.FindProductCategory(ViewBag.category.parent_id, nowLang);

                ViewData["Title"] = ViewBag.category.LangData[0].title;

                ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");

                ViewBag.Product = DB.ProductSet.Where(m => m.active == true)
               .Where(m => m.uri == "ICPackaging")
               .Where(m => m.category_id == id)
               .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();

            }


            return View();

        }


        [Route("IC_PackagingData/{id}")]
        [Route("{culture}/IC_PackagingData/{id}")]

        public async Task<IActionResult> IC_PackagingData(string id)
        {


            ViewBag.showType = "product";

            var data = DB.ProductSet.Where(m => m.id == id)
              .Where(m => m.uri == "ICPackaging")
              .Include(x => x.LangData.Where(l => l.language_id == nowLang)).FirstOrDefault();

            LanguageResource LangData = new LanguageResource();


            if (data == null)
            {
                var categoryData = _repository.FindProductCategory(id, nowLang);

                LangData = categoryData.LangData.FirstOrDefault();

                ViewBag.showType = "category";

                if (LangData == null)
                {
                    HttpContext.Session.SetString("turnlang", nowLang);
                    return RedirectToAction("IC_Packaging");
                }
                ProductCategory category = _repository.FindProductCategory(LangData.parent_id, nowLang);
                ViewBag.categoryLangData = category.LangData.FirstOrDefault();


            }
            else
            {
                LangData = data.LangData.FirstOrDefault();

                if (LangData == null)
                {
                    return RedirectToAction("IC_Packaging");
                }

                ProductCategory category = _repository.FindProductCategory(data.category_id, nowLang);
                ViewBag.categoryLangData = category.LangData.FirstOrDefault();


                ProductCategory topCategory = _repository.FindProductCategory(category.parent_id, nowLang);
                ViewBag.topCategoryLangData = topCategory.LangData.FirstOrDefault();

            }



            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");



            return View();

        }


        [Route("IC_PackagingSearch")]
        [Route("{culture}/IC_PackagingSearch")]

        public async Task<IActionResult> IC_PackagingSearch(string keywords, int? page = 1)
        {

            ViewBag.category = _repository.FindProductCategory("ICPackaging", nowLang);

            ViewData["Title"] = ViewBag.category.LangData[0].title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, ViewBag.category.LangData[0].seo, "");


            //每頁幾筆            
            const int pageSize = 9;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, "ICPackaging", keywords);

            ViewBag.Count = _repository.FindUsePageSearchCount("ICPackaging", nowLang, keywords);

            ViewBag.page = page;

            ViewBag.Keywords = keywords;

            return View(await _repository.FindProductSetSearchList("ICPackaging", nowLang, pageSize, keywords, page ));

        }




        protected IPagedList<ProductSet> GetPagedProcess(int? page, int pageSize, string id, string keywords)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id, keywords);
            IPagedList<ProductSet> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ProductSet> GetStuffFromDatabase(string id, string keywords)
        {
            return _repository.FindUsePageSearch(id, nowLang, keywords);
        }



    }
}