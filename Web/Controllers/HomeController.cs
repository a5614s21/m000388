﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using Microsoft.Extensions.Localization;

using Web.Services;
using Microsoft.AspNetCore.Http.Features;

namespace WebApp.Controllers
{

    public class HomeController : BaseController
    {

        public HomeController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }


        [Route("")]
        [Route("Index")]
        [Route("{culture}")]
        [Route("{culture}/Index")]

        public IActionResult Index()
        {


            if (nowLang.ToLower() == "siteadmin")
            {
                return Redirect("~/Siteadmin/Home/Index"); ;
            }


            var _repository = new AdvertisingCategoryRepository();

            //輪播Banner
            ViewBag.banner = _repository.FindAdvertising("banner", nowLang);

            //訊息輪播
            ViewBag.FinancialReport = _repository.FindCategoryAdvertising("report", nowLang);


            //服務區塊
            ViewBag.ServiceAd = _repository.FindAdvertising("ServiceAd", nowLang);


            //中間編輯器
            ViewBag.NumEditor = _repository.FindAdvertising("NumEditor", nowLang);

            //中間編輯器
            ViewBag.MainEditor = _repository.FindAdvertising("MainEditor", nowLang);

            //中間區塊
            ViewBag.MainAd = _repository.FindAdvertising("MainAd", nowLang);


            //下方區塊
            ViewBag.DownAd = _repository.FindAdvertising("DownAd", nowLang);


            //最新消息
            ViewBag.news = (new ArticleNewsRepository()).FindTop("All", nowLang);




            return View();
        }


        [Route("SendToSubscribe")]
        [HttpPost]
        public async Task SendToSubscribe(IFormCollection form)
        {

            CipherService cipherService = new CipherService();

            string email = cipherService.Encrypt(form["email"].ToString());

            //判斷是否已經訂閱
            int isSubscribe = DB.Contact.Where(m => m.email == email).Count();

            if (isSubscribe == 0)
            {
                Contact entity = new Contact()
                {
                    id = Guid.NewGuid().ToString(),
                    uri = "Subscribe",
                    lang = form["lang"].ToString(),
                    name = email,
                    contact = null,
                    email = email,
                    options = null,
                    active = true,
                    send = false,
                    updated_at = DateTime.Now,
                    created_at = DateTime.Now,
                };
                DB.Contact.Add(entity);
                DB.SaveChanges();
            }

        }





        [Route("Sitmap")]
        [Route("{culture}/Sitmap")]
        public IActionResult Sitmap()
        {
            ViewData["Title"] = _localizer.GetLocalizedHtmlString("Sitmap");

            //檔案下載用
            ViewBag.filesType = DB.ProductSpec.Where(x => x.active == true)
                       .Where(x => x.uri == "files")
                       .Include(x => x.LangData.Where(l => l.language_id == nowLang))
                       .OrderBy(m => m.sort)
                       .ToList();

            return View();
        }

       
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            ViewData["Title"] = "404";

            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }



        [Route("OkCookie")]
        public bool OkCookie()
        {
            var consentFeature = HttpContext.Features.Get<ITrackingConsentFeature>();
            consentFeature.GrantConsent();

            var cookieOptions = new CookieOptions
            {
                // Set the secure flag, which Chrome's changes will require for SameSite none.
                // Note this will also require you to be running on HTTPS.
                Secure = true,

                // Set the cookie to HTTP only which is good practice unless you really do need
                // to access it client side in scripts.
                HttpOnly = true,

                // Add the SameSite attribute, this will emit the attribute with a value of none.
                SameSite = SameSiteMode.None

                // The client should follow its default cookie policy.
                // SameSite = SameSiteMode.Unspecified
            };

            Response.Cookies.Append("GDPR", "yes", cookieOptions);
           


            return true;
        }


    }
}