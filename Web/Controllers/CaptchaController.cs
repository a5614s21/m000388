﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

[Route("api/[controller]")]
[ApiController]
public class CaptchaController : ControllerBase
{
    [HttpGet]
    public async Task<FileContentResult> CaptchaAsync([FromServices] ICaptcha _captcha)
    {
        var code = await _captcha.GenerateRandomCaptchaAsync();

        HttpContext.Session.Remove("verificationCode");

        HttpContext.Session.SetString("verificationCode", code);

        var result = await _captcha.GenerateCaptchaImageAsync(code);

        return File(result.CaptchaMemoryStream.ToArray(), "image/png");
    }
}