﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class CareersController : BaseController
    {
        ArticlePageRepository _repository = new ArticlePageRepository();


        public CareersController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);


        }

        [Route("Careers/Awards")]
        [Route("{culture}/Careers/Awards")]

        public async Task<IActionResult> Awards(string id)
        {
            ArticlePage data = await _repository.FindArticlePage("Awards", nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

          

             ViewBag.SubAvtice = GetActive("Awards");
             ViewBag.TopAvtice = GetTopActive("Awards");

            //取得對應的語系ID
            List<string> ids = DB.LanguageResource.Where(m => m.model_type == "ArticleColumn")
                .Where(l => l.language_id == nowLang).Select(m => m.ArticleColumnId).ToList();


            ViewBag.data = DB.ArticleColumn.Where(m=>m.category_id == "awards").Where(m=>m.active == true)
                   .Where(m => ids.Contains(m.id))
                 .Include(
                            x => x.LangData
                            .Where(l => l.language_id == nowLang)
                       )
                .OrderBy(m=>m.sort).ToList();

            ViewBag.MenuTitle = _localizer.GetLocalizedHtmlString("Awards");

            return View();

        }


        [Route("Careers/Experience")]
        [Route("{culture}/Careers/Experience")]

        public async Task<IActionResult> Experience(string id)
        {

            
            ViewData["Title"] = "訓儲人員心得";

            if (nowLang != "zh-TW")
            {
                HttpContext.Session.SetString("turnlang", nowLang);

                return RedirectToAction("Awards", "Careers");
            }

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

             ViewBag.SubAvtice = GetActive("Experience");
             ViewBag.TopAvtice = GetTopActive("Experience");


            ViewBag.data = DB.ArticleColumn.Where(m => m.category_id == "experience").Where(m => m.active == true)
                 .Include(
                            x => x.LangData
                            .Where(l => l.language_id == nowLang)
                       )
                .OrderBy(m => m.sort).ToList();

            List<ArticleColumn> resultData = new List<ArticleColumn>();
            int isdatacount = 0;
            foreach (var item in ViewBag.data)
            {
                string tempid = item.id;
                var datalistlang = DB.LanguageResource.Where(m => m.ArticleColumnId == tempid).Select(m=>m.language_id).ToList();
                if (datalistlang.Count() != 0)
                {
                    if (datalistlang.Count() > 1)
                    {
                        foreach (var subitem in datalistlang)
                        {
                            if (subitem == nowLang)
                            {
                                isdatacount = 1;
                            }
                        }
                    }
                    else
                    {
                        foreach (var subitem in datalistlang)
                        {
                            if (subitem == nowLang)
                            {
                                isdatacount = 1;
                            }
                        }
                    }
                }

                if (isdatacount == 1)
                {
                    resultData.Add(item);
                }
                isdatacount = 0;
            }

            ViewBag.data = resultData;
            ViewBag.MenuTitle = "訓儲人員心得";

            return View();

        }


        [Route("Careers/{id}")]
        [Route("{culture}/Careers/{id}")]

        public async Task<IActionResult> Careers(string id)
        {

            ArticlePage data = await _repository.FindArticlePage(id, nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            if (id == "Careers1" || id == "Careers2" || id == "Careers3")
            {
                if (nowLang != "zh-TW")
                {
                    HttpContext.Session.SetString("turnlang", nowLang);

                    return RedirectToAction("Awards", "Careers");
                }
            }

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.SubAvtice = GetActive(routerId);

            ViewBag.TopAvtice = GetTopActive(routerId);

            ViewBag.certificationCategory = GetCertificationCategory();

            ViewBag.MenuTitle = LangData?.title;

            List<string> jobId = new List<string> {
                "Recruitment" , "JobApply",
            };

            List<string> exId = new List<string> {
                "Careers1" , "Careers2", "Careers3",
            };

            ViewBag.addLinkTitle = "";
            ViewBag.addLink = "";

            if(jobId.Contains(id))
            {
                ViewBag.addLinkTitle = _localizer.GetLocalizedHtmlString("Recruitment");
                ViewBag.addLink = ViewBag.culturePath + "Careers/Recruitment";
            }
            if (exId.Contains(id))
            {
                ViewBag.addLinkTitle = "研發替代役專區";
                ViewBag.addLink = ViewBag.culturePath + "Careers/Careers1";
            }



            return View("Editor");

        }



        public List<ArticleCategory> GetCertificationCategory()
        {
            return _repository.FindArticleCategories("certification", nowLang);
        }

        public Dictionary<string, string> GetActive(string routerId)
        {
            Dictionary<string, string> active = new Dictionary<string, string>()
            {
                  {"Awards" , (routerId == "Awards") ? "active" : "" },
                  {"Recruitment" , (routerId == "Recruitment") ? "active" : "" },
                   {"JobApply" , (routerId == "JobApply") ? "active" : "" },
                  {"TrainingDevelopment" , (routerId == "TrainingDevelopment") ? "active" : "" },
                  {"IncentivesBenefits" , (routerId == "IncentivesBenefits") ? "active" : "" },
                  {"HRSystem" , (routerId == "HRSystem") ? "active" : "" },
                  {"HealthManagement" , (routerId == "HealthManagement") ? "active" : "" },
                  {"Experience" , (routerId == "Experience") ? "active" : "" },
                  {"Careers1" , (routerId == "Careers1") ? "active" : "" },
                  {"Careers2" , (routerId == "Careers2") ? "active" : "" },
                  {"Careers3" , (routerId == "Careers3") ? "active" : "" },
            };

            return active;
        }


        public Dictionary<string, string> GetTopActive(string routerId)
        {

            List<string> CompanyProfile = new List<string>
            {
                "Recruitment",
                "JobApply",
            };

            List<string> Careers = new List<string>
            {
                "Careers1",
                "Careers2",
                "Careers3",
                "Experience",
            };


            Dictionary<string, string> active = new Dictionary<string, string>()
            {
                  {"Recruitment" , (CompanyProfile.Contains(routerId)) ? "active" : "" },
                   {"Careers" , (Careers.Contains(routerId)) ? "active" : "" },
            };

            return active;
        }


        protected IPagedList<ArticleNews> GetPagedProcess(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id);
            IPagedList<ArticleNews> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleNews> GetStuffFromDatabase(string id)
        {
            return (new Web.Repositories.Web.ArticleNewsRepository()).FindUsePage("All", nowLang, "video");
        }


        protected IPagedList<ArticleDownload> GetPagedProcessDownload(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabaseDownload(id);
            IPagedList<ArticleDownload> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleDownload> GetStuffFromDatabaseDownload(string id)
        {
            return _repository.FindUsePage(id, nowLang , "certification");
        }

    }
}
