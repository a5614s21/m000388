﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class PageController : BaseController
    {
        ArticlePageRepository _repository = new ArticlePageRepository();


        public PageController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);


        }

        [Route("About/Milestones")]
        [Route("{culture}/About/Milestones")]

        public async Task<IActionResult> Milestones(string id)
        {


            ViewData["Title"] = _localizer.GetLocalizedHtmlString("Milestones");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            ViewBag.SubAvtice = GetActive("Milestones");
            ViewBag.TopAvtice = GetTopActive("Milestones");


            ViewBag.data = await _repository.FindArticleColumnMilestones("history", nowLang);


            //ViewBag.certificationCategory = GetCertificationCategory();

            return View();

        }


        [Route("About/Locations")]
        [Route("{culture}/About/Locations")]

        public async Task<IActionResult> Locations(string id)
        {
            ArticleCategory category = await _repository.FindArticleCategory("location", nowLang);

            ViewBag.category = category.LangData.FirstOrDefault();

            ViewBag.allLocation = await _repository.FindArticleLocations(nowLang);


            ViewBag.subCategory = _repository.FindArticleCategories("location" , nowLang);


            ViewData["Title"] = _localizer.GetLocalizedHtmlString("LocationsandBusinessContac");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, category.LangData.FirstOrDefault()?.seo, "");

            ViewBag.SubAvtice = GetActive("Locations");
            ViewBag.TopAvtice = GetTopActive("Locations");

            //ViewBag.certificationCategory = GetCertificationCategory();

            ViewBag.Location = DB.ArticleLocation.Where(m => m.active == true).Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();


            return View();

        }

        [Route("About/Video")]
        [Route("{culture}/About/Video")]

        public async Task<IActionResult> Video(string id, int? page = 1)
        {

            ViewData["Title"] = _localizer.GetLocalizedHtmlString("Video");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            ViewBag.SubAvtice = GetActive("Video");
            ViewBag.TopAvtice = GetTopActive("Video");

            //ViewBag.certificationCategory = GetCertificationCategory();

            //每頁幾筆            
            const int pageSize = 9;

            ViewBag.pagedList = GetPagedProcess(page, pageSize, id);

            ViewBag.page = page;




            return View((new Web.Repositories.Web.ArticleNewsRepository()).FindList("All", nowLang, pageSize, "video", page));

        }


        [Route("About/{id}")]
        [Route("{culture}/About/{id}")]

        public async Task<IActionResult> About(string id)
        {

            ArticlePage data = await _repository.FindArticlePage(id, nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.SubAvtice = GetActive(routerId);

            ViewBag.TopAvtice = GetTopActive(routerId);

            //ViewBag.certificationCategory = GetCertificationCategory();


            return View("Editor");

        }



        [Route("About/Certification/{id}")]
        [Route("{culture}/About/Certification/{id}")]

        public async Task<IActionResult> Certification(string id, int? page = 1)
        {
            ViewBag.CategoryId = id;

            ArticleCategory category = await _repository.FindArticleCategory(id, nowLang);

            LanguageResource LangData = category.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            if (LangData == null)
            {
                HttpContext.Session.SetString("turnlang", nowLang);
                return RedirectToAction("About",new { id= "CertificationOverview" });
            }
            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.SubAvtice = GetActive("Certification");
            ViewBag.TopAvtice = GetTopActive("Certification");

            //ViewBag.certificationCategory = GetCertificationCategory();

            //每頁幾筆            
            const int pageSize = 5;

            ViewBag.pagedList = GetPagedProcessDownload(page, pageSize, id);

            ViewBag.page = page;


            return View(await _repository.FindList(id, nowLang, pageSize , "certification", page));

        }



        [Route("Terms/{id}")]
        [Route("{culture}/Terms/{id}")]

        public async Task<IActionResult> Terms(string id)
        {

            ArticlePage data = await _repository.FindArticlePage(id, nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            //ViewBag.SubAvtice = GetActive(routerId);

            //ViewBag.TopAvtice = GetTopActive(routerId);

          // //ViewBag.certificationCategory = GetCertificationCategory();


            return View("Terms");

        }

        [Route("Privacy")]
        [Route("{culture}/Privacy")]
        public async Task<IActionResult> Privacy()
        {

            ArticlePage data = await _repository.FindArticlePage("Privacy", nowLang);

            LanguageResource LangData = data.LangData?.FirstOrDefault();

            ViewBag.LangData = LangData;

            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            //ViewBag.SubAvtice = GetActive(routerId);

            //ViewBag.TopAvtice = GetTopActive(routerId);

            // //ViewBag.certificationCategory = GetCertificationCategory();


            return View("Privacy");

        }

        [Route("Sitemap")]
        [Route("{culture}/Sitemap")]
        public async Task<IActionResult> Sitemap()
        {

            ViewData["Title"] = _localizer.GetLocalizedHtmlString("SITEMAP");

            ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");

            ViewBag.newsCategories = await (new ArticleNewsRepository()).FindCategories(nowLang);


            ViewBag.Product = DB.ProductSet.Where(m => m.active == true)
             .Where(m => m.uri == "ICPackaging")
             .Include(x => x.LangData.Where(l => l.language_id == nowLang)).OrderBy(m => m.sort).ToList();


            return View("Sitemap");

        }


        /*  public List<ArticleCategory> GetCertificationCategory()
          {
              return _repository.FindArticleCategories("certification", nowLang);
          }
        */
        public Dictionary<string, string> GetActive(string routerId)
        {
            Dictionary<string, string> active = new Dictionary<string, string>()
            {
                  {"Overview" , (routerId == "Overview") ? "active" : "" },
                  {"Milestones" , (routerId == "Milestones") ? "active" : "" },
                  {"Organization" , (routerId == "Organization") ? "active" : "" },
                  {"Vision" , (routerId == "Vision") ? "active" : "" },
                  {"Stakeholder" , (routerId == "Stakeholder") ? "active" : "" },
                  {"Security" , (routerId == "Security") ? "active" : "" },
                  {"Locations" , (routerId == "Locations") ? "active" : "" },
                  {"Video" , (routerId == "Video") ? "active" : "" },
                  {"Certification" , (routerId == "Certification") ? "active" : "" },
                  {"CertificationOverview" , (routerId == "CertificationOverview") ? "active" : "" },
            };

            return active;
        }


        public Dictionary<string, string> GetTopActive(string routerId)
        {

            List<string> CompanyProfile = new List<string>
            {
                "Overview",
                "Milestones",
                "Organization"
            };


            Dictionary<string, string> active = new Dictionary<string, string>()
            {
                  {"CompanyProfile" , (CompanyProfile.Contains(routerId)) ? "active" : "" },
                  {"Certification" , (routerId == "Certification" || routerId == "CertificationOverview") ? "active" : "" },
            };

            return active;
        }


        protected IPagedList<ArticleNews> GetPagedProcess(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id);
            IPagedList<ArticleNews> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleNews> GetStuffFromDatabase(string id)
        {
            return (new Web.Repositories.Web.ArticleNewsRepository()).FindUsePage("All", nowLang, "video");
        }


        protected IPagedList<ArticleDownload> GetPagedProcessDownload(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabaseDownload(id);
            IPagedList<ArticleDownload> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleDownload> GetStuffFromDatabaseDownload(string id)
        {
            return _repository.FindUsePage(id, nowLang , "certification");
        }

    }
}
