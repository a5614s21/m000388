﻿using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Data;
using WebApp.Models;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using WebApp.Services;
using System.Text;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Localization;
using System.Globalization;
using Web.Repositories.Web;
using Newtonsoft.Json;
using X.PagedList;
using Microsoft.Extensions.Localization;

namespace WebApp.Controllers
{

    public class NewsController : BaseController
    {
        ArticleNewsRepository _repository = new ArticleNewsRepository();

        public NewsController(IStringLocalizerFactory factory)
        {
            _localizer = new LocalService(factory);
        }

        [Route("News/{id}")]
        [Route("{culture}/News/{id}")]

        public async Task<IActionResult> News(string id, int? page = 1,string? lang="")
        {
            if (lang != "" && lang != null)
            {
                List<ArticleCategory> categories = await _repository.FindCategories(lang);
                ViewBag.categories = categories;
                ViewBag.category = null;
                ViewBag.nowLang = lang;
                nowLang = lang;

                if (id != "All")
                {
                    ArticleCategory data = categories.Where(m => m.id == id).FirstOrDefault();

                    ViewBag.category = data;

                    LanguageResource LangData = data.LangData?.FirstOrDefault();

                    ViewBag.LangData = LangData;

                    ViewData["Title"] = LangData?.title;

                    ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");
                }
                else
                {
                    ViewData["Title"] = _localizer.GetLocalizedHtmlString("News");
                    ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");
                }



                //每頁幾筆            
                const int pageSize = 8;

                ViewBag.pagedList = GetPagedProcess(page, pageSize, id);

                ViewBag.page = page;


                return View(_repository.FindList(id, lang, pageSize, "news", page));
            }
            else
            {
                List<ArticleCategory> categories = await _repository.FindCategories(nowLang);
                ViewBag.categories = categories;
                ViewBag.category = null;

                if (id != "All")
                {
                    ArticleCategory data = categories.Where(m => m.id == id).FirstOrDefault();

                    ViewBag.category = data;

                    LanguageResource LangData = data.LangData?.FirstOrDefault();

                    ViewBag.LangData = LangData;
                    if (LangData == null)
                    {
                        HttpContext.Session.SetString("turnlang", nowLang);

                        return RedirectToAction("News", "News", new { id = "All", lang = nowLang });
                    }


                    ViewData["Title"] = LangData?.title;

                    ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");
                }
                else
                {
                    ViewData["Title"] = _localizer.GetLocalizedHtmlString("News");
                    ViewBag.Seo = helper.Seo(ViewBag.WebData, "", "");
                }



                //每頁幾筆            
                const int pageSize = 8;

                ViewBag.pagedList = GetPagedProcess(page, pageSize, id);

                ViewBag.page = page;

                var allnews = _repository.FindList(id, nowLang, pageSize, "news", page);

                return View(_repository.FindList(id, nowLang, pageSize, "news", page));
            }

        }

        [Route("NewsDetail/{id}")]
        [Route("{culture}/NewsDetail/{id}")]
        public async Task<IActionResult> Detail(string id)
        {

            ArticleNews data = await _repository.FindDetail(id , nowLang);

            LanguageResource LangData = data.LangData.FirstOrDefault();

            ViewBag.data = LangData;
            if (LangData == null)
            {
                HttpContext.Session.SetString("turnlang",nowLang);

                return RedirectToAction("News", "News", new { id = "All" ,lang=nowLang});
            }
            ViewData["Title"] = LangData?.title;

            ViewBag.Seo = helper.Seo(ViewBag.WebData, LangData?.seo, "");

            ViewBag.MonthDay = "";
            ViewBag.Year = "";

            if (!string.IsNullOrEmpty(data?.start_at.ToString()))
            {
                if (nowLang == "zh-TW")
                {
                    ViewBag.MonthDay = DateTime.Parse(data?.start_at.ToString()).ToString("MMM dd日");
                    ViewBag.Year = DateTime.Parse(data?.start_at.ToString()).ToString("yyyy");
                }
                else if (nowLang == "zh-CN")
                {
                    ViewBag.MonthDay = DateTime.Parse(data?.start_at.ToString()).ToString("MMM dd日");
                    ViewBag.Year = DateTime.Parse(data?.start_at.ToString()).ToString("yyyy");
                }
                else
                {
                    ViewBag.MonthDay = DateTime.Parse(data?.start_at.ToString()).ToString("MMM dd");
                    ViewBag.Year = DateTime.Parse(data?.start_at.ToString()).ToString("yyyy");
                }
            }
            else
            {
                if(LangData?.created_at.ToString() == null)
                {
                    return RedirectToAction("News", "News", new { id = "All" });
                }
                else
                {
                    if (nowLang == "zh-TW")
                    {
                        ViewBag.MonthDay = DateTime.Parse(LangData?.start_at.ToString()).ToString("MMM dd日");
                        ViewBag.Year = DateTime.Parse(LangData?.start_at.ToString()).ToString("yyyy");
                    }
                    else if (nowLang == "zh-CN")
                    {
                        ViewBag.MonthDay = DateTime.Parse(LangData?.start_at.ToString()).ToString("MMM dd日");
                        ViewBag.Year = DateTime.Parse(LangData?.start_at.ToString()).ToString("yyyy");
                    }
                    else
                    {
                        ViewBag.MonthDay = DateTime.Parse(LangData?.start_at.ToString()).ToString("MMM dd");
                        ViewBag.Year = DateTime.Parse(LangData?.start_at.ToString()).ToString("yyyy");
                    }
                }
            }

            ArticleCategory category = await _repository.FindCategory(data.category_id , nowLang);


            ViewBag.category = category.LangData.FirstOrDefault();

            //上下筆
            List<string> newsList = _repository.FindDetailOnlyId(data.category_id , nowLang , "news");

            List<string> ids = new List<string>();


            foreach (string val in newsList)
            {
                ids.Add(val);
            }

            int arrayIndex = ids.IndexOf(id);

            ArticleNews prevData = null;

            try
            {
                if (ids[arrayIndex - 1] != null)
                {
                    string prevId = ids[arrayIndex - 1].ToString();
                    prevData = DB.ArticleNews.Where(m => m.id == prevId).FirstOrDefault();
                }
            }
            catch { }

            ViewBag.prevData = prevData;

            ArticleNews nextData = null;
            try
            {
                if (ids[arrayIndex + 1] != null)
                {
                    string nextId = ids[arrayIndex + 1].ToString();
                    nextData = DB.ArticleNews.Where(m => m.id == nextId).FirstOrDefault();
                }
            }
            catch { }


            ViewBag.nextData = nextData;


            return View();

        }

        protected IPagedList<ArticleNews> GetPagedProcess(int? page, int pageSize, string id)
        {
            // 過濾從client傳送過來有問題頁數
            if (page.HasValue && page < 1)
                return null;
            // 從資料庫取得資料
            var listUnpaged = GetStuffFromDatabase(id);
            IPagedList<ArticleNews> pagelist = listUnpaged.ToPagedList(page ?? 1, pageSize);
            // 過濾從client傳送過來有問題頁數，包含判斷有問題的頁數邏輯
            if (pagelist.PageNumber != 1 && page.HasValue && page > pagelist.PageCount)
                return null;
            return pagelist;
        }
        protected IQueryable<ArticleNews> GetStuffFromDatabase(string id)
        {
            return _repository.FindUsePage(id, nowLang , "news");
        }

    }
}