﻿using Microsoft.EntityFrameworkCore.Migrations;
using WebApp.Models;
using WebApp.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;

namespace WebApp.Data.Migrations
{
    public class TablesColumnDescription
    {
        public void run(MigrationBuilder migrationBuilder)
        {

            //WebData
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'網站名稱' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'網址' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'url'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'Email' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'email'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'聯絡資訊' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'contact'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'社群' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'social'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'LOGO' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'logo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'GA' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'ga'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'WebData', 'COLUMN', N'updated_at'");


            //SystemMenu
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'來源鍵值' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'相關連結' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'link'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'上層ID' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'parent_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資料表名稱' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'table_name'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'模組' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'model'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'圖示' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'icon'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'SystemMenu', 'COLUMN', N'updated_at'");


            //ArticleCategory
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'相關連結' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'link'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'上層ID' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'parent_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'updated_at'");

            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'Icon' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'icon'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'Banner' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'列表圖' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'list_pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'首頁列表圖' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'index_pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ArticleCategory', 'COLUMN', N'path'");

            //ArticleNews
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'分類ID' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'category_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'開始日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'start_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'結束日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'end_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ArticleNews', 'COLUMN', N'path'");

            //ArticlePage
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'分類ID' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'category_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ArticlePage', 'COLUMN', N'path'");

            //Columns
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'欄位名稱' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'分鍵值' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'模組' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'model'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'主欄位名稱' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'column_name'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'副欄位名稱' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'sub_column_name'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'欄位設定' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'顯示區域' ,'SCHEMA', N'dbo','TABLE', N'Columns', 'COLUMN', N'layout'");


            //ProductCategory
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'相關連結' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'link'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'上層ID' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'parent_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ProductCategory', 'COLUMN', N'path'");

            //ArticleColumn
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'分類ID' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'category_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'開始日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'start_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'結束日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'end_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ArticleColumn', 'COLUMN', N'path'");

            //ProductSet
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'分類ID' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'category_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'開始日期' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'start_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'結束日期' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'end_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ProductSet', 'COLUMN', N'path'");


            //ArticleDownload
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'lang'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'title'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'URI' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'uri'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'分類ID' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'category_id'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'資訊內容' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'details'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'啟用停用' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'active'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'排序' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'sort'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'SEO' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'seo'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'建立日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'created_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'updated_at'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'其他設定' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'options'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'圖片' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'pic'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'自訂連結' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'path'");
            migrationBuilder.Sql("execute sp_addextendedproperty 'MS_Description', N'檔案' ,'SCHEMA', N'dbo','TABLE', N'ArticleDownload', 'COLUMN', N'files'");


        }

    }
}
